$( document ).ready(function() {
	// Timeout needed for the div elements to load. Otherwise JS script is executed before div has been added to the page.
	$(document).on("click",".printbutton",function(){
		window.print();
	})
	// functions for jumping to specific page 
	$(document).on("click",".bscroll1",function(){
		location.hash = "#overview_data"
	})
	$(document).on("click",".bscroll2",function(){
		location.hash = "#overview_pipeline"
	})
	$(document).on("click",".bscroll3",function(){
		location.hash = "#frequency_plot"
	})
	$(document).on("click",".bscroll4",function(){
		location.hash = "#mutation_plot"
	})
	$(document).on("click",".bscroll5",function(){
		location.hash = "#consensus_chart"
	})
	$(document).on("click",".bscrollG1",function(){
		location.hash = "#lab_map"
	})
	$(document).on("click",".bscrollG2",function(){
		location.hash = "#sending_lab_map"
	})

	// add your new page here
});

browserLanguage = navigator.language; //de = deutsch
IEbrowserLanguage = navigator.browserLanguage;
function getBrowserLanguage(){
	if((browserLanguage === "de") || (IEbrowserLanguage === "de-DE")){
		const lang = "german"
	}
	else{
		const lang = "english"
	}
	return lang
}

