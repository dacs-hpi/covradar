We are grateful to the data contributors who shared the data used in this Web Application via the GISAID Initiative*: the Authors, the Originating Laboratories responsible for obtaining the specimens, and the Submitting Laboratories that generated the genetic sequences and metadata.

*
Elbe, S., and Buckland-Merrett, G. (2017) Data, disease and diplomacy: GISAID’s innovative contribution to global health. Global Challenges, 1:33-46. DOI: [10.1002/gch2.1018](http://dx.doi.org/10.1002/gch2.1018) PMCID: [31565258](http://dx.doi.org/10.1002/gch2.1018);

Shu, Y., McCauley, J. (2017) GISAID: Global initiative on sharing all influenza data – from vision to reality. EuroSurveillance, 22(13) DOI: [10.2807/1560-7917.ES.2017.22.13.30494](http://dx.doi.org/10.2807/1560-7917.ES.2017.22.13.30494) PMCID: [PMC5388101](http://dx.doi.org/10.2807/1560-7917.ES.2017.22.13.30494)