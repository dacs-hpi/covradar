import yaml
import locale

def get_language_dict():
    with open("frontend/app/shared/assets/text_english_german.yaml", 'r') as stream:
        dict_lang=(yaml.safe_load(stream))
    #return dict_lang
    if locale.getlocale()[0][0:3] == "de_":
        # if True:
        dict_lang_de={}
        config_plots = dict(locale='de')
        for subpage in dict_lang.keys():
            dict_lang_de[subpage]={k:v['german'] for (k, v) in dict_lang[subpage].items()}
        return dict_lang_de
    else:
        config_plots = dict(locale='en')
        dict_lang_en={}
        for subpage in dict_lang.keys():
            dict_lang_en[subpage]={k:v['english'] for (k, v) in dict_lang[subpage].items()}
        return dict_lang_en


