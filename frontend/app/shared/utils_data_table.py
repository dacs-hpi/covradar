import datetime
import plotly.express as px
import numpy as np
import pandas as pd


class CountryTimePlot:
    def __init__(self, consensus):
        self.consensus_df = consensus[["region", "date", 'number_of_sequences']]
        self.country_numbers = 9
        self.df_weekly = self.create_final_df()

    def get_top_countries(self, df):
        df_top_countries = df.groupby('region').sum()
        df_top_countries = df_top_countries.sort_values('number_of_sequences', ascending=False)
        top_countries_index = df_top_countries.index[:self.country_numbers]
        top_countries = top_countries_index.tolist()
        top_countries.append('other')
        return top_countries_index, top_countries

    def create_df_weekly(self):
        df = self.consensus_df
        df = df.drop(df[df.region == 'Wuhan'].index)  # drop Wuhan first case
        df = df.drop(df[df.region == 'Global'].index)  # drop Global.
        # Note:  index still goes up to 415 even though only have 372 rows after dropping
        df.reset_index(drop=True, inplace=True)

        # There is one case in 2013, cause plot shifting.  delete it for now
        df = df.drop(df[df.date == '2013W30'].index)
        df['date_from_cw'] = [datetime.datetime.strptime(i + '-1', "%YW%W-%w") for i in df['date']]
        # df_weekly = df_weekly.drop('date_from_cw',1)
        df_weekly = df.drop('date', 1)
        # if country not in top 10, replace with other
        top_countries_index, top_countries = self.get_top_countries(df)
        df_weekly['region'] = df_weekly['region'].apply(
            lambda x: 'other' if x not in top_countries_index else x)
        return df_weekly, top_countries

    def create_df_to_max_date(self, df_weekly, top_countries):
        max_date = max(df_weekly['date_from_cw'])
        to_max_date = []  # more efficient to append a list than dataframe. List of countries that needs to fill to max date
        for t in top_countries:
            if len(df_weekly.loc[df_weekly['region'] == t, 'date_from_cw']) != 0:
                country_max = max(df_weekly.loc[df_weekly['region'] == t, 'date_from_cw'])
                if country_max < max_date:
                    to_max_date.append([t, 0, max_date])
        df_to_max_date = pd.DataFrame(to_max_date,
                                      columns=['region', 'number_of_sequences', 'date_from_cw'])
        return df_to_max_date

    def add_accumulative_seq(self, df_weekly):
        # generate a column with accumulative count
        df_weekly['accumulative sequences'] = 0
        df_weekly.loc[0,'accumulative sequences'] = df_weekly['number_of_sequences'][0]
        for i in range(len(df_weekly) - 1):
            i = i + 1
            if df_weekly['country'][i] == df_weekly['country'][i - 1]:

                df_weekly.loc[i,'accumulative sequences'] = df_weekly['number_of_sequences'][i] + df_weekly['accumulative sequences'][
                    i - 1]
            else:
                df_weekly.loc[i,'accumulative sequences'] = df_weekly['number_of_sequences'][i]
        return df_weekly

    def create_final_df(self):
        df_weekly, top_countries = self.create_df_weekly()
        # bring all countries to same maximum date, so that wont have problem in accumulative plot
        df_to_max_date = self.create_df_to_max_date(df_weekly, top_countries)

        df_weekly = pd.concat([df_weekly, df_to_max_date])
        df_weekly = df_weekly.set_index(pd.DatetimeIndex(df_weekly['date_from_cw']))
        df_weekly = df_weekly.groupby('region').resample('W').sum()
        df_weekly['date'] = [df_weekly.index[i][1] for i in np.arange(len(df_weekly))]
        df_weekly['country'] = [df_weekly.index[i][0] for i in np.arange(len(df_weekly))]
        df_weekly.reset_index(drop=True, inplace=True)
        df_weekly = self.add_accumulative_seq(df_weekly)
        df_weekly = df_weekly.rename({'number_of_sequences': 'weekly sequences'}, axis=1).reset_index(drop=True)
        return df_weekly

    def create_fig(self, yaxis):
        """
        param yaxis: "weekly sequences", "accumulated sequences"
        """
        # weird error when initialized the first time.
        # See https://community.plotly.com/t/valueerror-invalid-value-in-basedatatypes-py/55993/4
        try:
            fig = px.bar(self.df_weekly[['date', yaxis, 'country']], x="date", y=yaxis, color="country")
        except ValueError:
            fig = px.bar(self.df_weekly[['date', yaxis, 'country']], x="date", y=yaxis, color="country")
        fig.update_layout(
            font_size=10
        )
        return fig

