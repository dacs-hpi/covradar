from dash.dependencies import Input, Output, State
from datetime import timedelta
import dash
from dash.exceptions import PreventUpdate
from frontend.app.shared.utils_world_map import WorldMap, DateSlider
from shared.residue_converter.data_loader import voc_label_dict
from frontend.app.shared.cache import cache
from dash_extensions.snippets import send_bytes
import zipfile
from io import StringIO
import pandas as pd


def register_callbacks(app, db_connection, df_dict, lang_dict):

    text_dict = lang_dict['world_map']
    # world_map = WorldMap(df_dict["global_metadata"].copy(deep=True), df_dict["variants_of_concern"].copy(deep=True),
    #                      df_dict["sequence_voc"].copy(deep=True), df_dict["location_coordinates"].copy(deep=True))
    world_map = WorldMap(df_dict["global_metadata"], df_dict["variants_of_concern"],
                         df_dict["sequence_voc"], df_dict["location_coordinates"])
    date_slider_ww = DateSlider(df_dict["global_metadata"])
    # # menu hide button
    # @app.callback(
    #     Output('left_id', 'style'),
    #     [Input('test_toggle', 'value')])
    #     # def update_style(value):
    #     if value:
    #         css_style = {"left": "-100%"}
    #     else:
    #         css_style = {"left": "0%"}
    #     return css_style

    @app.callback(
        [Output("world_map_explanation_collapse", "is_open"),
         Output("world_map_explanation_collapse_button", "children")
         ],
        [Input("world_map_explanation_collapse_button", "n_clicks")],
        [State("world_map_explanation_collapse", "is_open")], prevent_initial_call=True,
    )
    def toggle_collapse(n, is_open):
        if n:
            button_name = text_dict['disclaimer_button'] if is_open else text_dict['disclaimer_hide']
            return not is_open, button_name
        return is_open, text_dict['disclaimer_button']

    # plot description modal button
    @app.callback(
        Output("map-info-modal", "is_open"),
        [Input("map_info_button_page", "n_clicks"),
         Input("map_info_button_left", "n_clicks"),
         Input("map_info_button_close", "n_clicks")],
        [State("map-info-modal", "is_open")], prevent_initial_call=True,
    )
    def toggle_collapse(n1, n2, n3, is_open):
        if n1 or n2 or n3:
            return not is_open
        return is_open

    # slider interval updated during drag (drag_value)
    # slider interval updated by play-button and Interval
    @app.callback(
        Output('date_slider_ww', 'value'),
        [Input('date_slider_ww', 'drag_value'),
         Input('selected_interval_ww', 'value'),
         Input('auto_stepper_ww', "n_intervals"),],
        [State('date_slider_ww', 'value'),],
        prevent_initial_call=True,
    )
    def update_slider_interval(drag_value, interval, n_intervals, slider_value):
        """
        slider moved by user drag, changed location of slider with drag_value
        OR
        slider moved by auto_stepper (activated by play-button)
        """
        ctx = dash.callback_context
        if interval is None:
            interval = 0
        if ctx.triggered:
            # comp_id = name of trigger input, e.g. date_slider, auto_stepper
            comp_id = ctx.triggered[0]['prop_id'].split('.')[0]
        # if interval changed or slider moved:
        if comp_id == "selected_interval_ww" or comp_id== "date_slider_ww":
            if not drag_value:
                return slider_value
            if len(drag_value) == 2:
                second_date = drag_value[-1]
                if DateSlider.get_date_x_days_before(DateSlider.unix_to_date(second_date), interval) > date_slider_ww.min_date:
                    new_first_date = DateSlider.unix_time_millis(DateSlider.get_date_x_days_before
                                                                 (DateSlider.unix_to_date(second_date), interval))
                else:
                    new_first_date = DateSlider.unix_time_millis(date_slider_ww.min_date)
                return [new_first_date, second_date]
            else:
                return slider_value
        # if play button starts auto_stepper
        if comp_id == 'auto_stepper_ww':
            if n_intervals == 0:
                #raise PreventUpdate
                return slider_value
            if interval is None:
                interval = 7
            if n_intervals + interval >= len(date_slider_ww.date_list):
                return [DateSlider.unix_time_millis(date_slider_ww.date_list[-interval]),
                        DateSlider.unix_time_millis(date_slider_ww.date_list[-1])]
            first_date = DateSlider.unix_time_millis(date_slider_ww.date_list[n_intervals-1])
            second_date = DateSlider.unix_time_millis(date_slider_ww.date_list[n_intervals+interval-1]) # first_date + interval*86400
            return [first_date, second_date]

    @app.callback(
        [Output('auto_stepper_ww', 'max_intervals'),
         Output('auto_stepper_ww', 'disabled'),
         Output('play_button_ww', 'className'),],
        [Input('play_button_ww', 'n_clicks'),
        Input('auto_stepper_ww', 'n_intervals'),],
        [State('selected_interval_ww', 'value'),
         State('play_button_ww', 'className')], prevent_initial_call=True,
    )
    def stepper_control(n_clicks, n_intervals, interval, button_icon):
        """
        stop and start auto-stepper (disabled value), returns play or stop icon for button
        """
        ctx = dash.callback_context
        if ctx.triggered:
            # comp_id = name of trigger input, e.g. date_slider, auto_stepper
            comp_id = ctx.triggered[0]['prop_id'].split('.')[0]
        else:
            comp_id = 'auto_stepper_ww'
        if interval is None:
            interval = 0
        steps = len(date_slider_ww.date_list)-interval
        # stop stepper
        if comp_id=='play_button_ww':
            # start stepper
            if button_icon == "fa fa-play-circle":
                return steps, False, "fa fa-stop-circle"
            # pause stepper, disabled=Stop
            elif button_icon == "fa fa-stop-circle":
                return steps, True, "fa fa-play-circle"
        else:
            if n_intervals == steps:
                return 0, True, "fa fa-play-circle"
            else:
                raise PreventUpdate

    @app.callback(
        Output('auto_stepper_ww', 'n_intervals'),
        [Input('auto_stepper_ww', 'disabled'),],
        [State('auto_stepper_ww', 'max_intervals')], prevent_initial_call=True,
    )
    def reset_stepper(disabled, max_int):
        """
        set n_intervals to 0 is end of slider is reached by Interval
        """
        if disabled and max_int==0:
            return 0
        else:
            raise PreventUpdate

    # map
    @app.callback(
        Output('world_map_fig', 'figure'),
        [Input('dropdown_mutation_ww', 'value'),
         Input('dropdown_method_ww', 'value'),
         Input('date_slider_ww', 'value'),
         Input('selected_interval_ww', 'value'),
         Input('dropdown_mode_ww', 'value'),
         Input('selected_nth_ww', 'value')],
         [State('dropdown_mutation_ww', 'options')],
         prevent_initial_call=False, # False = important!
    )
    @cache.memoize()
    def update_map(mutations, method, dates, interval, mode, nth, mutation_options):
        """
        Args:
            mutations (list): list of str mutations (from dropdown left menu)
            method (str): 'Frequency' or 'Increase' (from dropdown left menu)
            dates (int): selected date (date slider)
            interval (int): int size time interval (input field left menu)
            mode (str): select mode 'absolute frequencies' or 'n-th most frequent mutation'.
            nth (int): fig needs return value df (df: with every nth most frequent mutation per postal code)
            mutation_options (list): getting option in 'all mutations in dropdown'
        Returns:
            created map from function get_world_map
        """
        # update mutations, when "all" selector is used

        if mutations == 'all mutations in dropdown':
            mutations = world_map.get_mutations_from_dropdown_options(mutation_options)
        # put mutation into list if only a single mutation was selected
        elif isinstance(mutations, str):
            mutations = [mutations]

        # date and interval
        mutation_number = len(world_map.mutations_of_concern) + 1
        second_date = DateSlider.unix_to_date(dates[1])
        if interval is None:
            interval = 0
        if DateSlider.get_date_x_days_before(second_date, interval) < date_slider_ww.min_date:
            interval = (second_date - date_slider_ww.min_date).days + 1

        date_list = [d for d in [second_date - timedelta(days=x) for x in reversed(range(interval))]]

        # map
        nth = 1 if nth is None else -1 if nth > mutation_number else nth
        fig = world_map.get_world_map(mutations, method, date_list, mode, nth)
        return fig

    # info text
    @app.callback(
        Output('chosen_method_and_voc_ww', 'children'),
        [Input('dropdown_mutation_ww', 'value'),
         Input('dropdown_method_ww', 'value'),
         Input('date_slider_ww', 'value'),
         Input('selected_interval_ww', 'value'),
         Input('dropdown_mode_ww', 'value'),
         Input('selected_nth_ww', 'value')],
        [State('dropdown_mutation_ww', 'options')],
         prevent_initial_call=True,
    )
    @cache.memoize()
    def update_info(mutations, method, dates, interval, mode, nth, mutation_options):
        # update mutations, when "all" selector is used
        if mutations == 'all mutations in dropdown':
            mutations = world_map.get_mutations_from_dropdown_options(mutation_options)   
        table_md = world_map.get_params_table(mutations, method, dates, interval, mode, nth, date_slider_ww, text_dict)
        return table_md

    # update plots
    @app.callback(
        [Output('results_per_location', 'figure'),
         Output('mutation_development_ww', 'figure'),
         Output('chosen_location', 'children'),
         ],
        [Input('world_map_fig', 'clickData'),
         Input('dropdown_mutation_ww', 'value'),
         Input('dropdown_method_ww', 'value'),
         Input('yaxis_type_ww', 'value'),
         Input('date_slider_ww', 'value'),
         Input('selected_interval_ww', 'value'),],
        [State('dropdown_mutation_ww', 'options')],
         prevent_initial_call=True,
    )
    @cache.memoize()
    def update_plots(click_data, mutations, method, yaxis_type, dates, interval, mutation_options):
        # get click data {'points': [{'curveNumber': 0, 'pointNumber': 35, 'pointIndex': 35, 'lon': 30.802498, 'lat': 26.820553, 'hovertext': 'Egypt', 'marker.size': 1, 'customdata': ['A475V', 32, 'Egypt', 26.820553, 30.802498, 1]}]}
        # click_data['points'][0]['customdata'] = ['L18F', 3, 'Berlin', 10115, 32.5337, 13.3872, 30]
        # = [mut, nb_seq, location_name, location_ID, lat, lon, size
        # print(f"start plot: {datetime.now()}")
        try:
            location_name = click_data['points'][0]['hovertext']
            location_ID = click_data['points'][0]['customdata'][3]
        except TypeError:
            location_ID = 13353
            location_name = "Berlin"
        if click_data is None:
            location_ID = 13353
            location_name = "Berlin"
        # update mutations, when "all" selector is used
        if mutations == 'all mutations in dropdown':
            mutations = world_map.get_mutations_from_dropdown_options(mutation_options)
        # date from slider
        second_date = DateSlider.unix_to_date(dates[1])
        if DateSlider.get_date_x_days_before(second_date, interval) < date_slider_ww.min_date:
            interval = (second_date - date_slider_ww.min_date).days + 1
        if interval is None:
            interval = 0
        date_list = [d for d in [second_date - timedelta(days=x) for x in reversed(range(interval))]]
        # title text
        title_text = location_name
        # 1. plot
        if method == 'Increase':
            fig = world_map.get_slope_bar_plot(date_list, mutations, location_ID)
        elif method == 'Mutation Proportion':
            fig = world_map.get_frequency_bar_chart(mutations, date_list,  location_ID)
        elif method == 'Frequency':
            fig = world_map.get_frequency_bar_chart(mutations, date_list, location_ID)
        fig_develop = world_map.get_frequency_development_scatter_plot(mutations, yaxis_type, date_list, location_ID)
        return fig, fig_develop, title_text

    # # update hide elem
    # @app.callback(
    #     [Output('number_mutations', 'children'),
    #      Output('selected_nth', 'max'),
    #      Output('hide_elem', 'style')],
    #     [Input('dropdown_mutation', 'value'),
    #      Input('dropdown_mode', 'value')], prevent_initial_call=True,
    # )
    # def update_hide_elem(mutations, mode):
    #     # for mode left menu, show input_field for nth mutations only for mode 'n-th most frequent mutations
    #     nb_var = len(mutations)
    #     visibility = 'none' if mode == 'absolute frequencies' else 'block'
    #     return text_dict["var_nb_text"] % (str(nb_var)), nb_var, {'display': visibility}

    # update method selection for mode n-th frequent
    @app.callback(
        [Output('dropdown_method_ww', 'options'),
         Output('dropdown_method_ww', 'value'),
         Output('selected_nth_ww', 'disabled')],
        [Input('dropdown_mode_ww', 'value')],
    )
    def update_methods(mode):
        if mode == 'n-th most frequent mutation':
            return [{'label': text_dict['dropdown_methods_name'][0], 'value': text_dict['dropdown_methods_value'][0]}], 'Frequency', False
        else:
            return [{'label': name, 'value': value} for name, value in
                    zip(text_dict['dropdown_methods_name'],
                        text_dict['dropdown_methods_value'])], 'Frequency', True

    @app.callback(
        [Output('dropdown_mutation_ww', 'value')],
        [Input('dropdown_mutation_ww', 'value')],
        prevent_initial_call=True,
        )
    def update_moc_options(mocs):
        # remove other MOCs when "all mutations in dropdown" selected
        all_selector = "all mutations in dropdown"
        if not mocs:
            return [all_selector]
        elif mocs[-1] == all_selector:
            return [all_selector]
        elif mocs[0] == all_selector and len(mocs) > 1:
            return mocs[1:]
        else:
            return dash.no_update

    # download
    @app.callback(
        Output('download-world-map', 'data'),
        Input("world_map_download_csv", "n_clicks"),
        [State('dropdown_mutation_ww', 'value'),
         State('dropdown_method_ww', 'value'),
         State('date_slider_ww', 'value'),
         State('selected_interval_ww', 'value'),
         State('dropdown_mode_ww', 'value'),
         State('selected_nth_ww', 'value'),
         ], prevent_initial_call=True,
    )
    def download_map(n_clicks, mutations, method, dates, interval, mode, nth):
        if n_clicks:
            # params table
            table_md = world_map.get_params_table(mutations, method, dates, interval, mode, nth, date_slider_ww, text_dict)
            separated_table_md = table_md.split("\n\n")
            table0_io = StringIO(separated_table_md[0])
            table1_io = StringIO(separated_table_md[1])
            df_params = pd.read_table(table0_io, sep="|", header=0, skipinitialspace=True).iloc[:,1:-1]
            df_params.columns = ['parameter', 'selected']
            for index, row in df_params.iterrows():
                row['parameter'] = row['parameter'].replace(" ", "")
                row['selected'] = row['selected'].replace(",", "")
            df_params.drop(df_params.index[[0]], inplace=True)
            df_params_bytes = df_params.to_csv(index=False).encode('utf-8')
            df_params_filename = 'parameter_settings.csv'
            # sequences table
            df_seqs = pd.read_table(table1_io, sep="|", header=0, skipinitialspace=True).iloc[:,1:-1]
            df_seqs.columns = ['sequences', 'number']
            df_seqs.drop(df_seqs.index[[0]], inplace=True)
            df_seqs_bytes = df_seqs.to_csv(index=False).encode('utf-8')
            df_seqs_filename = 'sequences_info.csv'
            # map df
            if interval is None:
                interval = 0
            second_date = DateSlider.unix_to_date(dates[1])
            dates = [d for d in [second_date - timedelta(days=x) for x in reversed(range(interval))]]
            df_map, column_of_interest = world_map.get_world_map_df(method, mutations, dates)
            cols = ["location", "location_ID", "lat", "lon", "mutations", "number_sequences"]
            df_map = df_map[cols]
            df_map_bytes = df_map.to_csv(index=False).encode('utf-8')
            df_map_filename = 'map_data.csv'
            # time courses
            location_IDs = list(set(df_map["location_ID"]))
            cols = ["date", "mutations", "number_sequences", "zip", "lat", "lon"]
            time_course_dfs = [world_map.get_df_for_scatter_plot(mutations, dates, location_ID=l_ID).reindex(columns=cols).rename(columns={'mutations':'mutation'}).to_csv(index=False).encode('utf-8') for l_ID in location_IDs]
            # create zip
            def write_archive(bytes_io):
                with zipfile.ZipFile(bytes_io, "w") as zf:
                    zf.writestr(df_params_filename, df_params_bytes)
                    zf.writestr(df_seqs_filename, df_seqs_bytes)
                    zf.writestr(df_map_filename, df_map_bytes)
                    for df_bytes, zip_code in zip(time_course_dfs, location_IDs):
                        zf.writestr("time_courses/"+str(zip_code)+".csv", df_bytes)
            return send_bytes(write_archive, "map_data.zip")









