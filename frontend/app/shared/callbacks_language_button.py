from dash.dependencies import Input, Output, State, ALL
import yaml
import dash_html_components as html
from frontend.app.shared.utils_world_map import DateSlider

def import_language_dict(lang):
    with open("frontend/app/shared/assets/text_english_german.yaml", 'r') as stream:
        dict_lang=(yaml.safe_load(stream))
    #return dict_lang
    if lang == "german":
        # if True:
        dict_lang_de={}
        config_plots = dict(locale='de')
        for subpage in dict_lang.keys():
            dict_lang_de[subpage]={k:v['german'] for (k, v) in dict_lang[subpage].items()}
        return dict_lang_de
    elif lang == "english":
        config_plots = dict(locale='en')
        dict_lang_en={}
        for subpage in dict_lang.keys():
            dict_lang_en[subpage]={k:v['english'] for (k, v) in dict_lang[subpage].items()}
        return dict_lang_en

    return 

def register_callbacks(app, db_connection, df_dict, lang_dict):
    selected_lan = "german" if lang_dict["get_lang"]["lang_tester"]=="German" else "english"

    @app.callback(
        [Output({'type':ALL, 'index': ALL}, 'children'),
        Output("imprint_contact", "label"),
        Output("imprint_policy", "label"),
        # modal data overview
        Output("modal_data_overview_header", "children"),
        Output("modal_data_overview", "children"),
        Output("modal_data_overview_options_header", "children"),
        Output("modal_data_overview_options", "children"),
        Output("data_overview_info_button_close", "children"),
        # modal freq plot
        Output("modal_freq_plot_header", "children"),
        Output("modal_freq_plot_general", "children"),
        Output("modal_freq_plot_consensus_opt", "children"),
        Output("modal_freq_plot_consensus", "children"),
        Output("modal_freq_plot_seq_opt", "children"),
        Output("modal_freq_plot_seq", "children"),
        Output("modal_freq_plot_annotation_opt", "children"),
        Output("modal_freq_plot_annotation", "children"),
        Output("freq_plot_info_button_close", "children"), 
        # modal nt+aa plot
        Output("modal_nt_aa_plot_header", "children"),
        Output("modal_nt_plot_header", "children"),
        Output("modal_nt_plot", "children"),
        Output("modal_aa_plot_header", "children"),
        Output("modal_aa_plot", "children"),
        Output("modal_nt_aa_plot_options_header", "children"),
        Output("modal_nt_aa_plot_options", "children"),
        Output("nt_aa_plot_info_button_close", "children"),
        # modal consensus chart 
        Output("modal_consensus_header", "children"),
        Output("modal_consensus_general", "children"),
        Output("modal_consensus_options_header", "children"),
        Output("modal_consensus_options", "children"),
        Output("consensus_chart_info_button_close", "children")],
        [Input('language_button', 'n_clicks')],
        State({'type':ALL, 'index': ALL}, 'id'),
        prevent_initial_call=False
    )
    def switch_langues(n_clicks, text_elements):

        if (n_clicks % 2) == 0:
            lang = selected_lan
        else:
            lang = "english" if selected_lan != "english" else "german"

        link_pointer = "here." if lang=="english" else "hier."
 
        lang_dict = import_language_dict(lang)
        contact_label = lang_dict['right_menu']["imprint_contact"]
        policy_label = lang_dict['right_menu']["imprint_policy"]

        # reload for frequency plot description modal
        modal_data_overview_header = lang_dict['overview_data']["description"]
        modal_data_overview = lang_dict['overview_data']["modal_data_overview"]
        modal_data_overview_options_header = lang_dict['overview_data']["modal_data_overview_options_header"]
        modal_data_overview_options = lang_dict['overview_data']["modal_data_overview_options"]
        data_overview_info_button_close = lang_dict['overview_data']["modal_close"]
        
        # reload for frequency plot description modal
        modal_freq_plot_header = lang_dict['baseFrequencyPlot']["description"]
        modal_freq_plot_general = lang_dict['baseFrequencyPlot']["modal_freq_plot_general"]
        modal_freq_plot_consensus_opt = lang_dict['baseFrequencyPlot']["consensus_option"]
        modal_freq_plot_consensus = lang_dict['baseFrequencyPlot']["modal_freq_plot_consensus"]
        modal_freq_plot_seq_opt = lang_dict['baseFrequencyPlot']["Div_seq_opt"]
        modal_freq_plot_seq = lang_dict['baseFrequencyPlot']["modal_freq_plot_seq"]
        modal_freq_plot_annotation_opt = lang_dict['baseFrequencyPlot']["Div_annotation_options"]
        modal_freq_plot_annotation = lang_dict['baseFrequencyPlot']["modal_freq_plot_annotation"]
        freq_plot_info_button_close = lang_dict['baseFrequencyPlot']["modal_close"]

        # reload for nt+aa mutation plot description modal
        modal_nt_aa_plot_header = lang_dict['mutation_distribution_plot']["description"]
        modal_nt_plot_header = lang_dict['mutation_distribution_plot']["H6_dist"]
        modal_nt_plot = lang_dict['mutation_distribution_plot']["modal_nt_plot"]
        modal_aa_plot_header = lang_dict['mutation_distribution_plot']["H6_AA_dist"]
        modal_aa_plot = lang_dict['mutation_distribution_plot']["modal_aa_plot"]
        modal_nt_aa_plot_options_header = lang_dict['mutation_distribution_plot']["modal_nt_aa_plot_options_header"]
        modal_nt_aa_plot_options = lang_dict['mutation_distribution_plot']["modal_nt_aa_plot_options"]
        nt_aa_plot_info_button_close = lang_dict['mutation_distribution_plot']["modal_close"]

        # reload for consensus chart description modal
        modal_consensus_header = lang_dict['consensus_table']['description']
        modal_consensus_general = lang_dict['consensus_table']['modal_consensus_general']
        modal_consensus_options_header = lang_dict['consensus_table']['modal_consensus_options_header']
        modal_consensus_options = lang_dict['consensus_table']['modal_consensus_options']
        consensus_chart_info_button_close = lang_dict['consensus_table']['modal_close']

        texts = []
        for el in text_elements:
            if el["index"] == "disclaimer_text":
                link = html.A(
                               link_pointer,
                               href="https://outbreak.info/compare-lineages?gene=S&gene=ORF1a&gene=ORF1b&threshold=75",
                               target="_blank"
                           )
                texts.append([lang_dict[el["type"]][el["index"]],link])
            elif el["index"] == "div_nb_days":
                date_slider = DateSlider(df_dict["global_metadata"])
                days = (date_slider.max_date - date_slider.min_date).days
                suffix = str(days + 1) + ')'
                texts.append(lang_dict[el["type"]][el["index"]] + suffix)
            elif el["index"] == "db_text_ebi":
                link_charite = html.A(
                            lang_dict[el["type"]]['db_source'],
                            href="https://civnb.info/sequences/",
                            target="_blank"
                           )
                link_ebi_paper = html.A(
                                        "Madeira et al.",
                                        href="https://pubmed.ncbi.nlm.nih.gov/30976793/",
                                        target="_blank"
                                    )
                link_ebi_data = html.A(
                                        lang_dict[el["type"]]['db_source'],
                                        href="ftp://ftp.ebi.ac.uk/pub/databases/covid19dataportal/viral_sequences/sequences",
                                        target="_blank"
                                    )
                texts.append([lang_dict[el["type"]]["db_text_ebi1"], link_charite, lang_dict[el["type"]]['db_text_ebi2'], link_ebi_paper, ", ", link_ebi_data, ")."])
            elif el["index"] == "db_text_rki":
                link_rki_data = html.A(
                            "DESH",
                            href="https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/DESH/DESH.html",
                            target="_blank"
                        )
                texts.append([lang_dict[el["type"]]["db_text_rki1"], link_rki_data, lang_dict[el["type"]]['db_text_rki2']])
            else:
                texts.append(lang_dict[el["type"]][el["index"]])
        return texts, contact_label, policy_label, \
            modal_data_overview_header, modal_data_overview, modal_data_overview_options_header, modal_data_overview_options, data_overview_info_button_close, \
            modal_freq_plot_header, modal_freq_plot_general, modal_freq_plot_consensus_opt, modal_freq_plot_consensus, modal_freq_plot_seq_opt, modal_freq_plot_seq, modal_freq_plot_annotation_opt, modal_freq_plot_annotation, freq_plot_info_button_close, \
            modal_nt_aa_plot_header, modal_nt_plot_header, modal_nt_plot, modal_aa_plot_header, modal_aa_plot, modal_nt_aa_plot_options_header, modal_nt_aa_plot_options, nt_aa_plot_info_button_close, \
            modal_consensus_header, modal_consensus_general, modal_consensus_options_header, modal_consensus_options, consensus_chart_info_button_close