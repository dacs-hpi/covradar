# from frontend.app.shared.profiler import cumulative_profiler
from frontend.app.shared.utils import data_filter_sql
from frontend.app.shared.utils_data_table import CountryTimePlot
from frontend.app.shared.cache import cache
from dash.dependencies import Input, Output, State
from dash_extensions.snippets import send_data_frame
from frontend.app.shared.cache import cache
datatable = False

def register_callbacks(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['overview_data']


    #country over time
    @app.callback(
        Output('country_cases_over_time', 'figure'),
        [Input('yaxis-type', 'value'),
         Input('url', 'pathname')]
         )
    @cache.memoize()
    def update_cases_temporal_fig(yaxis, pathname):
        data_table_obj = CountryTimePlot(df_dict["consensus"])
        # data_table_obj = CountryTimePlot(df_dict["consensus"].copy(deep=True))
        fig = data_table_obj.create_fig(yaxis)
        return fig

    if datatable:
        # Filter Buttons for Data Table: country, date range, origin
        # In print version, table with selected filter is shown
        @app.callback(
            [Output('table1', 'data'),
            Output('meta_no_seq', 'children')],
            [Input('country-dropdown', 'value'), 
                Input('my-date-picker-range', 'start_date'),
                Input('my-date-picker-range', 'end_date'),
                Input('origin-dropdown', 'value'),
                Input('host-dropdown', 'value'),
                Input('url', 'pathname')])
        #@cumulative_profiler(1)
        @cache.memoize()
        def update_data_table(country, start_date, end_date, origin, hosts, pathname):
            # initial start_date and end_date have format YYYY-MM-DDT00:00:00 which leads to bug in date filter
            start_date = start_date.split("T")[0]
            end_date = end_date.split("T")[0]
            filtered_data = data_filter_sql(df_dict, country, start_date, end_date, origin, hosts)
            filtered_data_dict = filtered_data.to_dict('records')
            total_seqs = len(filtered_data)
            text = 'Number of sequences in filter: {}'.format(total_seqs)

            return filtered_data_dict, text

        # When using the filter, the page is set to 0 again.
        @app.callback(
            Output('table1', 'page_current'),
            [Input('country-dropdown', 'value'), 
                Input('my-date-picker-range', 'start_date'),
                Input('my-date-picker-range', 'end_date'),
                Input('origin-dropdown', 'value')])
        def update_data_table(country, start_date, end_date, origin):
            return 0

    # description modal button
    @app.callback(
        Output("data-overview-info-modal", "is_open"),
        [Input("data_overview_info_button_page", "n_clicks"), Input("data_overview_info_button_left", "n_clicks"), Input("data_overview_info_button_close", "n_clicks")],
        [State("data-overview-info-modal", "is_open")], prevent_initial_call=True,
    )
    def toggle_collapse(n1, n2, n3, is_open):
        if n1 or n2 or n3:
            return not is_open
        return is_open

    # download csv button
    @app.callback(
        Output("download-data-overview", "data"),
        Input("data_overview_download_tsv", "n_clicks"),
        [State('yaxis-type', 'value')],
        prevent_initial_call=True,
    )
    def toggle_collapse(n_clicks, yaxis):
        if n_clicks:
            data_table_obj = CountryTimePlot(df_dict["consensus"])
            df = data_table_obj.df_weekly
            if yaxis == "weekly sequences":
                filename = 'data_distribution_weekly.csv'
            else:
                filename = 'data_distribution_accumulative.csv'
            return send_data_frame(df.to_csv, filename)
