import copy
import time
import plotly.express as px
from datetime import timedelta, date
from scipy.stats import linregress
import pandas as pd
import math
from shared.residue_converter.data_loader import voc_label_dict
import pandas as pd
pd.options.mode.chained_assignment = None 


class PieAndScatterPlot(object):
    def __init__(self, global_metadata, variants_of_concern, sequence_voc, location_coordinates):
        """
        df_location: location_ID | location (str name) | lat | lon
        df_all_dates_all_voc: df containing information to all voc of all dates
                                location_ID | date | id_list | mutations | number_sequences
        df_count: number all seq per day per location
                    date | location_ID | id_list | number_sequences |
        column id_list = comma separated str with all strain_ids containing same voc on same date and same location
        """
        super(PieAndScatterPlot, self).__init__()
        self.mutations_of_concern = self.get_mutations_of_concern(variants_of_concern)
        self.mutations = copy.deepcopy(self.mutations_of_concern) + ['wildtype']
        self.color_dict = self.get_color_dict()
        self.min_date= global_metadata["date"].min().date()
        self.max_date = global_metadata["date"].max().date()
        self.df_location = location_coordinates[['location_ID', 'name', 'lat', 'lon']].rename(columns={"name": "location"})
        self.df_all_dates_all_voc = self.get_full_df(sequence_voc, variants_of_concern, global_metadata)
        self.df_count = self.get_number_sequences_all_dates_all_var(self.df_all_dates_all_voc[['location_ID', 'date', 'id_list']])

    def get_full_df(self, sequence_voc, variants_of_concern, global_metadata):
        # 1. join vocs:
        df_sq_id_to_voc = pd.merge(sequence_voc, variants_of_concern[["amino_acid", "id"]], how="left",
                                   left_on="voc_id", right_on="id")[['strain_id', 'amino_acid']]
        # 2. join metadata
        df_all_dates_all_voc = pd.merge(global_metadata[["strain_id", "location_ID", "date"]], df_sq_id_to_voc, how="left",
                                        left_on="strain_id", right_on="strain_id")[["strain_id", "location_ID", "date", "amino_acid"]]
        # 3. fill strain_ids without matching voc (amino_acid with "wildtype")
        del df_sq_id_to_voc
        df_all_dates_all_voc = df_all_dates_all_voc.fillna(value={"amino_acid":"wildtype"})
        # 4. location_ID, date, amino_acid --> concat all strain_ids into one comma separated string list and count
        #df_all_dates_all_voc = df_all_dates_all_voc.groupby(["location_ID", "date", "amino_acid"])["strain_id"].apply(list).reset_index(name='id_list')
        df_all_dates_all_voc = df_all_dates_all_voc.groupby(["location_ID", "date", "amino_acid"])["strain_id"] \
            .apply(lambda x: ','.join([str(y) for y in x])).reset_index(name='id_list')
        # 5. add sequence count
        #df_all_dates_all_voc["number_sequences"] = df_all_dates_all_voc["id_list"].apply(lambda x: len(x))
        df_all_dates_all_voc["number_sequences"] = df_all_dates_all_voc["id_list"].apply(lambda x: len(x.split(',')))
        df_all_dates_all_voc = df_all_dates_all_voc.rename(columns={"amino_acid": "mutations"})
        # 6. fill with 0 no mutation
        full_df_without_nb_seq = df_all_dates_all_voc[['location_ID', 'date']].drop_duplicates()
        full_df_without_nb_seq["number_sequences"] = 0
        full_df_without_nb_seq = full_df_without_nb_seq.merge(pd.DataFrame(self.mutations, columns=["mutations"]), how='cross')
        # combine real seq counts (per voc per date per location) with zero values
        df_all_dates_all_voc = pd.concat([df_all_dates_all_voc, full_df_without_nb_seq]) \
            .drop_duplicates(subset=['location_ID', 'date','mutations'], keep="first") \
            .reset_index(drop=True).fillna(0)
        df_all_dates_all_voc = df_all_dates_all_voc[['location_ID', 'date', 'id_list','mutations','number_sequences']]
        df_all_dates_all_voc = df_all_dates_all_voc.astype({'location_ID': 'int32', 'id_list': 'str'})
        return df_all_dates_all_voc[["location_ID", "date", "id_list", "mutations", "number_sequences"]]

    # def get_full_df_with_sql(self, db_connection):
    #     df_with_nb_seq_per_voc = SQL_get_all_dates_data_df(db_connection)
    #     # get all dates all locations table with 0 number_sequences for every voc:
    #     full_df_without_nb_seq = df_with_nb_seq_per_voc[['location_ID', 'date']].drop_duplicates()
    #     full_df_without_nb_seq["number_sequences"] = 0
    #     full_df_without_nb_seq = full_df_without_nb_seq.merge(pd.DataFrame(self.mutations, columns=["mutations"]), how='cross')
    #     # combine real seq counts (per voc per date per location) with zero values
    #     df_all_dates_all_voc = pd.concat([df_with_nb_seq_per_voc, full_df_without_nb_seq]) \
    #         .drop_duplicates(subset=['location_ID', 'date','mutations'], keep="first") \
    #         .reset_index(drop=True).fillna(0)
    #     df_all_dates_all_voc = df_all_dates_all_voc[['location_ID', 'date', 'id_list','mutations','number_sequences']]
    #     df_all_dates_all_voc = df_all_dates_all_voc.astype({'location_ID': 'int32', 'id_list': 'str'})
    #     return df_all_dates_all_voc.sort_values(by=['location_ID'])

    def get_number_sequences_per_interval(self, dates, mutations, location_ID=None):
        """
        param df: df_all_dates_all_voc
        param dates: [date(2021, 12, 12), date(2021, 12, 13), ...]
        """
        if isinstance(mutations, str):
            mutations = list(mutations)
        df = self.df_all_dates_all_voc[self.df_all_dates_all_voc['date'].isin(dates) & self.df_all_dates_all_voc['mutations'].isin(mutations)]
        if location_ID:
            df = df[df.location_ID == location_ID]
        seq_set = set(','.join(list(df['id_list'])).split(','))
        if '0' in seq_set:
            return len(seq_set)-1
        else:
            return len(seq_set)

    def add_location_info_to_df(self, df):
        if df.empty:
            return df
        else:
            return pd.merge(df, self.df_location, on="location_ID")

    def get_color_dict(self):
        """
        defined color by mutation
        color scheme contains 24 different colors, if #mutations>24 use second color scheme with 24 colors
        more mutations --> color schema starts again (max 48 different colors)
        wildtype= green, no_mutation (no sequence meets the user selected mutations, dates, location) = grey
        """
        color_dict = {}
        color_schemes = [px.colors.qualitative.Light24, px.colors.qualitative.Dark24]
        mutations = [var[1:-1] if '`' in var else var for var in self.mutations_of_concern]
        for i, mutation in enumerate(mutations):
            j = i % 48
            if j < 24:
                color_dict[mutation] = color_schemes[0][j]
            elif j > 23 and j < 48:
                color_dict[mutation] = color_schemes[1][j-24]
        color_dict['no_mutation'] = 'grey'
        color_dict['wildtype'] = 'green'
        return color_dict

    def get_mutations_of_concern(self, variants_of_concern):
        """
        returns: list of voc, ["Y144-144del", "N501Y", "L18F", "N501Y", "R190S", "E484K,R683G", ... ]
        """
        mutations = [f"{voc}" for voc in variants_of_concern['amino_acid'].unique().tolist()]
        # important mutations to front positions
        for var in ["Y144-144del", "N501Y", "L18F"]:
            if var in mutations:
                mutations.remove(var)
                mutations.insert(1, var)
        return mutations

    def get_mutations_from_dropdown_options(self, mutation_options):
        mutations = [x['value'] for x in mutation_options]
        mutations.remove('all mutations in dropdown')
        return mutations

    # pandas df methods instead of sql queries
    def get_nb_of_seq(self, x):
        x_s = set(x.split(","))
        if '0' in x_s:
            return len(x_s)-1
        else:
            return len(x_s)

    def get_number_sequences_all_dates_all_var(self, da_loc_id_list_df):
        """
        return date  location_ID  number_sequences
        """

        da_loc_nb_df = da_loc_id_list_df.groupby(['date','location_ID'])["id_list"].agg(lambda x: ",".join(x)).reset_index()
        da_loc_nb_df['number_sequences']= da_loc_nb_df['id_list'].apply(lambda x: self.get_nb_of_seq(x)).drop(columns=['id_list'])
        return da_loc_nb_df

    def get_number_mut_sequences_per_location(self, full_df, dates, mutations):
        df_loc_seq_nb_for_selected_voc = full_df[full_df['date'].isin(dates)].drop(columns=['date'])
        df_loc_seq_nb_for_selected_voc = df_loc_seq_nb_for_selected_voc[df_loc_seq_nb_for_selected_voc
            .mutations.isin(mutations)].drop(columns=['mutations', "number_sequences"]).drop_duplicates()
        df_loc_seq_nb_for_selected_voc2 = df_loc_seq_nb_for_selected_voc.groupby(['location_ID'])["id_list"].agg(lambda x: ",".join(x)).reset_index()
        df_loc_seq_nb_for_selected_voc2['number_sequences']= df_loc_seq_nb_for_selected_voc2['id_list'].apply(lambda x: self.get_nb_of_seq(x))
        return df_loc_seq_nb_for_selected_voc2.drop(columns=['id_list'])

    def get_df_for_increase_map_or_plot(self, mutations, dates, location_ID=None):
        mutations = [var[1:-1] if '`' in var else var for var in mutations]
        if not location_ID:
            df = self.df_all_dates_all_voc[
                self.df_all_dates_all_voc['date'].isin(dates) & self.df_all_dates_all_voc['mutations']
                    .isin(mutations)].groupby(['location_ID', 'mutations', 'date']).sum().reset_index()
        else:
            df = self.df_all_dates_all_voc[
                self.df_all_dates_all_voc['location_ID'].isin([location_ID]) & self.df_all_dates_all_voc['date']
                    .isin(dates) & self.df_all_dates_all_voc['mutations'].isin(mutations)].groupby(
                ['location_ID', 'mutations', 'date']).sum().reset_index()
        return df

    def get_df_for_frequency_bar(self, mutations, dates, location_ID):
        mutations = [var[1:-1] if '`' in var else var for var in mutations]
        df = self.df_all_dates_all_voc[['location_ID', 'mutations','number_sequences']][
            self.df_all_dates_all_voc['location_ID'].isin([location_ID]) & self.df_all_dates_all_voc['date']
                .isin(dates) & self.df_all_dates_all_voc['mutations'].isin(mutations)].groupby(
            ['location_ID', 'mutations']).sum().reset_index()
        return df

    def get_increase_df(self, dates, mutations, location_ID=None):
        """
        shows change in frequency of the different virus mutations, calculate lin regression with scipy.stats module and
        returns the slope of the regression line (x:range (interval)), y:number of sequences per day in selected interval
        :param dates: list of date objects in selected interval e.g. [2021-01-01, 2021-01-10]
        :param mutations: list of str, selected mutations (from dropdown left menu)
        :param location_ID: int
        """
        # df:     location_ID | date | mutations | number_sequences
        df = self.get_df_for_increase_map_or_plot(mutations, dates, location_ID=location_ID)
        df = df.groupby(['location_ID', 'mutations']).agg(
            {'number_sequences': lambda x: list(x), 'date': lambda x: list(x)})
        if df.empty:
            df = pd.DataFrame([], columns=["number_sequences", "date", "slope"])
        else:
            df.reset_index(inplace=True)
            slopes = []
            for i in range(len(df['number_sequences'])):
                dates = [(date.date() - self.min_date).days for date in df['date'][i]]
                nu = df['number_sequences'][i]
                if len(set(nu)) == 1:
                    slopes.append(0)
                else:
                    slopes.append(linregress(dates, nu).slope)
            df['slope'] = slopes
            df = df.astype({"slope": float})
        return df

    def get_nth_mutation_per_region(self, df_all, column_of_interest, ordinal_number=1):
        """
        group by location_ID and take every nth row from group,
        if ordinal number > number of rows per group -> take last row
        :param df_all: df with location, mutations and number_sequences
        :param column_of_interest: 'number_sequences' or 'slope'
        :param ordinal_number: int the n of nth
        :return df: with every nth most frequent mutation per postal code
        """
        ordinal_number -= 1
        df_all.sort_values(by=["location_ID", column_of_interest], inplace=True, ascending=False)
        grouped_df = df_all.groupby(["location_ID"], as_index=False)
        df = grouped_df.nth(ordinal_number)

        # no entry for nth mutation, not needed anymore, now all with seq (number_seq = 0)
        if len(grouped_df) != len(df):
            l = []
            for location_ID in set(df_all["location_ID"]):
                try:
                    l.append(grouped_df.get_group(location_ID).iloc[ordinal_number])
                except IndexError:
                    pass
            df = pd.DataFrame(l, columns=["location_ID", "mutations", "number_sequences"])
        return df

    def get_df_for_scatter_plot(self, mutations, dates, location_ID):
        mutations = [var[1:-1] if '`' in var else var for var in mutations]
        df = self.df_all_dates_all_voc[
            self.df_all_dates_all_voc['location_ID'].isin([location_ID]) & self.df_all_dates_all_voc['date'].isin(
                dates) & self.df_all_dates_all_voc['mutations'].isin(mutations)].reset_index(drop=True)
        return df[["location_ID","date", "mutations", "number_sequences"]]

    def get_mutation_proportion_df(self, dates, mutations):
        """
        :param dates: list of dates [type:date] of selected date and interval
        :param mutations: list of str, selected mutations (from dropdown left menu)
        :return df  location_ID |  mutation_proportion
        returns proportion of ([all variants not in voc](if wildtype in param mutations) + vocs in param mutations)
        but this case can only occur if wildtype included in drop down selected mutations
        """
        # df_loc_seq_nb_for_selected_voc:  location_ID | number_sequences
        df_loc_seq_nb_for_selected_voc = self.get_number_mut_sequences_per_location(self.df_all_dates_all_voc,
                                                                                    dates, mutations)
        if not df_loc_seq_nb_for_selected_voc.empty:
            df_count = self.df_count[self.df_count['date'].isin(dates)].groupby('location_ID', as_index=False).sum()
            var_to_nb_seq_dict = dict(zip(df_count.location_ID, df_count.number_sequences))
            df_loc_seq_nb_for_selected_voc = df_loc_seq_nb_for_selected_voc.groupby(['location_ID'], as_index=False).sum()
            df_loc_seq_nb_for_selected_voc['mutation_proportion'] = df_loc_seq_nb_for_selected_voc.apply(
                lambda row: row.number_sequences / var_to_nb_seq_dict[int(row.location_ID)] * 100, axis=1)
            df_loc_seq_nb_for_selected_voc['number_sequences'] = df_loc_seq_nb_for_selected_voc.apply(
                lambda row: var_to_nb_seq_dict[row.location_ID], axis=1)
        else:
            df_loc_seq_nb_for_selected_voc['mutation_proportion'] = 0.0
        return df_loc_seq_nb_for_selected_voc

    def drop_rows_by_value(self, df, value, column):
        index_rows = df[df[column] == value].index
        df.drop(index_rows, inplace=True)
        return df

    def calculate_ticks_from_dates(self, dates, date_numbers):
        """
        set tickvals and ticktext for displaying dates in plot
        show only 6 dates for readability: /6
        """
        unique_dates = list(set(list(dates)))
        unique_dates.sort()
        unique_date_numbers = list(set(date_numbers))
        unique_date_numbers.sort()
        tickvals_date = unique_date_numbers[0::math.ceil(len(unique_date_numbers) / 6)]
        ticktext_date = unique_dates[0::math.ceil(len(unique_dates) / 6)]
        return tickvals_date, ticktext_date

    def create_frequency_plot(self, df):
        fig = px.bar(df, y="number_sequences", x="mutations", color="mutations", orientation="v", hover_name="mutations",
                     hover_data={'mutations': False, "number_sequences": True},
                     color_discrete_map=self.color_dict, labels={"number_sequences": "# sequences", "mutations": ""},
                     height=300)
        fig.update_layout(showlegend=False, xaxis_tickangle=-45,
                          margin={'l': 20, 'b': 30, 'r': 10, 't': 10},
                          xaxis_title="selected mutations",
                          title_x=0.5
                          )
        return fig

    # plot methods
    def get_frequency_bar_chart(self, mutations, dates, location_ID):
        """
        :param mutations: list of str, selected mutations (from dropdown left menu)
        :param dates: list of dates [type:date] of selected date and interval
        :param location_ID: ID of hovered location
        :return fig bar chart showing mutation information of last hovered plz
        """
        df = self.get_df_for_frequency_bar(mutations, dates, location_ID=location_ID)
        df = self.drop_rows_by_value(df, 0, "number_sequences")
        df['mutations'] = df['mutations'].map(voc_label_dict).fillna(df['mutations'])
        if df.empty:
            df = pd.DataFrame(data=[['no_mutation', 0]], columns=["mutations", "number_sequences"])
        # this try/except block is a hack that catches randomly appearing errors of data with wrong type, unclear why this is working
        try:
            fig = self.create_frequency_plot(df)
        except ValueError:
            fig = self.create_frequency_plot(df)
        return fig

    def create_slop_plot(self, df):
        fig = px.bar(df, y="slope", x="mutations", color="mutations", orientation="v", hover_name="mutations",
                     hover_data={'mutations': False, "slope": True},
                     color_discrete_map=self.color_dict,
                     height=300)
        fig.update_layout(showlegend=False, xaxis_tickangle=-45,
                          margin={'l': 20, 'b': 30, 'r': 10, 't': 10},
                          title_x=0.5
                          )
        return fig

    def get_slope_bar_plot(self, dates, mutations, location_ID):
        df = self.get_increase_df(dates, mutations, location_ID)
        df = self.drop_rows_by_value(df, 0, "slope")
        if df.empty:
            columns = ["number_sequences", "date", "slope", "mutations"]
            row = [[0,"",0,"no_mutation"]]
            df = df.append(pd.DataFrame(row, columns=columns))
        # this try/except block is a hack that catches randomly appearing errors of data with wrong type, unclear why this is working
        try:
            fig = self.create_slop_plot(df)
        except ValueError:
            fig = self.create_slop_plot(df)
        return fig

    def create_scatter_plot(self, df, tickvals_date, ticktext_date, axis_type):
        fig = px.scatter(df, x='date_numbers', y='number_sequences', color='mutations', trendline='ols',
                                color_discrete_map=self.color_dict,
                                labels={"date_numbers": "date", "number_sequences": "# sequences"},
                                height=300)
        fig.update_yaxes(type='linear' if axis_type == 'linear' else 'log')
        fig.update_xaxes(showgrid=False,
                         # show only 7 values
                         tickmode="array",
                         tickvals=tickvals_date,
                         ticktext= ticktext_date,
                         tickangle=-45),
        fig.update_layout(legend_title_text='',
                          showlegend=False,
                          margin={'l': 20, 'b': 30, 'r': 10, 't': 10},
                          title_x=0.5)
        fig.update_traces(marker={'size': 5})
        return fig

    def get_frequency_development_scatter_plot(self, mutations, axis_type, dates, location_ID):

        """
        :param mutations: list of str, selected mutations (from dropdown left menu)
        :param axis_type: lin or log
        :param dates: list of dates [type:datetime.date] of selected date and interval
        :return fig: scatter plot showing number of sequences per day of last hovered location
        """
        # TODO: same lines on top of each other have color of latest MOC -> change to mixed color
        if len(dates) == 0:
            dates = [dat for dat in
                     [self.max_date - timedelta(days=x) for x in reversed(range(7))]]
        df = self.get_df_for_scatter_plot(mutations, dates, location_ID)
        # remove rows if MOC no seq in time-interval
        for var in mutations:
            if df[df.mutations == var]['number_sequences'].sum() == 0:
                df = df[df.mutations != var]
        df['mutations'] = df['mutations'].map(voc_label_dict).fillna(df['mutations'])
        # dummy dataframe for showing empty results
        if df.empty:
            df = pd.DataFrame(data=[[location_ID, pd.Timestamp(dates[-1]), 'no_mutations', 0]],
                              columns=["location_ID","date", "mutations", "number_sequences"])
        # date_numbers: assign date to number, numbers needed for calculation of trendline
        date_numbers = [(d.date() - self.min_date).days for d in df["date"]]
        df['date_numbers'] = date_numbers
        dates = df['date'].apply(lambda x: x.strftime("%Y-%m-%d"))
        tickvals_date, ticktext_date = self.calculate_ticks_from_dates(dates, date_numbers)
        # this try/except block is a hack that catches randomly appearing errors of data with wrong type, unclear why this is working
        try:
            fig = self.create_scatter_plot(df, tickvals_date, ticktext_date, axis_type)
        except ValueError:
            fig = self.create_scatter_plot(df, tickvals_date, ticktext_date, axis_type)
        return fig


class WorldMap(PieAndScatterPlot):
    def __init__(self, global_metadata, variants_of_concern, sequence_voc, location_coordinates):
        """

        """
        super(WorldMap, self).__init__(global_metadata, variants_of_concern, sequence_voc, location_coordinates)

    def get_world_map_df(self, method, mutations, dates):
        """
        :param method: 'Frequency' or 'Mutation Proportion' or 'Increase'
        :param mutations: list of selected voc mutations
        :param dates: list of dates of selcted date and interval, date = class 'datetime.date'
        """
        if method == 'Frequency':
            #  df:     location_ID   mutations  number_sequences
            mutations = [var[1:-1] if '`' in var else var for var in mutations]
            df = self.df_all_dates_all_voc[['location_ID', 'mutations', 'number_sequences']][
                self.df_all_dates_all_voc['date'].isin(dates) & self.df_all_dates_all_voc['mutations']
                    .isin(mutations)].groupby(['location_ID', 'mutations']).sum().reset_index()
            column_of_interest = 'number_sequences'
        elif method == 'Mutation Proportion':
            # df:   location | number_sequences | mutations_proportion
            # mutations_proportion = number of seq with selected voc / number all seq by lab; number_seq = number of all
            df = self.get_mutation_proportion_df(dates, mutations)
            column_of_interest = 'mutation_proportion'
        elif method == 'Increase':
            #  df: date | mutations | number_sequences
            df = self.get_increase_df(dates, mutations)
            # replace negative slope by 0 (cannot be shown)
            df['slope'] = df['slope'].fillna(0)
            num = df._get_numeric_data()
            num[num < 0] = 0
            column_of_interest = 'slope'

        df = self.add_location_info_to_df(df)
        return df, column_of_interest

    def get_most_frequent_map_df(self, world_map_df, column_of_interest, nth):
        def _circle_size(v, max_val, no_cats=29, basis_size=1):
            if v == 0:
                return 0
            delta = max_val / no_cats
            result = int(v/delta)  + basis_size
            return result
        # df: location | mutations | number_sequences
        if world_map_df.empty:
            world_map_df = pd.DataFrame(data=[['51', '10', "", 10115, 'no_mutation', 0, 0]],
                                        columns=["lat", "lon", "location", "location_ID", "mutations",
                                                 column_of_interest, 'scaled_column'])
        for i in range(1, nth+1):
            df_nth = self.get_nth_mutation_per_region(world_map_df, column_of_interest, i)
            if i == 1:
                df_nth_combined = df_nth
            else:
                df_nth_combined = pd.concat([df_nth_combined, df_nth])
        df = pd.DataFrame(data=[['', 0]], columns=["location",  "location_ID", "lat", "lon", column_of_interest]) \
            if df_nth_combined.empty else df_nth_combined
        max_val = df['number_sequences'].max()
        df['scaled_column'] = df[column_of_interest].apply(lambda x: _circle_size(x, max_val))
        df['mutations'] = df['mutations'].map(voc_label_dict).fillna(df['mutations'])
        return df

    def create_mutation_proportion_map(self, df, shown_hover_data):
        fig = px.scatter_mapbox(df,
                                lat="lat",
                                lon="lon",
                                color='mutation_proportion',
                                size='scaled_column',
                                color_continuous_scale=px.colors.sequential.Bluered,
                                range_color=(0, 100),
                                hover_name="location",
                                hover_data=shown_hover_data,
                                center=dict(lat=51.5, lon=10.5),
                                zoom=2,
                                size_max=30,
                                opacity=0.5,
                                labels={el:el.replace("_", " ").title() for el in df.columns})
        fig.update_layout(coloraxis_colorbar=dict(title="% mutations"),
                          margin={"r": 0, "t": 0, "l": 0, "b": 0},
                          legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01),
                          mapbox_style="carto-positron")
        return fig

    def create_map_fig(self, df, shown_hover_data, color_column, size_column):
        fig = px.scatter_mapbox(df,
                                lat="lat",
                                lon="lon",
                                color=color_column,
                                size=size_column,
                                color_discrete_map=self.color_dict,
                                hover_name="location",
                                hover_data=shown_hover_data,
                                center=dict(lat=51.5, lon=10.5),
                                zoom=2,
                                size_max=30,
                                opacity=0.5,
                                labels={el:el.replace("_", " ").title() for el in df.columns})
        fig.update_layout(
            margin={"r": 0, "t": 0, "l": 0, "b": 0},
            legend=dict(yanchor="top", y=0.99, xanchor="left", x=0.01),
            mapbox_style="carto-positron",
        )
        #fig.update_traces(visible='legendonly')
        return fig

    def get_world_map(self, mutations, method, dates, mode, nth):
        """
        :param mutations: list of str mutations (from dropdown left menu)
        :param method: 'Frequency' or 'Increase' (from dropdown left menu)
        :param interval: int size time interval (input field left menu)
        :param date: selected date (date slider)
        :return fig: map with mutations per location
        """
        def _circle_size(v, max_val, no_cats=29, basis_size=1):
            if v == 0:
                return 0
            delta = max_val / no_cats
            result = int(v/delta)  + basis_size
            return result

        def _add_zero(v):
            v = str(v)
            if len(v) == 4:
                return "0"+v
            return v

        if mode == 'absolute frequencies':
            df, column_of_interest = self.get_world_map_df(method, mutations, dates)
            df = self.drop_rows_by_value(df, 0, column_of_interest)
            if method == 'Mutation Proportion':
                if df.empty:
                    df = pd.DataFrame(data=[['51', '10', "", 10115, 'no_mutation', 0, 0, 0]],
                                    columns=["lat", "lon", "location", "location_ID", "mutations", "number_sequences",
                                             'scaled_column', column_of_interest])
                # size list of no_cats different bubble sizes
                max_val = df["number_sequences"].max()
                df['scaled_column'] = df["number_sequences"].apply(lambda x: _circle_size(x, max_val))
                shown_hover_data = {"number_sequences":True, column_of_interest: ":.2f", 'location': False,
                                    'location_ID': False, 'lat': False, 'lon': False,
                                    "scaled_column":False}
                fig = self.create_mutation_proportion_map(df, shown_hover_data)

            elif method == 'Frequency':
                if df.empty:
                    df = pd.DataFrame(data=[['51', '10', "", 10115, 'no_mutation', 0, 0]],
                                    columns=["lat", "lon", "location", "location_ID", "mutations",
                                             column_of_interest, "scaled_column"])
                max_val = df['number_sequences'].max()
                df['scaled_column'] = df[column_of_interest].apply(lambda x: _circle_size(x, max_val))
                df['mutations'] = df['mutations'].map(voc_label_dict).fillna(df['mutations'])
                shown_hover_data = {'mutations': True, column_of_interest: True, 'location': False, 'location_ID': False,
                                    'lat': False, 'lon': False, 'scaled_column':False}
                fig = self.create_map_fig(df, shown_hover_data, color_column="mutations", size_column='scaled_column')

            elif method == 'Increase':
                if df.empty:
                    df = pd.DataFrame(data=[[51, 10, "", 10115, 'no_mutation', 0]],
                                    columns=["lat", "lon", "location", "location_ID", "mutations", column_of_interest])
                shown_hover_data = {'mutations': True, column_of_interest: True, 'location': False, 'location_ID': False,
                                    'lat': False, 'lon': False}
                fig = self.create_map_fig(df, shown_hover_data, color_column="mutations", size_column=column_of_interest)

        elif mode == 'n-th most frequent mutation':
            # df: location | mutations | number_sequences
            world_map_df, column_of_interest = self.get_world_map_df("Frequency", mutations, dates)
            df = self.get_most_frequent_map_df(world_map_df, column_of_interest, nth)
            shown_hover_data = {'mutations': True, column_of_interest: True, 'location': False, 'location_ID': False,
                                'lat': False, 'lon': False, "scaled_column":False}
            fig = self.create_map_fig(df, shown_hover_data, color_column="mutations", size_column='scaled_column')
        return fig

    def get_params_table(self, mutations, method, dates, interval, mode, nth, date_slider, text_dict):
        # date and interval
        if interval is None:
            interval = 0
        second_date = DateSlider.unix_to_date(dates[1])
        if DateSlider.get_date_x_days_before(second_date, interval) < date_slider.min_date:
            interval = (second_date - date_slider.min_date).days + 1
        # text in info box:
        all_dates = [d for d in [second_date - timedelta(days=x) for x in reversed(range(interval))]]
        sum_selected_var_seq = self.get_number_sequences_per_interval(all_dates, mutations)
        sum_wildtype_seq = self.get_number_sequences_per_interval(all_dates, ["wildtype"])
        mutations = [var[1:-1] if '`' in var else var for var in mutations]
        mutations = [voc_label_dict[var] if var in voc_label_dict else var for var in mutations]
        # TODO I excluded "common_X" for now in the map, because this leads to underrepresentation/misinterpretation.
        # (Only cases with ALL these mutations are shown)
        mutations = [el for el in mutations if "common" not in str(el)]
        variant_str = ', '.join(mutations)
        table_md = text_dict['param_table'] % (variant_str, method, mode, str(second_date), str(interval))
        if mode == 'n-th most frequent mutation':
            table_md = table_md + text_dict['param_table2'] % (nth)
        table_md = table_md + "\n" + text_dict["param_table_seq"] % (str(sum_selected_var_seq), str(sum_wildtype_seq))
        # text = text_dict["info_text"] % (variant_str, method, str(sum_wildtype_seq), str(sum_selected_var_seq), str(second_date.date()), str(interval))
        return table_md


class DateSlider:
    def __init__(self, global_metadata):
        min_date, max_date = global_metadata["date"].min(), global_metadata["date"].max()
        ## self.date_list, self.min, self.max date type: 'datetime.date' (YYYY, M, D)
        self.min_date= min_date.date() #date.fromisoformat('2021-01-25')
        self.max_date = max_date.date()
        # if not min_date.date() < self.min_date:
        #     self.min_date = min_date.date()
        self.date_list = [self.max_date - timedelta(days=x) for x in
                          reversed(range((self.max_date - self.min_date).days + 1))]

    @staticmethod
    def unix_time_millis(dt):
        """ Convert datetime to unix timestamp
        :param dt: datetime.date e.g. datetime.date(2021, 3, 7)
        :return: int, e.g. 1615071600"""
        return int(time.mktime(dt.timetuple()))

    @staticmethod
    def unix_to_date(unix):
        """ Convert unix timestamp to date
        :param unix: int e.g. 1615071600
        :return: datetime.date, e.g.(2021, 3, 7) """
        return date.fromtimestamp(unix)

    @staticmethod
    def get_date_x_days_before(date, interval=7):
        return date - timedelta(days=interval)

    @staticmethod
    def get_days_between_date(first_date, second_date):
        return (second_date - first_date).days

    def get_date_list_by_range(self):
        """
        :return date_list: list of dates,
        """
        date_range = range((self.max_date - self.min_date).days)
        date_list = [self.max_date - timedelta(days=x) for x in date_range]
        return date_list

    def get_marks_date_range(self, n=4):
        """
            :returns n marks for labeling
        """
        marks = {}
        nth = int(len(self.date_list) / n) + 1
        for i, date in enumerate(self.date_list):
            if i % nth == 0:
                marks[self.unix_time_millis(date)] = date.strftime('%Y-%m-%d')
        marks[self.unix_time_millis(self.date_list[-1])] = self.date_list[-1].strftime('%Y-%m-%d')
        return marks


