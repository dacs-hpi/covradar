import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from shared.residue_converter.position_converter import aa2na_genome, na_gene_to_genome, na2aa_gene, na_genome_to_gene,\
    na2aa_genome, aa2na_gene
from shared.residue_converter.data_loader import get_reference, get_aa_dict
from shared.residue_converter.nt_aa_translation import get_codon, translate_nt_to_aa
from dash.dependencies import Input, Output, State



def register_callbacks(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['right_menu']
    msa_first = df_dict['msa_first']
    first_msa = df_dict['first_msa']

    # position converter collapse button
    @app.callback(
        Output("position_converter_collapse", "is_open"),
        [Input("position_converter_collapse_button", "n_clicks")],
        [State("position_converter_collapse", "is_open")],
    )
    def toggle_collapse(n, is_open):
        if n:
            return not is_open
        return is_open

    # imprint
    @app.callback(
    Output("imprint", "is_open"),
    [Input("imprint_open", "n_clicks"), Input("imprint_close", "n_clicks")],
    [State("imprint", "is_open")],
    )
    def toggle_imprint(n1, n2, is_open):
        if n1 or n2:
            return not is_open
        return is_open

    # gisaid acknowledgement
    @app.callback(
    Output("acknowledgement", "is_open"),
    [Input("acknowledgement_open", "n_clicks"), Input("acknowledgement_close", "n_clicks")],
    [State("acknowledgement", "is_open")],
    )
    def toggle_acknowledgement(n1, n2, is_open):
        if n1 or n2:
            return not is_open
        return is_open

    @app.callback(
        [Output('converter_output_msa', 'children'), Output('converter_output_firstcase', 'children'),
        Output('converter_output_genome', 'children'), Output('converter_output_aa', 'children'),
        Output('converter_reference_nt', 'children'), Output('converter_reference_aa', 'children'), ],
        [Input('converter_dropdown', 'value'), Input('converter_input', 'value')])
    def update_data_table(dropdown, position):
        msa, firstcase, genome, aa, reference_nt, reference_aa = "", "", "", "", "", ""
        if isinstance(position, int):
            if dropdown == "MSA":
                if position in msa_first:
                    firstcase = msa_first[position]
                    firstcase_wo_gap = int(firstcase.split(".")[0])
                    genome = na_gene_to_genome(firstcase_wo_gap)
                    aa = na2aa_gene(firstcase_wo_gap)
                    reference_nt, reference_aa = get_ref_nt_aa(genome, aa)
                else:
                    spike_len = str(max(msa_first))
                    firstcase = "MSA position outside range 1-"+spike_len
                    genome = "MSA position outside range 1-"+spike_len
                    aa = "MSA position outside range 1-"+spike_len
                    reference_nt = "MSA position outside range 1-"+spike_len
                    reference_aa = "MSA position outside range 1-"+spike_len
                msa = position

            elif dropdown == "Spike":
                if str(position) in first_msa:
                    msa = first_msa[str(position)]
                    genome = na_gene_to_genome(position)
                    aa = na2aa_gene(position)
                    reference_nt, reference_aa = get_ref_nt_aa(genome, aa)
                else:
                    msa = "Spike position outside range 1-3822"
                    genome = "Spike position outside range 1-3822"
                    aa = "Spike position outside range 1-3822"
                    reference_nt = "Spike position outside range 1-3822"
                    reference_aa = "Spike position outside range 1-3822"
                firstcase = position

            elif dropdown == "Genome":
                if position >= 21563 and position <= 25384:
                    firstcase = na_genome_to_gene(position)
                    msa = first_msa[str(firstcase)]
                    aa = na2aa_genome(position)
                    reference_nt, reference_aa = get_ref_nt_aa(position, aa)
                else:
                    firstcase = "Genome position outside range 21563-25384"
                    msa = "Genome position outside range 21563-25384"
                    aa = "Genome position outside range 21563-25384"
                    reference_nt = "Genome position outside range 21563-25384"
                    reference_aa = "Genome position outside range 21563-25384"
                genome = position
                
            elif dropdown == "Codon":
                if position >= 1 and position <= 1274:
                    f_split = aa2na_gene(position)
                    f_split = [str(f_split[0]), str(f_split[1])]
                    firstcase = " - ".join([f_split[0], f_split[1]])
                    msa = str(first_msa[f_split[0]]) + " - " + str(first_msa[f_split[1]])
                    g_split = aa2na_genome(position, gene="S")
                    genome = " - ".join([str(g_split[0]), str(g_split[1])])
                    reference_nt = get_codon(g_split[0], get_reference())
                    reference_aa = get_aa_dict()[reference_nt]
                else:
                    firstcase = "Codon position outside range 1-1274"
                    msa = "Codon position outside range 1-1274"
                    genome = "Codon position outside range 1-1274"
                    reference_nt = "Codon position outside range 1-1274"
                    reference_aa = "Codon position outside range 1-1274"
                aa = position

        return msa, firstcase, genome, aa, reference_nt, reference_aa


def get_ref_nt_aa(reference_nt_position, aa):
    reference_nt = get_reference()[reference_nt_position - 1:reference_nt_position]
    codon_start = aa2na_genome(aa, gene="S")[0]
    reference_codon = get_codon(codon_start, get_reference())
    reference_aa = get_aa_dict()[reference_codon]
    return reference_nt, reference_aa
