from functools import lru_cache
import math
import datetime
import time
import plotly.express as px

from frontend.app.shared.utils import date_to_weekstr, filter_lineages, weekstr_to_datetime
from shared.residue_converter.data_loader import get_reference, get_aa_dict
from shared.residue_converter.nt_aa_translation import get_codon, translate_nt_to_aa, translate_multiple_nt_to_aa, translate_nt_to_aa_pattern
from shared.residue_converter.position_converter import aa2na_genome, na_gene_to_genome, na2aa_gene
import numpy as np
import pandas as pd


class MutationTimePlot:
    def __init__(self, df_dict, country,
        position, #position here is first case position
        origin,
        hosts,
        lineages,
        db_connection,
        kind='nt'): # kind: 'nt' or 'aa'

        self.country = country
        self.position = position
        self.kind = kind
        self.consensus = df_dict["consensus"]
        self.global_position=df_dict["global_position"]
        self.global_metadata= self.apply_filter_on_metadata(df_dict["global_metadata"], origin, hosts, lineages)
        #self.global_SNP = df_dict["global_SNP"]
        self.db_con = db_connection
        self.msa_df = self.global_position[['msa']]
        # [0.0|0.3|0.7] indicator for position of nt within triplet of aa
        self.codon_pos_indicator = round((self.position / 3) % math.floor(self.position / 3), 1) if self.position >= 3 \
            else round((self.position / 3), 1)
        self.codon_msa_positions, self.codon_pos_list = self.get_msa_codon_positions(self.position, self.codon_pos_indicator)
        self.msa_position, self.REF, self.codon = self.get_first_case_and_msa_position()

    @staticmethod
    @lru_cache(maxsize=128)
    def get_ref_nt_aa(reference_nt_position, aa):
        reference_nt = get_reference()[reference_nt_position - 1:reference_nt_position]
        codon_start = aa2na_genome(aa, gene="S")[0]
        reference_codon = get_codon(codon_start, get_reference())
        reference_aa = get_aa_dict()[reference_codon]
        return reference_nt, reference_aa, reference_codon, codon_start

    def get_msa_codon_positions(self, position, codon_pos_indicator):
        switcher_pos1 = {
            0.3: position,
            0.7: position - 1,
            0.0: position - 2
        }
        switcher_pos2 = {
            0.3: position + 1,
            0.7: position,
            0.0: position - 1
        }
        switcher_pos3 = {
            0.3: position + 2,
            0.7: position + 1,
            0.0: position
        }
        codon_pos_list = [str(switcher_pos1.get(codon_pos_indicator)), str(switcher_pos2.get(codon_pos_indicator)),
                          str(switcher_pos3.get(codon_pos_indicator))]
        codon_msa_positions = self.global_position[self.global_position.first_case.isin(codon_pos_list)]
        codon_msa_positions = codon_msa_positions['msa'].reset_index(drop=True)
        return codon_msa_positions, codon_pos_list

    def apply_filter_on_metadata(self, df_meta, origin, hosts, lineages):
        # country condition
        if self.country != "Global":
            df_meta = df_meta[df_meta.country==self.country]
        # origin condition
        if origin != 'all origins':
            df_meta = df_meta[df_meta.origin==origin]
        # hosts condition
        if hosts != "all":
            if isinstance(hosts, str): # when only 1 host selected
                hosts = [hosts]
            df_meta = df_meta[df_meta.host.isin(hosts)]
        # lineage condition
        if lineages != "all":
            if isinstance(lineages, str): # when only 1 lineage selected
                lineages = [lineages]
            df_meta = df_meta[filter_lineages(df_meta.lineage.to_list(), lineages)]
        return df_meta

    def combine_mutations_of_same_codon(self, strain_ids, positions, ALTs):
        index = 0
        indices_to_delete = []  # list of non-last mutations within codon
        aa_alt = []  # resulting aa's considering single or multiple mutations

        indices = np.where(self.codon_msa_positions.values == positions.values[:, None])
        for i, (strain_id, pos, ALT) in enumerate(zip(strain_ids, positions, ALTs)):
            if index == 0:
                mutations_same_codon = [[pos, ALT]]
                single_mutation = True
            else:
                if strain_id == previous_strain_id:  # mutation belongs to the same codon
                    mutations_same_codon.append([pos, ALT])
                    single_mutation = False
                    indices_to_delete.append(index - 1)
                else:  # mutation belongs to another codon
                    if (single_mutation):
                        aa_alt.append(
                            self.apply_mutation_to_codon(mutations_same_codon[0][0],
                                                         mutations_same_codon[0][1], indices[1][i])[2])
                        mutations_same_codon = [[pos, ALT]]
                    else:
                        aa_alt.append(self.apply_multiple_mutations_to_codon(mutations_same_codon)[2])
                        mutations_same_codon = [[pos, ALT]]
                        single_mutation = True
            previous_strain_id = strain_id
            index += 1
        if index != 0:  # append last entry, when df was not empty
            if (single_mutation):
                aa_alt.append(self.apply_mutation_to_codon(pos, ALT)[2])
            else:
                mutations_same_codon.append([pos, ALT])
                aa_alt.append(self.apply_multiple_mutations_to_codon(mutations_same_codon)[2])
        return aa_alt, indices_to_delete

    # @app_line_profiler
    def apply_mutation_to_codon(self, msa_position, ALT, idx=None):
        if idx is None:
            codon_index = np.where(self.codon_msa_positions == msa_position)[0][0]
        else:
            codon_index = idx
        first_case_position = self.codon_pos_list[codon_index]
        genome = na_gene_to_genome(int(first_case_position))
        aa = na2aa_gene(int(first_case_position))
        reference_nt, reference_aa, reference_codon, codon_start = self.get_ref_nt_aa(genome, aa)
        nt_mutation_pattern = "%s%s%s" % (reference_nt, genome, ALT)

        aa_pattern = translate_nt_to_aa_pattern(nt_mutation_pattern, gene="S")
        # aa_mut = translate_nt_to_aa(nt_mutation_pattern=nt_mutation_pattern, gene="S")
        # aa_pattern = aa_mut.aa_mutation_pattern
        ref_aa = aa_pattern[0]
        codon = aa_pattern[1:-1]
        mut_aa = aa_pattern[-1]
        return str(reference_aa), codon, str(mut_aa)

    
    def apply_multiple_mutations_to_codon(self, mutations):
        multiple_nt_mutation_patterns = []
        for mut in mutations:
            codon_index = np.where(self.codon_msa_positions == mut[0])[0][0]
            first_case_position = self.codon_pos_list[codon_index]
            ALT = mut[1]
            aa = na2aa_gene(int(first_case_position))
            genome = na_gene_to_genome(int(first_case_position))
            reference_nt, reference_aa, reference_codon, codon_start = self.get_ref_nt_aa(genome, aa)
            nt_mutation_pattern = "%s%s%s" % (reference_nt, genome, ALT)
            multiple_nt_mutation_patterns.append(nt_mutation_pattern)
        aa_pattern = translate_multiple_nt_to_aa(multiple_nt_mutation_patterns=multiple_nt_mutation_patterns, gene="S")
        ref_aa = aa_pattern[0]
        codon = aa_pattern[1:-1]
        mut_aa = aa_pattern[-1]
        return str(ref_aa), codon, str(mut_aa)

    def get_global_meta_df(self, df_mut):
        df_meta = self.global_metadata[["date", "country"]]

        df_meta = pd.DataFrame(df_meta.groupby('date').size())  # count() does not work unless there is a column with all 1
        df_meta['date'] = [df_meta.index[i] for i in np.arange(len(df_meta))]
        
        df_meta["date_from_cw"] = df_meta["date"].dt.to_period("W").dt.start_time + pd.DateOffset(weeks=1)


        df_meta = df_meta.set_index('date_from_cw')
        # this try/except block is a hack that catches randomly appearing errors of data with wrong type, unclear why this is working
        try:
            df_meta = df_meta.resample('W').sum()
        except TypeError:
            df_meta = df_meta.resample('W').sum()

        df_meta['date_from_cw'] = [df_meta.index[i] for i in np.arange(len(df_meta))]

        df_meta['sum_mut'] = [df_mut.loc[df_mut['date_from_cw'] == i, 'count'].sum() for i in
                              df_meta['date_from_cw']]
        df_meta['count'] = df_meta[0] - df_meta['sum_mut']
        return df_meta
    
    def get_country_meta_df(self):
        df_meta=self.global_metadata[["date", "country"]]


        df_meta["date_from_cw"] = df_meta["date"].dt.to_period("W").dt.start_time + pd.DateOffset(weeks=1)

        df_meta = df_meta.set_index('date_from_cw')
        # this try/except block is a hack that catches randomly appearing errors of data with wrong type, unclear why this is working
        try:
            df_meta = df_meta.groupby('country').resample('W').count()
        except TypeError:
            df_meta = df_meta.groupby('country').resample('W').count()
        df_meta['date_from_cw'] = [df_meta.index[i][1] for i in np.arange(len(df_meta))]
        return df_meta

    def create_global_mut_df(self, df_global_SNP):
        df_mut = pd.DataFrame()
        df_mut['date_from_cw'] = pd.to_datetime(df_global_SNP['date_from_cw'])
        df_mut[self.kind] = df_global_SNP['ALT']
        df_mut['count'] = 1
        df_mut = df_mut.set_index('date_from_cw')
        df_mut = df_mut.groupby(self.kind).resample('W').sum()
        df_mut['date_from_cw'] = [df_mut.index[i][1] for i in np.arange(len(df_mut))]
        df_mut[self.kind] = [df_mut.index[i][0] for i in np.arange(len(df_mut))]
        return df_mut

    def create_global_SNP_df(self, msa_position):

        if self.kind == 'aa':
            df_global_SNP = pd.read_sql(f"""SELECT * FROM global_SNP 
                                            WHERE strain_id IN ({str(self.global_metadata['strain_id'].tolist())[1:-1]})
                                            AND position IN ({str(self.codon_msa_positions.tolist())[1:-1]})"""
                                            , con=self.db_con)
            
            # falsch
            aa_alt, indices_to_delete = self.combine_mutations_of_same_codon(df_global_SNP["strain_id"],
                                                                             df_global_SNP['position'],
                                                                             df_global_SNP['ALT'])
            df_global_SNP = df_global_SNP.drop(indices_to_delete)
            df_global_SNP['ALT'] = aa_alt
            df_global_SNP.set_index("strain_id", inplace=True)
        else:
            # request all mutations at a single nt position
            df_global_SNP = pd.read_sql(f"""SELECT * FROM global_SNP 
                                            WHERE strain_id IN ({str(self.global_metadata['strain_id'].tolist())[1:-1]})
                                            AND position = {msa_position}"""
                                            , con=self.db_con, index_col="strain_id")

            # replace degenerated bases with the actual nt
        if self.global_metadata.index.name != "strain_id" :
            self.global_metadata.set_index("strain_id", inplace = True)
            
        df_global_SNP = df_global_SNP.join(self.global_metadata, how = "inner")
        #self.global_metadata.index = range(len(self.global_metadata.index))
        self.global_metadata.reset_index(inplace=True)
        df_global_SNP.reset_index(inplace=True)
        df_global_SNP['calendar_week'] = df_global_SNP['date'].apply(
            lambda x: str(x.isocalendar()[0]) + 'W' + str("%02d" % x.isocalendar()[1]))
        df_global_SNP['date_from_cw'] = df_global_SNP["date"].dt.to_period("W").apply(lambda r: r.start_time) + pd.DateOffset(weeks=1)
        return df_global_SNP[["strain_id", "country", "date", "position", "REF_allele", "ALT", "calendar_week", "date_from_cw"]]

    def get_global_df(self, df_mut, df_meta):
        df_ref = df_meta[['count', 'date_from_cw']].copy()
        df_ref[self.kind] = self.REF

        # remove entries in df_mut where the REF aa needed to be reinserted
        # and add the counts to the respective entries in df_ref
        if self.kind == 'aa':
            grouped = df_mut.groupby(df_mut.aa)
            try:
                df_unreal_muts = grouped.get_group(self.REF)
            except:
                df_unreal_muts = pd.DataFrame(columns=df_mut.columns)
            df_same_date = df_ref[df_ref['date_from_cw'].isin(df_unreal_muts.date_from_cw.values)]
            df_mut.drop(df_mut[df_mut.aa == self.REF].index, inplace=True)
            for row in df_same_date.itertuples():
                date = row.date_from_cw
                df_ref.loc[df_ref.date_from_cw == date, 'count'] = row.count + df_unreal_muts.loc[
                    df_unreal_muts['date_from_cw'] == date]['count'][0]

        df_bar = pd.concat([df_mut, df_ref])
        df_bar.reset_index(drop=True, inplace=True)
        return df_bar

    def create_country_metadata_SNP_df(self, msa_position):
        strain_ids = self.global_metadata.strain_id[self.global_metadata.country == self.country].tolist() # select current 


        #df = pd.merge(self.global_metadata, df, how="inner", on="strain_id")
        # request all mutations within aa codon
        if self.kind == 'aa':
            start = self.codon_msa_positions[0]
            end = self.codon_msa_positions[2]
            df = pd.read_sql(f"""SELECT * FROM global_SNP 
                                 WHERE position BETWEEN {start} AND {end}
                                 AND strain_id IN ({str(strain_ids)[1:-1]})"""
                                , con=self.db_con)
            
            aa_alt, indices_to_delete = self.combine_mutations_of_same_codon(df['strain_id'], df['position'], df['ALT'])
            df = df.drop(indices_to_delete)
            df['ALT'] = aa_alt
            df.set_index("strain_id", inplace=True)
            # replace 'REF' with the apparent aa
        else:
            df = pd.read_sql(f"""SELECT * FROM global_SNP 
                                 WHERE position = {msa_position} AND strain_id IN ({str(strain_ids)[1:-1]})
                                 """
                                , con=self.db_con, index_col="strain_id")#[["strain_id", "position", "REF_allele", "ALT"]]
            #df = df[(df.position == msa_position) & (df.country == self.country)][
            #    ["strain","strain_id", "country", "date", "position", "REF_allele", "ALT"]]
            
        if self.global_metadata.index.name != "strain_id" :
            self.global_metadata.set_index("strain_id", inplace = True)

        df = df.join(self.global_metadata, how = "inner")

        #self.global_metadata.index = range(len(self.global_metadata.index))
        self.global_metadata.reset_index(inplace=True)
        df.reset_index(inplace=True)

        return df[["strain","strain_id", "country", "date", "position", "REF_allele", "ALT"]]

    def create_df_mut_country(self, df):
        df_mut = pd.DataFrame()
        df_mut['date_from_cw'] = pd.to_datetime(df['date_from_cw'])
        df_mut[self.kind] = df['ALT']
        df_mut['count'] = 1
        df_mut = df_mut.set_index('date_from_cw')
        df_mut = df_mut.groupby(self.kind).resample('W').sum()
        df_mut['date_from_cw'] = [df_mut.index[i][1] for i in np.arange(len(df_mut))]
        df_mut[self.kind] = [df_mut.index[i][0] for i in np.arange(len(df_mut))]
        return df_mut

    def get_mutation_time_plot_country_df(self, df, df_meta):
        
        # if no mutation at this position, only plot ref allele
        if df.empty:
            df_bar = df_meta[['country', 'date_from_cw']]
            df_bar = df_bar.rename(columns={'country': 'count'})
            df_bar[self.kind] = self.REF
        else:
            with open("test.txt", "a") as f:
                f.write(f'{df["date"].dtype}')
            df['date_from_cw'] = df['date'].dt.to_period("W").dt.start_time + pd.DateOffset(weeks=1)
            df_mut = self.create_df_mut_country(df)

            ######get total sequence number from meta table, not from consensus table
            df_meta['sum_mut'] = [df_mut.loc[df_mut['date_from_cw'] == i, 'count'].sum() for i in
                                  df_meta['date_from_cw']]
            df_meta['count'] = df_meta['country'] - df_meta['sum_mut']

            ######################
            # df_ref = df_consensus.drop(['number_of_sequences', 'sum_mut'], axis=1)
            df_ref = df_meta[['count', 'date_from_cw']].copy()
            df_ref[self.kind] = self.REF

            # remove entries in df_mut where the REF aa needed to be reinserted
            # and add the counts to the respective entries in df_ref
            if self.kind == 'aa':
                grouped = df_mut.groupby(df_mut.aa)
                try:
                    df_unreal_muts = grouped.get_group(self.REF)
                except:
                    df_unreal_muts = pd.DataFrame(columns=df_mut.columns)
                df_same_date = df_ref[df_ref['date_from_cw'].isin(df_unreal_muts.date_from_cw.values)]
                df_mut.drop(df_mut[df_mut.aa == self.REF].index, inplace=True)
                for row in df_same_date.itertuples():
                    date = row.date_from_cw
                    df_ref.loc[df_ref.date_from_cw == date, 'count'] = row.count + df_unreal_muts.loc[
                        df_unreal_muts['date_from_cw'] == date]['count'][0]

            df_bar = pd.concat([df_mut, df_ref])

        df_bar.reset_index(drop=True, inplace=True)
        return df_bar

    def get_first_case_and_msa_position(self):
        consensus_first_case = self.consensus[(self.consensus.date == 'first-case') &
                                              (self.consensus.region == 'Wuhan')][["consensus"]]
        first_case = [x for x in list(consensus_first_case.values[0][0]) if x != '-']
        REF = first_case[self.position - 1]
        msa_position = int(list(self.global_position[self.global_position.first_case == str(self.position)]['msa'])[0])
        codon = None
        if self.kind == 'aa':
            REF,codon,_ = self.apply_mutation_to_codon(msa_position, REF)
        return msa_position, REF, codon

    def process_df_bar(self, df_bar):
        total_counts = pd.DataFrame(df_bar.groupby(['date_from_cw'])['count'].sum())
        total_counts['date'] = [total_counts.index[i] for i in np.arange(len(total_counts))]
        df_bar['total'] = 0
        for i in np.arange(len(df_bar)):
            df_bar.loc[i, 'total'] = total_counts.loc[
                total_counts['date'] == df_bar['date_from_cw'].iloc[i], 'count'].values

        df_bar['frequency'] = df_bar['count'] / df_bar['total']
        df_bar['frequency'] = df_bar['frequency'].fillna(0)  # some rows have nan because total is 0

        # one case in 2013 caused twisting in plot.  Drop any cases before 2019 November (actually only one case)
        df_bar = df_bar.drop(df_bar[df_bar.date_from_cw < datetime.datetime.fromisoformat('2019-11-01 00:00:00')].index)

        # df_bar = df_bar.rename({'count': 'absolute counts'}, axis=1)
        df_bar = df_bar.rename({'date_from_cw': 'date'}, axis=1)
        return df_bar

    def create_df_for_plot(self):
        if not any([self.global_metadata.empty, self.global_position.empty]): # self.global_SNP.empty,
            if self.country == 'Global':
                df_global_SNP = self.create_global_SNP_df(self.msa_position)
                df_mut = self.create_global_mut_df(df_global_SNP)

                df_meta = self.get_global_meta_df(df_mut)
                df_bar = self.get_global_df(df_mut, df_meta)
            else:
                df_meta=self.get_country_meta_df()
                df = self.create_country_metadata_SNP_df(self.msa_position)
                df_bar = self.get_mutation_time_plot_country_df(df, df_meta)
            df_bar = self.process_df_bar(df_bar)
            return df_bar
        else:
            return None
    
    def create_mutation_time_plot(self, yaxis):
        """
        param yaxis: "count" or "frequency"
        """
        df_bar = self.create_df_for_plot()

        if df_bar is None:
            df_bar = pd.DataFrame({'date': pd.Series(dtype='datetime64[ns]'), yaxis: pd.Series(dtype='int'),
                                   self.kind: pd.Series(dtype='str')})
        # this try/except block is a hack that catches randomly appearing errors of data with wrong type, unclear why this is working
        try:
            fig = px.bar(df_bar[['date', yaxis, self.kind]], x="date", y=yaxis, color=self.kind)
        except ValueError:
            fig = px.bar(df_bar[['date', yaxis, self.kind]], x="date", y=yaxis, color=self.kind)
        # fig = px.bar(df_bar, x="date_from_cw", y="count", color="bp")

        # title_text =  "Distribution of each allele at postion =" + str(position) + " (first case), country =" + str(country) + ", ref_allele (first case) = " + str(REF)
        if self.kind == 'aa':
            title_text = "Distribution of %(kind)s at codon=%(codon)s, due to mutations at positions=%(position)s (first case)<br>with reference-%(kind)s=%(REF)s in country=%(country)s." \
                         % {'kind': self.kind, 'position': self.codon_pos_list, 'codon': self.codon, 'country': self.country, 'REF': self.REF}
        else:
            title_text = "Distribution of %(kind)s at position=%(position)s (first case) with reference-%(kind)s=%(REF)s in country=%(country)s." \
                         % {'kind': self.kind, 'position': self.position, 'country': self.country, 'REF': self.REF}

        fig.update_layout(
            title={
                'text': title_text,
                'y': 0.9,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'},
            font=dict(size=8),
            autosize=True,
            margin=dict(
                l=0,
                r=0,
                pad=0,
            ),
        )
        if self.kind == 'aa':
            return fig, df_bar, self.codon
        else:
            return fig, df_bar
