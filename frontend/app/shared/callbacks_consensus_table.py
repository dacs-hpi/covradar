import pandas as pd
from shared.residue_converter.data_loader import get_reference
from shared.residue_converter.nt_aa_translation import translate_nt_to_aa, Mutation
from shared.residue_converter.position_converter import na_gene_to_genome
# from frontend.app.shared.profiler import cumulative_profiler
from frontend.app.shared.utils import consensustable
from dash.dependencies import Input, Output, State
from dash_extensions.snippets import send_data_frame
import pandas as pd
from frontend.app.shared.cache import cache


def register_callbacks(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['consensus_table']
    first_msa = df_dict['first_msa']

    # frequency plot description modal button
    @app.callback(
        Output("consensus-chart-info-modal", "is_open"),
        [Input("consensus_chart_info_button_page", "n_clicks"), Input("consensus_chart_info_button_left", "n_clicks"), Input("consensus_chart_info_button_close", "n_clicks")],
        [State("consensus-chart-info-modal", "is_open")], prevent_initial_call=True,
    )
    def toggle_collapse(n1, n2, n3, is_open):
        if n1 or n2 or n3:
            return not is_open
        return is_open

    # consensus table description collapse button
    @app.callback(
        Output('consensus_table_collapse', 'is_open'),
        [Input('consensus_table_collapse_button', 'n_clicks')],
        [State('consensus_table_collapse', 'is_open')],
    )
    def toggle_collapse(n_click, is_open):
        if n_click:
            return not is_open
        return is_open

    @app.callback([Output('table2', 'columns'), Output('table2', 'data'),
                   Output('table2', 'style_data_conditional'),
                   Output('consensus_table_print_country', 'children'),
                   Output('consensus_table_print_date', 'children'),
                   Output('consensus_table_print_positions', 'children'),
                   ],
                  [Input('submit_consensus', 'n_clicks'),
                   Input('url', 'pathname')
                  ],
                  [State('consensus-country-dropdown', 'value'),
                   State('consensus-cw-1',
                         'value'), State('consensus-cw-2', 'value'),
                   State('pos_input1', 'value'), State('pos_input2', 'value'),
                   ])
    # @cumulative_profiler(1)
    @cache.memoize()
    def update_output(n_clicks, pathname ,country, cw1, cw2, pos1, pos2):

        pos1_msa = first_msa[str(pos1)]
        pos2_msa = first_msa[str(pos2)] if pos2 == 3822 else first_msa[str(
            pos2 + 1)] - 1  # we want to get the following gaps

        table = consensustable(df_dict, pos1_msa, pos2_msa, cw1, cw2, country)

        # returns just column header "cw" if selection is empty
        if table.empty:
            cols_empty = [{'name': 'cw', 'id': 'cw'}]
            return cols_empty, [], [], country, 'From {} to {}'.format(
                cw1, cw2), 'From {} to {}'.format(pos1, pos2)

        header = list(table.columns.values)  # get table header

        # first two rows are header rows
        header_rows = 2

        reference_sequence = get_reference()  # get reference sequence
        nt_alphabet = ['A', 'C', 'G', 'T']

        # the columns of interests, i.e., containing mutation positions begin in column 2 and
        # appear every second column
        # omitting *_cov columns in range below
        for i in range(2, len(header), 2):
            nt_column_index = table.columns.get_loc(header[i])
            # create new column name based on position-based name, e.g., 665 ->
            # 665_aa
            new_header = str(header[i]) + '_aa'
            table.insert(loc=nt_column_index + 1, column=new_header,
                         value='.')  # insert new column 'new_header'
            # get reference position from header
            reference_position = table.iloc[1, nt_column_index]
            # get reference nucleotide from reference sequence based on reference position
            # translated to genomic position
            if "." not in reference_position: # no insertion position
                reference_nt = reference_sequence[na_gene_to_genome(
                    int(reference_position)) - 1:na_gene_to_genome(int(reference_position))]
            else:
                reference_nt = "-"
            # create mutation patterns in rows not containing ".", i.e.,
            # containing nt mutations
            mutation_patterns = []
            for variant in table[header[i]]:
                if variant in nt_alphabet:
                    # if variant is a nt substitution, translate it
                    if reference_nt != "-":
                        mutation_patterns.append(reference_nt + str(na_gene_to_genome(int(reference_position))) + variant)
                    else:
                        mutation_patterns.append('ins')
                elif variant == "-":
                    # if a deletion, mark as "-"
                    mutation_patterns.append("-")
                else:
                    # if an identity or the header, mark with "."
                    mutation_patterns.append(".")

            # translate nt patterns to aa patterns
            mutations = [translate_nt_to_aa(pattern)
                         if (pattern != '.' and pattern != '-' and pattern != 'ins')
                         else pattern
                         for pattern in mutation_patterns]
            # get aa mutation patterns if mutation occurs
            aas = []
            for m in mutations:
                if isinstance(m, Mutation) and m.aa_changed:
                    # if nt changed and the substitution is not silent
                    aas.append(m.aa_mutation_pattern)
                elif m == "-":
                    # if nt deletion
                    aas.append("-")
                elif m == "ins":
                    aas.append('ins')
                else:
                    # if nt didn't change or the substitution is silent
                    aas.append(".")

            # assume first-case is in the first row after the headers
            if mutations[header_rows] != '-':
                aas[header_rows] = mutations[header_rows].aa_mutation_pattern
            else:
                aas[header_rows] = "-"
            table[new_header] = aas
            # copy msa and reference position
            table.iloc[0:header_rows, nt_column_index +
                       1] = table.iloc[0:header_rows, nt_column_index]

        data_dict = table.iloc[header_rows:, :].to_dict('records')

        # header rows for consensus table column names
        msa_header = [str(v).split('_cov')[0]
                      for v in table.iloc[0, :].values.tolist()]
        msa_header[1] = msa_header[0]
        ref_header = [str(v).split('_cov')[0]
                      for v in table.iloc[1, :].values.tolist()]
        ref_header[1] = ref_header[0]
        cols = [{'name': [msa_header[i], ref_header[i]],
                 'id': str(c)} for i, c in enumerate(table.columns)]

        # update background colors
        style_data_conditional = [
            {
                'if': {
                    'filter_query': '{{{}}} = "A"'.format(c),
                    'column_id': str(c)
                },
                'backgroundColor': 'rgb(80,80,255)',
            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "A"'.format(c),
                    'column_id': str(c) + '_cov'
                },
                'backgroundColor': 'rgb(80,80,255)',
            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "C"'.format(c),
                    'column_id': str(c)
                },
                'backgroundColor': 'rgb(224,0,0)',
            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "C"'.format(c),
                    'column_id': str(c) + '_cov'
                },
                'backgroundColor': 'rgb(224,0,0)',
            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "T"'.format(c),
                    'column_id': str(c)
                },
                'backgroundColor': 'rgb(230,230,0)',
            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "T"'.format(c),
                    'column_id': str(c) + '_cov'
                },
                'backgroundColor': 'rgb(230,230,0)',
            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "G"'.format(c),
                    'column_id': str(c)
                },
                'backgroundColor': 'rgb(0,192,0)',

            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "G"'.format(c),
                    'column_id': str(c) + '_cov'
                },
                'backgroundColor': 'rgb(0,192,0)',

            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "-"'.format(c),
                    'column_id': str(c)
                },
                'backgroundColor': 'lightgrey',

            } for c in table.columns
        ] + [
            {
                'if': {
                    'filter_query': '{{{}}} = "-"'.format(c),
                    'column_id': str(c) + '_cov'
                },
                'backgroundColor': 'lightgrey',

            } for c in table.columns
        ]

        return cols, data_dict, style_data_conditional, country, 'From {} to {}'.format(
            cw1, cw2), 'From {} to {}'.format(pos1, pos2)


    # aa plot download csv button
    @app.callback(
        Output("download-consensus-chart", "data"),
        Input("consensus_chart_download_csv", "n_clicks"),
        [State('consensus-country-dropdown', 'value'),
        State('consensus-cw-1', 'value'),
        State('consensus-cw-2', 'value'),
        State('pos_input1', 'value'), State('pos_input2', 'value'),],
        prevent_initial_call=True,
    )
    def toggle_collapse(n_clicks, country, cw1, cw2, pos1, pos2):
        if n_clicks:
            pos1_msa = first_msa[str(pos1)]
            pos2_msa = first_msa[str(pos2)] if pos2 == 3822 else first_msa[str(pos2 + 1)] - 1  # we want to get the following gaps
            table = consensustable(df_dict, pos1_msa, pos2_msa, cw1, cw2, country)
            header = list(table.columns.values)  # get table header
            header_rows = 2 # first two rows are header rows
            reference_sequence = get_reference()  # get reference sequence
            nt_alphabet = ['A', 'C', 'G', 'T']
            # the columns of interests, i.e., containing mutation positions begin in column 2 and
            # appear every second column
            # omitting *_cov columns in range below
            for i in range(2, len(header), 2):
                nt_column_index = table.columns.get_loc(header[i])
                # create new column name based on position-based name, e.g., 665 ->
                # 665_aa
                new_header = str(header[i]) + '_aa'
                table.insert(loc=nt_column_index + 1, column=new_header,
                             value='.')  # insert new column 'new_header'
                # get reference position from header
                reference_position = table.iloc[1, nt_column_index]
                # get reference nucleotide from reference sequence based on reference position
                # translated to genomic position
                reference_nt = reference_sequence[na_gene_to_genome(
                    int(reference_position)) - 1:na_gene_to_genome(int(reference_position))]
                # create mutation patterns in rows not containing ".", i.e.,
                # containing nt mutations
                mutation_patterns = []
                for variant in table[header[i]]:
                    if variant in nt_alphabet:
                        # if variant is a nt substitution, translate it
                        mutation_patterns.append(reference_nt + str(na_gene_to_genome(int(reference_position))) + variant)
                    elif variant == "-":
                        # if a deletion, mark as "-"
                        mutation_patterns.append("-")
                    else:
                        # if an identity or the header, mark with "."
                        mutation_patterns.append(".")

                # translate nt patterns to aa patterns
                mutations = [translate_nt_to_aa(pattern)
                             if (pattern != '.' and pattern != '-')
                             else pattern
                             for pattern in mutation_patterns]
                # get aa mutation patterns if mutation occurs
                aas = []
                for m in mutations:
                    if isinstance(m, Mutation) and m.aa_changed:
                        # if nt changed and the substitution is not silent
                        aas.append(m.aa_mutation_pattern)
                    elif m == "-":
                        # if nt deletion
                        aas.append("-")
                    else:
                        # if nt didn't change or the substitution is silent
                        aas.append(".")

                # assume first-case is in the first row after the headers
                aas[header_rows] = mutations[header_rows].aa_mutation_pattern
                table[new_header] = aas
                # copy msa and reference position
                table.iloc[0:header_rows, nt_column_index +
                           1] = table.iloc[0:header_rows, nt_column_index]
            filename = 'Consensus_' + country + '_' + cw1 + '-' + cw2 + '_' + 'pos' + str(pos1) + '-' + str(pos2) + '.csv'
            return send_data_frame(table.to_csv, filename)
