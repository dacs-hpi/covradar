import pandas as pd
import sqlalchemy
from time import perf_counter
import multiprocessing as mp
import os
from sqlalchemy import create_engine
# import pyodbc


tables = ["global_statistics", "global_metadata", "global_position", "consensus", "base_count",
          "variants_of_concern", "sequence_voc", "location_coordinates"] # global_SNP",
# pandas normally uses python strings, which have about 50 bytes overhead. this is a catastrophe
stringType = 'string[pyarrow]'
intType = 'int32'

column_dtypes = {
    'base_count': {
        'region': stringType,
        # not a mistake, this is actually a string in the form <year-4-digits>W<week-nr>
        'date': stringType,
        'position': intType,
        'count': intType,
    },
    'consensus': {
        'region': stringType,
        # not a mistake, this is actually a string
        'date': stringType,
        'consensus': stringType,
        'number_of_sequences': intType,
    },
    'global_SNP': {
        'strain_id': intType,
        'position': intType,
        'REF_allele': stringType,
        'ALT': stringType,
    },
    'global_metadata': {
        'strain_id': intType,
        'accession': stringType,
        'authors': stringType,
        'lineage': stringType,
        'city': stringType,
        'country': stringType,
        'country_exposure': stringType,
        'date': 'datetime64',
        'division': stringType,
        'first_public': stringType,
        'host': stringType,
        'location': stringType,
        'origin': stringType,
        'originating_lab': stringType,
        'region': stringType,
        'strain': stringType,
        'submitting_lab': stringType,
        'virus': stringType,
        'location_ID': intType
    },
    'global_position': {
        'first_case': stringType,
        'msa': intType
    },
    'global_statistics': {
        'date': stringType,
        'min_date': stringType,
        'max_date': stringType,
        'number_seq': intType,
        'snps': intType,
        'multiallelic_sites': intType,
        'multiallelic_snp_sites': intType,
    },
    'sequence_voc': {
        'voc_id': intType,
        'strain_id': intType
    },
    'variants_of_concern': {
        'nucleotide': stringType,
        'amino_acid': stringType,
        'msa': stringType,
        'id': intType,
    },
    'location_coordinates': {
        'location_ID': intType,
        'country_ID': stringType,
        'lon': "float32",
        'lat': "float32",
        'name': stringType,
    }
}

needed_columns = {'global_metadata': ['strain_id', "country", 'lineage', "date", "origin", "strain", "virus", "host",
                                      'location_ID'],
                  'location_coordinates': ['location_ID', "name", "lat", "lon"]}


def get_database_connection(database_name):
    # DB configuration
    user = os.environ.get("MYSQL_USER", "root")
    ip = os.environ.get("MYSQL_HOST", "localhost")
    pw = os.environ.get("MYSQL_PW", "secret")
    # conn_str = f"DRIVER={{/usr/lib/x86_64-linux-gnu/odbc/libmyodbc8a.so}};SERVER={ip};UID={user};PWD={pw};DATABASE={database_name}"
    # return pyodbc.connect(conn_str)
    return create_engine(f'mysql+pymysql://{user}:{pw}@{ip}/{database_name}')


class DataFrameLoader():
    def __init__(self, db_name, tables):
        self.db_name = db_name
        self.tables = tables
        self.needed_columns = needed_columns
        self.column_dtypes = column_dtypes

    def load_db_from_sql(self, db_name, table_name):
        start = perf_counter()
        db_connection = get_database_connection(db_name)
        df_dict = {}
        try:
            # we cannot use read_sql_table, since it doesn't allow defining dtypes
            columns = pd.read_sql_query(f"SELECT * FROM {table_name} LIMIT 1;", con=db_connection).columns
            if table_name in self.needed_columns:
                columns = columns.intersection(self.needed_columns[table_name])
                types = {column: self.column_dtypes[table_name][column] for column in columns}
                queried_columns = ', '.join(columns)
            else:
                types = {column: self.column_dtypes[table_name][column] for column in columns}
                queried_columns = '*'
            df = pd.read_sql_query(sql=f"SELECT {queried_columns} FROM {table_name}", con=db_connection, dtype=types)
        # missing table e.g. in test databases
        except sqlalchemy.exc.ProgrammingError:
            print(f"table {table_name} not in database.")
            df = pd.DataFrame()
        print(f'Loading time {table_name}: {(perf_counter() - start)} sec.')
        if not df.empty:
            df_dict[table_name] = df
            return df_dict

    def load_from_sql_db(self):
        pool = mp.Pool(mp.cpu_count())
        dict_list = pool.starmap(self.load_db_from_sql, [(self.db_name, table_name) for table_name in self.tables])
        pool.close()
        pool.terminate()
        pool.join()
        df_dict = {}
        for df_d in dict_list:
            if df_d is not None:
                df_dict.update(df_d)
        return df_dict


def load_all_sql_files(db_name):
    loader = DataFrameLoader(db_name, tables)
    df_dict = loader.load_from_sql_db()
    # test db without table global_position
    # add commonly used first_msa and msa_first dicts
    if "global_position" in df_dict.keys():
        msa = df_dict['global_position']['msa']
        first = df_dict['global_position']['first_case']
        df_dict['msa_first'] = dict(zip(msa, first))
        df_dict['first_msa'] = dict(zip(first, msa))
    return df_dict




