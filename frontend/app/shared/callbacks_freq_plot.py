import pandas as pd

from frontend.app.shared.utils import allele_freq, cw_has_country
from frontend.app.shared.utils_base_freq_plotter import basefreqplotter
from dash.dependencies import Input, Output, State
from frontend.app.shared.cache import cache
from datetime import timedelta
import dash
from dash_extensions.snippets import send_bytes
import zipfile


def register_callbacks(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['baseFrequencyPlot']
    df = df_dict["global_metadata"]
    pos_table = df_dict["global_position"][["msa", "first_case" ]]
    first_msa = df_dict["first_msa"]
    msa_first = df_dict["msa_first"]


    # frequency plot description modal button
    @app.callback(
        Output("freq-plot-info-modal", "is_open"),
        [Input("freq_plot_info_button_page", "n_clicks"), Input("freq_plot_info_button_left", "n_clicks"), Input("freq_plot_info_button_close", "n_clicks")],
        [State("freq-plot-info-modal", "is_open")], prevent_initial_call=True,
    )
    def toggle_collapse(n1, n2, n3, is_open):
        if n1 or n2 or n3:
            return not is_open
        return is_open

    # Filter Buttons for Allele Frequency Plot: country, date range, origin
    @app.callback(
        [Output('allele-freq-plot', 'figure'),
         Output('allele-freq-plot', 'style'),
         Output('freq_plot_no_of_seq', 'children'),
         Output('freq_plot_cons_cov', 'children'),
         Output('freq_seq_filter_print_country', 'children'),
         Output('freq_seq_filter_print_date', 'children'),
         Output('freq_seq_filter_print_origin', 'children'),
         Output('freq_seq_filter_print_no_samples', 'children'),
         Output('freq_seq_filter_print_hosts', 'children'),
         Output('freq_cons_filter_print_country', 'children'),
         Output('freq_cons_filter_print_cw', 'children'),
         Output('freq_cons_filter_print_no_seqs', 'children'),
         ],
        [Input('submit_freqplot', 'n_clicks'),
         Input('url', 'pathname') ],
        [State('plot-country-dropdown', 'value'),
         State('plot-my-date-picker-range', 'start_date'),
         State('plot-my-date-picker-range', 'end_date'),
         State('plot-origin-dropdown', 'value'),
         State('freq-plot-cw', 'value'),
         State('plot-consensus-country-dropdown', 'value'),
         State('plot-host-dropdown', 'value'),
         State('plot-lineage-dropdown', 'value'),
         # values from the variants from the database and user specified input
         State('plot-variants-dropdown', 'value'),
         State('plot-variants-input', 'value'),
         State('plot-rois-dropdown', 'value'),
         ])
    # @cumulative_profiler(1)
    @cache.memoize()
    def update_figure(ncklicks, pathname, country, start_date, end_date, origin, calendarweek,
                      country_consensus, hosts, lineages, variants_db, variants_usr, domains):
        
        if calendarweek == "Wuhan-Hu-01":
            calendarweek = "first-case"

        consensus = df_dict['consensus']
        freq_list, total_seqs, base_cov = \
            allele_freq(df_dict, country, start_date, end_date, origin, calendarweek,
                        country_consensus, hosts, lineages, db_connection)
        if total_seqs == 0:
            fig_style = {'display': 'none'}
        else:
            fig_style = {'display': 'inline'}

        # create the heatmap figure
        fig = basefreqplotter(df_dict['global_position'], df_dict["first_msa"],
                              freq_list, base_cov, variants_db, variants_usr, domains)
        cons_cov = int(consensus[(consensus['date'] == calendarweek) & (consensus['region'] == country_consensus)].reset_index(drop=True)['number_of_sequences'].iloc[0])
        text_seq = text_dict['text_seq'].format(total_seqs)
        text_cov = text_dict['text_cov'].format(cons_cov)

        # printing
        if origin == "all origins":
            origins = ", ".join(set(df.loc[:, "origin"]))
        else:
            origins = origin
        return fig, fig_style, text_seq, text_cov, country, 'From {} to {}'.format(start_date,
                                                                                   end_date), origins, total_seqs, ", ".join(
            hosts), country_consensus, calendarweek, cons_cov

    @app.callback(
        [Output('plot-consensus-country-dropdown', 'options'),
         Output('plot-consensus-country-dropdown', 'value')],
        [Input('freq-plot-cw', 'value'),
         Input('url', 'pathname')],
        [State('plot-consensus-country-dropdown', 'options'),
         State('plot-consensus-country-dropdown', 'value')])
    @cache.memoize()
    def update_filter_options(calendarweek, pathname, options, country):

        if calendarweek == "Wuhan-Hu-01":
            calendarweek = "first-case"

        # disable country options, if there is no consensus sequences for this calendar week
        cw_country, _ = cw_has_country(df_dict)
        for opt in options:
            if opt["label"] not in cw_country[calendarweek]:
                opt["disabled"] = True
            else:
                opt["disabled"] = False
        if country not in cw_country[calendarweek]:
            country = cw_country[calendarweek][0]
        return options, country
    @cache.memoize()
    @app.callback(
        [Output('plot-my-date-picker-range', 'start_date'), Output('plot-my-date-picker-range', 'end_date')],
        [Input('plot-country-dropdown', 'value'),
         Input('url', 'pathname')],
        )
    @cache.memoize()
    def update_filter_options(country, pathname):
        #reset date interval for specific country
        df = df_dict["global_metadata"]
        if country != "Global":
            df = df[df['country'] == country]

        end_date = df.loc[:, "date"].max() - timedelta(days=7)
        start_date = df.loc[:, "date"].max() - timedelta(days=7)

        return start_date, end_date

    @app.callback(
        [Output('plot-country-dropdown', 'options'), Output('plot-country-dropdown', 'value')],
        [Input('plot-origin-dropdown', 'value')],
        [State('plot-country-dropdown', 'options'), State('plot-country-dropdown', 'value')]
    )
    def update_filter_options(origin, options, country):
        # show counries contained in origin
        if origin == "all origins":
            # enable all countries
            for opt in options:
                opt["disabled"] = False
        else:
            # countries is a ArrowStringArray
            global_metadata = df_dict["global_metadata"]
            countries = global_metadata[global_metadata['origin'] == origin]['country'].fillna('').unique()
            for opt in options:
                if opt["label"] == "Global":
                    opt["disabled"] = False
                elif opt["label"] not in countries:
                    opt["disabled"] = True
                else:
                    opt["disabled"] = False
            if country != "Global":
                if country not in countries:
                    country = countries[0]
        return options, country
    #  new: removed: download_freq_plot
    @app.callback(
        [Output('plot-lineage-dropdown', 'value')],
        [Input('plot-lineage-dropdown', 'value')],
        prevent_initial_call=True,
        )
    def update_lineage_options(lineages):
        # remove other lineages when "all" selected
        if not lineages:
            return ["all"]
        elif lineages[-1] == "all":
            return ["all"]
        elif lineages[0] == "all" and len(lineages) > 1:
            return lineages[1:]
        else:
            return dash.no_update

    @app.callback(
        [Output('plot-host-dropdown', 'value')],
        [Input('plot-host-dropdown', 'value')],
        prevent_initial_call=True,
        )
    def update_host_options(hosts):
        # remove other hosts when "all" selected
        if not hosts:
            return ["all"]
        elif hosts[-1] == "all":
            return ["all"]
        elif hosts[0] == "all" and len(hosts) > 1:
            return hosts[1:]
        else:
            return dash.no_update


    @app.callback(
        Output("download-freq-plot", "data"),
        Input("freq_plot_download_csv", "n_clicks"),
        [State('plot-country-dropdown', 'value'),
         State('plot-my-date-picker-range', 'start_date'),
         State('plot-my-date-picker-range', 'end_date'),
         State('plot-origin-dropdown', 'value'),
         State('freq-plot-cw', 'value'),
         State('plot-consensus-country-dropdown', 'value'),
         State('plot-host-dropdown', 'value'),
         State('plot-lineage-dropdown', 'value'),],
        prevent_initial_call=True,
    )
    def download_freq_plot_data(n_clicks, country, start_date, end_date, origin, calendarweek, country_consensus, hosts, lineages):
        # download frequency plot as csv file
        if n_clicks:
            if calendarweek == "Wuhan-Hu-01":
                calendarweek = "first-case"
            pos_dict = df_dict[" global_position"]
            freq_list, total_seqs, base_cov = allele_freq(df_dict, country, start_date, end_date, origin, calendarweek, country_consensus, hosts, lineages)
            # alternative frequencies
            df = pd.DataFrame(
                data={
                    'first_case_position': pos_dict["first_case"],
                    'msa_position': pos_dict["msa"],
                    'alternative_frequency': freq_list,
                    'base_coverage': base_cov
                    })
            df_bytes = df.to_csv(index=False).encode('utf-8')
            df_filename = "alternative_frequencies.csv"
            # parameter settings
            if isinstance(hosts, str): # when only 1 host is selected
                hosts = [hosts]
            if isinstance(lineages, str): # when only 1 lineage is selected
                lineages = [lineages]
            df_params = pd.DataFrame(
                data = {
                    'parameter': ['country', 'start_date', 'end_date', 'origin', 'consensus_country', 'consensus_calendarweek', 'hosts', 'lineages', 'sequences_in_filter'],
                    'value': [country, start_date, end_date, origin, country_consensus, calendarweek, ';'.join(hosts), ';'.join(lineages), str(total_seqs)]
                })
            df_params_bytes = df_params.to_csv(index=False).encode('utf-8')
            df_params_filename = "parameter_settings.csv"
            # create zip
            def write_archive(bytes_io):
                with zipfile.ZipFile(bytes_io, "w") as zf:
                    zf.writestr(df_filename, df_bytes)
                    zf.writestr(df_params_filename, df_params_bytes)
            return send_bytes(write_archive, "alternative_frequency_data.zip")
