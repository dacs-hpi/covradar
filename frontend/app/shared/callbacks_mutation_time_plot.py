from frontend.app.shared.utils_mutation_time_plot import MutationTimePlot
from dash.dependencies import Input, Output, State
from dash_extensions.snippets import send_bytes
from frontend.app.shared.cache import cache
import zipfile
import pandas as pd

def register_callbacks(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['mutation_distribution_plot']
    #mutation over time
    @app.callback(
        Output('nt_mutation_over_time', 'figure'),
        [Input('submit_mut_temporal', 'n_clicks'),
         Input('url', 'pathname')],
        [State('mut-yaxis-type', 'value'),
         State('mut_temporal_country_dropdown', 'value'),
         State("position_base_distribution", "value"),
         State('mut_temporal_origin_dropdown', 'value'),
         State('mut_temporal_host_dropdown', 'value'),
         State('mut_temporal_lineage_dropdown', 'value'),]
    )
    @cache.memoize()
    def update_temporal_nt_fig(n_clicks, pathname, yaxis, country, position, origin, hosts, lineages):
        mut_time_obj = MutationTimePlot(df_dict, country, position, origin, hosts, lineages, db_connection, kind='nt')
        fig = mut_time_obj.create_mutation_time_plot(yaxis)[0]
        return fig
    
    #mutation over time
    @app.callback(
        Output('aa_mutation_over_time', 'figure'),
        [Input('submit_mut_temporal', 'n_clicks'),
         Input('url', 'pathname')],
        [State('mut-yaxis-type', 'value'),
         State('mut_temporal_country_dropdown', 'value'),
         State("position_base_distribution", "value"),
         State('mut_temporal_origin_dropdown', 'value'),
         State('mut_temporal_host_dropdown', 'value'),
         State('mut_temporal_lineage_dropdown', 'value'),]
    )
    @cache.memoize()
    def update_temporal_aa_fig(n_clicks, pathname, yaxis, country, position, origin, hosts, lineages):
        mut_time_obj = MutationTimePlot(df_dict, country, position, origin, hosts, lineages, db_connection, kind='aa')
        fig = mut_time_obj.create_mutation_time_plot(yaxis)[0]
        return fig

    # nt plot description modal button
    @app.callback(
        Output("nt-aa-plot-info-modal", "is_open"),
        [Input("nt_dist_info_button_page", "n_clicks"),
         Input("aa_dist_info_button_page", "n_clicks"),
         Input("nt_aa_plot_info_button_left", "n_clicks"),
         Input("nt_aa_plot_info_button_close", "n_clicks")],
        [State("nt-aa-plot-info-modal", "is_open")], prevent_initial_call=True,
    )
    def toggle_collapse(n1, n2, n3, n4, is_open):
        if n1 or n2 or n3 or n4:
            return not is_open
        return is_open

    @app.callback(
        [Output('mut_temporal_lineage_dropdown', 'value')],
        [Input('mut_temporal_lineage_dropdown', 'value')],
        prevent_initial_call=True,
        )
    def update_lineage_options(lineages):
        # remove other lineages when "all" selected
        if not lineages:
            return ["all"]
        elif lineages[-1] == "all":
            return ["all"]
        elif lineages[0] == "all" and len(lineages) > 1:
            return lineages[1:]
        else:
            return dash.no_update

    @app.callback(
        [Output('mut_temporal_host_dropdown', 'value')],
        [Input('mut_temporal_host_dropdown', 'value')],
        prevent_initial_call=True,
        )
    def update_host_options(hosts):
        # remove other hosts when "all" selected
        if not hosts:
            return ["all"]
        elif hosts[-1] == "all":
            return ["all"]
        elif hosts[0] == "all" and len(hosts) > 1:
            return hosts[1:]
        else:
            return dash.no_update

    # distribution plots download csv button
    @app.callback(
        Output("download-nt-dist", "data"),
        [Input("nt_dist_download_tsv", "n_clicks"),
        Input("aa_dist_download_tsv", "n_clicks"),],
        [State('mut-yaxis-type', 'value'),
        State('mut_temporal_country_dropdown', 'value'),
        State("position_base_distribution", "value"),
        State('mut_temporal_origin_dropdown', 'value'),
        State('mut_temporal_host_dropdown', 'value'),
        State('mut_temporal_lineage_dropdown', 'value'),],
        prevent_initial_call=True,
    )
    def toggle_collapse(n_clicks_nt, n_clicks_aa, yaxis, country, position, origin, hosts, lineages):
        if n_clicks_nt or n_clicks_aa:
            # nt distribution
            mut_time_obj_nt = MutationTimePlot(df_dict, country, position, origin, hosts, lineages, kind='nt')
            df_nt = mut_time_obj_nt.create_df_for_plot().sort_values(by=['date'])
            df_nt = df_nt[df_nt['count'] > 0]
            col_order = ['total', 'date', 'nt', 'count', 'frequency']
            df_nt = df_nt.reindex(columns=col_order)
            df_nt_bytes = df_nt.to_csv(index=False).encode('utf-8')
            df_nt_filename = 'nt' + str(position) + '.csv'
            # aa distribution
            mut_time_obj_aa = MutationTimePlot(df_dict, country, position, origin, hosts, lineages, kind='aa')

            fig, df_aa, codon = mut_time_obj_aa.create_mutation_time_plot(yaxis)
            df_aa = df_aa[df_aa['count'] > 0]
            df_aa = df_aa.sort_values(by=['date'])
            col_order = ['total', 'date', 'aa', 'count', 'frequency']
            df_aa = df_aa.reindex(columns=col_order)
            df_aa_bytes = df_aa.to_csv(index=False).encode('utf-8')
            df_aa_filename = 'codon' + str(codon) + '.csv'
            # parameter settings
            if isinstance(hosts, str): # when only 1 host is selected
                hosts = [hosts]
            if isinstance(lineages, str): # when only 1 lineage is selected
                lineages = [lineages]
            df_params = pd.DataFrame(
                data = {
                    'parameter': ['country', 'position', 'origin', 'hosts', 'lineages'],
                    'value': [country, position, origin, ';'.join(hosts), ';'.join(lineages)]
                })
            df_params_bytes = df_params.to_csv(index=False).encode('utf-8')
            df_params_filename = "parameter_settings.csv"
            # info for aa distribution
            info = 'Note: The amino acid distribution is derived from mutations on all three nucleotides of the respective codon, not only the single selected nucleotide position!'
            info_bytes = str.encode(info)
            info_filename = "important_info.txt"
            # create zip
            def write_archive(bytes_io):
                with zipfile.ZipFile(bytes_io, "w") as zf:
                    zf.writestr(df_nt_filename, df_nt_bytes)
                    zf.writestr(df_aa_filename, df_aa_bytes)
                    zf.writestr(df_params_filename, df_params_bytes)
                    zf.writestr(info_filename, info_bytes)
            return send_bytes(write_archive, "nt_aa_distribution.zip")
