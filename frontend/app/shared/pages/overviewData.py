from datetime import timedelta

import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_table
import pandas as pd
from frontend.app.shared.utils import Header, get_statistics, gisaid_header
from dash_core_components import DatePickerRange, Dropdown, Graph
from dash_html_components import H5, H6, Br, Div, P, Table, Td, Tr, Hr, Img, A
from dash_extensions import Download


def create_layout(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['overview_data']

    all_columns = df_dict["global_metadata"].columns
    columns_dict = [{"name": i, "id": i} for i in all_columns]


    datatable = Div([
        Div([
            H6(
                [text_dict['H6_input_data']],
                className="subtitle padded d-print-none",
                id={"type":"overview_data", "index":"H6_input_data"}
                ),
            ], className="subtitle-container",
            ),
                Div(id="meta_no_seq", children="", className="filter_description d-print-none"),
                Div([
                    # data table filter options
                    # data table
                    dash_table.DataTable(
                        id='table1',
                        columns=columns_dict,
                        style_table={
                            'height': '90%',
                            'minWidth': '100%',
                            'overflow': 'auto',
                        },
                        style_header={  # table header
                            'backgroundColor': '#f2f6f9',
                            'padding-right': '1vw',
                            'padding-left': '1vw',
                        },
                        style_data={  # table cells
                            'whiteSpace': 'normal',
                            'height': 'auto',
                        },
                        style_data_conditional=[  # stripped rows
                            {
                                'if': {'row_index': 'odd'},
                                'backgroundColor': 'rgb(248, 248, 248)'
                            }
                        ],
                        export_format='xlsx',  # download button, xlsx|csv
                        sort_action='native',
                        filter_action='native',
                    )
                ],
                    className="d-print-none metadata_table_block",
                )
            ])

    content =  Div([
        Header(app, lang_dict["header"]),
        Div([
            # data distribution plot
            H6(text_dict['H6_data_dist'], className="subtitle padded", id={"type":"overview_data", "index":"H6_data_dist"}),
            A([
                Img(
                    src=app.get_asset_url("info_icon.png"),
                    className="description-button-page-img"           
                    )
                ], 
                id="data_overview_info_button_page",
                n_clicks=0,
                className="description-button-page-anchor",
                title=text_dict['tooltip_info_button']
                ),
            A([
                Img(
                    src=app.get_asset_url("download_icon.png"),
                    className="description-button-page-img"
                    ),
                Download(id="download-data-overview"),
                ], 
                id="data_overview_download_tsv",
                n_clicks=0,
                className="description-button-page-anchor",
                title=text_dict['tooltip_download_button']
                ),
            Div([
                Graph(
                    id="country_cases_over_time",
                    style={
                        'height': '100%',
                    },
                )
            ],
                className="data_distribution_block",
            ),
        ],
            className="sub_page",
        ),
        #plot description modal
        dbc.Modal(
            [
                dbc.ModalHeader(children=[text_dict["description"]], id="modal_data_overview_header"),
                dbc.ModalBody(children=[
                    P(children=[text_dict["modal_data_overview"]], id="modal_data_overview"),
                    P(children=[text_dict["modal_data_overview_options_header"]], className="description-modal-header", id="modal_data_overview_options_header"),
                    P(children=[text_dict["modal_data_overview_options"]], id="modal_data_overview_options"),
                    # changes to the modal also need to be adjusted in register_callbacks() of frontend/app/shared/callbacks_language_button.py
                    # such that the translation works properly
                    ]
                ),
                dbc.ModalFooter(
                    dbc.Button(children=[text_dict["modal_close"]], id="data_overview_info_button_close", className="button legend")
                ),
            ],
            id="data-overview-info-modal",
            size="lg"
        ),
    ],
        className="page row-height",
        id="overview_data",
    )

    return content


def create_menu_left(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['overview_data']
    all_countries = df_dict['global_metadata']['country'].drop_duplicates().fillna('').tolist()
    # remove None Values
    all_countries = sorted([i for i in all_countries if i])
    all_origins = df_dict['global_metadata']['origin'].drop_duplicates().fillna('').tolist()
    all_hosts = df_dict['global_metadata']['host'].drop_duplicates().fillna('').tolist()
    min_date, max_date = df_dict['global_metadata']['date'].min(), df_dict['global_metadata']['date'].max()

    host_ini = sorted(all_hosts)

    ini_country = "Germany"

    datatable_filter = Div([
                H6(text_dict["H6_input_data"], className="center", id={"type":"overview_data", "index":"H6_input_data"}),
                Hr(),
                Div([
                    Div([text_dict["Div_country"]], className="filter_description", id={"type":"overview_data", "index":"Div_country"}),
                    Dropdown(
                        id='country-dropdown',
                        options=[{'label': 'Global', 'value': 'Global'}] +
                                [{'label': country, 'value':country} for country in all_countries],
                        value=ini_country,
                    ),
                ],
                    className="filter_block"
                ),
                Div([
                    Div([text_dict["Div_origin"]], className="filter_description", id={"type":"overview_data", "index":"Div_origin"}),
                    Dropdown(
                        id='origin-dropdown',
                        options=[{'label': 'all_origins', 'value': 'all origins'}] +
                                [{'label': origin, 'value':origin} for origin in sorted(all_origins)],
                        value='all origins',
                    ),
                ],
                    className="filter_block"
                ),
                Div([
                    Div([text_dict["Div_host"]], className="filter_description", id={"type":"overview_data", "index":"Div_host"}),
                    Dropdown(
                        id='host-dropdown',
                        options=[{'label': host, 'value':host} for host in sorted(all_hosts)],
                        value=host_ini,
                        multi=True,
                    ),
                ],
                    className="filter_block"
                ),
                Div([
                    Div([text_dict["time_interval"]], className="filter_description", id={"type":"overview_data", "index":"time_interval"}),
                    DatePickerRange(
                        id='my-date-picker-range',
                        min_date_allowed=min_date,
                        max_date_allowed=max_date+timedelta(days=1),  # disabled days start at this date
                        initial_visible_month=max_date,
                        start_date=min_date,
                        end_date=max_date,
                        number_of_months_shown = 3,
                        display_format='DD.MM.YY',
                        with_portal=True,
                    ),
                ],
                    className="filter_block"
                ),
            ],
                className="filter_block_bundle"
            )

    content = Div([
        Div([
            Div([
                Div([
                    Div([
                        H6(text_dict["H6_data_dist"], className="center", id={"type":"overview_data", "index":"H6_data_dist"}),
                        A([
                            Img(
                                src=app.get_asset_url("info_icon.png"),
                                className="description-button-left-img"
                                )
                            ], 
                            id="data_overview_info_button_left",
                            n_clicks=0,
                            className="description-button-left-anchor",
                            title=text_dict['tooltip_info_button']
                            ),
                    ], className="subtitle-container",
                    ),
                    Hr(),
                    #gisaid_header(app),
                    Div(["Y Axis:"], className="filter_description"),
                    Dropdown(
                        id='yaxis-type',
                        options=[{'label': name, 'value': value} for name, value in zip(text_dict['div_y_axis_name'], text_dict['div_y_axis_value'])],
                        value='weekly sequences',
                    ),
                ],
                    className="filter_block"
                ),
            ],
                className="filter_block_bundle"
            ),
            #datatable_filter,
        ],
            className="sub-menu-left"
        )
    ],
        className="menu-left no-print"
    )

    return content
