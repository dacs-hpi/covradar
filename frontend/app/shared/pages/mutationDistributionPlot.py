import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash_extensions import Download
from frontend.app.shared.utils import Header, get_country_dropdown_values, gisaid_header, get_lineage_dropdown_values
import pandas as pd

def create_layout(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['mutation_distribution_plot']
    layout =  html.Div([  # page
        Header(app, lang_dict["header"]),
        html.Div([ # page content
            # base distribution plot
            html.Div([
                html.H6(text_dict['H6_dist'], className="subtitle padded", id={"type":"mutation_distribution_plot", "index":"H6_dist"}),
                html.A([
                    html.Img(
                        src=app.get_asset_url("info_icon.png"),
                        className="description-button-page-img"
                        )
                    ], 
                    id="nt_dist_info_button_page",
                    n_clicks=0,
                    className="description-button-page-anchor",
                    title=text_dict['tooltip_info_button']
                    ),
                html.A([
                    html.Img(
                        src=app.get_asset_url("download_icon.png"),
                        className="description-button-page-img"
                        ),
                    Download(id="download-nt-dist"),
                    ], 
                    id="nt_dist_download_tsv",
                    n_clicks=0,
                    className="description-button-page-anchor",
                    title=text_dict['tooltip_download_button']
                    ),
                ], className="subtitle-container",
            ),
            html.Div([
                dcc.Graph(
                    id = "nt_mutation_over_time",
                )
            ],
            className="h-40"
            ),
            # aa distribution plot
            html.Div([
                html.H6(text_dict['H6_AA_dist'], className="subtitle padded", id={"type":"mutation_distribution_plot", "index":"H6_AA_dist"}),
                html.A([
                    html.Img(
                        src=app.get_asset_url("info_icon.png"),
                        className="description-button-page-img"
                        )
                    ], 
                    id="aa_dist_info_button_page",
                    n_clicks=0,
                    className="description-button-page-anchor",
                    title=text_dict['tooltip_info_button']
                    ),
                html.A([
                    html.Img(
                        src=app.get_asset_url("download_icon.png"),
                        className="description-button-page-img"
                        ),
                    Download(id="download-aa-dist"),
                    ], 
                    id="aa_dist_download_tsv",
                    n_clicks=0,
                    className="description-button-page-anchor",
                    title=text_dict['tooltip_download_button']
                    ),
                ], className="subtitle-container",
            ),
            html.Div([
                dcc.Graph(
                    id = "aa_mutation_over_time",
                )
            ],
            className="h-40"
            )
        ], className="sub_page",
        ),  # page content
        #plot description modal
        dbc.Modal(
            [
                dbc.ModalHeader(children=[text_dict["description"]], id="modal_nt_aa_plot_header"),
                dbc.ModalBody(children=[
                    html.P(children=[text_dict["H6_dist"]], className="description-modal-header", id="modal_nt_plot_header"),
                    html.P(children=[text_dict["modal_nt_plot"]], id="modal_nt_plot"),
                    html.P(children=[text_dict["H6_AA_dist"]], className="description-modal-header", id="modal_aa_plot_header"),
                    html.P(children=[text_dict["modal_aa_plot"]], id="modal_aa_plot"),
                    html.P(children=[text_dict["modal_nt_aa_plot_options_header"]], className="description-modal-header", id="modal_nt_aa_plot_options_header"),
                    html.P(children=[text_dict["modal_nt_aa_plot_options"]], id="modal_nt_aa_plot_options"),
                    # changes to the modal also need to be adjusted in register_callbacks() of frontend/app/shared/callbacks_language_button.py
                    # such that the translation works properly
                    ]
                ),
                dbc.ModalFooter(
                    dbc.Button(children=[text_dict["modal_close"]], id="nt_aa_plot_info_button_close", className="button legend")
                ),
            ],
            id="nt-aa-plot-info-modal",
            size="lg"
        ),
    ],
    className="page row-height print-page-break-after",
    id="mutation_plot"
    )  # page
    return layout

def create_menu_left(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['mutation_distribution_plot']
    all_hosts = df_dict["global_metadata"]["host"].unique().tolist()
    all_hosts.insert(0, 'all')
    all_origins = df_dict["global_metadata"]["origin"].unique().tolist()
    all_origins.insert(0, 'all origins')

    lineage_dropdown_values = get_lineage_dropdown_values(df_dict)

    country_dropdown_values = get_country_dropdown_values(df_dict)
    if "Germany" in country_dropdown_values:
        ini_country_base_dist = "Germany"
    else:
        ini_country_base_dist = "Global"

    # initialization base distribution
    first_case = df_dict['global_position'].sort_values('msa', ascending=False)['first_case'].iloc[0]
    len_first_case = int(first_case.split('.')[0])
    ini_pos1 = 1
    ini_pos2 = len_first_case

    ini_host = "all"
    ini_origin = "all origins"
    ini_lineage = "all"
    ini_position = 1841

    filter = html.Div([
            html.Div([ 
            # Base distribution
            html.Div([
                html.Div([
                    html.H6(text_dict['H6_mut_dist'], className="center", id={"type":"mutation_distribution_plot", "index":"H6_mut_dist"}),
                    html.A([
                        html.Img(
                            src=app.get_asset_url("info_icon.png"),
                            className="description-button-left-img"
                            )
                        ], 
                        id="nt_aa_plot_info_button_left",
                        n_clicks=0,
                        className="description-button-left-anchor",
                        title=text_dict['tooltip_info_button']
                        ),
                ], className="subtitle-container",
            ),
                html.Hr(),
                #gisaid_header(app),
                html.Div([
                    html.Div(text_dict['y_axis'], id={"type":"mutation_distribution_plot", "index":"y_axis"}, className="filter_description"),
                    dcc.Dropdown(
                        id='mut-yaxis-type',
                        options=[{'label': i, 'value': i} for i in ['count', 'frequency']],
                        value='count',
                    ),
                ],
                className="filter_block"
                ),
                # country filter
                html.Div([
                    html.Div(text_dict['Div_country'], className="filter_description", id={"type":"mutation_distribution_plot", "index":"Div_country"}),
                    dcc.Dropdown(
                        id='mut_temporal_country_dropdown',
                        options=[{'label': 'Global', 'value': 'Global'}] + [{'label': c, 'value': c} for c in
                                                                            country_dropdown_values if
                                                                            c not in ["Global", "Wuhan"]],
                        value=ini_country_base_dist,
                    ),
                ],
                className="filter_block"
                ),
                # origin filter
                html.Div([
                    html.Div([text_dict['Div_origin']], className="filter_description", id={"type":"mutation_distribution_plot", "index":"Div_origin"}),
                    dcc.Dropdown(
                        id='mut_temporal_origin_dropdown',
                        options=[{'label': origin if origin != "all origins" else "all", 'value':origin} for origin in sorted(all_origins, key=str.casefold)],
                        value=ini_origin,
                    ),
                ],
                    className="filter_block"
                ),
                # host filter
                html.Div([
                    html.Div([text_dict['Div_host']], className="filter_description", id={"type":"mutation_distribution_plot", "index":"Div_host"}),
                    dcc.Dropdown(
                        id='mut_temporal_host_dropdown',
                        options=[{'label': host, 'value': host} for host in sorted(all_hosts, key=str.casefold)],
                        value=ini_host,
                        multi=True,
                    ),
                ],
                    className="filter_block scroll_box_10"
                ),
                # lineage filter
                html.Div([
                    html.Div([text_dict['Div_lineage']], className="filter_description", id={"type":"mutation_distribution_plot", "index":"Div_lineage"}),
                    dcc.Dropdown(
                        id='mut_temporal_lineage_dropdown',
                        options=[{'label': lineage, 'value': lineage} for lineage in lineage_dropdown_values],
                        value=ini_lineage,
                        multi=True,
                    ),
                ],
                    className="filter_block scroll_box_10"
                ),
                # basepair position filter
                html.Div([
                    html.Div([text_dict["Div_fst_pos"]], className="filter_description", id={"type":"mutation_distribution_plot", "index":"Div_fst_pos"}),
                    dcc.Input(
                        id="position_base_distribution",
                        type="number",
                        placeholder="",
                        value=ini_position,
                        min=ini_pos1,
                        max=ini_pos2,
                        className="input_field",
                    ),
                ],
                className="filter_block"
                ),
                html.Div([
                    html.Button(
                        html.Div(text_dict["submission_btn"], id={"type": "mutation_distribution_plot", "index": "submission_btn"}),
                        id='submit_mut_temporal', className="submit", n_clicks=0),
                ],
                className="filter_block"
                ),
            ],
            className="filter_block_bundle"
            ),
        ],
        className="sub-menu-left"
        )
    ],className="menu-left no-print")

    return filter
