from dash_core_components import Dropdown, DatePickerRange, Graph, Input
import dash_html_components as html
import dash_bootstrap_components as dbc
import pandas as pd
from frontend.app.shared.utils import Header, get_calendarweeks, get_statistics, \
    get_country_dropdown_values, get_lineage_dropdown_values, gisaid_header
from datetime import timedelta
from shared.residue_converter.data_loader import voc_label_dict
from dash_extensions import Download

def create_layout(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['baseFrequencyPlot']
    statistics = get_statistics(df_dict, text_dict)
    return html.Div([  # page
        Header(app, lang_dict["header"]),
        html.Div([  # page content
            # statistics
            html.H6([text_dict['H5_title']], className="subtitle padded", id={"type":"overview_pipeline", "index":"H5_title"}),
            html.Div([
                ''.join(statistics)
            ],
                className="info-box statistics",
            ),
            html.Div([
                html.H6(text_dict['plot'], className="subtitle padded", id={"type":"baseFrequencyPlot", "index":"plot"}),
                html.A([
                    html.Img(
                    src=app.get_asset_url("info_icon.png"),
                    className="description-button-page-img"
                    )
                ], 
                id="freq_plot_info_button_page",
                n_clicks=0,
                className="description-button-page-anchor",
                title=text_dict['tooltip_info_button']
                ),
                html.A([
                    html.Img(
                        src=app.get_asset_url("download_icon.png"),
                        className="description-button-page-img"
                        ),
                    Download(id="download-freq-plot"),
                    ], 
                    id="freq_plot_download_csv",
                    n_clicks=0,
                    className="description-button-page-anchor",
                    title=text_dict['tooltip_download_button']
                    ),

            ], className="subtitle-container",
            ),
            # consensus coverage
            html.Div(id='freq_plot_cons_cov', children='', className="filter_description"),
            # number of sequences in filter
            html.Div(id='freq_plot_no_of_seq', children='', className="filter_description"),
            html.Div([  # allele frequency plot
                Graph(
                    id="allele-freq-plot",
                    config={'modeBarButtonsToAdd': ['drawline', 'drawopenpath', 'drawclosedpath',
                                                    'drawcircle', 'drawrect', 'eraseshape']}
                ),
            ],
                className="h-80"
            ),  # allele frequency plot
        # This is displayed on the PDF instead of the data table.
            html.Div([
                html.H5(["Allele Frequency Plot Setting"], className="subtitle padded"),
                html.H6(["Selected Data Filter"], className="subtitle padded"),
                html.Table([
                    html.Tr([html.Td(['Country']), html.Td(id='freq_seq_filter_print_country')]),
                    html.Tr([html.Td(['Time Range']), html.Td(id='freq_seq_filter_print_date')]),
                    html.Tr([html.Td(['Origins']), html.Td(id='freq_seq_filter_print_origin')]),
                    html.Tr([html.Td(['Hosts']), html.Td(id='freq_seq_filter_print_hosts')]),
                    html.Tr([html.Td(['Number of Sequences']), html.Td(id='freq_seq_filter_print_no_samples')]),
                ]),
                html.H6(["Selected Consensus Sequence Filter"], className="subtitle padded"),
                html.Table([
                    html.Tr([html.Td(['Country']), html.Td(id='freq_cons_filter_print_country')]),
                    html.Tr([html.Td(['Calendarweek']), html.Td(id='freq_cons_filter_print_cw')]),
                    html.Tr([html.Td(['Number of Sequences']), html.Td(id='freq_cons_filter_print_no_seqs')]),
                ]),
            ],
                className="d-none d-print-inline"
            )
        ], className="sub_page",
        ),  # page content
        #plot description modal
        dbc.Modal(
            [
                dbc.ModalHeader(children=[text_dict["description"]], id="modal_freq_plot_header"),
                dbc.ModalBody(children=[
                    html.P(children=[text_dict["modal_freq_plot_general"]], id="modal_freq_plot_general"),
                    html.P(children=[text_dict["consensus_option"]], className="description-modal-header", id="modal_freq_plot_consensus_opt"),
                    html.P(children=[text_dict["modal_freq_plot_consensus"]], id="modal_freq_plot_consensus"),
                    html.P(children=[text_dict["Div_seq_opt"]], className="description-modal-header", id="modal_freq_plot_seq_opt"),
                    html.P(children=[text_dict["modal_freq_plot_seq"]], id="modal_freq_plot_seq"),
                    html.P(children=[text_dict["Div_annotation_options"]], className="description-modal-header", id="modal_freq_plot_annotation_opt"),
                    html.P(children=[text_dict["modal_freq_plot_annotation"]], id="modal_freq_plot_annotation"),
                    # changes to the modal also need to be adjusted in register_callbacks() of frontend/app/shared/callbacks_language_button.py
                    # such that the translation works properly
                    ]
                ),
                dbc.ModalFooter(
                    dbc.Button(children=[text_dict["modal_close"]], id="freq_plot_info_button_close", className="button legend")
                ),
            ],
            id="freq-plot-info-modal",
            size="lg"
        ),
    ],
        className="page row-height print-page-break-after",
        id="frequency_plot"
    )  # page
# %%
def create_menu_left(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['baseFrequencyPlot']
    all_hosts = df_dict["global_metadata"].host.unique().tolist()
    all_hosts.insert(0, 'all')
    all_origins = [origin.strip() for origin in df_dict["global_metadata"].origin.unique().tolist()]
    all_origins.insert(0, 'all origins')
    
    # retrieve variants of interest from db for dropdown
    all_mutations = df_dict["variants_of_concern"].amino_acid.tolist()
    ini_mutations = []
    # domains
    # TODO: add / get from database
    domains = ["NTD", "RBD", "FP", "HR1", "HR2", "CT"]

    # initialization freq plot
    df = df_dict['global_metadata']
    min_date, max_date = df['date'].min(), df['date'].max()
    start_date = max_date - timedelta(days=7)
    end_date = max_date

    cw_values = get_calendarweeks(df_dict)
    calendarweek_ini = "Wuhan-Hu-01" #cw_values[0][2]
    cons_country_ini = "Wuhan"

    lineage_dropdown_values = get_lineage_dropdown_values(df_dict)
    lineage_ini = "all"

    host_ini = "all"

    country_dropdown_values = get_country_dropdown_values(df_dict)

    ini_country_freq_plot = "Global"
    country_options = [{'label': 'Global', 'value': 'Global'}] + [{'label': c, 'value':c} for c in country_dropdown_values if c not in ["Global", "Wuhan"]]
    origin_ini = "all origins"
    content = html.Div([
        html.Div([
            # frequency plot
            html.Div([
                html.Div([
                    html.H6(text_dict['freq_plot'], className="center", id={"type":"baseFrequencyPlot", "index":"freq_plot"}),
                    html.A([
                        html.Img(
                        src=app.get_asset_url("info_icon.png"),
                        className="description-button-left-img"
                        )
                    ], 
                    id="freq_plot_info_button_left",
                    n_clicks=0,
                    className="description-button-left-anchor",
                    title=text_dict['tooltip_info_button']
                    ),
            ], className="subtitle-container",
            ),
                html.Hr(),
                #gisaid_header(app),
                html.Div([
                    html.Button(
                        html.Div(text_dict["submission_btn"], id={"type": "baseFrequencyPlot", "index": "submission_btn"}),
                        id='submit_freqplot', className="submit", n_clicks=0),
                ],
                    className="filter_block")
            ],
                className="filter_block_bundle"
            ),
            # consensus options
            html.Div([
                html.Div(text_dict['consensus_option'], className="filter_description_heading", id={"type":"baseFrequencyPlot", "index":"consensus_option"}),
                # calendar week
                html.Div([
                    html.Div([text_dict['week']], className="filter_description", id={"type":"baseFrequencyPlot", "index":"week"}),
                    Dropdown(
                        id='freq-plot-cw',
                        options=[{'label': c[2], 'value': c[2]} if c[2] != "first-case" else {'label': "Wuhan-Hu-01", 'value': "Wuhan-Hu-01"} for c in cw_values],
                        value=calendarweek_ini,
                    ),
                ],
                    className="filter_block"
                ),
                # country/region
                html.Div([
                    html.Div(text_dict['Div_country'], className="filter_description", id={"type":"baseFrequencyPlot", "index":"Div_country"}),
                    Dropdown(
                        id='plot-consensus-country-dropdown',
                        options=country_options,
                        value=cons_country_ini,
                    ),
                ],
                    className="filter_block"
                ),
            ],
                className="filter_block_bundle"
            ),
            # sequence options
            html.Div([
                html.Div([text_dict['Div_seq_opt']], className="filter_description_heading", id={"type":"baseFrequencyPlot", "index":"Div_seq_opt"}),
                html.Div([
                    html.Div([text_dict['Div_country_seq']], className="filter_description", id={"type":"baseFrequencyPlot", "index":"Div_country_seq"}),
                    Dropdown(
                        id='plot-country-dropdown',
                        options= country_options,
                        value=ini_country_freq_plot,
                    ),
                ],
                    className="filter_block"
                ),
                html.Div([
                    html.Div([text_dict['Div_origin']], className="filter_description", id={"type":"baseFrequencyPlot", "index":"Div_origin"}),
                    Dropdown(
                        id='plot-origin-dropdown',
                        options=[{'label': origin if origin != "all origins" else "all", 'value':origin} for origin in sorted(all_origins, key=str.casefold)],
                        value=origin_ini,
                    ),
                ],
                    className="filter_block"
                ),
                html.Div([
                    html.Div([text_dict['Div_host']], className="filter_description", id={"type":"baseFrequencyPlot", "index":"Div_host"}),
                    Dropdown(
                        id='plot-host-dropdown',
                        options=[{'label': host, 'value': host} for host in sorted(all_hosts, key=str.casefold)],
                        value=host_ini,
                        multi=True,
                    ),
                ],
                    className="filter_block scroll_box_10"
                ),
                html.Div([
                    html.Div([text_dict['time_interval']], className="filter_description", id={"type":"baseFrequencyPlot", "index":"time_interval"}),
                    DatePickerRange(
                        id='plot-my-date-picker-range',
                        min_date_allowed=min_date,
                        max_date_allowed=max_date + timedelta(days=1),
                        # disabled days start at this date
                        initial_visible_month=max_date,
                        start_date=start_date,
                        end_date=end_date,
                        number_of_months_shown = 3,
                        display_format='DD.MM.YY',
                        with_portal=True,
                    ),
                ],
                    className="filter_block"
                ),
                html.Div([
                    html.Div([text_dict['Div_lineage']], className="filter_description", id={"type":"baseFrequencyPlot", "index":"Div_lineage"}),
                    Dropdown(
                        id='plot-lineage-dropdown',
                        options=[{'label': lineage, 'value': lineage} for lineage in lineage_dropdown_values],
                        value=lineage_ini,
                        multi=True,
                    ),
                ],
                    className="filter_block scroll_box_10"
                ),
                # annotation options
                html.Div([text_dict['Div_annotation_options']], className="filter_description_heading", id={"type":"baseFrequencyPlot", "index":"Div_annotation_options"}),
                # selection of regions of interest, domains
                html.Div([
                    html.Div(text_dict['Div_rois'], className="filter_description",
                        id={"type": "baseFrequencyPlot", "index": "Div_rois"}),
                    Dropdown(id='plot-rois-dropdown',
                             options=[{'label': di, 'value': di} for di in domains],
                             value=["RBD"], multi=True),
                ],
                    className="filter_block scroll_box_10"
                ),

                # selection of variants of interest
                html.Div([
                    html.Div(text_dict['Div_vocs'], className="filter_description",
                        id={"type": "baseFrequencyPlot", "index": "Div_vocs"}),
                    Dropdown(id='plot-variants-dropdown',
                            options=[{'label': voc_label_dict[variant] if variant in voc_label_dict else variant, 
                                    'value': variant} for variant in sorted(all_mutations)],
                             value=ini_mutations, multi=True),
                ],
                    className="filter_block scroll_box_10"
                ),

                # text input for user codons
                html.Div([
                    html.Div(text_dict["Div_uservocs"], className="filter_description",
                        id={"type": "baseFrequencyPlot", "index": "Div_uservocs"}),
                    Input(id='plot-variants-input', type="text",
                          placeholder=text_dict["uservocs_placeholder"], 
                          className="input_field"),
                ],
                    className="filter_block"
                ),
            ],
                className="filter_block_bundle"
            ),
            # submit button freq plot
            html.Div([
                html.Div([
                    html.Button(
                        html.Div(text_dict["submission_btn_2"], id={"type": "baseFrequencyPlot", "index": "submission_btn_2"}),
                        id='submit_freqplot', className="submit", n_clicks=0),
                ],
                    className="filter_block")
            ],
                className="filter_block_bundle"
            ),
        ],
            className="sub-menu-left"
        )
    ],
        className="menu-left no-print"
    )
    return content
# %%
