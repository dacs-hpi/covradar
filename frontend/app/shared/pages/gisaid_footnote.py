import dash_html_components as html
import dash_core_components as dcc
import os


def create_layout():


    with open(os.path.join('frontend/app/shared/assets', 'gisaid_footnote.md'), 'r') as f:
        footnote = f.read()

    content = html.Div([
        dcc.Markdown(footnote),
    ],
        id="gisaid_footnote",
    )

    return content
