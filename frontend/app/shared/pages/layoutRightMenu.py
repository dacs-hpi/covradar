import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
from shared.version._version import get_versions
import os


def create_menu_right(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['right_menu']
    text_dict_buttons = lang_dict['buttons_right']
    converter_options = ["MSA", "Spike", "Genome", "Codon"]

    with open(os.path.join('frontend/app/shared/assets', 'imprint.md'), 'r') as f:
        imprint = f.read()

    with open(os.path.join('frontend/app/shared/assets', 'privacy_policy.md'), 'r') as f:
        privacy_policy = f.read()

    with open(os.path.join('frontend/app/shared/assets', 'acknowledgement.md'), 'r') as f:
        acknowledgement = f.read()

    sidebar_right = html.Div(
        [
            html.Div([
                # Page Navigator
                dbc.DropdownMenu(
                    [
                        dbc.DropdownMenuItem(text_dict_buttons["btn3"], n_clicks=0, className="tab bscroll1", id={"type": "buttons_right", "index": "btn3"}),
                        dbc.DropdownMenuItem(text_dict_buttons["btn4"], n_clicks=0, className="tab bscroll2", id={"type": "buttons_right", "index": "btn4"}),
                        dbc.DropdownMenuItem(text_dict_buttons["btn5"], n_clicks=0, className="tab bscroll3", id={"type": "buttons_right", "index": "btn5"}),
                        dbc.DropdownMenuItem(text_dict_buttons["btn6"], n_clicks=0, className="tab bscroll4", id={"type": "buttons_right", "index": "btn6"}),
                        dbc.DropdownMenuItem(text_dict_buttons["btn7"], n_clicks=0, className="tab bscroll5", id={"type": "buttons_right", "index": "btn7"}),
                    ],
                    label="Navigation",
                ),
                # html.Div([
                #     html.Button(text_dict_buttons["btn3"], n_clicks=0, className="tab bscroll1", id={"type": "buttons_right", "index": "btn3"}),
                #     html.Button(text_dict_buttons["btn4"], n_clicks=0, className="tab bscroll2", id={"type": "buttons_right", "index": "btn4"}),
                #     html.Button(text_dict_buttons["btn5"], n_clicks=0, className="tab bscroll3", id={"type": "buttons_right", "index": "btn5"}),
                #     html.Button(text_dict_buttons["btn6"], n_clicks=0, className="tab bscroll4", id={"type": "buttons_right", "index": "btn6"}),
                #     html.Button(text_dict_buttons["btn7"], n_clicks=0, className="tab bscroll5", id={"type": "buttons_right", "index": "btn7"}),
                #     # add new page here
                # ],
                # className="tab_block"
                # ),
                html.Div([
                    html.A(html.Button(text_dict_buttons["btn11"], className="button legend", id={"type": "buttons_right", "index": "btn11"}),
                                    href="/help", target="_blank"),
                    html.A(html.Button(text_dict_buttons["btn8"], className="button legend", id={"type": "buttons_right", "index": "btn8"}),
                                    href="/"),
                    html.Button("De/En", className="button", id="language_button", n_clicks=0),
                    # printbutton for js function
                    # html.Button(text_dict['buttons_right'][9], className="button printbutton"),
                    html.Button(html.Div(text_dict_buttons["btn10"], id={"type": "buttons_right", "index": "btn10"}), id="imprint_open", className="button legend"),
                    # gisaid acknowledgements
                    #html.Button(html.Div(text_dict_buttons["btn12"], id={"type": "buttons_right", "index": "btn12"}), id="acknowledgement_open", className="button acknowledgement"), 
                    dbc.Modal(
                        [
                            dbc.ModalHeader(html.Div(text_dict["imprint_title"], id={"type": "right_menu", "index": "imprint_title"})),
                            dbc.ModalBody(
                                dbc.Tabs(
                                    [
                                        dbc.Tab(dbc.Card(
                                            dbc.CardBody(
                                                [
                                                    dcc.Markdown(imprint),
                                                ]
                                            ),
                                            className="mt-3",
                                        ), label="Contact", id="imprint_contact"),
                                        dbc.Tab( dbc.Card(
                                            dbc.CardBody(
                                                [
                                                    dcc.Markdown(privacy_policy),
                                                ]
                                            ),
                                            className="mt-3",
                                        ), label="Privacy Policy", id="imprint_policy"),
                                    ]
                                )
                            ),
                            dbc.ModalFooter(
                                html.Button(html.Div(text_dict["imprint_close"], id={"type": "right_menu", "index": "imprint_close"}), id="imprint_close",
                                            className="ml-auto button")
                            ),
                        ],
                        id="imprint",
                    ),
                    dbc.Modal(
                        [
                            dbc.ModalHeader(html.Div(text_dict["acknowledgement_title"], id={"type": "right_menu", "index": "acknowledgement_title"})),
                            dbc.ModalBody(
                               dcc.Markdown(acknowledgement),
                            ),
                            dbc.ModalFooter(
                                html.Button(html.Div(text_dict["acknowledgement_close"], id={"type": "right_menu", "index": "acknowledgement_close"}), id="acknowledgement_close",
                                            className="ml-auto button")
                            ),
                        ],
                        id="acknowledgement",
                    ),
                ],
                className="button_block"
                ),
            ],
            className="sub-menu-right nav_block",
            ),

            html.Div([
                html.Div([
                    html.H6(text_dict['converter'], className="center", id={"type":"world_map", "index":"converter"}),
                    html.Hr(),
                    html.Button(
                        html.Div(text_dict["description"], id={"type": "right_menu", "index": "description"}),
                        id="position_converter_collapse_button",
                        className="button legend",
                    ),
                    dbc.Collapse(
                        dbc.Card(dbc.CardBody(
                            html.Div(text_dict["converter_description"], id={"type": "right_menu", "index": "converter_description"}),
                            )),
                        id="position_converter_collapse",
                    ),

                ],
                    className="filter_block "
                ),
                html.Div([
                    html.Div([
                        dcc.Dropdown(
                            id='converter_dropdown',
                            options=[{'label': c, 'value':c} for c in converter_options],
                            value=converter_options[0],
                        ),
                    ]),
                    html.Div([
                        dcc.Input(
                            id="converter_input",
                            type="number",
                            value=1,
                            debounce=False,
                            className="input_field",
                        ),
                    ]),
                ],
                    className="filter_block"
                ),
                html.Div([
                    html.Table([
                        html.Tr([html.Td(['MSA']), html.Td(id='converter_output_msa')]),
                        html.Tr([html.Td(['Spike']), html.Td(id='converter_output_firstcase')]),
                        html.Tr([html.Td(['Genome']), html.Td(id='converter_output_genome')]),
                        html.Tr([html.Td(['Codon']), html.Td(id='converter_output_aa')]),
                        html.Tr([html.Td(['Reference nt']), html.Td(id='converter_reference_nt')]),
                        html.Tr([html.Td(['Reference aa']), html.Td(id='converter_reference_aa')]),
                    ]),
                ],
                    className="filter_block"
                ),
            ],
                className="sub-menu-right converter_block"
            ),
            html.Div([
                html.Div([
                    html.H6(text_dict['supported'], className="center english"),
                    html.Img(
                        src=app.get_asset_url("rki_logo.png"),
                        className="supporter_logo",
                    ),
                    html.Img(
                        src=app.get_asset_url("denbi_cloud_logo.png"),
                        className="supporter_logo",
                    ),
                ],
                className="supporter_logo_box"
                ),
            ],
            className="sub-menu-right supporter_block",
            ),
            html.Div([
                html.Div("Version " + get_versions()['version'].split("+")[0], className="center"),
            ],
            className="sub-menu-right version_block",
            ),
        ],
        className="menu-right no-print",
    )
    return sidebar_right
