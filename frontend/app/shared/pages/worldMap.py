from frontend.app.shared.utils import Header, gisaid_header
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from frontend.app.shared.utils_world_map import DateSlider
from shared.residue_converter.data_loader import voc_label_dict
from dash_extensions import Download


def create_layout(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['world_map']
    date_slider = DateSlider(df_dict['global_metadata'])
    """
    contain layout page
    """

    world_map = html.Div([
        dcc.Graph(
            id="world_map_fig"
        ),
        html.Div([
            html.I(id='play_button_ww', n_clicks=0, className='fa fa-play-circle', ),
            html.Div([
                dcc.Interval(id='auto_stepper_ww',
                             # TODO this might cause the error: { message: "Circular Dependencies", html: "Error: Dependency Cycle Found: auto_stepper.n_intervals -> auto_stepper.disabled -> auto_stepper.n_intervals" }
                             interval=600,
                             n_intervals=0,
                             max_intervals=0,
                             disabled=True),
                dcc.RangeSlider(id='date_slider_ww',
                                updatemode='mouseup',
                                min=date_slider.unix_time_millis(date_slider.min_date),
                                max=date_slider.unix_time_millis(date_slider.max_date),
                                marks=date_slider.get_marks_date_range(),
                                step=86400,  # unix time one day
                                allowCross=False,
                                value=[date_slider.unix_time_millis(
                                    date_slider.get_date_x_days_before(date_slider.max_date)),
                                    date_slider.unix_time_millis(date_slider.max_date)],
                                ),
            ], id="slider_box_ww"
            ),
            dbc.Tooltip(text_dict["tooltip_slider"], target="slider_box_ww",
                        id={"type": "world_map", "index": "tooltip_slider"}),
        ],
            className="d-print-none",)
    ],
        className="germany_block")

    map_chart_header = html.Div([
        html.H5(id='chosen_location')
    ])

    map_chart_dist = html.Div([  # plots
        html.Div([
            html.H6(text_dict["H6_var_dist"], id={"type": "world_map", "index": "H6_var_dist"}),
            dcc.Graph(id='results_per_location'),
        ]),
    ]),
    map_chart_dev = html.Div([
        html.Div([
            html.H6(text_dict["H6_scatter"], id={"type": "world_map", "index": "H6_scatter"}),
            dcc.Graph(id='mutation_development_ww'),
        ]),
    ]),

    map_charts = html.Div([
        html.Div([
            html.H6(text_dict["H6_var_dist"], id={"type": "world_map", "index": "H6_var_dist"}),
            dcc.Graph(id='results_per_location'),
        ]),
        html.Div([
            html.H6(text_dict["H6_scatter"], id={"type": "world_map", "index": "H6_scatter"}),
            dcc.Graph(id='mutation_development_ww'),
        ]),
    ]),

    info_texts = html.Div([
        html.Div([  # info-box
            dcc.Markdown(id='chosen_method_and_voc_ww'),
        ], className="info-box"),
    ])

    disclaimer = html.Div([
        text_dict['disclaimer_text'],
        html.A(
            "here", href="https://outbreak.info/compare-lineages?gene=S&gene=ORF1a&gene=ORF1b&threshold=75"
        )
    ], className="disclaimer-box", id={"type": "world_map", "index": "disclaimer_text"})

    content_pdf = \
        html.Div([
            html.Div([
                Header(app, lang_dict["header"]),
                html.Div([  # all graphs and date slider and info-box
                    html.H6(text_dict['H6_sending_map'], className="subtitle padded",
                            id={"type": "world_map", "index": "H6_sending_map"}),
                    html.A([
                        html.Img(
                            src=app.get_asset_url("info_icon.png"),
                            className="description-button-page-img"
                            )
                        ], 
                        id="map_info_button_page",
                        n_clicks=0,
                        className="description-button-page-anchor",
                        title=text_dict['tooltip_info_button']
                        ),
                    html.A([
                        html.Img(
                            src=app.get_asset_url("download_icon.png"),
                            className="description-button-page-img"
                        ),
                        Download(id="download-world-map"),
                    ],
                        id="germany_map_download_csv",
                        n_clicks=0,
                        className="description-button-page-anchor",
                        title=text_dict['tooltip_download_button']
                    ),

                ], className="subtitle-container",
                ),
                #plot description modal
                dbc.Modal(
                    [
                        dbc.ModalHeader(children=[text_dict["description"]], id="modal_map_header"),
                        dbc.ModalBody(children=[
                            html.P(children=[text_dict["modal_map"]], id="modal_map"),
                            html.P(children=[text_dict["modal_map_options_header"]], className="description-modal-header", id="modal_map_options_header"),
                            html.P(children=[text_dict["modal_map_options"]], id="modal_map_options"),
                            # changes to the modal also need to be adjusted in register_callbacks() of frontend/app/shared/callbacks_language_button.py
                            # such that the translation works properly
                            ]
                        ),
                        dbc.ModalFooter(
                            dbc.Button(children=[text_dict["modal_close"]], id="map_info_button_close", className="button legend")
                        ),
                    ],
                    id="map-info-modal",
                    size="lg"
                ),
                dbc.Card(
                    dbc.CardBody([
                        dbc.Row([
                            dbc.Col(disclaimer, width=12),
                        ], align="center", className="dbc-row-14",
                        ),
                        html.Br(),
                        dbc.Row([
                            dbc.Col(world_map, width=12, style={"height":"100%"}),
                        ], align="center",
                        ),
                        html.Br(),
                        dbc.Row([
                            dbc.Col(map_chart_header, width=10, style={"height":"100%"})
                        ], align="center",
                        ),
                        html.Br(),
                        dbc.Row([
                            dbc.Col(info_texts, width=2, style={"height":"100%"}),
                            dbc.Col(map_charts, width=10, style={"height":"100%"}),
                        ], align="center",
                        ),
                        html.Br(),
                    ],
                    ), )
            ],
                className="sub_page"
            )
        ],
            className="page",
            id="ww_map"
        )
    return content_pdf




def create_menu_left(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['world_map']
    all_variants_list =[f"{voc}" for voc in df_dict["variants_of_concern"]['amino_acid'].unique().tolist()] + ["wildtype"]
    # TODO I excluded "common_X" for now in the map, because this leads to underrepresentation/misinterpretation. 
    # (Only cases with ALL these mutations are shown)
    all_variants_list = [el for el in all_variants_list if len(el) < 6]
    all_variants_list.insert(0, 'all mutations in dropdown')
    exclude = ["D614G"]
    # mutations_ini = [i for i in all_variants_list if i not in voc_label_dict and i not in exclude]
    mutations_ini = 'all mutations in dropdown'
    date_slider = DateSlider(df_dict['global_metadata'])
    days = (date_slider.max_date - date_slider.min_date).days + 1

    content = html.Div([
        html.Div([
            html.Div([
                html.Div([
                    html.H6(text_dict['H6_loc'], className="center", id={"type": "world_map", "index": "H6_loc"}),
                    html.A([
                        html.Img(
                            src=app.get_asset_url("info_icon.png"),
                            className="description-button-left-img"
                            )
                        ], 
                        id="map_info_button_left",
                        n_clicks=0,
                        className="description-button-left-anchor",
                        title=text_dict['tooltip_info_button']
                        ),
            ], className="subtitle-container",
            ),
                html.Hr(),
                #gisaid_header(app),
                html.Div([
                    html.Div([
                        html.Div([text_dict['H6_mutation_selection']], className="filter_description",
                                 id={"type": "world_map", "index": "H6_mutation_selection"}),
                    ], id="selected_mutations_headers_ww"),
                    html.Div([
                        dcc.Dropdown(
                            id='dropdown_mutation_ww',
                            options=[{'label': voc_label_dict[
                                variant] if variant in voc_label_dict else variant.replace(",", ", "),
                                      'value': variant} for variant in all_variants_list],
                            value=mutations_ini,
                            multi=True,
                        ),
                    ], className="voc_block_items")
                ], className="filter_block scroll_box_25"
                ),
                html.Div([
                    html.Div([
                        html.Div([text_dict["select_mode"]], className="filter_description",
                                 id={"type": "world_map", "index": "select_mode"}),
                    ], id='mode_headers_ww'),
                    dcc.Dropdown(
                        id='dropdown_mode_ww',
                        options=[{'label': name, 'value': value} for name, value in
                                 zip(text_dict['dropdown_mode_name'],
                                     text_dict['dropdown_mode_value'])],
                        value='n-th most frequent mutation'
                    )], className="filter_block"),
                html.Div([
                    html.Div(
                        html.Div([text_dict["var_nb_text"]], id={"type": "world_map", "index": "var_nb_text"},
                                 className="filter_description"),
                        id='number_mutations_ww'),
                    dcc.Input(
                        id="selected_nth_ww",
                        type="number",
                        placeholder=1,
                        value=1,
                        min=1,
                        max=len(all_variants_list),
                        className="input_field",
                    )
                ], id='hide_elem_ww', className="filter_block"),
                html.Div([
                    html.Div([
                        html.Div([text_dict['Div_method']], className="filter_description",
                                 id={"type": "world_map", "index": "Div_method"})],
                        id="method_lab_blue_ww"),
                    dcc.Dropdown(
                        id='dropdown_method_ww',
                        options=[{'label': name, 'value': value} for name, value in
                                 zip(text_dict['dropdown_methods_name'],
                                     text_dict['dropdown_methods_value'])],
                        value='Frequency'
                    )], className="filter_block"),
                html.Div([
                    html.Div([
                        html.Div([text_dict["div_nb_days"] + str(days) + ')'], className="filter_description",
                                 id={"type": "world_map", "index": "div_nb_days"}),
                    ], id="interval_headers_ww"),
                    dcc.Input(
                        id="selected_interval_ww",
                        type="number",
                        placeholder=28,
                        value=28,
                        className="input_field",
                        min=1,
                    )
                ], className="filter_block"),
                html.Div([
                    html.Div([
                        html.Div([text_dict["y_axis"]], className="filter_description",
                                 id={"type": "world_map", "index": "y_axis"}),
                    ],
                        id="y_axis_scale_ww"
                    ),
                    dcc.RadioItems(
                        id='yaxis_type_ww',
                        options=[{'label': " {}  ".format(i), 'value': i} for i in ['linear', 'log']],
                        value='linear',
                        # labelStyle={'display': 'inline-block'}),
                    )
                ], className="filter_block"),
            ], className="filter_block_bundle map")
        ], className="sub-menu-left",
        ),
        dbc.Tooltip(text_dict["tooltip_method"], target="method_lab_blue_ww",
                    id={"type": "world_map", "index": "tooltip_method"}),
        dbc.Tooltip(text_dict["tooltip_interval"], target="interval_headers_ww",
                    id={"type": "world_map", "index": "tooltip_interval"}),
        dbc.Tooltip(text_dict["tooltip_mutations"], target="selected_mutations_headers_ww",
                    id={"type": "world_map", "index": "tooltip_mutations"}),
        dbc.Tooltip(text_dict["tooltip_nth"], target="number_mutations_ww",
                    id={"type": "world_map", "index": "tooltip_nth"}),
        dbc.Tooltip(text_dict["tooltip_mode"], target="mode_headers_ww",
                    id={"type": "world_map", "index": "tooltip_mode"}),
        dbc.Tooltip(text_dict["tooltip_yaxis"], target="y_axis_scale_ww",
                    id={"type": "world_map", "index": "tooltip_yaxis"}),

    ], className="menu-left")
    return content
