#!/usr/bin/env python3
import math
from dash_core_components import Dropdown, Input
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_table
from dash_extensions import Download
from frontend.app.shared.utils import Header, consensustable, get_calendarweeks, get_country_dropdown_values, gisaid_header


def create_layout(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['consensus_table']
    return html.Div(
        [
            Header(app, lang_dict["header"]),
            html.Div([
                html.Div([
                    html.H6([text_dict["H6_cons_seq_tab"]
                        ], 
                        className="subtitle padded", 
                        id={"type":"consensus_table", "index":"H6_cons_seq_tab"}
                        ),
                    html.A([
                        html.Img(
                        src=app.get_asset_url("info_icon.png"),
                        className="description-button-page-img"
                        )
                    ], 
                        id="consensus_chart_info_button_page",
                        n_clicks=0,
                        className="description-button-page-anchor",
                        title=text_dict['tooltip_info_button']
                    ),
                    html.A([
                        html.Img(
                            src=app.get_asset_url("download_icon.png"),
                            className="description-button-page-img"
                        ),
                        Download(id="download-consensus-chart"),
                        ], 
                        id="consensus_chart_download_csv",
                        n_clicks=0,
                        className="description-button-page-anchor",
                        title=text_dict['tooltip_download_button']
                    ),
            ], className="subtitle-container",
            ),
                # data table
                html.Div(
                    [
                        dash_table.DataTable(
                            id='table2',
                            columns=[],
                            style_table={
                                'max-height': '100%',
                                'overflow': 'auto',
                                'minWidth': '100%',  # table width
                            },
                            style_header={  # table header
                                'backgroundColor': '#f2f6f9',
                            },
                            style_cell={
                                'height': 'auto',
                                'whiteSpace': 'normal',
                                'textAlign': 'center',
                            },
                            style_data_conditional = [],
                            # export_format='xlsx',  # download button, xlsx|csv
                            merge_duplicate_headers=True,
                        )
                    ],
                    className="consensus_table_block"
                ),
            # This is displayed on the PDF instead of the data table.
                html.Div([
                    html.H5(["Consensus Table Setting"], className="subtitle padded"),
                    html.Table([
                        html.Tr([html.Td(['Country']), html.Td(id='consensus_table_print_country')]),
                        html.Tr([html.Td(['Time Range']), html.Td(id='consensus_table_print_date')]),
                        html.Tr([html.Td(['Position Interval']), html.Td(id='consensus_table_print_positions')]),
                    ]),
                ],
                    className="d-none d-print-inline"
                )
            ],
            className="sub_page",
            ),
            dbc.Modal(
            [
                dbc.ModalHeader(children=[text_dict["description"]], id="modal_consensus_header"),
                dbc.ModalBody(children=[
                    html.P(children=[text_dict["modal_consensus_general"]], id="modal_consensus_general"),
                    html.P(children=[text_dict["modal_consensus_options_header"]], className="description-modal-header", id="modal_consensus_options_header"),
                    html.P(children=[text_dict["modal_consensus_options"]], id="modal_consensus_options"),
                    html.Img(
                        src=app.get_asset_url("consensus_chart_description.png"),
                        className="description-modal-image"
                        )
                    # changes to the modal also need to be adjusted in register_callbacks() of frontend/app/shared/callbacks_language_button.py
                    # such that the translation works properly
                    ]
                ),
                dbc.ModalFooter(
                    dbc.Button(children=[text_dict["modal_close"]], id="consensus_chart_info_button_close", className="button legend")
                ),
            ],
            id="consensus-chart-info-modal",
            size="xl"
        ),
        ],
        className="page row-height",
        id="consensus_chart")


def create_menu_left(app, db_connection, df_dict, lang_dict):
    text_dict = lang_dict['consensus_table']
    country_dropdown_values = get_country_dropdown_values(df_dict)
    cw_values = get_calendarweeks(df_dict)

    if "Global" in country_dropdown_values:
        ini_country = "Global"
    else:
        ini_country = country_dropdown_values[0]

    # initialization table
    len_first_case = 3822  # int(db_connection.execute("SELECT first_case FROM global_position ORDER BY msa DESC LIMIT 1").fetchone()[0])
    ini_pos1 = 1
    ini_pos2 = len_first_case

    if len(cw_values) >= 10:
        consensus_start_week = cw_values[-4][2]
    else:
        consensus_start_week = cw_values[-1*len(cw_values)][2]



    content = html.Div([
        html.Div([
            html.Div([
                html.Div([
                    html.H6(text_dict['H6_cons_tab'], className="center", id={"type":"consensus_table", "index":"H6_cons_tab"}),
                    html.A([
                            html.Img(
                            src=app.get_asset_url("info_icon.png"),
                            className="description-button-left-img"
                            )
                        ], 
                        id="consensus_chart_info_button_left",
                        n_clicks=0,
                        className="description-button-left-anchor",
                        title=text_dict['tooltip_info_button']
                        ),
            ], className="subtitle-container",
            ),
            html.Hr(),
            #gisaid_header(app),
            ],
            className="filter_block_bundle"
            ),
            html.Div([
                html.Div([
                    # country filter
                    html.Div([
                        html.Div([text_dict["Div_country"]], className="filter_description", id={"type":"consensus_table", "index":"Div_country"}),
                        Dropdown(
                                id='consensus-country-dropdown',
                                options=[{'label': c, 'value':c} for c in country_dropdown_values if c not in ["Wuhan"]],
                                value=ini_country,
                            ),
                    ]),
                ],
                className="filter_block"
                ),
                html.Div([
                    # customized position range
                    html.Div([
                        html.Div([text_dict['Div_pos_range']], className="filter_description", id={"type":"consensus_table", "index":"Div_pos_range"}),
                        Input(id="pos_input1", type="number", placeholder="", debounce=True, value=ini_pos1, min=1, max=len_first_case, className="input_field"),
                        Input(id="pos_input2", type="number", placeholder="", debounce=True, value=ini_pos2, min=1, max=len_first_case, className="input_field"),
                    ]),
                ],
                className="filter_block"
                ),
                html.Div([
                    # calendar week filter
                    html.Div([
                        html.Div([text_dict['Div_week']], className="filter_description", id={"type":"consensus_table", "index":"Div_week"}),
                        html.Div([Dropdown(
                                id='consensus-cw-1',
                                options=[{'label': c[2], 'value':c[2]} for c in cw_values if c[2] != "first-case"],
                                value=consensus_start_week,
                                ),
                        ]),
                        html.Div([Dropdown(
                                id='consensus-cw-2',
                                options=[{'label': c[2], 'value':c[2]} for c in cw_values if c[2] != "first-case"],
                                value=cw_values[-1][2],
                            )
                        ]),
                    ]),
                ],
                className="filter_block"
                ),
            ],
            className="filter_block_bundle"
            ),
            # submit button
            html.Div([
                html.Div([
                    html.Button(
                        html.Div(text_dict["submission_btn"], id={"type": "consensus_table", "index": "submission_btn"}),
                        id='submit_consensus', className="submit", n_clicks=0),
                ],
                className="filter_block")
            ],
            className="filter_block_bundle"
            ),
        ],
        className="sub-menu-left"
        )
    ],className="menu-left no-print")
    return content