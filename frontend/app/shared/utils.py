import math
import dash_html_components as html
import numpy as np
import pandas as pd
import datetime
import plotly.express as px

from functools import lru_cache

def Header(app, text_dict):
    return get_header(app, text_dict)


def get_header(app, text_dict):
    header = html.Div(
        [
            html.Img(
                src=app.get_asset_url("hpi_logo_web.svg"),
                className="logo",
            ),
        ],
        className="header",
    )
    return header


def gisaid_header(app):
    header = html.Div(
        [
            html.Div([
                "Enabled by data from "
            ], className="gisaid_header_div"
            ),
            html.Div([
                html.A([
                    html.Img(
                        src=app.get_asset_url("gisaid_logo.png"),
                        className="gisaid_logo",
                    ),
                ], href='https://www.gisaid.org/', target="_blank"),
            ], className="gisaid_header_div"
            ),
        ],
        className="gisaid_header",
    )
    return header


def get_spike_length(df_dict):
    return len(df_dict["global_position"])


def get_statistics(df_dict, text_dict):
    stats = df_dict["global_statistics"]
    statistics = []
    date = "[" + stats.at[0, 'date'].replace("-", "/") + "] "
    statistics.append(date)
    #taxa = stats.at[0, 'taxa']
    #sites = stats.at[0, 'sites']
    #statistics.append(text_dict['statistic_1'] % (taxa, sites))
    #undetermined = stats.at[0, 'undetermined']
    #if int(undetermined) > 0:
    #    statistics.append(text_dict['statistic_2'] % (undetermined))
    #duplicates = stats.at[0, 'duplicates']
    #statistics.append(text_dict['statistic_3'] % (duplicates))
    #partitions = stats.at[0, 'partitions']
    #patterns = stats.at[0, 'patterns']
    #if int(partitions) > 1:
    #    statistics.append(text_dict['statistic_4'] % (partitions, patterns))
    #else:
    #    statistics.append(text_dict['statistic_5'] % (patterns))
    #gaps = stats.at[0, 'gaps']
    #statistics.append(text_dict['statistic_6'] % (gaps))
    #invariants = stats.at[0, 'invariants']
    #statistics.append(text_dict['statistic_7'] % (invariants))
    snps = stats.at[0, 'snps']
    statistics.append(text_dict['statistic_8'] % (snps))
    multiallelic_sites = stats.at[0, 'multiallelic_sites']
    statistics.append(text_dict['statistic_9'] % (multiallelic_sites))
    multiallelic_snp_sites = stats.at[0, 'multiallelic_snp_sites']
    statistics.append(text_dict['statistic_10'] % (multiallelic_snp_sites))
    #statistics = ''.join(statistics)

    return statistics


def get_first_case(df_dict):
    cons = df_dict["consensus"]
    first = cons[(cons['date'] == 'first-case') & (cons['region'] == 'Wuhan')].reset_index(drop=True)['consensus']
    return first


#@app_line_profiler
def freq_by_consensus(df_dict, cw, region, mutation_table, total_seqs, spike_len):
    # -1 skip
    # 1 is reference
    # 0 is allele
    alt_ref_relation = {'A': {'A': 1, 'C': 0, 'T': 0, 'G': 0, '-': 0, 'N': -1},
                        'C': {'A': 0, 'C': 1, 'T': 0, 'G': 0, '-': 0, 'N': -1},
                        'T': {'A': 0, 'C': 0, 'T': 1, 'G': 0, '-': 0, 'N': -1},
                        'G': {'A': 0, 'C': 0, 'T': 0, 'G': 1, '-': 0, 'N': -1},
                        '-': {'A': 0, 'C': 0, 'T': 0, 'G': 0, '-': 1, 'N': -1},
                        'W': {'A': -1, 'C': 0, 'T': -1, 'G': 0, '-': 0, 'N': -1},
                        'S': {'A': 0, 'C': -1, 'T': 0, 'G': -1, '-': 0, 'N': -1},
                        'M': {'A': -1, 'C': -1, 'T': 0, 'G': 0, '-': 0, 'N': -1},
                        'K': {'A': 0, 'C': 0, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'R': {'A': -1, 'C': 0, 'T': 0, 'G': -1, '-': 0, 'N': -1},
                        'Y': {'A': 0, 'C': -1, 'T': -1, 'G': 0, '-': 0, 'N': -1},
                        'N': {'A': -1, 'C': -1, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'B': {'A': 0, 'C': -1, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'D': {'A': -1, 'C': 0, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'H': {'A': -1, 'C': -1, 'T': -1, 'G': 0, '-': 0, 'N': -1},
                        'V': {'A': -1, 'C': -1, 'T': 0, 'G': -1, '-': 0, 'N': -1}}

    if total_seqs == 0:
        freq_list = np.full(spike_len, math.nan)
        base_cov = np.zeros(spike_len, dtype=int)
        return freq_list, base_cov

    # ini
    freq_list = np.zeros(spike_len, dtype=float)
    base_cov = np.zeros(spike_len, dtype=int)

    # load first case
    consensus = df_dict["consensus"]
    first_case = consensus[consensus['date'] == 'first-case'].reset_index(drop=True)['consensus']
    first_np = np.array(list(first_case.values[0]))
    # mutation_table index = position
    mutation_table.set_index('position', inplace=True)
    positions = np.sort(mutation_table.index.unique())
    positions_first_diff_cons = []
    # load consensus
    if cw == "first-case":
        cons_np = first_np.copy()
    else:
        cons = consensus[(consensus['region'] == region) & (consensus['date'] == cw)].reset_index(drop=True)['consensus']
        cons_np = np.array(list(cons.values[0]))
        diff = first_np != cons_np
        for i, d in enumerate(diff):
            if d:
                if i + 1 not in positions:
                    # consensus is different from first case but on this position there are no further alleles
                    # all frequencies on that position are 1
                    if cons_np[i] == "N":
                        freq_list[i] = None
                        # base_cov is 0
                    else:
                        freq_list[i] = 1  # position is 1-based
                        base_cov[i] = total_seqs
                else:
                    positions_first_diff_cons.append(i + 1)
    for i in positions:
        if cons_np[i - 1] == "N":
            freq_list[i - 1] = None
            # base_cov is 0
        else:
            alt_list = mutation_table.loc[i]["ALT"]
            if isinstance(alt_list, str):
                alt_list = [alt_list]
            else:
                alt_list = alt_list.to_list()
            if i in positions_first_diff_cons:
                alt_list.append(first_np[i - 1])  # position is 1-based
            no_seqs = len(alt_list)
            tot = total_seqs
            ref = cons_np[i - 1]  # consensus base, position is 1-based

            for alt in alt_list:
                check = alt_ref_relation[alt][ref]
                if check == -1:  # sequence base is taken from sample set at this position
                    no_seqs -= 1
                    tot -= 1
                elif check == 1:  # base is reference
                    no_seqs -= 1

            freq = 0 if tot == 0 else float(no_seqs / tot)
            freq_list[i - 1] = freq  # position is 1-based
            base_cov[i - 1] = no_seqs

    # profile.print_stats()
    return freq_list, base_cov


def filter_lineages(lineage_column, lineages):
    bool_vectors = []
    for lineage in lineages:
        bool_vectors.append([True if lin.startswith(lineage) else False for lin in lineage_column])
    return list(map(any, zip(*bool_vectors)))


# @cumulative_profiler(1)
# @app_line_profiler
def allele_freq_sql(df_dict, country, start_date, end_date, origin, hosts, lineages, db_connection):
    df_meta = df_dict['global_metadata'].set_index('strain_id')
    #df_snp = df_dict['global_SNP'].set_index('strain_id')
    df_meta = df_meta[df_meta['date'] >= pd.Timestamp(start_date)]
    df_meta = df_meta[df_meta['date'] <= pd.Timestamp(end_date)]
    # origin condition
    if origin != 'all origins':
        df_meta = df_meta[df_meta.origin==origin]
    if hosts != "all":
        if isinstance(hosts, str): # when only 1 host selected
            hosts = [hosts]
        df_meta = df_meta[df_meta.host.isin(hosts)]
    # lineage condition
    if lineages != "all":
        if isinstance(lineages, str): # when only 1 lineage selected
            lineages = [lineages]
        df_meta = df_meta[filter_lineages(df_meta.lineage.to_list(), lineages)]
        # lineage_condition = "AND (" + " OR ".join(single_lineage_conditions) + ") "
    if country != 'Global':
        df_meta = df_meta[df_meta['country'] == country]
    if origin != 'all origins':
        df_meta = df_meta[df_meta['origin'] == origin]
    if len(df_meta.index) > 0:
        df_snp = pd.read_sql(f"SELECT * FROM global_SNP WHERE strain_id IN ({str(df_meta.index.tolist())[1:-1]})", con=db_connection, index_col="strain_id")
    else:
        df_snp = pd.DataFrame(columns=['position', 'REF_allele', 'ALT'])

    mutation_table = df_meta.join(df_snp, how='inner').reset_index(drop=True)[['position', 'strain', 'country','date', 'REF_allele', 'ALT', 'origin']]

    # profile.print_stats()
    return mutation_table, len(df_meta)


def allele_freq(df_dict, country, start_date, end_date, origin, cw, country_consensus, hosts, lineages, db_connection):
    spike_length = get_spike_length(df_dict)
    mutation_table, total_seqs = allele_freq_sql(df_dict, country, start_date, end_date,
                                                 origin, hosts, lineages, db_connection)
    freq_list, base_cov = freq_by_consensus(df_dict, cw, country_consensus, mutation_table,
                                            total_seqs, spike_length)
    return freq_list, total_seqs, base_cov


def data_filter_sql(df_dict, country, start_date, end_date, origin, hosts):
    """This returns the filtered data for the metadata table.

    Args:
        country (string): country name or "global" for all countries
        start_date (string): start day (YYYY-MM-DD) of date range
        end_date (string): end day (YYYY-MM-DD) of date range
        origin (string): source of data or "all origins" for all databases
        hosts (list): source of data or "all origins" for all databases

    Returns:
        pandas dataframe: filtered subsample of data
    """
    df_meta = df_dict["global_metadata"]
    df_meta = df_meta[df_meta['date'] >= pd.Timestamp(start_date)]
    df_meta = df_meta[df_meta['date'] <= pd.Timestamp(end_date)]
    df_meta = df_meta[df_meta['host'].isin(hosts)].reset_index(drop=True)
    if country != 'Global':
        df_meta = df_meta[df_meta['country'] == country]
    if origin != 'all origins':
        df_meta = df_meta[df_meta['origin'] == origin]
    return df_meta


def get_calendarweeks(df_dict, exclude=["2013W30"]):
    dates = df_dict['consensus']['date'].drop_duplicates().sort_values()
    dates = dates[~dates.isin(exclude)].to_list()
    calendar_week_range_values = [(0, 0, "first-case")] + sorted(
        [(int(l[:4]), int(l[5:]), l) for l in dates if l != "first-case"],
        key=lambda element: (element[0], element[1]))
    return calendar_week_range_values

def get_lineage_dropdown_values(df_dict):
    try:
        lineage_dropdown_values = sorted(list(filter(None,set(df_dict["global_metadata"]["lineage"]))), key=str.casefold)
        lineage_dropdown_values.insert(0, 'unassigned')
    except:
        lineage_dropdown_values = []
    lineage_dropdown_values.insert(0, 'all')
    return lineage_dropdown_values


def get_country_dropdown_values(df_dict):
    return sorted(df_dict['consensus']['region'].drop_duplicates().to_list())


def consensustable(df_dict, pos1, pos2, cw1, cw2, region):
    """Returns pandas dataframe containing the bases of the consensus sequences filtered by MSA
    position, country and calendar week if there is at least one difference.

    Args:
        df_dict:
        pos1 (int): base starting position referring MSA
        pos2 (int): base ending position referring MSA
        cw1 (str): calendar week start
        cw2 (str): calendar week end
        region (str): country

    Returns:
        pandas dataframe: columns: MSA position, rows: calendar week
    """
    # db query, filtered by calendar week, region and position
    cw_range = get_calendarweeks(df_dict)
    use_cw = []
    if cw1 == cw2:
        use_cw.append(cw1)
    else:
        sort_cw = sorted([(cw1[:4], cw1[5:], cw1), (cw2[:4], cw2[5:], cw2)],
                         key=lambda x: (x[0], x[1]))
        max_cw = sort_cw[1]
        min_cw = sort_cw[0]
        boundaries = []
        for i, el in enumerate(cw_range):
            if el[2] == min_cw[2]:
                boundaries.append(i)
            elif el[2] == max_cw[2]:
                boundaries.append(i)
        for el in cw_range[boundaries[0]: boundaries[1] + 1]:
            use_cw.append(el[2])

    consensus_table = df_dict['consensus']
    consensus_table = consensus_table[consensus_table['region'] == region]
    consensus_table = consensus_table[consensus_table['date'].isin(use_cw)]
    consensus_table = consensus_table[['date', 'number_of_sequences', 'consensus']]
    consensus_table = consensus_table.sort_values('date').reset_index(drop=True)

    # no consensus sequences for the cw
    if consensus_table.empty:
        return consensus_table

    # not every country has consensus for every calendar week
    use_cw = [el for el in use_cw if el in consensus_table["date"].values]

    # first case
    df_first = df_dict['consensus']
    df_first = df_first[df_first['region'] == 'Wuhan']
    df_first = df_first[df_first['date'] == 'first-case']
    first_list_1 = df_first['consensus'].to_list()
    # indices here are tested to work
    enforce_position = lambda consensus: consensus[pos1 - 1: pos2]
    first_list = list(map(enforce_position, first_list_1))

    # use only columns if they show mutations
    cons_list = list(map(enforce_position, consensus_table['consensus'].to_list()))
    cons_list.reverse()  # we want most recent calendar week first
    first_cons_list = first_list + cons_list

    use_cols = []
    use_pos = []

    for i, col in enumerate(zip(*first_cons_list)):
        if len(set(col)) != 1:  # use only columns with alleles
            if "N" not in col:  # no columns with N calls (meaning no coverage for this position)
                use_cols.append(pd.Series(col, name=i + pos1))  # name will be column name
                use_pos.append(i + pos1)
    base_table = pd.concat(use_cols, axis=1, keys=[s.name for s in use_cols])

    # change index to calendar weeks
    use_cw.reverse()
    base_table.index = ["Wuhan-Hu-01"] + use_cw

    # annotation, same base will be replaced by a dot
    annotation_dict = dict(base_table.loc["Wuhan-Hu-01"]) # first row of table
    base_table.iloc[1:, :] = base_table.iloc[1:, :].replace(annotation_dict, ".")

    # 2 header rows: msa and first-case positions
    # TODO: 3rd header row with genome position
    # TODO: column names are str
    pos_dict = df_dict['msa_first']

    # we need duplicated positions and column for coverage
    row_msa = pd.DataFrame([np.repeat(base_table.columns.values, 2)],
                           columns=np.repeat(base_table.columns.values, 2),
                           index=["msa"])
    row_first = row_msa.copy()
    row_first.index = ["ref"]
    row_first.replace(pos_dict, inplace=True)

    # sequence coverage per calendar week
    sequence_coverage = consensus_table.loc[:, ["date", "number_of_sequences"]]
    sequence_coverage.set_index("date", inplace=True)
    sequence_coverage.loc["Wuhan-Hu-01", "number_of_sequences"] = 1

    table_plus_seq_cov = pd.concat([base_table, sequence_coverage], axis=1, join='inner')

    # base coverage
    base = df_dict["base_count"][['date', 'position', 'count', 'region']]
    base_coverage = base[base['region'] == region].reset_index(drop=True)
    base_coverage.set_index("date", inplace=True)
    base_cov_table_rows = []
    for ind in set(base_coverage.index):
        row = base_coverage.loc[ind].set_index("position")
        base_cov_table_rows.append(pd.Series(row.loc[use_pos]["count"], name=ind))
    base_cov_table = pd.concat(base_cov_table_rows, axis=1,
                               keys=[r.name for r in base_cov_table_rows]).T
    base_cov_table.columns = [str(v) + "_cov" for v in base_cov_table.columns]
    base_cov_table.loc["Wuhan-Hu-01"] = 1

    table = pd.concat([table_plus_seq_cov, base_cov_table], axis=1, join='inner')

    # sort columns
    cols = ["number_of_sequences"]
    for el in use_pos:
        cols.append(el)
        cols.append(str(el) + "_cov")
    table = table[cols]

    # add header rows to table
    table.loc["msa"] = np.append("number_of_sequences", row_msa)
    table.loc["ref"] = np.append("number_of_sequences", row_first)
    # putting them in front of table
    # pd.concat does not work because of duplicate column names
    table = table.iloc[[-2, -1] + [el for el in range(len(table.index[:-2]))]]

    # index as first column
    table.insert(loc=0, column='cw', value=table.index)

    return table


def cw_has_country(df_dict, exclude=None):
    if exclude is None:
        exclude = []
    df = df_dict["consensus"]
    consensus_table = df[~df.date.isin(exclude)][["date", "region"]]
    cw_country = {el: list(consensus_table.loc[consensus_table["date"] == el, "region"].unique())
                  for el in consensus_table["date"].unique()}
    country_cw = {el: list(consensus_table.loc[consensus_table["region"] == el, "date"].unique())
                  for el in consensus_table["region"].unique()}
    return cw_country, country_cw


def country_time_plot(df_dict, yaxis):
    df = df_dict["consensus"][['date', 'region', 'number_of_sequences']]

    country_numbers = 9

    df = df.drop(df[df.region == 'Wuhan'].index)  # drop Wuhan first case
    df = df.drop(df[df.region == 'Global'].index)  # drop Global.
    # Note:  index still goes up to 415 even though only have 372 rows after dropping
    df.reset_index(drop=True, inplace=True)

    # There is one case in 2013, cause plot shifting.  delete it for now
    df = df.drop(df[df.date == '2013W30'].index)

    df_top_countries = df.groupby('region').sum()
    df_top_countries = df_top_countries.sort_values('number_of_sequences', ascending=False)

    df['date_from_cw'] = [datetime.datetime.strptime(i + '-1', "%YW%W-%w") for i in df['date']]

    # df_weekly = df_weekly.drop('date_from_cw',1)
    df_weekly = df.drop('date', 1)
    # if country not in top 10, replace with other
    df_weekly['region'] = df_weekly['region'].apply(
        lambda x: 'other' if x not in df_top_countries.index[:country_numbers] else x)

    # bring all countries to same maximum date, so that wont have problem in accumulative plot
    max_date = max(df_weekly['date_from_cw'])
    top_countries = df_top_countries.index[:country_numbers].tolist()
    top_countries.append('other')

    to_max_date = []  # more efficient to append a list than dataframe. List of countries that needs to fill to max date
    for t in top_countries:
        if len(df_weekly.loc[df_weekly['region'] == t, 'date_from_cw']) != 0:
            country_max = max(df_weekly.loc[df_weekly['region'] == t, 'date_from_cw'])
            if country_max < max_date:
                to_max_date.append([t, 0, max_date])

    df_to_max_date = pd.DataFrame(to_max_date,
                                  columns=['region', 'number_of_sequences', 'date_from_cw'])

    df_weekly = pd.concat([df_weekly, df_to_max_date])

    df_weekly = df_weekly.set_index(pd.DatetimeIndex(df_weekly['date_from_cw']))
    df_weekly = df_weekly.groupby('region').resample('W').sum()

    df_weekly['date'] = [df_weekly.index[i][1] for i in np.arange(len(df_weekly))]
    df_weekly['country'] = [df_weekly.index[i][0] for i in np.arange(len(df_weekly))]

    df_weekly.reset_index(drop=True, inplace=True)

    # generate a column with accumulative count
    df_weekly['accumulative sequences'] = 0
    df_weekly.loc[0,'accumulative sequences'] = df_weekly['number_of_sequences'][0]
    for i in range(len(df_weekly) - 1):
        i = i + 1
        if df_weekly['country'][i] == df_weekly['country'][i - 1]:

            df_weekly.loc[i,'accumulative sequences'] = df_weekly['number_of_sequences'][i] + df_weekly['accumulative sequences'][
                i - 1]
        else:
            df_weekly.loc[i,'accumulative sequences'] = df_weekly['number_of_sequences'][i]

    df_weekly = df_weekly.rename({'number_of_sequences': 'weekly sequences'}, axis=1)

    fig = px.bar(df_weekly[['date', yaxis, 'country']], x="date", y=yaxis, color="country")

    fig.update_layout(
        font_size=10
    )
    return fig, df_weekly


@lru_cache(maxsize=None)
def weekstr_to_datetime(date_str):
    return datetime.datetime.strptime(date_str+"-1", "%GW%V-%w")

@lru_cache(maxsize=None)
def date_to_weekstr(date):
    return f"{date.isocalendar()[0]}W{'%02d' % date.isocalendar()[1]}"
