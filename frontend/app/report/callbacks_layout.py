from frontend.app.shared.pages import (
    baseFrequencyPlot,
    mutationDistributionPlot,
    consensusChart,
    overviewData,
    worldMap,
    layoutRightMenu,
    gisaid_footnote,
    # add filename of new page here
    )

# from frontend.app.shared.profiler import cumulative_profiler
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
from frontend.app.shared.cache import cache

def register_callbacks(app, db_connection, df_global_dict, text_dict):

    # right side menu bar
    @app.callback(Output("right-side-menu", "children"), [Input("url", "pathname")])
    def display_right_menu(pathname):
        return (
            layoutRightMenu.create_menu_right(app, db_connection, df_global_dict, text_dict)
        )

    # map
    @app.callback(Output("left-side-menu-map", "children"), [Input("url", "pathname")])
    def display_left_menu_map(pathname):
        return (
            worldMap.create_menu_left(app, db_connection, df_global_dict, text_dict),
        )

    @app.callback(Output("page-content-map", "children"), [Input("url", "pathname")])
    def display_page_map(pathname):
        return (
            worldMap.create_layout(app, db_connection, df_global_dict, text_dict),
        )

    # overview data
    @app.callback(Output("left-side-menu-data", "children"), [Input("url", "pathname")])
    def display_left_menu_data(pathname):
        return (
            overviewData.create_menu_left(app, db_connection, df_global_dict, text_dict)
        )

    @app.callback(Output("page-content-data", "children"), [Input("url", "pathname")])
    def display_page_data(pathname):
        return (
            dbc.Spinner(id="overview-data", size='lg', color='primary', type='border')
        )

    # alternative allele frequency plot
    @app.callback(Output("left-side-menu-freq", "children"), [Input("url", "pathname")])
    def display_left_menu_freq(pathname):
        return (
            baseFrequencyPlot.create_menu_left(app, db_connection, df_global_dict, text_dict)
        )

    @app.callback(Output("page-content-freq", "children"), [Input("url", "pathname")])
    def display_page_freq(pathname):
        return (
            dbc.Spinner(id="base-freq-plot", size='lg', color='primary', type='border')
        )

    # mutation distribtuion plot
    @app.callback(Output("left-side-menu-dist", "children"), [Input("url", "pathname")])
    def display_left_menu_dist(pathname):
        return (
            mutationDistributionPlot.create_menu_left(app, db_connection, df_global_dict, text_dict)
        )

    @app.callback(Output("page-content-dist", "children"), [Input("url", "pathname")])
    def display_page_dist(pathname):
        return (
            dbc.Spinner(id="mutation-dist-plot", size='lg', color='primary', type='border'),
        )

    # consensus table
    @app.callback(Output("left-side-menu-cons", "children"), [Input("url", "pathname")])
    def display_left_menu_cons(pathname):
        return (
            consensusChart.create_menu_left(app, db_connection, df_global_dict, text_dict)
        )

    @app.callback(Output("page-content-cons", "children"), [Input("url", "pathname")])
    def display_page_cons(pathname):
        return (
            dbc.Spinner(id="consensus-chart", size='lg', color='primary', type='border')
        )

    # gisaid footer
    # @app.callback(Output("page-content-footer", "children"), [Input("url", "pathname")])
    # def display_page_footer(pathname):
    #     return (
    #         gisaid_footnote.create_layout()
    #     )

    def register_page(page, spinner_id):
        @app.callback(Output(spinner_id, "children"), [Input("url", "pathname")])
        def display_page(pathname):
            return page.create_layout(app, db_connection, df_global_dict, text_dict)

    register_page(overviewData, "overview-data")
    register_page(baseFrequencyPlot, "base-freq-plot")
    register_page(mutationDistributionPlot, "mutation-dist-plot")
    register_page(consensusChart, "consensus-chart")
    # add your new page here
    # register_page(FILENAME, "new-page")

