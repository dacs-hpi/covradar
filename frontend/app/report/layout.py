from dash_core_components import Location
import dash_html_components as html
import dash_bootstrap_components as dbc


layout = dbc.Container(html.Div([Location(id="url", refresh=False),
    html.Div([
        dbc.Row([ # map
            dbc.Col(html.Div(id="left-side-menu-map", className="menu-content"), width=2, className="d-print-none", id="left_col_map"),
            dbc.Col(html.Div(id="page-content-map"), width=8, id="pdf_col_map row-height"),  # pages added in callbacks.py
            dbc.Col(html.Div(id="right-side-menu"), width=2, className="d-print-none", id="right_col")
        ]),
        dbc.Row([ # data
            dbc.Col(html.Div(id="left-side-menu-data", className="menu-content"), width=2, className="d-print-none", id="left_col_data"),
            dbc.Col(html.Div(id="page-content-data", className="page-content"), width=8, id="pdf_col_data"),  # pages added in callbacks.py
        ]),
        dbc.Row([ # stats
            dbc.Col(html.Div(id="left-side-menu-stats", className="menu-content"), width=2, className="d-print-none", id="left_col_stats"),
            dbc.Col(html.Div(id="page-content-stats", className="page-content"), width=8, id="pdf_col_stats"),  # pages added in callbacks.py
        ]),
        dbc.Row([ # freq plot
            dbc.Col(html.Div(id="left-side-menu-freq", className="menu-content"), width=2, className="d-print-none", id="left_col_freq"),
            dbc.Col(html.Div(id="page-content-freq", className="page-content"), width=8, id="pdf_col_freq"),  # pages added in callbacks.py
        ]),
        dbc.Row([ # dist plot
            dbc.Col(html.Div(id="left-side-menu-dist", className="menu-content"), width=2, className="d-print-none", id="left_col_dist"),
            dbc.Col(html.Div(id="page-content-dist", className="page-content"), width=8, id="pdf_col_dist"),  # pages added in callbacks.py
        ]),
        dbc.Row([ # consensus
            dbc.Col(html.Div(id="left-side-menu-cons", className="menu-content"), width=2, className="d-print-none", id="left_col_cons"),
            dbc.Col(html.Div(id="page-content-cons", className="page-content"), width=8, id="pdf_col_cons"),  # pages added in callbacks.py
        ]),
        # dbc.Row([ # gisaid footer
        #     dbc.Col(html.Div(id="left-side-menu-footer", className="menu-content"), width=2, className="d-print-none", id="left_col_footer"),
        #     dbc.Col(html.Div(id="page-content-footer", className="page-content"), width=8, id="pdf_col_footer"),  # pages added in callbacks.py
        # ]),
    ])
]),
fluid=True,
)
