from flask import Blueprint, render_template, url_for
import os
from os import listdir
from os.path import isfile, join
from frontend.app.shared.data import get_database_connection
import pandas as pd
import os

auth_blueprint = Blueprint('auth_blueprint', __name__, static_url_path='/public', static_folder='./public', template_folder="templates/auth/")

# get information for latest and global DB
if 'MYSQL_DBglobal' in os.environ:
    db_connection_global = get_database_connection(os.environ['MYSQL_DBglobal'])
    statistics_global = pd.read_sql_query(f"SELECT date FROM global_statistics", con=db_connection_global)
    modified_global = statistics_global["date"][0]
    number_seqs_global = pd.read_sql_query(f"select number_seq from global_statistics;", con=db_connection_global).values[0][0]
    dates_global = pd.read_sql_query(f"SELECT min_date, max_date FROM global_statistics", con=db_connection_global)
    interval_global = str(dates_global["min_date"][0]) + " to " + str(dates_global["max_date"][0])
else:
    modified_global = ""
    number_seqs_global= "0"
    interval_global = ""

if 'MYSQL_DBlatest' in os.environ:
    db_connection_latest = get_database_connection(os.environ['MYSQL_DBlatest'])
    statistics_latest = pd.read_sql_query(f"SELECT date FROM global_statistics", con=db_connection_latest)
    modified_latest = statistics_latest["date"][0]
    number_seqs_latest = pd.read_sql_query(f"select number_seq from global_statistics;", con=db_connection_latest).values[0][0]
    dates_latest = pd.read_sql_query(f"SELECT min_date, max_date FROM global_statistics", con=db_connection_latest)
    interval_latest = str(dates_latest["min_date"][0]) + " to " + str(dates_latest["max_date"][0])
else:
    modified_latest = ""
    number_seqs_latest= "0"
    interval_latest = ""

# Controllers API
@auth_blueprint.route('/')
def home():
    # dir_path = os.path.dirname(os.path.realpath(__file__)) + "/public/daily_picture"
    # images_maps = [f for f in listdir(dir_path) if isfile(join(dir_path, f)) and f.endswith("_map.svg")]
    # images_plots = [f for f in listdir(dir_path) if isfile(join(dir_path, f)) and f.endswith("_plot.svg")]
    # load_home_html = render_template('home.html')
    # home_page = load_home_html.split("<!-- images do not delete -->")[0]
    # start_row = '<div class="row">'
    # end_div = '</div>'
    # col = '\
    #     <div class="col-md-4">\
    #         <div class="thumbnail">\
    #         <a href="/public/daily_picture/%s">\
    #             <img src="/public/daily_picture/%s" alt="" style="width:100">\
    #         </a>\
    #         </div>\
    #     </div>\l
    # '
    # for i in range(0, len(images_maps), 3):
    #     use_images = images_maps[i:i+3]
    #     home_page += start_row
    #     for im in use_images:
    #         home_page += col % (im, im)
    #     home_page += end_div
    # for i in range(0, len(images_plots), 3):
    #     use_images = images_plots[i:i+3]
    #     home_page += start_row
    #     for im in use_images:
    #         home_page += col % (im, im)
    #     home_page += end_div
    # home_page += load_home_html.split("<!-- images do not delete -->")[1]
    home_page = render_template(
        'home.html',
        modified_global=modified_global,
        modified_latest=modified_latest,
        number_seqs_global=number_seqs_global,
        number_seqs_latest=number_seqs_latest,
        interval_global=interval_global,
        interval_latest=interval_latest)
    return home_page

@auth_blueprint.route('/logout')
def logout():
    # dir_path = os.path.dirname(os.path.realpath(__file__)) + "/public/daily_picture"
    # images_maps = [f for f in listdir(dir_path) if isfile(join(dir_path, f)) and f.endswith("_map.svg")]
    # images_plots = [f for f in listdir(dir_path) if isfile(join(dir_path, f)) and f.endswith("_plot.svg")]
    # load_home_html = render_template('home.html')
    # home_page = load_home_html.split("<!-- images do not delete -->")[0]
    # start_row = '<div class="row">'
    # end_div = '</div>'
    # col = '\
    #     <div class="col-md-4">\
    #         <div class="thumbnail">\
    #         <a href="/public/daily_picture/%s">\
    #             <img src="/public/daily_picture/%s" alt="" style="width:100">\
    #         </a>\
    #         </div>\
    #     </div>\
    # '
    # for i in range(0, len(images_maps), 3):
    #     use_images = images_maps[i:i+3]
    #     home_page += start_row
    #     for im in use_images:
    #         home_page += col % (im, im)
    #     home_page += end_div
    # for i in range(0, len(images_plots), 3):
    #     use_images = images_plots[i:i+3]
    #     home_page += start_row
    #     for im in use_images:
    #         home_page += col % (im, im)
    #     home_page += end_div
    # home_page += load_home_html.split("<!-- images do not delete -->")[1]
    home_page = render_template(
        'home.html',
        modified_global=modified_global,
        modified_latest=modified_latest,
        number_seqs_global=number_seqs_global,
        number_seqs_latest=number_seqs_latest,
        interval_global=interval_global,
        interval_latest=interval_latest)
    return home_page

@auth_blueprint.route('/help')
def helppage():
    app_page = render_template('help_page.html')
    return app_page
