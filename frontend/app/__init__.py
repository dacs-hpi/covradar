import dash
from flask.helpers import get_root_path
from frontend.app.shared.cache import cache

def register_dashapp(app, title, base_pathname, external_scripts, layout, register_callbacks_fun,
                     external_stylesheets, db_connection, df_dict, lang_dict):
    # Meta tags for viewport responsiveness
    # currently empty to be able to view the page on mobile
    meta_viewport = {"name": "viewport", "content": ""}

    my_dashapp = dash.Dash(__name__,
                           server=app,
                           url_base_pathname=f'/{base_pathname}/',
                           assets_folder=get_root_path(__name__) + f'/shared/assets/',
                           meta_tags=[meta_viewport],
                           external_scripts=external_scripts,
                           external_stylesheets=external_stylesheets,
                           suppress_callback_exceptions=True)
    cache.init_app(my_dashapp.server)

    with app.app_context():
        my_dashapp.title = title
        my_dashapp.layout = layout
        for callback in register_callbacks_fun:
            callback(my_dashapp, db_connection, df_dict, lang_dict)
