import unittest
from frontend.app.shared.utils_mutation_time_plot import MutationTimePlot
from frontend.app.shared.data import get_database_connection
import pandas as pd
from pandas._testing import assert_frame_equal
import datetime
import os
import pickle
from frontend.tests.test_utils import dpath


def to_date(x):
    # creates datetime object from string in format YYYY-MM-DD
    return datetime.datetime.strptime(x, "%Y-%m-%d")


class TestFunctions(unittest.TestCase):
    def setUp(self):
        pd.set_option("display.max_rows", None, "display.max_columns", None)
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_consensus_N.pickle"), "rb"))

    def test_mutation_time_plot(self):
        columns = ["count", "date", "aa", "total", "frequency"]
        rows = [[28, to_date('2020-01-12'), "G", 30, 0.933],
                [0, to_date('2020-01-19'), "G", 0, 0.0],
                [0, to_date('2020-01-26'), "G", 0, 0.0],
                [0, to_date('2020-02-02'), "G", 0, 0.0],
                [10, to_date('2020-02-09'), "G", 13, 0.769231]]
        correct_df = pd.DataFrame(rows, columns=columns).round({"frequency": 3})
        mut_obj = MutationTimePlot(self.df_dict,  'Germany', 1841, origin="all origins", hosts=["human"], lineages="all", db_connection=get_database_connection("test_consensus_N"), kind='aa')
        time_plot_df = mut_obj.create_mutation_time_plot("count")[1].round({"frequency": 3})
        assert_frame_equal(time_plot_df.iloc[:5], correct_df, check_datetimelike_compat=True, check_dtype=False)

    def test_mutation_time_plot_country(self):
        columns = ["count", "date", "nt", "total", "frequency"]
        rows =[[30, to_date('2020-01-12'), "A", 30, 1.0],
               [0, to_date('2020-01-19'), "A", 0, 0.0],
               [0, to_date('2020-01-26'), "A", 0, 0.0],
               [0, to_date('2020-02-02'), "A", 0, 0.0],
               [13, to_date('2020-02-09'), "A", 13, 1.0]]
        correct_df = pd.DataFrame(rows, columns=columns)
        mut_obj = MutationTimePlot(self.df_dict,  'Germany', 1, origin="all origins", hosts=["human"], lineages="all", db_connection=get_database_connection("test_consensus_N"),kind='nt')
        time_plot_df = mut_obj.create_mutation_time_plot("count")[1]
        assert_frame_equal(time_plot_df.iloc[:5], correct_df, check_datetimelike_compat=True, check_dtype=False)

        columns = ["count", "date", "aa", "total", "frequency"]
        rows =[[30, to_date('2020-01-12'), "M", 30, 1.0],
               [0, to_date('2020-01-19'), "M", 0, 0.0],
               [0, to_date('2020-01-26'), "M", 0, 0.0],
               [0, to_date('2020-02-02'), "M", 0, 0.0],
               [13, to_date('2020-02-09'), "M", 13, 1.0]]
        correct_df = pd.DataFrame(rows, columns=columns)
        mut_obj = MutationTimePlot(self.df_dict,  'Germany', 2, origin="all origins", hosts=["human"], lineages="all", db_connection=get_database_connection("test_consensus_N"),kind='aa')
        time_plot_df = mut_obj.create_mutation_time_plot("frequency")[1]
        assert_frame_equal(time_plot_df.iloc[:5], correct_df, check_datetimelike_compat=True, check_dtype=False)

    def test_mutation_time_plot_global(self):
        # mutation_time_plot(self.db_connection,  'Global', 100, 'count', kind='aa')[1]
        columns = ["count", "date", "nt", "total", "frequency"]
        rows =[[1, to_date('2020-05-24'), "N", 360 ,  0.002778],
               [1, to_date('2019-12-08'), "G", 1 ,  1.000000],
               [0, to_date('2019-12-15'), "G", 0 ,  0.000000],
               [0, to_date('2019-12-22'), "G", 0 ,  0.000000],
               [0, to_date('2019-12-29'), "G", 0 ,  0.000000],
               [3, to_date('2020-01-05'), "G", 3 ,  1.000000],
               [224, to_date('2020-01-12'), "G", 224 ,  1.000000]]
        correct_df = pd.DataFrame(rows, columns=columns).round({"frequency": 4})
        mut_obj = MutationTimePlot(self.df_dict,  'Global', 1850, origin="all origins", hosts=["human"], lineages="all", db_connection=get_database_connection("test_consensus_N"), kind='nt')
        time_plot_df = mut_obj.create_mutation_time_plot("frequency")[1].round({"frequency": 4})
        assert_frame_equal(time_plot_df.iloc[:7], correct_df, check_datetimelike_compat=True, check_dtype=False)
        assert(len(time_plot_df)==47)
        assert(set(time_plot_df['nt'])== {'N', 'G'})

        columns = ["count", "date", "aa", "total", "frequency"]
        rows =[[1, to_date('2020-06-28'), "C", 661, 0.001513],
               [1, to_date('2019-12-08'), "R", 1, 1.000000],
               [0, to_date('2019-12-15'), "R", 0, 0.0],
               [0, to_date('2019-12-22'), "R", 0, 0.0],
               [0, to_date('2019-12-29'), "R", 0, 0.0],
               [3, to_date('2020-01-05'), "R", 3, 1.0],
               [224, to_date('2020-01-12'), "R", 224, 1.0]]

        correct_df = pd.DataFrame(rows, columns=columns).round({"frequency": 4})
        mut_obj = MutationTimePlot(self.df_dict,  'Global', 100, origin="all origins", hosts=["human"], lineages="all", db_connection=get_database_connection("test_consensus_N"), kind='aa')
        time_plot_df = mut_obj.create_mutation_time_plot("count")[1].round({"frequency": 4})
        assert_frame_equal(time_plot_df.iloc[:7], correct_df, check_datetimelike_compat=True, check_dtype=False)

    def test_global_SNP_and_mut_and_meta_and_global_df(self):
        columns = ["strain_id", "country", "date", "position", "REF_allele", "ALT", "calendar_week", "date_from_cw"]
        rows =[[9044, "USA", to_date('2020-05-14'), 1859, "G", "N", "2020W20",to_date('2020-05-18') ]]
        correct_df = pd.DataFrame(rows, columns=columns)
        mut_obj = MutationTimePlot(self.df_dict,  'Global', 1850, origin="all origins", hosts=["human"], lineages="all", db_connection=get_database_connection("test_consensus_N"), kind='nt')
        global_SNP_df = mut_obj.create_global_SNP_df(mut_obj.msa_position).reset_index(drop=True)
        assert_frame_equal(global_SNP_df, correct_df, check_datetimelike_compat=True, check_dtype=False)
        #                  count date_from_cw nt
        # nt date_from_cw
        # N  2020-05-24        1   2020-05-24  N
        count_row = [1]
        date_from_cw = [to_date('2020-05-24')]
        nt = ["N"]
        df_mut = mut_obj.create_global_mut_df(global_SNP_df)
        assert(list(df_mut["count"]) == count_row)
        assert(list(df_mut["nt"]) == nt)
        assert(list(df_mut["date_from_cw"]) == date_from_cw)

        #                 0 date_from_cw  sum_mut  count
        #            date_from_cw
        #            2019-12-08       1   2019-12-08        0      1
        #            2019-12-15       0   2019-12-15        0      0
        df_meta = mut_obj.get_global_meta_df(df_mut)
        assert(len(df_meta)==46)
        assert(sum(df_meta["count"])==18960)

        columns = ["count", "date_from_cw", "nt"]
        rows =[[1, to_date('2020-05-24'), "N"],
               [1, to_date('2019-12-08'), "G"],
               [0, to_date('2019-12-15'), "G"],
               [0, to_date('2019-12-22'), "G"],
               [0, to_date('2019-12-29'), "G"],
               [3, to_date('2020-01-05'), "G"],
               [224, to_date('2020-01-12'), "G"]]
        correct_df = pd.DataFrame(rows, columns=columns)
        df_bar = mut_obj.get_global_df(df_mut, df_meta)
        assert_frame_equal(df_bar.iloc[:7], correct_df, check_datetimelike_compat=True, check_dtype=False)
        assert(len(df_bar)==47)
        assert(set(df_bar['nt'])== {'N', 'G'})

    # def test_empty_df(self):
    #     """
    #             if df_meta.empty:
    #         df_meta = pd.DataFrame({'date': pd.Series(dtype='datetime64[ns]'), 'country': pd.Series(dtype='str'),
    #                                 'date_from_cw': pd.Series(dtype='datetime64[ns]'),
    #                                 'count': pd.Series(dtype='int32')})
    #     """
    #     df_dict=self.df_dict
    #     df_dict['global_metadata'] = pd.DataFrame({'strain_id': pd.Series(dtype='int32'),
    #                                                'date': pd.Series(dtype='datetime64[ns]'),
    #                                                'country': pd.Series(dtype='str'),
    #                                                'host': pd.Series(dtype='str')})
    #     mut_obj = MutationTimePlot(df_dict,  'Global', 100, origin="all origins", hosts=["human"], lineages="all", db_connection=get_database_connection("test_consensus_N"), kind='aa')
    #     time_plot_df = mut_obj.create_mutation_time_plot("count")[1]
