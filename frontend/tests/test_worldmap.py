import unittest
from frontend.app.shared.utils_world_map import WorldMap, DateSlider
import pandas as pd
from pandas._testing import assert_frame_equal
from datetime import date, timedelta
import os
import pickle
from frontend.tests.test_utils import dpath


def to_date(d):
    return date.fromisoformat(d)


class TestWorldMap(unittest.TestCase):
    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_world_maps.pickle"), "rb"))
        self.world_map = WorldMap(self.df_dict["global_metadata"], self.df_dict["variants_of_concern"]
                                  ,self.df_dict["sequence_voc"], self.df_dict["location_coordinates"])
        self.variants1 = ["L18F", "T20N", "A475V", "D138Y", "E484K"]
        self.variants1_with_wildtype = ["L18F", "T20N", "A475V", "D138Y", "E484K", "wildtype"]
        self.variants2 = ["L18F", "E484K"]
        self.variants2_with_wildtype = ["L18F", "E484K", "wildtype"]
        self.date1 = date.fromisoformat('2022-01-01')
        self.date2 = date.fromisoformat('2022-01-12')
        self.interval1 = 15
        self.interval2 = 2
        self.dates21 = [d for d in [self.date2 - timedelta(days=x) for x in reversed(range(self.interval1))]]

    def test_get_df_for_frequency_bar(self):
        # strainID:voc_id
        # 1:39 , 2, 3, 4:39,40,42,45,46 5:39,40,42,45,46, 8
        # 'L18F': 39, 'T20': 40, 'A475V': 42, 'D138Y: 45, 'E484K'; 46);
        columns = [ "location_ID", "mutations", "number_sequences"]
        rows = [[10115, "A475V", 2],
                [ 10115, "D138Y", 2],
                [10115, "E484K", 2],
                [10115, "L18F", 3],
                [10115, "T20N", 2],
                [10115, "wildtype", 3]]
        correct_df = pd.DataFrame(rows, columns=columns)
        correct_df = correct_df.sort_values(by=['mutations']).reset_index(drop=True)
        df = self.world_map.get_df_for_frequency_bar(self.variants1_with_wildtype, self.dates21, location_ID=10115)
        df = df.sort_values(by=['mutations']).reset_index(drop=True)
        assert_frame_equal(df, correct_df, check_datetimelike_compat=True, check_dtype=False)

    def test_get_slope_bar_plot_lab(self):
        #strain ID: 6:42,46, 7:40, 13:40,42,45, 14:40,42,45, 15:39,45
        # 'L18F': 39, 'T20': 40, 'A475V': 42, 'D138Y: 45, 'E484K'; 46);
        columns = ["location_ID", "mutations", "number_sequences", "date", "slope"]
        rows = [[80331, "A475V", [1, 2], [to_date('2022-01-01'), to_date('2022-01-10')], 0.1111],
                [80331, "D138Y", [0, 3], [to_date('2022-01-01'), to_date('2022-01-10')], 0.3333],
                [80331, "E484K", [1, 0], [to_date('2022-01-01'), to_date('2022-01-10')], -0.1111],
                [80331, "L18F", [0, 1], [to_date('2022-01-01'), to_date('2022-01-10')], 0.1111],
                [80331, "T20N",[1, 2], [to_date('2022-01-01'), to_date('2022-01-10')], 0.1111],
                [80331, "wildtype", [0, 0], [to_date('2022-01-01'), to_date('2022-01-10')], 0.000]]
        correct_df = pd.DataFrame(rows, columns=columns)
        correct_df = correct_df.sort_values(by=['location_ID', 'mutations']).reset_index(drop=True)
        df = self.world_map.get_increase_df(self.dates21, self.variants1_with_wildtype, location_ID=80331)
        df = df.sort_values(by=['location_ID', 'mutations']).reset_index(drop=True)
        df = df.round({"slope": 4})
        assert_frame_equal(df, correct_df, check_datetimelike_compat=True, check_dtype=False)

    def test_get_frequency_development_scatter_plot_lab(self):
        #strain ID: 6:42,46, 7:40
        # 'L18F': 39, 'T20': 40, 'A475V': 42, 'D138Y: 45, 'E484K'; 46);
        columns = ["location_ID","date", "mutations", "number_sequences"]
        rows = [[80331, to_date('2022-01-01'), "A475V", 1],
                [80331, to_date('2022-01-01'), "D138Y", 0],
                [80331, to_date('2022-01-01'), "E484K", 1],
                [80331, to_date('2022-01-01'), "L18F", 0],
                [80331, to_date('2022-01-01'), "T20N", 1],
                [80331, to_date('2022-01-01'), "wildtype", 0],
                [80331, to_date('2022-01-10'), "A475V", 2],
                [80331, to_date('2022-01-10'), "D138Y", 3],
                [80331, to_date('2022-01-10'), "E484K", 0],
                [80331, to_date('2022-01-10'), "L18F", 1],
                [80331, to_date('2022-01-10'), "T20N", 2],
                [80331, to_date('2022-01-10'), "wildtype", 0]
                ]
        correct_df = pd.DataFrame(rows, columns=columns)
        correct_df = correct_df.sort_values(by=['date', 'mutations']).reset_index(drop=True)
        df = self.world_map.get_df_for_scatter_plot(self.variants1_with_wildtype, self.dates21, location_ID=80331)
        df = df.sort_values(by=['date', 'mutations']).reset_index(drop=True)
        assert_frame_equal(df, correct_df, check_datetimelike_compat=True, check_dtype=False)

    def test_get_world_frequency(self):
        # method Frequency
        columns = ["location_ID", "mutations", "number_sequences", 'location', 'lat', 'lon']
        rows = [[ 1, 'A475V', 2, 'France', 46.2276, 2.2137], [2, 'A475V', 1, 'Austria', 47.5162, 14.5501],[10115, "A475V", 2, "Berlin", 32.5337, 13.3872], [30161, 'A475V', 0, "Hannover", 52.3842, 9.7446],[80331, "A475V", 3, "München", 48.1379, 11.5722],
               [ 1, 'D138Y', 0, 'France', 46.2276, 2.2137], [2, 'D138Y', 1, 'Austria', 47.5162, 14.5501] , [10115, "D138Y", 2, "Berlin", 32.5337, 13.3872], [30161, 'D138Y', 0, "Hannover", 52.3842, 9.7446],[80331, "D138Y", 3, "München", 48.1379, 11.5722],
             [ 1, 'E484K', 2, 'France', 46.2276, 2.2137], [2, 'E484K', 1, 'Austria', 47.5162, 14.5501], [10115, "E484K", 2, "Berlin", 32.5337, 13.3872], [30161, 'E484K', 0, "Hannover", 52.3842, 9.7446],[80331, "E484K", 1, "München", 48.1379, 11.5722],
               [ 1, 'L18F', 3, 'France', 46.2276, 2.2137], [2, 'L18F', 1, 'Austria', 47.5162, 14.5501], [10115, "L18F", 3, "Berlin", 32.5337, 13.3872], [30161, 'L18F', 2, "Hannover", 52.3842, 9.7446],[80331, "L18F", 1, "München", 48.1379, 11.5722],
               [ 1, 'T20N', 1, 'France', 46.2276, 2.2137], [2, 'T20N', 1, 'Austria', 47.5162, 14.5501],[10115, "T20N", 2, "Berlin", 32.5337, 13.3872], [30161, 'T20N', 2, "Hannover", 52.3842, 9.7446],[80331, "T20N", 3, "München", 48.1379, 11.5722],
               [ 1, 'wildtype', 1, 'France', 46.2276, 2.2137],[2, 'wildtype', 1, 'Austria', 47.5162, 14.5501] , [10115, "wildtype", 3, "Berlin", 32.5337, 13.3872], [30161, 'wildtype', 2, "Hannover", 52.3842, 9.7446],[80331, "wildtype", 0, "München", 48.1379, 11.5722],
                ]
        correct_df1 = pd.DataFrame(rows, columns=columns)
        correct_df1 = correct_df1.sort_values(by=['location_ID', 'mutations']).reset_index(drop=True)
        df1, _ = self.world_map.get_world_map_df('Frequency', self.variants1_with_wildtype, self.dates21)
        df1 = df1.round({"lat": 4, 'lon': 4})
        df1 = df1.sort_values(by=['location_ID','mutations']).reset_index(drop=True)
        assert_frame_equal(df1, correct_df1, check_datetimelike_compat=True, check_dtype=False)
    
    def test_get_world_map_mutation_proportion(self):
        # method Mutation Proportion all variants, all dates
        columns = ["location_ID", "number_sequences", "mutation_proportion", "location", "lat", "lon" ]
        rows = [[ 1, 4, 75.0, "France", 46.2276, 2.2137],
                [ 2, 2, 50.0, "Austria", 47.5162, 14.5501],
                [10115, 6, 50.0, "Berlin", 32.5337, 13.3872],
                [30161, 4, 50.0, "Hannover", 52.3842, 9.7446],
                [80331, 5, 100.0 , "München", 48.1379, 11.5722]]
        correct_df2 = pd.DataFrame(rows, columns=columns)
        correct_df2 = correct_df2.sort_values(by=['location_ID']).reset_index(drop=True)
        df2, _ = self.world_map.get_world_map_df('Mutation Proportion', self.variants1, self.dates21)
        df2 = df2.round({"lat": 4, 'lon': 4, "mutation_proportion":4})
        df2 = df2.sort_values(by=['location_ID']).reset_index(drop=True)
        assert_frame_equal(df2, correct_df2, check_datetimelike_compat=True, check_dtype=False)

    def test_get_world_map_mutation_proportion_2(self):
        # voc: ['L18F', 'E484K']
        # dates: 29.12.2021 - 12.01.2022
        columns = ["location_ID", "number_sequences", "mutation_proportion", "location", "lat", "lon", ]
        rows = [[1, 4, 75.0, "France", 46.2276, 2.2137],
                [2, 2, 50.0, "Austria", 47.5162, 14.5501],
                [10115, 6, 50.0, "Berlin", 32.5337, 13.3872],
                [30161, 4, 50.0, "Hannover", 52.3842, 9.7446],
                [80331, 5, 40.0, "München", 48.1379, 11.5722]]
        correct_df2b = pd.DataFrame(rows, columns=columns).sort_values(by=['location_ID']).reset_index(drop=True)
        df2b, _ = self.world_map.get_world_map_df('Mutation Proportion', self.variants2, self.dates21)
        df2b = df2b.round({"lat": 4, 'lon': 4, "mutation_proportion": 4}).sort_values(by=['location_ID']).reset_index(drop=True)
        assert_frame_equal(df2b, correct_df2b, check_datetimelike_compat=True, check_dtype=False)

        # method increase
        columns = ["location_ID", "mutations", "number_sequences", "date", "slope", "location", "lat", "lon"]
        rows = [[1, "A475V", [2], [to_date("2022-01-07")], 0.00000, "France", 46.2276, 2.2137],
                [1, "T20N", [1], [to_date("2022-01-07")], 0.0000, "France", 46.2276, 2.2137],
                [2, "A475V", [1], [to_date("2022-01-07")], 0.0000, "Austria", 47.5162, 14.5501],
                [2, "T20N", [1], [to_date("2022-01-07")], 0.0000, "Austria", 47.5162, 14.5501],
                [10115, "A475V", [2,0], [to_date("2022-01-01"), to_date("2022-01-10")], 0.0000, "Berlin", 32.5337, 13.3872],
                [10115, "T20N", [2,0], [to_date("2022-01-01"), to_date("2022-01-10")], 0.0000, "Berlin", 32.5337, 13.3872],
                [30161, "A475V", [0], [to_date("2022-01-10")], 0.0000, "Hannover", 52.3842, 9.7446],
                [30161, "T20N", [2], [to_date("2022-01-10")], 0.0000, "Hannover", 52.3842, 9.7446],
                [80331, "A475V", [1, 2], [to_date("2022-01-01"), to_date("2022-01-10")], 0.1111, "München", 48.1379, 11.5722],
                [80331, "T20N", [1, 2], [to_date("2022-01-01"), to_date("2022-01-10")], 0.1111, "München", 48.1379, 11.5722],
                ]

        correct_df3 = pd.DataFrame(rows, columns=columns)
        df3, _ = self.world_map.get_world_map_df('Increase', ["A475V", "T20N"], self.dates21)
        df3 = df3.round({"lat": 4, 'lon': 4, "slope": 4})
        assert_frame_equal(df3, correct_df3, check_datetimelike_compat=True, check_dtype=False)

    def test_df_with_wildtype_selected_frequency(self):
        mutations = ["wildtype"]
        columns = ["location_ID", "mutations", "number_sequences", "location", "lat", "lon"]
        rows = [[1, "wildtype", 1, "France", 46.2276, 2.2137],
                [2, "wildtype", 1, "Austria", 47.5162, 14.5501],
                [10115, "wildtype", 3, "Berlin", 32.5337, 13.3872],
                [30161, "wildtype", 2, "Hannover", 52.3842, 9.7446],
                [80331, "wildtype", 0, "München", 48.1379, 11.5722]]
        correct_df1 = pd.DataFrame(rows, columns=columns)
        df1, _ = self.world_map.get_world_map_df('Frequency', mutations, self.dates21)
        df1 = df1.round({"lat": 4, 'lon': 4})
        assert_frame_equal(df1, correct_df1, check_datetimelike_compat=True, check_dtype=False)

    def test_df_with_no_vars_selected_mutation_proportion(self):
        mutations = ["wildtype"]
        columns = ["location_ID", "number_sequences", "mutation_proportion", "location", "lat", "lon", ]
        rows = [[1, 4, 25.0, "France", 46.2276, 2.2137],
                [2, 2, 50.0, "Austria", 47.5162, 14.5501],
                [10115, 6, 50.0, "Berlin", 32.5337, 13.3872],
                [30161, 4, 50.0, "Hannover", 52.3842, 9.7446],
                [80331, 5, 0.0, "München", 48.1379, 11.5722]]
        correct_df2 = pd.DataFrame(rows, columns=columns)
        df2, _ = self.world_map.get_world_map_df('Mutation Proportion', mutations, self.dates21)
        df2 = df2.round({"lat": 4, 'lon': 4})
        assert_frame_equal(df2, correct_df2, check_datetimelike_compat=True, check_dtype=False)

    def test_df_with_no_vars_selected_lab_increase(self):
        columns = ["number_sequences", "date", "slope"]
        rows = []
        mutations = []
        correct_df3 = pd.DataFrame(rows, columns=columns)
        df3, _ = self.world_map.get_world_map_df('Increase', mutations, self.dates21)
        assert_frame_equal(df3, correct_df3, check_datetimelike_compat=True, check_dtype=False)

        columns = ["number_sequences", "date", "slope"]
        rows = []
        correct_df5 = pd.DataFrame(rows, columns=columns)
        df5 = self.world_map.get_increase_df(self.dates21, mutations, location_ID=30161)
        assert_frame_equal(df5, correct_df5, check_datetimelike_compat=True, check_dtype=False)

    def test_get_frequency_bar_chart(self):
        fig = self.world_map.get_frequency_bar_chart(self.variants1, self.dates21, location_ID=10115)
        assert(fig['data'][0]['name'] == 'A475V')
        assert(round(list(fig['data'][0]['y'])[0]) == 2)
        assert(fig['data'][1]['name'] == 'D138Y')
        assert(round(list(fig['data'][1]['y'])[0]) == 2)
        assert(fig['data'][2]['name'] == 'E484K')
        assert(round(list(fig['data'][2]['y'])[0]) == 2)
        assert(fig['data'][3]['name'] == 'L18F')
        assert(round(list(fig['data'][3]['y'])[0]) == 3)
        assert(fig['data'][4]['name'] == 'T20N')
        assert(round(list(fig['data'][4]['y'])[0]) == 2)

    def test_get_number_sequences_per_interval(self):
        nb_mut = self.world_map.get_number_sequences_per_interval(self.dates21, self.variants1)
        assert(14 == nb_mut)
        nb_wt = self.world_map.get_number_sequences_per_interval(self.dates21, ["wildtype"])
        assert(7 == nb_wt)

    def test_get_frequency_development_scatter_plot(self):
        fig = self.world_map.get_frequency_development_scatter_plot(self.variants1, 'linear', self.dates21, 80331)
        assert(fig['layout']['xaxis']['ticktext'] == ("2022-01-01", "2022-01-10"))
        assert(fig['data'][0]['legendgroup'] == 'A475V')
        assert(list(fig['data'][0]['x']) == [0, 9])
        assert([round(x) for x in list(fig['data'][0]['y'])] == [1, 2])

    def test_empty_df(self):
        pass

    def test_get_full_df(self):
        columns = ["location_ID", "date", "id_list", "mutations", "number_sequences"]
        rows = [[1, to_date("2022-01-07"), "18", "T20N", 1],
                [1, to_date("2022-01-07"), "17,18,19", "L18F", 3],
                [1, to_date("2022-01-07"), "17,19", "E484K", 2],
                [1, to_date("2022-01-07"), "0", "D138Y", 0],
                [1, to_date("2022-01-07"), "17,19", "A475V", 2],
                [1, to_date("2022-01-07"), "16", "wildtype", 1],
                [2, to_date("2022-01-07"), "21", "L18F", 1],
                [2, to_date("2022-01-07"), "21", "A475V", 1],
                [2, to_date("2022-01-07"), "20", "wildtype", 1],
                [2, to_date("2022-01-07"), "21", "E484K", 1],
                [2, to_date("2022-01-07"), "21", "T20N", 1],
                [2, to_date("2022-01-07"), "21", "D138Y", 1],
                [10115, to_date("2022-01-10"), "0", "T20N", 0],
                [10115, to_date("2022-01-10"), "0", "L18F", 0],
                [10115, to_date("2022-01-10"), "0", "A475V", 0],
                [10115, to_date("2022-01-10"), "0", "D138Y", 0],
                [10115, to_date("2022-01-10"), "0", "E484K", 0],
                [10115, to_date("2022-01-01"), "2,3", "wildtype", 2],
                [10115, to_date("2022-01-10"), "8", "wildtype", 1],
                [10115, to_date("2022-01-01"), "4,5", "A475V", 2],
                [10115, to_date("2022-01-01"), "4,5", "D138Y", 2],
                [10115, to_date("2022-01-01"), "4,5", "E484K", 2],
                [10115, to_date("2022-01-01"), "1,4,5", "L18F", 3],
                [10115, to_date("2022-01-01"), "4,5", "T20N", 2],
                [30161, to_date("2022-01-10"), "0", "E484K", 0],
                [30161, to_date("2022-01-10"), "10,9", "wildtype", 2],
                [30161, to_date("2022-01-10"), "11,12", "L18F", 2],
                [30161, to_date("2022-01-10"), "11,12", "T20N", 2],
                [30161, to_date("2022-01-10"), "0", "D138Y", 0],
                [30161, to_date("2022-01-10"), "0", "A475V", 0],
                [80331, to_date("2022-01-01"), "6", "E484K", 1],
                [80331, to_date("2022-01-01"), "0", "wildtype", 0],
                [80331, to_date("2022-01-01"), "7", "T20N", 1],
                [80331, to_date("2022-01-01"), "0", "D138Y", 0],
                [80331, to_date("2022-01-01"), "0", "L18F", 0],
                [80331, to_date("2022-01-10"), "13,14", "T20N", 2],
                [80331, to_date("2022-01-10"), "15", "L18F", 1],
                [80331, to_date("2022-01-10"), "13,14,15", "D138Y", 3],
                [80331, to_date("2022-01-10"), "13,14", "A475V", 2],
                [80331, to_date("2022-01-10"), "0", "E484K", 0],
                [80331, to_date("2022-01-01"), "6", "A475V", 1],
                [80331, to_date("2022-01-10"), "0", "wildtype", 0],]

        correct_df = pd.DataFrame(rows, columns=columns).sort_values(by=['location_ID', 'date', 'mutations']).reset_index(drop=True)
        df_all_dates_all_voc = self.world_map.df_all_dates_all_voc.sort_values(by=['location_ID', 'date', 'mutations']).reset_index(drop=True)
        correct_df['id_list']= correct_df['id_list'].apply(lambda x: sorted(x.split(',')))
        df_all_dates_all_voc['id_list']= df_all_dates_all_voc['id_list'].apply(lambda x: sorted(x.split(',')))
        assert_frame_equal(df_all_dates_all_voc, correct_df, check_datetimelike_compat=True, check_dtype=False)

    def test_get_most_frequent_map_df(self):
        columns = ["location_ID", "mutations", "number_sequences", "location", "lat", "lon", "scaled_column"]
        rows = [[80331, "A475V", 3, "München", 48.137900, 11.572200, 30],
                [30161, "L18F", 2, "Hannover", 52.384200, 9.744600, 20],
                [10115, "L18F", 3, "Berlin", 32.533700, 13.387200, 30],
                [2, "A475V", 1, "Austria", 47.516231, 14.550072, 10],
                [1, "L18F", 3, "France", 46.227638, 2.213749, 30]]
        correct_df = pd.DataFrame(rows, columns=columns).sort_values(by=['location_ID','mutations']).reset_index(drop=True)

        nth=1
        world_map_df, column_of_interest = self.world_map.get_world_map_df("Frequency", self.variants1, self.dates21)
        df = self.world_map.get_most_frequent_map_df(world_map_df, column_of_interest, nth).sort_values(by=['location_ID','mutations']).reset_index(drop=True)
        assert_frame_equal(df, correct_df, check_datetimelike_compat=True, check_dtype=False)


class TestDateSlider(unittest.TestCase):
    def setUp(self):
        global_metadata = pickle.load(open(dpath("pickle_dumps/test_world_maps.pickle"), "rb"))["global_metadata"]
        self.date_slider = DateSlider(global_metadata)

    def test_dates(self):
        datetime_list = [date(2022, 1, 1), date(2022, 1, 2), date(2022, 1, 3),
                         date(2022, 1, 4), date(2022, 1, 5), date(2022, 1, 6),
                         date(2022, 1, 7), date(2022, 1, 8), date(2022, 1, 9),
                         date(2022, 1, 10)]
        assert(self.date_slider.min_date == date(2022, 1, 1))
        assert(self.date_slider.max_date == date(2022, 1, 10))
        assert(self.date_slider.date_list == datetime_list)
