import unittest
from pandas._testing import assert_frame_equal
import pandas as pd
import numpy as np
import datetime
import math
import os
import pickle

from frontend.app.shared.utils import freq_by_consensus, consensustable, cw_has_country, filter_lineages
from frontend.app.shared.utils_data_table import CountryTimePlot

DB_DUMP_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), "sql_dumps")


def to_date(x):
    # creates datetime object from string in format YYYY-MM-DD
    return datetime.datetime.strptime(x, "%Y-%m-%d").date()

def dpath(path):
    """
    get path to a data file (relative to the directory this
    test lives in)
    """
    return os.path.realpath(os.path.join(os.path.dirname(__file__), path))

class TestCwHasCountry(unittest.TestCase):
    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_consensus.pickle"), "rb"))

    def test_cw_has_country(self):
        columns = ["date", "region"]
        rows = [["2019W40", "Germany"],
                ["2020W10", "Germany"],
                ["2020W40", "Germany"],
                ["2019W40", "Norway"],
                ["2020W10", "Norway"],
                ["2020W40", "Norway"],
                ["first-case", "Wuhan"]]
        correct_df = pd.DataFrame(rows, columns=columns)
        df = self.df_dict["consensus"]
        consensus_table = df[~df.date.isin([])][["date", "region"]]
        assert_frame_equal(consensus_table, correct_df, check_datetimelike_compat=True, check_dtype=False)

        cw_country, country_cw = cw_has_country(self.df_dict)
        correct_cw_country = {'2019W40': ['Germany', 'Norway'], '2020W10': ['Germany', 'Norway'],
                              '2020W40': ['Germany', 'Norway'], 'first-case': ['Wuhan']}
        correct_country_cw = {'Germany': ['2019W40', '2020W10', '2020W40'], 'Norway': ['2019W40', '2020W10', '2020W40'],
                              'Wuhan': ['first-case']}
        assert (cw_country == correct_cw_country)
        assert (country_cw == correct_country_cw)


class TestFreq(unittest.TestCase):

    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_freq.pickle"), "rb"))
        self.mutation_table = pd.DataFrame.from_dict(pickle.load(open(dpath("pickle_dumps/test_freq_m.pickle"), "rb")))
        self.total_seqs = 3
        self.country_consensus = "x"
        cons = self.df_dict['consensus']
        self.spike_length = len(cons[cons['date'] == 'first-case']['consensus'].reset_index(drop=True).iloc[0])

    def test_freqs(self):
        # tests if right frequencies are calculated
        cw = "2020W36"
        freq_list, base_cov = freq_by_consensus(self.df_dict, cw, self.country_consensus, self.mutation_table,
                                                self.total_seqs, self.spike_length)
        correct_freq = np.array(
            [float(1 / 3), float(2 / 3), float(2 / 3), 1, float(1 / 3), 0, float(1 / 3), 0, float(1 / 3), 0, 0,
             float(1 / 3), 0, float(1 / 3), 0, float(1 / 3), 0, float(1 / 3), float(1 / 3), 0, float(1 / 3), 0,
             float(1 / 3), 0, float(1 / 3), 0])
        correct_cov = np.array([1, 2, 2, 3, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0])
        np.testing.assert_array_equal(freq_list, correct_freq)
        np.testing.assert_array_equal(base_cov, correct_cov)

    def test_freqs2(self):
        # tests if right frequencies are calculated when consensus is first-case
        cw = "first-case"
        freq_list, base_cov = freq_by_consensus(self.df_dict, cw, self.country_consensus, self.mutation_table,
                                                self.total_seqs, self.spike_length)
        correct_freq = np.array(
            [float(1 / 3), float(2 / 3), float(2 / 3), 0, float(1 / 3), 0, float(1 / 3), 0, float(1 / 3), 0, 0,
             float(1 / 3), 0, float(1 / 3), 0, float(1 / 3), 0, float(1 / 3), float(1 / 3), 0, float(1 / 3), 0,
             float(1 / 3), 0, float(1 / 3), 0])
        correct_cov = np.array([1, 2, 2, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0])
        np.testing.assert_array_equal(freq_list, correct_freq)
        np.testing.assert_array_equal(base_cov, correct_cov)


class TestFreq2(unittest.TestCase):
    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_freq2.pickle"), "rb"))

    def test_freqs2_1(self):
        # tests if frequencies are 0 if no sequences are selected
        country = "Germany"
        start_date = "2020-01-01"
        end_date = "2020-10-19"
        origin = "all origins"
        calendarweek = "2020W03"
        country_consensus = "China"
        hosts = "Felis catus"
        lineages = "all"
        spike_length = 3831
        freq_list = 3831 * [math.nan]
        total_seqs = 0
        base_cov = 3831 * [0]
        correct_freq = np.full(spike_length, math.nan)
        correct_cov = np.zeros(spike_length)
        self.assertEqual(total_seqs, 0)
        np.testing.assert_array_equal(freq_list, correct_freq)
        np.testing.assert_array_equal(base_cov, correct_cov)


class TestAlleleFreq(unittest.TestCase):
    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_sql_load.pickle"), "rb"))

    def test_filter_lineages(self):
        lineage_column = ['A', 'A.1', 'A.2', 'A.2.2', 'A.2', 'A.2.2', 'B', 'B.1', 'B.1.2', 'B.2', 'B.2.2', 'C', 'C.1']
        lineages = ['A', 'B.1']
        correct_bool_list = [True, True, True, True, True, True, False, True, True, False, False, False, False]
        bool_list = filter_lineages(lineage_column, lineages)
        assert (correct_bool_list == bool_list)

class TestConsensus(unittest.TestCase):

    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_consensus.pickle"), "rb"))

    def test_consensustable_1(self):
        # tests if consensus table is correct
        # all positions, all calendar weeks
        pos1, pos2, cw1, cw2, region = 1, 6, "2019W40", "2020W40", "Germany"
        cons_table = consensustable(self.df_dict, pos1, pos2, cw1, cw2, region)
        rows = [["msa", "number_of_sequences", "2", "2", "4", "4", "6", "6"],
                ["ref", "number_of_sequences", "1.1", "1.1", "2", "2", "4", "4"],
                ["Wuhan-Hu-01", 1, "A", 1, "A", 1, "A", 1],
                ["2020W40", 120, ".", 87, "T", 85, "G", 83],
                ["2020W10", 110, "T", 93, "T", 91, "G", 89],
                ["2019W40", 100, "T", 99, "T", 97, "T", 95]]

        correct = pd.DataFrame(rows, columns=["cw", "number_of_sequences", 2, "2_cov", 4, "4_cov", 6, "6_cov"])
        correct.index = correct["cw"]
        correct.index.name = None
        assert_frame_equal(cons_table, correct)

    def test_consensustable_2(self):
        # tests if consensus table is correct
        # position interval, all calendar weeks
        pos1, pos2, cw1, cw2, region = 2, 5, "2019W40", "2020W40", "Germany"
        cons_table = consensustable(self.df_dict, pos1, pos2, cw1, cw2, region)
        rows = [["msa", "number_of_sequences", "2", "2", "4", "4"],
                ["ref", "number_of_sequences", "1.1", "1.1", "2", "2"],
                ["Wuhan-Hu-01", 1, "A", 1, "A", 1],
                ["2020W40", 120, ".", 87, "T", 85],
                ["2020W10", 110, "T", 93, "T", 91],
                ["2019W40", 100, "T", 99, "T", 97]]

        correct = pd.DataFrame(rows, columns=["cw", "number_of_sequences", 2, "2_cov", 4, "4_cov"])
        correct.index = correct["cw"]
        correct.index.name = None
        assert_frame_equal(cons_table, correct)

    def test_consensustable_3(self):
        # tests if consensus table is correct
        # all positions, calendar week interval
        pos1, pos2, cw1, cw2, region = 1, 6, "2020W10", "2020W10", "Germany"
        cons_table = consensustable(self.df_dict, pos1, pos2, cw1, cw2, region)
        rows = [["msa", "number_of_sequences", "2", "2", "4", "4", "6", "6"],
                ["ref", "number_of_sequences", "1.1", "1.1", "2", "2", "4", "4"],
                ["Wuhan-Hu-01", 1, "A", 1, "A", 1, "A", 1],
                ["2020W10", 110, "T", 93, "T", 91, "G", 89]]

        correct = pd.DataFrame(rows, columns=["cw", "number_of_sequences", 2, "2_cov", 4, "4_cov", 6, "6_cov"])
        correct.index = correct["cw"]
        correct.index.name = None
        assert_frame_equal(cons_table, correct)

    def test_consensustable_4(self):
        # tests if consensus table is correct
        # all positions, all calendar weeks, another country
        pos1, pos2, cw1, cw2, region = 1, 6, "2019W40", "2020W40", "Norway"
        cons_table = consensustable(self.df_dict, pos1, pos2, cw1, cw2, region)
        rows = [["msa", "number_of_sequences", "1", "1", "2", "2", "3", "3", "5", "5"],
                ["ref", "number_of_sequences", "1", "1", "1.1", "1.1", "1.2", "1.2", "3", "3"],
                ["Wuhan-Hu-01", 1, "A", 1, "A", 1, "A", 1, "A", 1],
                ["2020W40", 210, "G", 178, "T", 177, ".", 176, "T", 174],
                ["2020W10", 200, "G", 184, ".", 183, "T", 182, "T", 180],
                ["2019W40", 190, "T", 190, ".", 189, "T", 188, "T", 186]]

        correct = pd.DataFrame(rows,
                               columns=["cw", "number_of_sequences", 1, "1_cov", 2, "2_cov", 3, "3_cov", 5, "5_cov"])
        correct.index = correct["cw"]
        correct.index.name = None
        assert_frame_equal(cons_table, correct)


class TestConsensusN(unittest.TestCase):
    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_consensus_N.pickle"), "rb"))

    def test_consensustable_N1(self):
        # tests if consensus table is correct
        # columns with only reference base or N get removed
        pos1, pos2, cw1, cw2, region = 1, 3831, "2020W20", "2020W20", "Germany"
        cons_table = consensustable(self.df_dict, pos1, pos2, cw1, cw2, region)
        rows = [["msa", "number_of_sequences", "56", "56", "1850", "1850", "2178", "2178"],
                ["ref", "number_of_sequences", "56", "56", "1841", "1841", "2169", "2169"],
                ["Wuhan-Hu-01", 1, "C", 1, "A", 1, "C", 1],
                ["2020W20", 1, "T", 1, "G", 1, "T", 1]]

        correct = pd.DataFrame(rows,
                               columns=["cw", "number_of_sequences", 56, "56_cov", 1850, "1850_cov", 2178, "2178_cov"])
        correct.index = correct["cw"]
        correct.index.name = None
        assert_frame_equal(cons_table, correct)

    def test_allele_freq_to_consensustable_N2(self):
        # frequency is None, if consensus base is N
        cw = "2020W20"  # contains N calls
        host = ["human"]
        mutation_table = pd.DataFrame.from_dict(pickle.load(open(dpath("pickle_dumps/test_consensus_N_m.pickle"), "rb")))
        total_seqs = 1
        country_consensus = "Germany"
        spike_length = 3831

        freq_list, base_cov = freq_by_consensus(self.df_dict, cw, country_consensus, mutation_table, total_seqs,
                                                spike_length)
        positions_none = [3647, 3648, 3649, 3650, 3651, 3652, 3653, 3654, 3655, 3656, 3657, 3658, 3659, 3660, 3661,
                          3662, 3663, 3664, 3665, 3666, 3667, 3668]
        np.testing.assert_array_equal(freq_list[positions_none], np.full(freq_list[positions_none].shape, math.nan))

        # tests if all mutations in consensus sequence are also in frequency plot
        pos1, pos2, cw1, cw2, region = 1, spike_length, "2020W20", "2020W20", "Germany"
        cons_table = consensustable(self.df_dict, pos1, pos2, cw1, cw2, region)
        snp_positions = [int(i) for i in set(cons_table.iloc[0, 2:].to_list())]
        bool_list = freq_list[[i - 1 for i in snp_positions]] >= 0.5
        self.assertTrue(bool_list.all())


class TestDataTable(unittest.TestCase):
    def setUp(self):
        self.df_dict = pickle.load(open(dpath("pickle_dumps/test_consensus_N.pickle"), "rb"))
        self.country_plot = CountryTimePlot(self.df_dict["consensus"])

    def test_create_fig(self):
        """ df: ['region', 'weekly sequences', 'date_from_cw', 'date', 'country',
       'accumulative sequences']
        region                            object
        weekly sequences                   int64
        date_from_cw              datetime64[ns]
        date                      datetime64[ns]
        country                           object
        accumulative sequences             int64
        """
        expected_x_values = [datetime.datetime(2020, 1, 12, 0, 0),
                             datetime.datetime(2020, 1, 19, 0, 0),
                             datetime.datetime(2020, 1, 26, 0, 0),
                             datetime.datetime(2020, 2, 2, 0, 0),
                             datetime.datetime(2020, 2, 9, 0, 0),
                             datetime.datetime(2020, 2, 16, 0, 0),
                             datetime.datetime(2020, 2, 23, 0, 0),
                             datetime.datetime(2020, 3, 1, 0, 0),
                             datetime.datetime(2020, 3, 8, 0, 0),
                             datetime.datetime(2020, 3, 15, 0, 0),
                             datetime.datetime(2020, 3, 22, 0, 0),
                             datetime.datetime(2020, 3, 29, 0, 0),
                             datetime.datetime(2020, 4, 5, 0, 0),
                             datetime.datetime(2020, 4, 12, 0, 0),
                             datetime.datetime(2020, 4, 19, 0, 0),
                             datetime.datetime(2020, 4, 26, 0, 0),
                             datetime.datetime(2020, 5, 3, 0, 0),
                             datetime.datetime(2020, 5, 10, 0, 0),
                             datetime.datetime(2020, 5, 17, 0, 0),
                             datetime.datetime(2020, 5, 24, 0, 0),
                             datetime.datetime(2020, 5, 31, 0, 0),
                             datetime.datetime(2020, 6, 7, 0, 0),
                             datetime.datetime(2020, 6, 14, 0, 0),
                             datetime.datetime(2020, 6, 21, 0, 0),
                             datetime.datetime(2020, 6, 28, 0, 0),
                             datetime.datetime(2020, 7, 5, 0, 0),
                             datetime.datetime(2020, 7, 12, 0, 0),
                             datetime.datetime(2020, 7, 19, 0, 0),
                             datetime.datetime(2020, 7, 26, 0, 0),
                             datetime.datetime(2020, 8, 2, 0, 0),
                             datetime.datetime(2020, 8, 9, 0, 0),
                             datetime.datetime(2020, 8, 16, 0, 0),
                             datetime.datetime(2020, 8, 23, 0, 0),
                             datetime.datetime(2020, 8, 30, 0, 0),
                             datetime.datetime(2020, 9, 6, 0, 0),
                             datetime.datetime(2020, 9, 13, 0, 0),
                             datetime.datetime(2020, 9, 20, 0, 0),
                             datetime.datetime(2020, 9, 27, 0, 0),
                             datetime.datetime(2020, 10, 4, 0, 0),
                             datetime.datetime(2020, 10, 11, 0, 0),
                             datetime.datetime(2020, 10, 18, 0, 0)]
        expected_y_values = [4, 0, 0, 0, 13, 3, 0, 0, 13, 26, 47, 60, 41, 28, 18, 8, 2, 8,
                             2, 1, 1, 0, 3, 2, 11, 11, 9, 5, 10, 9, 14, 23, 11, 12, 2, 0,
                             0, 5, 13, 13, 27]
        fig = self.country_plot.create_fig("weekly sequences")
        assert (list(fig['data'][0]['x']) == expected_x_values)
        assert (list(fig['data'][0]['y']) == expected_y_values)

    def test_df(self):
        sum_acc_seq = 9873
        sum_weekly_seq = 455
        assert (len(self.country_plot.df_weekly) == 41)
        assert (self.country_plot.df_weekly["accumulative sequences"].sum() == sum_acc_seq)
        assert (self.country_plot.df_weekly["weekly sequences"].sum() == sum_weekly_seq)
