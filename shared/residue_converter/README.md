# Table of Content
- [1. Nucleotide and Amino Acid Translations](#nt_aa_trans)
    - [1.1 aa2nt Tool](#aa2nt)
    - [1.2 nt2aa Tool](#nt2aa)
    - [1.3 nt_seq2aa_seq Tool](#nt_seq2aa_seq)
- [2. Position Conversion](#position_conversion)
    - [2.1 aa2na_gene Tool](#aa2na_gene)
    - [2.2 aa2na_genome Tool](#aa2na_genome)
    - [2.3 na2aa_gene Tool](#na2aa_gene)
    - [2.4 na2aa_genome Tool](#na2aa_genome)
    - [2.5 na_gene_to_genome Tool](#na_gene_to_genome)
    - [2.6 na_genome_to_gene Tool](#na_genome_to_gene)
- [3. Comment on deletions](#deletion)
- [4. Comment on frame shifts/ ribosomal slippage](#slippage)


# 1. Nucleotide and Amino Acid Translations <a name="nt_aa_trans"></a>

To translate amino acid mutations to nucleotide mutations and vice versa the `converter.py` script can be used. The script provides the following tools for this task:

| subcommand       | purpose                                                                                                          | example      |
|------------------|------------------------------------------------------------------------------------------------------------------|--------------|
| aa2nt            | Reverse-translates an AA substitution or deletion pattern into the corresponding possible nt mutation pattern(s) | aa variant: R190S --> G22132T / G22132C / G22132Y |
| nt2aa            | Translates an nt substitution pattern (genomic position) into the corresponding AA substitution pattern          | nt variant: G22132T --> R190S |
| nt_seq2aa_seq    | Translates nucleotide sequence to corresponding amino acid sequence                                              | nt seq: CATTTCCAG --> HFQ |

Each tool provides a help page that can be accessed with the -h option. 

## 1.1 aa2nt Tool <a name="aa2nt"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --mut STR                            | amino acid mutation pattern (e.g R190S)                                          | required                          |
| --gene STR                           | gene name                                                                        | Default: S                        |
| --nt_possible_mutations_distance STR | maximum Hamming distance between the reference codon and possible mutated codons | Default: 1                        |
| --ref_path STR                       | FASTA file containing the reference sequence                                     | Default: NC_045512.2              |
| --gff_path STR                       | GFF file containing the genome annotation                                        | Default: NC_045512.2              |

The `--gene` argument can be set one of the following options:
ORF1ab, S, ORF3a, E, M, ORF6, ORF7a, ORF7b, ORF8, N or ORF10.

```
# exemplary execution

python converter.py aa2nt --mut R190S
# AA variant: R190S --> G22132T / G22132C / G22132Y

python converter.py aa2nt --mut I18L --gene ORF6
# AA variant: I18L --> A27253C
```

## 1.2 nt2aa Tool <a name="nt2aa"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --mut STR                            | nucleotide mutation pattern (e.g G22132T)                                        | required                          |
| --gene STR                           | gene name                                                                        | Default: S                        |
| --nt_possible_mutations_distance STR | maximum Hamming distance between the reference codon and possible mutated codons | Default: 1                        |
| --ref_path STR                       | FASTA file containing the reference sequence                                     | Default: NC_045512.2              |
| --gff_path STR                       | GFF file containing the genome annotation                                        | Default: NC_045512.2              |

The `--gene` argument can be set one of the following options:
ORF1ab, S, ORF3a, E, M, ORF6, ORF7a, ORF7b, ORF8, N or ORF10.

```
# exemplary execution

python converter.py nt2aa --mut G22132T
# nt variant: G22132T --> R190S

python converter.py nt2aa --mut A27253C --gene ORF6
# nt variant: A27253C --> I18L
```

## 1.3 nt_seq2aa_seq Tool <a name="nt_seq2aa_seq"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --seq STR                            | sequence of nucleotides divisible by 3 (e.g. GGTACT)                             | required                          |

```
# exemplary execution

python converter.py nt_seq2aa_seq --seq CATTTCCAG
# CATTTCCAG --> HFQ
```

# 2. Position Conversion <a name="position_conversion"></a>

The `converter.py` script further offers tools for position conversion tasks:

| subcommand         | purpose                                                                               | example                             |
|--------------------|---------------------------------------------------------------------------------------|-------------------------------------|
| aa2na_gene         | Convert amino acid position to gene position                                          | aa pos: 484 --> (1450, 1452)        |
| aa2na_genome       | Convert amino acid position within gene to whole-genome position                      | aa pos: 484 --> (23012, 23014)      |
| na2aa_gene         | Convert nucleotide position within gene to amino acid position within gene            | na pos: 1450 --> 484                |
| na2aa_genome       | Convert whole-genome nucleotide position to amino acid position within gene           | na pos: 23012 --> 484               |
| na_gene_to_genome  | Convert gene position to whole-genome position                                        | na pos: 1450 --> 23012              |
| na_genome_to_gene  | Convert whole-genome position to gene position                                        | na pos: 23012 --> 1450              |

Again, each tool provides a help page that can be accessed with the -h option.

## 2.1 aa2na_gene Tool <a name="aa2na_gene"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --aa_pos INT                         | amino acid position                                                              | required                          |

```
# exemplary execution

python converter.py aa2na_gene --aa_pos 484
# (1450, 1452)
```

## 2.2 aa2na_genome Tool <a name="aa2na_genome"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --aa_pos INT                         | amino acid position within gene                                                  | required                          |
| --gene STR                           | gene name                                                                        | Default: S                        |

```
# exemplary execution

python converter.py aa2na_genome --aa_pos 484
# (23012, 23014)

python converter.py aa2na_genome --aa_pos 18 --gene ORF6
# (27253, 27255)
```

## 2.3 na2aa_gene Tool <a name="na2aa_gene"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --na_pos INT                         | gene nucleotide position                                                         | required                          |
| --gene STR                           | gene name                                                                        | Default: S                        |

```
# exemplary execution

python converter.py na2aa_gene --na_pos 1450
# 484
```

## 2.4 na2aa_genome Tool <a name="na2aa_genome"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --na_pos INT                         | whole genome position                                                            | required                          |
| --gene STR                           | gene name                                                                        | Default: S                        |

```
# exemplary execution

python converter.py na2aa_genome --na_pos 23012
# 484

python converter.py na2aa_genome --na_pos 27253 --gene ORF6
# 18
```

## 2.5 na2aa_genome Tool <a name="na2aa_genome"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --na_pos INT                         | gene nucleotide position                                                         | required                          |
| --gene STR                           | gene name                                                                        | Default: S                        |

```
# exemplary execution

python converter.py na_gene_to_genome --na_pos 1450
# 23012

python converter.py na_gene_to_genome --na_pos 52 --gene ORF6
# 27253
```

## 2.6 na_genome_to_gene Tool <a name="na_genome_to_gene"></a>

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --na_pos INT                         | whole-genome position                                                            | required                          |
| --gene STR                           | gene name                                                                        | Default: S                        |

```
# exemplary execution

python converter.py na_genome_to_gene --na_pos 23012
# 1450

python converter.py na_genome_to_gene --na_pos 27253 --gene ORF6
# 52
```

# 3. Comment on deletions <a name="deletion"></a>
Only deletions starting at the start of a codon are properly supported. Predicting the effect of anarbitrary deletion is complicated, especially when we only know the amino acid deletion. For example, an important variant HV69–70del could correspond to del:21765:6 (starts in the middle of the codon, confirmed in the literature) or del:21767:6 (would start at the beginning of the codon). We can only efficiently detect the possible deletions starting at the beginning of the codon. In this case we add a message "other mutations possible" to the returned list of possible nucleotide mutations. Incase of translating nucleotide deletions, we support only codon-starting positions and throw an exception otherwise.

# 4. Comment on frame shifts/ ribosomal slippage <a name="slippage"></a>
The position converter can also work with other genes than the spike. However, a special problem is then related to the fact that ORF1a encodes the polyprotein pp1a (which then gets cut into individualmature proteins), while the "truncated" part of ORF1a (up to the ribosomal slippage) plus the downstream ORF1b encodethe pp1ab polyprotein. Both ORF1a and ORF1b are parts of ORF1ab. A correct annotation for example in a GFF file canbe difficult. The original GFF file from NCBI for Wuhan-Hu-1 uses the annotation "ORF1ab" for all those regions. In the CovRadar GFF file underlying the position converter script ORF1a is annotated as ORF1a. ORF1b is annotated as ORF1b. The truncated part of ORF1a that gets translated if ORF1b is being translated is annotated as ORF1a_trunc.
