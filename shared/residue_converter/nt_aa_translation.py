"""
Translate amino acid mutations to nt mutations and vice versa.

@hrichard @JakubBartoszewicz @MelaniaNowicka
"""

from functools import lru_cache
import re

from frontend.app.shared.profiler import app_line_profiler
try:
    from shared.residue_converter.data_loader import get_gene_pos, get_reference, get_aa_dict, get_codon_dict
    from shared.residue_converter.position_converter import *
except ModuleNotFoundError:  # pragma: no cover
    from data_loader import get_gene_pos, get_reference, get_aa_dict, get_codon_dict
    from position_converter import *
from textwrap import wrap


class Mutation:
    """
    A mutation class storing information about mutation pattern, positions, etc.

    Attributes
    ----------

    gene : str
        gene name, e.g., "S"
    aa_mutation_pattern : str
      mutation pattern in aa+pos+aa format, e.g., R190S
    aa_reference : list
        reference aa(s)
    aa_variant : str
        mutated aa variant
    aa_position_start : int
        aa start position in gene
    aa_position_end : int
        aa end position in gene
    nt_possible_mutations : list
        possible nt mutations, e.g., ['G22345T', 'A24535T']
    nt_possible_mutations_distance : int
        max hamming distance between codons

    nt_mutation_pattern : str
       mutation pattern in nt+genomic_pos+nt format, e.g., G22345T
    nt_reference : str
        reference nt
    nt_variant :  str
        mutated nt variant
    aa_changed : bool
      if False - no change of aa, elif True - change of aa
    nt_genomic_position_start : int
        nt start position in genome
    nt_genomic_position_end : int
        nt end position in genome
    nt_gene_position_start : int
        nt start position in gene
    nt_gene_position_end : int
        nt end position in gene

    Methods
    -------
    to_string(self)
        returns a string representation of mutation patterns

     @JakubBartoszewicz @MelaniaNowicka
    """
    __slots__ = (
        'gene',
        
        'aa_mutation_pattern',
        'aa_reference',
        'aa_variant',
        'aa_position_start',
        'aa_position_end',
        'nt_possible_mutations',
        'nt_possible_mutations_distance',
        
        'nt_mutation_pattern',
        'nt_reference',
        'nt_variant',
        'aa_changed',
        'nt_genomic_position_start',
        'nt_genomic_position_end',
        'nt_gene_position_start',
        'nt_gene_position_end',
    )

    def __init__(self):
        self.gene = None  # gene name
        # aa
        self.aa_mutation_pattern = None  # e.g., R190S (aa-pos-aa)
        self.aa_reference = None  # reference aa (deletions -> list)
        self.aa_variant = None  # mutated aa variant
        self.aa_position_start = None  # aa start position in gene
        self.aa_position_end = None  # aa end position in gene
        self.nt_possible_mutations = None  # possible nt mutations
        self.nt_possible_mutations_distance = None  # max hamming distance between codons
        # nt
        self.nt_mutation_pattern = None  # e.g., C225T (nt-pos-nt)
        self.nt_reference = None  # reference nt
        self.nt_variant = None  # mutated nt variant
        self.aa_changed = None  # if 0 - no change of aa, elif 1 - change of aa
        self.nt_genomic_position_start = None  # nt start position in genome
        self.nt_genomic_position_end = None  # nt end position in genome
        self.nt_gene_position_start = None  # nt start position in gene
        self.nt_gene_position_end = None  # nt end position in gene

    def __str__(self):
        if self.nt_mutation_pattern is not None:
            return f"nt variant: {self.nt_mutation_pattern} --> {self.aa_mutation_pattern}"
        else:
            possible_mutations = " / ".join(self.nt_possible_mutations)
            return f"AA variant: {self.aa_mutation_pattern} --> {possible_mutations}"

    def to_string(self):
        return self.__str__()


def translate_aa_to_nt(aa_mutation_pattern, gene="S", nt_possible_mutations_distance=1, ref_path=None, gff_path=None):
    """
    Reverse-translates an AA substitution or deletion pattern into the corresponding possible nt mutation pattern(s).

    Parameters
    ----------
    aa_mutation_pattern : str
        amino acid mutation pattern (e.g R190S)
    gene : str
        gene name
    nt_possible_mutations_distance : int
        maximum Hamming distance between the reference codon and possible mutated codons.
    ref_path : str
        load the reference from file in path. Use the hardcoded reference sequence if None. (Default: None)
    gff_path : str
        load the annotation from file in path. Use the hardcoded reference annotation if None. (Default: None)

    Returns
    -------
    m : Mutation
        instance of the Mutation class describing the AA mutation and possible nt mutations

    @hrichard, @MelaniaNowicka, @JakubBartoszewicz
    """

    m = Mutation()

    m.nt_possible_mutations_distance = nt_possible_mutations_distance  # max hamming distance between codons

    m.aa_mutation_pattern = aa_mutation_pattern
    m.gene = gene

    m.aa_reference, m.aa_position_start, m.aa_position_end, m.aa_variant = \
        parse_aa_mutation(aa_mutation_pattern)

    gene_position = get_gene_pos(path=gff_path)
    ref_sequence = get_reference(path=ref_path)

    if m.gene not in gene_position:
        raise ValueError(f"Error gene {m.gene} is not in the annotation.")

    m.nt_genomic_position_start, m.nt_genomic_position_end = \
        (aa2na_genome(m.aa_position_start, gene=m.gene)[0], aa2na_genome(m.aa_position_end, gene=m.gene)[1])

    m.nt_gene_position_start = na_genome_to_gene(m.nt_genomic_position_start, gene=m.gene)
    m.nt_gene_position_end = na_genome_to_gene(m.nt_genomic_position_end, gene=m.gene)

    codon_reference = ""
    for i, aa in enumerate(m.aa_reference):
        codon_reference = get_codon(m.nt_genomic_position_start + (i * 3), ref_sequence)
        if aa != get_aa_dict()[codon_reference]:
            raise ValueError(f"Reference AA at position {m.aa_position_start + i} ({get_aa_dict()[codon_reference]}) "
                             f"is not {aa}.")

    if m.aa_variant == "del":
        # put the deletion pattern here
        m.nt_possible_mutations = \
            [f"del:{m.nt_genomic_position_start}:{m.nt_genomic_position_end - m.nt_genomic_position_start + 1}",
             "other mutations possible"]
    else:
        possible_codons = compatible_codons(codon_reference, m.aa_variant,
                                            max_hamming_distance=nt_possible_mutations_distance)
        m.nt_possible_mutations = []
        for pc in possible_codons:
            if len(pc) == 1:
                m.nt_possible_mutations.append(f"{pc[0][1]}{m.nt_genomic_position_start + pc[0][0]}{pc[0][2]}")
            else:
                mutation_str = f"({pc[0][1]}{m.nt_genomic_position_start + pc[0][0]}{pc[0][2]}"
                for i in range(1, len(pc)):
                    mutation_str = mutation_str + " AND "
                    mutation_str = mutation_str + f"{pc[i][1]}{m.nt_genomic_position_start + pc[i][0]}{pc[i][2]}"
                mutation_str = mutation_str + ")"
                m.nt_possible_mutations.append(mutation_str)

    m.aa_changed = True if m.aa_reference != m.aa_variant else False

    return m

# @app_line_profiler
def translate_nt_to_aa(nt_mutation_pattern, gene="S", ref_path=None, gff_path=None):
    """
    Translates an nt substitution pattern (genomic position) into the corresponding AA substitution pattern.

    Parameters
    ----------
    nt_mutation_pattern : str
        nucleotide mutation pattern (e.g G22132T)
    gene : str
        gene name
    ref_path : str
        load the reference from file in path. Use the hardcoded reference sequence if None. (Default: None)
    gff_path : str
        load the annotation from file in path. Use the hardcoded reference annotation if None. (Default: None)

    Returns
    -------
    m : Mutation
        instance of the Mutation class describing the nt mutation, aa mutation and if the amino acid changed.

    @MelaniaNowicka, @JakubBartoszewicz @JuliusTembrockhaus
    """
    

    m = Mutation()

    m.nt_mutation_pattern = nt_mutation_pattern
    m.gene = gene

    m.nt_reference, m.nt_genomic_position_start, m.nt_genomic_position_end, m.nt_variant = \
        parse_nt_mutation(nt_mutation_pattern)

    gene_position = get_gene_pos(path=gff_path)
    ref_sequence = get_reference(path=ref_path)

    if m.gene not in gene_position:
        raise ValueError(f"Error gene {m.gene} is not in the annotation.")

    gene_regions = gene_position[m.gene]
    if m.nt_genomic_position_start < gene_regions[-1][1]:
        raise ValueError(f"Position of NT {m.nt_genomic_position_start} outside gene {m.gene}.")

    if m.nt_genomic_position_end > gene_regions[-1][2]:
        raise ValueError(f"Position of NT {m.nt_genomic_position_end} outside gene {m.gene}.")

    m.nt_gene_position_start = na_genome_to_gene(m.nt_genomic_position_start, gene=m.gene)
    m.nt_gene_position_end = na_genome_to_gene(m.nt_genomic_position_end, gene=m.gene)

    if m.nt_variant == 'del' and (m.nt_gene_position_start - 1) % 3 != 0:
        raise NotImplementedError("This deletion does not start at the first nucleotide of a codon. "
                                  "Deletions starting in the middle of the codon are not supported.")

    m.aa_position_start = na2aa_gene(m.nt_gene_position_start)
    m.aa_position_end = na2aa_gene(m.nt_gene_position_end)

    aa_mutation_length = m.aa_position_end - m.aa_position_start + 1

    codon_reference_list = []
    for aa in range(0, aa_mutation_length):
        codon_start, nt_position_in_codon = na_genome_to_codon_coordinates(m.nt_genomic_position_start + aa*3, m.gene)
        # get the reference codon
        codon_reference = get_codon(codon_start, ref_sequence)
        codon_reference_list.append(codon_reference)

    for i, nt in enumerate(m.nt_reference):
        position = m.nt_genomic_position_start+i
        if nt != ref_sequence[position-1:position]:
            raise ValueError(f"Reference nt at position {position} ({ref_sequence[position-1:position]}) "
                             f"is not {nt}. Provided reference in mutation pattern "
                             f"{m.nt_mutation_pattern}: {m.nt_reference}.")

    if m.nt_variant == 'del':
        
        m.aa_reference = "".join([get_aa_dict()[codon_ref] for codon_ref in codon_reference_list])
        m.aa_variant = "del"
        m.aa_mutation_pattern = \
            "".join([m.aa_reference, str(m.aa_position_start), "-", str(m.aa_position_end), m.aa_variant])
    else:
        
        m.aa_reference = get_aa_dict()[codon_reference_list[0]]
        codon_variant = list(codon_reference_list[0])
        nt_position_in_codon = na_genome_to_codon_coordinates(m.nt_genomic_position_start, m.gene)[1]
        codon_variant[nt_position_in_codon] = m.nt_variant
        codon_variant = "".join(codon_variant)
        try:
            m.aa_variant = get_aa_dict()[codon_variant]
        except:
            m.aa_variant = m.aa_reference # if codon ambiguous, use reference aa
        m.aa_mutation_pattern = "".join([m.aa_reference, str(m.aa_position_start), m.aa_variant])
    m.aa_changed = True if m.aa_reference != m.aa_variant else False

    return m

@lru_cache(maxsize=128)
def translate_nt_to_aa_pattern(nt_mutation_pattern, gene="S"):
    return translate_nt_to_aa(nt_mutation_pattern, gene).aa_mutation_pattern

def translate_multiple_nt_to_aa(multiple_nt_mutation_patterns, gene="S", ref_path=None, gff_path=None):
    """
    Translates multiple nt substitution patterns (genomic position) into the corresponding AA substitution pattern.

    Parameters
    ----------
    multiple_nt_mutation_patterns : list of str
        e.g ['G22132T', 'A23416T']
    gene : str
        gene name
    ref_path : str
        load the reference from file in path. Use the hardcoded reference sequence if None. (Default: None)
    gff_path : str
        load the annotation from file in path. Use the hardcoded reference annotation if None. (Default: None)

    Returns
    -------
    aa_mutation_pattern : str
      mutation pattern in aa+pos+aa format, e.g., R190S

    @MelaniaNowicka, @JakubBartoszewicz @JuliusTembrockhaus
    """

    mutations = []

    for nt_mutation_pattern in multiple_nt_mutation_patterns:

        m = Mutation()
        m.nt_mutation_pattern = nt_mutation_pattern
        m.gene = gene

        m.nt_reference, m.nt_genomic_position_start, m.nt_genomic_position_end, m.nt_variant = \
            parse_nt_mutation(nt_mutation_pattern)

        gene_position = get_gene_pos(path=gff_path)
        ref_sequence = get_reference(path=ref_path)

        if m.gene not in gene_position:
            raise ValueError(f"Error gene {m.gene} is not in the annotation.")

        gene_regions = gene_position[m.gene]
        if m.nt_genomic_position_start < gene_regions[-1][1]:
            raise ValueError(f"Position of NT {m.nt_genomic_position_start} outside gene {m.gene}.")

        if m.nt_genomic_position_end > gene_regions[-1][2]:
            raise ValueError(f"Position of NT {m.nt_genomic_position_end} outside gene {m.gene}.")

        m.nt_gene_position_start = na_genome_to_gene(m.nt_genomic_position_start, gene=m.gene)
        m.nt_gene_position_end = na_genome_to_gene(m.nt_genomic_position_end, gene=m.gene)

        m.aa_position_start = na2aa_gene(m.nt_gene_position_start)
        m.aa_position_end = na2aa_gene(m.nt_gene_position_end)
        aa_mutation_length = m.aa_position_end - m.aa_position_start + 1
    
        codon_reference_list = []
        for aa in range(0, aa_mutation_length):
            codon_start, nt_position_in_codon = na_genome_to_codon_coordinates(m.nt_genomic_position_start + aa*3, m.gene)
            # get the reference codon
            codon_reference = get_codon(codon_start, ref_sequence)
            codon_reference_list.append(codon_reference)
        for i, nt in enumerate(m.nt_reference):
            position = m.nt_genomic_position_start+i
            if m.nt_reference[i] != ref_sequence[position-1:position]:
                raise ValueError(f"Reference nt at position {position} ({ref_sequence[position-1:position]}) "
                                 f"is not {m.nt_reference[i]}. Provided reference in mutation pattern "
                                 f"{m.nt_mutation_pattern}: {m.nt_reference}.")
        mutations.append(m)

    mutations[0].aa_reference = get_aa_dict()[codon_reference_list[0]]
    codon_variant = list(codon_reference_list[0])

    for m in mutations:
        nt_position_in_codon = na_genome_to_codon_coordinates(m.nt_genomic_position_start, m.gene)[1]
        codon_variant[nt_position_in_codon] = m.nt_variant

    codon_variant = "".join(codon_variant)
    if codon_variant != '---': # no deletion
        try:
            aa_variant = get_aa_dict()[codon_variant]
        except:
            aa_variant = mutations[0].aa_reference # if codon ambiguous, use reference aa
    else: 
        aa_variant = '-'
    aa_mutation_pattern = "".join([mutations[0].aa_reference, str(mutations[0].aa_position_start), aa_variant])

    return aa_mutation_pattern


def get_codon(pos, refseq):
    """
    Gets the codon starting at position pos, given sequence refseq (as a str)
    pos is 1-based
    @hrichard
    """
    return refseq[(pos-1):(pos+2)]


def compatible_codons(codon_orig, aa_dest, max_hamming_distance=1):
    """
    Gives the list of codons coding for the Amino acid AA_dest that are at a hamming distance of max_hamming_distance of
    codon_orig

    Parameter
    ---------
        codon_orig: str
            the original codon
        aa_dest: str
            the amino acid in which we wish to mutate
    Returns: list[Tuple(int, str, str)]
    -------
        a list of all the single mutations with their positions

    compatible_codons("TGT", "F", 1) -> [(1, G, T)] (F is ['TGT', 'TTT'])
    compatible_codons("AGT", "R", 1) -> [(0, A, C), (2, T, A), (2, T, G)] (R is ['CGT','CGC','CGA','CGG','AGA','AGG'])

    @hrichard @JakubBartoszewicz @MelaniaNowicka
    """
    if aa_dest not in get_codon_dict():
        raise ValueError(f"Error AA {aa_dest} not a traditional amino acid.")
    c_comp = []
    for codon_dest in get_codon_dict()[aa_dest]:  # iterate over synonymous codons for aa_dest
        # i_diff = [True, True, False] (differences in codons)
        i_diff = [codon_dest[i] != codon_orig[i] for i in range(3)]
        if sum(i_diff) <= max_hamming_distance:  # compare sum of differences with max hamming distance
            differing_positions = [i for i, x in enumerate(i_diff) if x]
            required_substitutions = []
            for i in differing_positions:
                required_substitutions.append((i, codon_orig[i], codon_dest[i]))
            c_comp.append(required_substitutions)
    return c_comp


def parse_aa_mutation(aa_mutation):
    """
    Parameter:
    ----------
    aa_mut: an AA mutation of the form [A-Z][0-9]+[A-Z/] or [A-Z]+([0-9]+-[0-9]+)del

    Return
    ------
    A 4-tuple with (ref AA.s, start position, end position, Alt AA.s) Alt AA.s = del if it is a deletion

    @hrichard @JakubBartoszewicz @MelaniaNowicka
    """
    if "del" in aa_mutation:
        p = re.compile(r"([A-Z]*)([0-9]+)-([0-9]+)*")
        try:
            aa_reference, mutation_position, mutend = p.findall(aa_mutation)[0]
        except IndexError:
            raise ValueError("Unrecognized AA variant: {pattern}".format(pattern=aa_mutation))
        for aa in aa_reference:
            if aa not in get_codon_dict():
                raise ValueError("Unrecognized AA reference: {ref} in {pattern}".format(ref=aa_reference,
                                                                                        pattern=aa_mutation))
        mutation_pattern = (aa_reference, int(mutation_position), int(mutend), "del")
    else:
        p = re.compile(r"([A-Z]+)([0-9]+)([A-Z/]+)")
        try:
            aa_reference, mutation_position, aa_new = p.findall(aa_mutation)[0]
        except IndexError:
            raise ValueError("Unrecognized AA variant: {pattern}".format(pattern=aa_mutation))
        if aa_reference not in get_codon_dict() or aa_new not in get_codon_dict():
            raise ValueError("Unrecognized AA variant: {pattern}".format(pattern=aa_mutation))
        else:
            mutation_pattern = (aa_reference, int(mutation_position), int(mutation_position), aa_new)

    return mutation_pattern


NT_MUTATION_REGEX = re.compile(r"([A-Z]+)([0-9]+)([A-Z\-/]+)")

# @app_line_profiler
@lru_cache(maxsize=128)
def parse_nt_mutation(nt_mutation):
    """
    Parameter:
    ----------
    nt_mutation: a nt mutation of the form [A-Z]+[0-9]+[A-Z/]+

    Return
    ------
    A 4-tuple with (ref nt.s,  start position, end position,  Alt nt.s)
    @MelaniaNowicka, @JakubBartoszewicz
    """

    nt_alphabet = ['A', 'C', 'T', 'G']
    nt_alt_alphabet = ['A', 'C', 'T', 'G', '-', 'N', 'W', 'S', 'M', 'K', 'R', 'Y', 'B', 'D', 'H', 'V']
    if "del" in nt_mutation:
        p = re.compile(r"del:([0-9]+):([0-9]+)")
        re_out = p.findall(nt_mutation)
        if len(re_out) != 1:
            raise ValueError(f"Unrecognized mutation pattern: {nt_mutation}.")
        nt_deletion_start_position, nt_deletion_length = re_out[0]
        nt_deletion_start_position, nt_deletion_length = int(nt_deletion_start_position), int(nt_deletion_length)
        if nt_deletion_length % 3 != 0:
            raise ValueError(f"Unsupported deletion length (does not correspond to codons).")
        nt_deletion_end_position = nt_deletion_start_position + nt_deletion_length - 1
        reference = get_reference()
        nt_reference = reference[(nt_deletion_start_position-1):nt_deletion_end_position]
        nt_mutation = (nt_reference, nt_deletion_start_position, nt_deletion_end_position, "del")
    else:
        try:
            nt_reference, mutation_position, nt_new = NT_MUTATION_REGEX.findall(nt_mutation)[0]
        except IndexError:
            raise ValueError("Unrecognized NT variant: {pattern}".format(pattern=nt_mutation))
        if nt_reference not in nt_alphabet or nt_new not in nt_alt_alphabet:
            raise ValueError("Unrecognized NT variant: {pattern}".format(pattern=nt_mutation))

        nt_mutation = (nt_reference, int(mutation_position), int(mutation_position), nt_new)

    return nt_mutation


def translate_nt_seq_aa_seq(nt_sequence):
    """
    Translates nt sequence to aa sequence.

    Parameter:
    ----------
    nt_sequence : str
        sequence of nucleotides (note, must be divisible by 3)

    Return
    ------
    aa_sequence : str
        sequence of amino acids

    @JakubBartoszewicz @MelaniaNowicka
    """

    if len(nt_sequence) % 3 != 0:
        raise ValueError("Nt sequence length not divisible by 3.")
    codon_list = wrap(nt_sequence, 3)
    aa_sequence = "".join([get_aa_dict()[codon] for codon in codon_list])

    return aa_sequence
