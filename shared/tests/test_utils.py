import unittest
import numpy as np
from shared.residue_converter.position_converter import na2aa_msa_gene, aa2na_gene, aa2na_genome, na2aa_gene, \
    na2aa_genome, na_gene_to_genome, na_genome_to_gene
from shared.residue_converter.nt_aa_translation import translate_aa_to_nt, translate_nt_to_aa, \
    translate_nt_seq_aa_seq, compatible_codons, translate_multiple_nt_to_aa
from shared.residue_converter.data_loader import get_reference, parse_gff, get_gene_pos, read_reference
from shared.residue_converter.data_loader import FILE_NAME_REF, FILE_NAME_GFF, REFSEQ, D_GENEPOS


class TestConverter(unittest.TestCase):

    def test_aa2na_gene(self):
        inp = range(1, 1275)
        out = np.reshape(np.arange(1, 3823), (-1, 3))
        for i, v in enumerate(inp):
            self.assertEqual(aa2na_gene(v), (out[i][0], out[i][2]))

    def test_aa2na_genome(self):
        inp = range(1, 1275)
        out = np.reshape(np.arange(21563, 25385), (-1, 3))
        for i, v in enumerate(inp):
            self.assertEqual(aa2na_genome(v), (out[i][0], out[i][2]))
        inp = range(1, 4407)
        out = np.reshape(np.arange(266, 13484), (-1, 3))
        for i, v in enumerate(inp):
            self.assertEqual(aa2na_genome(v, gene="ORF1a"), (out[i][0], out[i][2]))
        inp = 4408
        self.assertRaises(ValueError, aa2na_genome, inp, "ORF1a")

    def test_na2aa_gene(self):
        inp = range(1, 3823)
        out_nested = [3 * [i] for i in range(1, 1275)]
        out = [item for sublist in out_nested for item in sublist]
        for i, v in enumerate(inp):
            self.assertEqual(na2aa_gene(v), out[i])

    def test_na2aa_genome(self):
        inp = range(21563, 25385)
        out_nested = [3 * [i] for i in range(1, 1275)]
        out = [item for sublist in out_nested for item in sublist]
        for i, v in enumerate(inp):
            self.assertEqual(na2aa_genome(v), out[i])
        inp = range(266, 13484)
        out_nested = [3 * [i] for i in range(1, 4407)]
        out = [item for sublist in out_nested for item in sublist]
        for i, v in enumerate(inp):
            self.assertEqual(na2aa_genome(v, gene="ORF1a"), out[i])

    def test_na_gene_to_genome(self):
        inp = range(1, 3823)
        out = range(21563, 25385)
        for i, v in enumerate(inp):
            self.assertEqual(na_gene_to_genome(v), out[i])
        inp = range(1, 13219)
        out = range(266, 13484)
        for i, v in enumerate(inp):
            self.assertEqual(na_gene_to_genome(v, gene="ORF1a"), out[i])

    def test_na_genome_to_gene(self):
        inp = range(21563, 25385)
        out = range(1, 3823)
        for i, v in enumerate(inp):
            self.assertEqual(na_genome_to_gene(v), out[i])
        inp = range(266, 13484)
        out = range(1, 13219)
        for i, v in enumerate(inp):
            self.assertEqual(na_genome_to_gene(v, gene="ORF1a"), out[i])

    def test_na2aa_msa_gene(self):
        inp = "777.2"
        out = "259.1"
        self.assertEqual(na2aa_msa_gene(inp), out)
        inp = "777"
        out = "259"
        self.assertEqual(na2aa_msa_gene(inp), out)
        inp = "1"
        out = "1"
        self.assertEqual(na2aa_msa_gene(inp), out)


class TestTranslator(unittest.TestCase):

    def test_translate_aa_to_nt(self):
        inp = "N501Y"
        out = ["A23063T"]
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp).nt_possible_mutations, out)
        out = "AA variant: N501Y --> A23063T"
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp).to_string(), out)

        inp = "K417N"
        out = ["G22813T", "G22813C", "G22813Y"]
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp).nt_possible_mutations, out)
        out = "AA variant: K417N --> G22813T / G22813C / G22813Y"
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp).to_string(), out)

        inp = "R190B"
        self.assertRaises(ValueError, translate_aa_to_nt, inp)
        inp = "B190S"
        self.assertRaises(ValueError, translate_aa_to_nt, inp)

        inp = "R190S"
        out = ["G22132T", "G22132C", "G22132Y"]
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp).nt_possible_mutations, out)

        inp = "R190S"
        out = ['(A22130T AND G22131C)', 'G22132T', 'G22132C', 'G22132Y']
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp,
                                            nt_possible_mutations_distance=2).nt_possible_mutations, out)
        out = "AA variant: R190S --> (A22130T AND G22131C) / G22132T / G22132C / G22132Y"
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp,
                                            nt_possible_mutations_distance=2).to_string(), out)

        inp = "R190S"
        out = ['(A22130T AND G22131C AND G22132T)', '(A22130T AND G22131C AND G22132C)',
               '(A22130T AND G22131C AND G22132A)', '(A22130T AND G22131C)', 
                'G22132T', 'G22132C', '(A22130T AND G22131C AND G22132N)',
               '(A22130T AND G22131C AND G22132W)', '(A22130T AND G22131C AND G22132S)',
               '(A22130T AND G22131C AND G22132M)', '(A22130T AND G22131C AND G22132K)',
               '(A22130T AND G22131C AND G22132R)', '(A22130T AND G22131C AND G22132Y)',
               'G22132Y']
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp,
                                            nt_possible_mutations_distance=3).nt_possible_mutations, out)

        inp = "del:22345:3"
        self.assertRaises(ValueError, translate_aa_to_nt, inp)

        inp = "I2230T"
        out = ["T6954C"]
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp, gene="ORF1a").nt_possible_mutations, out)

        inp = "I2230T"
        self.assertRaises(ValueError, translate_aa_to_nt, inp, "orf123")

        inp = "W2230T"
        self.assertRaises(ValueError, translate_aa_to_nt, inp, "ORF1a")

        inp = "HV69-70del"
        # Note: one important variant is del:21765:6, but it starts it the middle of a codon (currently unsupported)
        out = ["del:21767:6", 'other mutations possible']
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp).nt_possible_mutations, out)

        inp = "Y144-144del"
        # Note: one important variant is del:21991:3, but it starts it the middle of a codon (currently unsupported)
        out = ["del:21992:3", 'other mutations possible']
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp).nt_possible_mutations, out)

        inp = "SGF3675-3677del"
        out = ["del:11288:9", 'other mutations possible']
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp, gene="ORF1a").nt_possible_mutations, out)

        inp = "SGF3675-3677del"
        out = ["del:11288:9", 'other mutations possible']
        self.assertEqual(translate_aa_to_nt(aa_mutation_pattern=inp, gene="ORF1a").nt_possible_mutations, out)

        inp = "BB69-70del"
        self.assertRaises(ValueError, translate_aa_to_nt, inp)

        inp = "HV69-70"
        self.assertRaises(ValueError, translate_aa_to_nt, inp)

    def test_translate_nt_to_aa(self):
        inp = "A23063T"
        out = "N501Y"
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).aa_mutation_pattern, out)
        out = "nt variant: A23063T --> N501Y"
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).to_string(), out)

        inp = "G22813T"
        out = "K417N"
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).aa_mutation_pattern, out)

        inp = "G22813Y"
        out = "K417N"
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).aa_mutation_pattern, out)

        inp = "A22813T"
        self.assertRaises(ValueError, translate_nt_to_aa, inp)

        inp = "A21562T"
        self.assertRaises(ValueError, translate_nt_to_aa, inp, "orf123")

        inp = "A21562T"
        self.assertRaises(ValueError, translate_nt_to_aa, inp)

        inp = "A25385G"
        self.assertRaises(ValueError, translate_nt_to_aa, inp)

        inp = "del:21767:6"
        out = "HV69-70del"
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).aa_mutation_pattern, out)

        inp = "del:21765:6"
        self.assertRaises(NotImplementedError, translate_nt_to_aa, inp)

        inp = "del:21767:8"
        self.assertRaises(ValueError, translate_nt_to_aa, inp)

        inp = "SGF3675-3677del"
        self.assertRaises(ValueError, translate_nt_to_aa, inp)

        inp = "R190S"
        self.assertRaises(ValueError, translate_nt_to_aa, inp)

        inp = "ATACAT"
        self.assertRaises(ValueError, translate_nt_to_aa, inp)

        inp = "G22813T"
        out = True
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).aa_changed, out)

        inp = "G22813G"
        out = False
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).aa_changed, out)

        inp = "T21994C"
        out = False
        self.assertEqual(translate_nt_to_aa(nt_mutation_pattern=inp).aa_changed, out)
    
    def test_translate_multiple_nt_to_aa(self):
        inp = ['C21767T', 'T21769C']
        out = "H69Y"
        self.assertEqual(translate_multiple_nt_to_aa(multiple_nt_mutation_patterns=inp), out)

        inp = ['G21770A', 'C21772Y']
        out = "V70I"
        self.assertEqual(translate_multiple_nt_to_aa(multiple_nt_mutation_patterns=inp), out)

        inp = ['C21767-', 'A21768-', 'T21769-']
        out = "H69-"
        self.assertEqual(translate_multiple_nt_to_aa(multiple_nt_mutation_patterns=inp), out)

        inp = ['A21791N', 'A21792N', 'G21793N']
        out = "K77K"
        self.assertEqual(translate_multiple_nt_to_aa(multiple_nt_mutation_patterns=inp), out)

    def test_translate_nt_seq_aa_seq(self):
        inp = "ACTTCTAACTTTAGAGTCCAACCAACAGAATCTATTGTTAGATTTCCTAATATTACAAACTTGTGCCCT"
        out = "TSNFRVQPTESIVRFPNITNLCP"
        self.assertEqual(translate_nt_seq_aa_seq(inp), out)

        inp = "ACTTCTAACTTTAGAGTCCAACCAACAGAATCTATTGTTAGATTTCCTAATATTACAAACTTGTGCCC"
        self.assertRaises(ValueError, translate_nt_seq_aa_seq, inp)

    def test_compatible_codons(self):
        inp_codon = "AAA"
        inp_aa = "N"
        out = [[(2, 'A', 'T')], [(2, 'A', 'C')], [(2, 'A', 'Y')]]
        self.assertEqual(compatible_codons(inp_codon, inp_aa), out)

        inp_aa = "B"
        self.assertRaises(ValueError, compatible_codons, inp_codon, inp_aa)


class TestDataLoader(unittest.TestCase):

    def test_read_reference(self):
        inp = FILE_NAME_REF
        out = REFSEQ
        self.assertEqual(read_reference(inp), out)

    def test_parse_gff(self):
        inp = FILE_NAME_GFF
        out = D_GENEPOS
        self.assertEqual(parse_gff(inp, attr_id="gene"), out)
        self.assertRaises(ValueError, parse_gff, inp, attr_id="foobar")

    def test_get_reference(self):
        inp = FILE_NAME_REF
        out = REFSEQ
        self.assertEqual(get_reference(inp), out)
        self.assertEqual(get_reference(None), out)
        self.assertEqual(get_reference(), out)

    def test_get_gene_pos(self):
        inp = FILE_NAME_GFF
        out = D_GENEPOS
        self.assertEqual(get_gene_pos(inp), out)
        self.assertEqual(get_gene_pos(None), out)
        self.assertEqual(get_gene_pos(), out)


if __name__ == '__main__':
    unittest.main(exit=True)
