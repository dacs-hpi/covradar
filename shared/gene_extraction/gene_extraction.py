from Bio import SeqIO
import pandas as pd
import argparse
import os
import psutil
from datetime import datetime
from extract_ref_gene import extract_ref_gene_seq
from extract_gene_from_multifasta import extract_gene_from_multifasta

def execute_gene_extraction(fasta, metadata, gene, output_dir, alignments, max_ns, max_length_deviation, ref_path, gff_path, threads):

    # create fasta file for reference gene
    ref_genes_folder = 'reference_genes/'
    if not os.path.isdir(ref_genes_folder):
        os.system('mkdir -p ' + ref_genes_folder)
    ref_gene_file = extract_ref_gene_seq(gene, ref_genes_folder, ref_path, gff_path)
    print('Reference gene file is created at ' + ref_gene_file)

    # create results folder
    dt_string = datetime.now().strftime("%Y%m%d_%H%M")
    results_folder = output_dir + dt_string + '/'
    if not os.path.isdir(results_folder):
        os.system('mkdir -p ' + results_folder)

    # create local alignment for query fasta 
    if alignments == None:
        local_alignment_outfile = results_folder+'local_alignment_'+'.tsv'
        print('The local alignment for the query fasta will be generated now...')
        os.system('pblat -threads={} {} {} {}'.format(threads, ref_gene_file, fasta, local_alignment_outfile))
        if os.path.isfile(local_alignment_outfile):
            print('The local alignment was succesfully generated at ' + local_alignment_outfile)
        else:
            return 1
    else: 
        local_alignment_outfile = alignments
        print('The local alignment file ' + local_alignment_outfile + ' will be used')

    # extract genes 
    extracted_genes = results_folder + 'extracted_genes.fasta'
    extracted_metadata = results_folder + 'extracted_metadata.tsv'
    discarded_genes = results_folder + 'discarded_genes.fasta'
    discarded_metadata = results_folder + 'discarded_metadata.tsv'
    print('The gene sequences will be extracted from the query fasta now...')
    extract_gene_from_multifasta(fasta, ref_gene_file, local_alignment_outfile, metadata, extracted_genes, extracted_metadata, discarded_genes, discarded_metadata, max_ns, max_length_deviation)
    print('Finished. Check the folder ' + results_folder + ' to investigate the results')

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Extracts genes from a multifasta')
    parser.add_argument('--fasta', metavar='STR', type=str, help='FASTA file containing the query sequences where the gene need to be extracted', required=True)
    parser.add_argument('--metadata', metavar='STR', type=str, help='metadata file in tsv format', required=True)
    parser.add_argument('--gene', metavar='STR', help="gene name [ORF1ab, S, ORF3a, E, M, ORF6, ORF7a, ORF7b, ORF8, N, ORF10]", type=str, required=True)
    parser.add_argument('--output_dir', metavar='STR', type=str, help='output directory (Default: extracted_genes/)', default='extracted_genes/')
    parser.add_argument('--alignments', metavar='STR', type=str, help='TSV file containing blast alignments (will be automatically generated if not given)', default=None)
    parser.add_argument('--max_ns', metavar='INT', type=int, help='maximum percentage of Ns allowed to keep the gene (Default: 5)', default=5)
    parser.add_argument('--max_length_deviation', metavar='INT', type=int, help='maximum length deviation (%%) of sequence when compared with the reference gene (Default: 5)', default=5)
    parser.add_argument('--ref_path', metavar='STR', help="FASTA file containing the reference sequence (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    parser.add_argument('--gff_path', metavar='STR', help="GFF file containing the genome annotation (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    parser.add_argument('--threads', metavar='INT', type=int, help='number of threads to use (Default: %d)' % (psutil.cpu_count()), default=psutil.cpu_count())
    args = parser.parse_args()

    execute_gene_extraction(args.fasta, args.metadata, args.gene, args.output_dir, args.alignments, args.max_ns, args.max_length_deviation, args.ref_path, args.gff_path, args.threads)

    
