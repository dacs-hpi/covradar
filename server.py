from flask import Flask
import os
import sys
import pickle
import dash_bootstrap_components as dbc
from frontend.app import register_dashapp
from frontend.app.auth.auth import auth_blueprint

from frontend.app.shared.callbacks_language_button import register_callbacks as register_callbacks_lang_button
from frontend.app.shared.callbacks_right_menu import register_callbacks as register_callbacks_shared_right_menu
from frontend.app.shared.callbacks_freq_plot import register_callbacks as register_callbacks_shared_freq_plot
from frontend.app.shared.callbacks_consensus_table import register_callbacks as register_callbacks_shared_cons
from frontend.app.shared.callbacks_data_table import register_callbacks as register_callbacks_shared_data
from frontend.app.shared.callbacks_mutation_time_plot import register_callbacks as register_callbacks_shared_mut
from frontend.app.shared.callbacks_world_map import register_callbacks as register_callbacks_shared_world_map

from frontend.app.report.layout import layout as layout_report
from frontend.app.report.callbacks_layout import register_callbacks as register_callbacks_report_layout

from frontend.app.report_latest.layout import layout as layout_report_latest
from frontend.app.report_latest.callbacks_layout import register_callbacks as register_callbacks_report_latest_layout

from frontend.app.shared.load_lang_dict import get_language_dict
from frontend.app.shared.data import load_all_sql_files, get_database_connection

def main():
    if 'MYSQL_DBglobal' not in os.environ and 'MYSQL_DBlatest' not in os.environ:
        sys.exit('Error: One of the system environment variables MYSQL_DBglobal or MYSQL_DBlatest need to be set in the .bashrc file before running the server. Restart the terminal after the variable has been set.')

    flask_app = Flask(__name__)

    # authentication framework / starting page
    flask_app.register_blueprint(auth_blueprint)

    # language dict, all text in english or german depending on browser language (locale)
    dict_lang = get_language_dict()


    # international report
    if 'MYSQL_DBglobal' in os.environ:
        db_global = os.environ['MYSQL_DBglobal']
        db_connection_global = get_database_connection(db_global)
        try:
            df_global_dict = pickle.load(open("/mnt/storage_staging/gisaid_global.pickle","rb"))
        except:
            df_global_dict = load_all_sql_files(db_global)

        external_scripts_report = [
            {'src':"https://code.jquery.com/jquery-3.5.1.min.js",
            'integrity':"sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=",
            'crossorigin':"anonymous"}
        ]
        external_stylesheets_report = [dbc.themes.BOOTSTRAP,
                                    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css']
        register_callbacks_report = [register_callbacks_report_layout,
                                    register_callbacks_shared_right_menu,
                                    register_callbacks_shared_freq_plot,
                                    register_callbacks_shared_cons,
                                    register_callbacks_shared_data,
                                    register_callbacks_shared_mut,
                                    register_callbacks_shared_world_map,
                                    register_callbacks_lang_button]
        register_dashapp(flask_app, 'Global Report', 'report', external_scripts_report, layout_report,
                        register_callbacks_report, external_stylesheets_report, db_connection_global, df_global_dict, dict_lang)

    # Latest report
    if 'MYSQL_DBlatest' in os.environ:
        db_latest = os.environ['MYSQL_DBlatest']
        db_connection_latest = get_database_connection(db_latest)
        #df_latest_dict = pickle.load(open("df_dict.pickle", "rb"))
        try:
            df_latest_dict = pickle.load(open("/mnt/storage_staging/gisaid_latest.pickle","rb"))
        except:
            df_latest_dict = load_all_sql_files(db_latest)

        external_scripts_report_latest = [
            {'src':"https://code.jquery.com/jquery-3.5.1.min.js",
            'integrity':"sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=",
            'crossorigin':"anonymous"}
        ]
        external_stylesheets_report_latest=[dbc.themes.BOOTSTRAP,
                                    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css']
        register_callbacks_report_latest = [
            register_callbacks_report_latest_layout,
            register_callbacks_shared_right_menu,
            register_callbacks_shared_freq_plot,
            register_callbacks_shared_cons,
            register_callbacks_shared_data,
            register_callbacks_shared_mut,
            register_callbacks_shared_world_map,
            register_callbacks_lang_button
                                            ]

        register_dashapp(flask_app, 'Latest Global report', 'latest', external_scripts_report_latest, layout_report_latest,
                        register_callbacks_report_latest, external_stylesheets_report_latest, db_connection_latest, df_latest_dict, dict_lang)

    return flask_app

if __name__ == '__main__':
    flask_app = main()
    # Do not change port from 8888 otherwise it will crash the kubernetes cluster
    # Also leave debug=False before merging in main
    flask_app.run(debug=False, host="0.0.0.0", port="8888")

if __name__ == 'server':  # for visual studio debugging
    flask_app = main()