To contribute code to covradar fork the repository and send a pull request.

When submitting code, please make every effort to follow existing conventions and style in order to keep the code as readable as possible.

Where appropriate, please provide unit tests or integration tests. Unit tests should be pytest based tests and be added to <project>/tests.

Please make sure all tests pass before submitting a pull request. It is also good if you squash your commits and add the tags #major or #minor to the pull request title if need be, otherwise your pull request will be considered a patch bump. Please check [https://semver.org/](https://semver.org/) for more information about versioning.

## How to test

Setup and activate the environment:
```bash
conda env create -f frontend/environment.yml -n app_env
conda activate app_env
```

Setup test sql database:
```bash
conda install mysql tqdm -y
./frontend/tests/setup_test_sql_db.py
```

To set the required environment variables, source the newly created env.sh:
```bash
source frontend/env.sh
```

Install testing dependencies:
```bash
pip install -r frontend/tests/test_requirements.txt
```

Run the tests:
```bash
pytest -v
```

# How to upload the results in the database

## conda environment

Install and activate conda environment. The yml file is in backend/sql
```
conda env create -f environment.yml -n sql_covradar
conda activate sql_covradar
```

## variable setup

In .bashrc setup the environment variables (adjust host, user, password):

```
# environment variables for mysql connection
export MYSQL_HOST=host
export MYSQL_USER=user
export MYSQL_PW=password
```

## Create Database
If the database does not exist yet, you have to create it before (adjust user, database).
```
mysql -u user -p
mysql> CREATE DATABASE database;

```

## Run upload script
```
python backend/sql/create_spike_database.py
```

Here are the parameters of that script:
```
$ python create_spike_database.py --help
usage: create_spike_database.py [-h] [-w WORKDIR] [-d DATE] [-u USER]
                                [--host HOST] [-p PASSWORD] [-db DATABASE]

Inserts pipeline results into a mysql database

optional arguments:
  -h, --help            show this help message and exit
  -w WORKDIR, --workdir WORKDIR
                        Pipeline workdir [default: ..]
  -d DATE, --date DATE  Date [default: today]
  -u USER, --user USER  MySQL user [default: MYSQL_USER]
  --host HOST           MySQL host [default: MYSQL_HOST]
  -p PASSWORD, --password PASSWORD
                        MySQL password [default: MYSQL_PW]
  -db DATABASE, --database DATABASE
                        MySQL DB name [default: Spike]
```

### Database names
We have 4 databases you can upload to:
- SpikeGisaid: Contains EBI, Charité and Gisaid data. This is not publically available because only people with a Gisaid account are allowed to use the app with Gisaid data.
- Spike: Contains EBI and Charité data. This is shown on covradar.de / covradar.net.
- RKIPrivate: Used by the Germany map. Only for RKI intern.
- RKIPublic: Used by the Germany map. This will be shown on covradar.de / covradar.net.


## Profiling your code
1. install line_profiler: `conda install line_profiler`
2. add `from frontend.app.shared.profiler import app_line_profiler` in the file you want to profile. 
Alternatively, you can use a cumulative that profiles whole functions rather than individual lines.
3. add the `@app_line_profiler` decorator right before the definition of the function you want to profile.
4. Run `server.py`. You will see the stats for each line of the profiled function in your stdout.

Suggestion: profile only one function at a time to avoid clashes.
