FROM continuumio/miniconda3:latest
ENV VERSION 1.0.0
ENV TOOL covradar

# meta data
LABEL base_image="continuumio/miniconda3"
LABEL about.summary="A snakemake pipeline for molecular surveillance of COVID-19's spike protein -- The App"
LABEL about.license=""
LABEL about.tags="COVID-19 SARS-CoV-2 spike surveillance monitor"
LABEL about.home="https://gitlab.com/dacs-hpi/covradar"
LABEL maintainer="https://gitlab.com/dacs-hpi/covradar"

# install basics
RUN until apt-get update; do   echo "== apt-get update failed, retrying ==";   sleep 5; done && until apt-get install -y procps wget gzip; do   echo "== apt-get install failed, retrying ==";   sleep 5; done && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# The EXPOSE instruction indicates the ports on which a container 
# will listen for connections
# Since Flask apps listen to port 5000  by default, we expose it
EXPOSE 5000

# Sets the working directory for following COPY and CMD instructions
# Notice we haven’t created a directory by this name - this instruction 
# creates a directory with this name if it doesn’t exist
WORKDIR /app

# Install any needed packages specified in requirements.txt
RUN conda config --add channels defaults && \ 
    conda config --add channels conda-forge && \
    conda config --add channels anaconda 

# Copy all content from the frontend folder into the container
COPY frontend /app/frontend/
# Copy all content from the shared folder into the container
COPY shared /app/shared/
# Copy relevant content from the root folder into the container
COPY server.py wsgi.py setup.cfg /app/
# Copy files to print the current version
COPY versioneer.py .gitattributes /app/
COPY .git /app/.git/

# install all dependencies from the environment.yml via conda into the basedir (so no activation of the conda env is needed)
RUN conda env update -n root -f frontend/environment.yml && conda clean -a

# Add docker-compose-wait tool to wait for the database container
ENV WAIT_VERSION 2.7.2
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait

# Execute via start
CMD /wait && python server.py
