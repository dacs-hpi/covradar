import argparse
import os
import pandas as pd
import ast
import plotly.express as px
import plotly.offline as offline
from datetime import date

def produce_graphs(codon_numbers, region, yaxis, results_dir, output_dir):

    # check if positions argument is correctly specified
    error_msg = 'ERROR: The codon_numbers argument must either be a single codon in range 1-1274 or multiple positions separated by ",".'
    try:
        # pos_array = list(map(int, positions.split(',')))
        pos_array = codon_numbers.split(',')
        min_range, max_range = 1, 1274
        for pos in pos_array:
            if float(pos) < min_range or float(pos) > max_range:
                return print(error_msg)
    except:
        return print(error_msg)

    # check if region argument is correctly specified
    possible_regions = os.listdir(results_dir + 'analysis/freqs_per_cw/')
    error_msg = 'ERROR: The region argument can only be set to ' + str(possible_regions) + ' at the moment. If the desired region is not available, adjust the regions configuration in the backend/config.yaml file and re-run the CovRadar pipeline.'
    if region not in possible_regions:
        return print(error_msg)

    # check if yaxis argument is correctly specified 
    error_msg = 'ERROR: The yaxis argument can only be set to "count" or "freq".'
    if yaxis != "count" and yaxis != "freq":
        return print(error_msg)

    # check if output_dir argument is correctly specified 
    error_msg = 'ERROR: The specified output directory does not exist. Please create the directory beforehand or specify another one.'
    valid_dir = os.path.isdir(output_dir)
    if valid_dir == False:
        return print(error_msg)

    # check if output_dir end with '/'output_dir
    error_msg = 'ERROR: The output directory needs to end with "/".'
    if output_dir[-1] == '/':
        valid_ending = True
    else:
        valid_ending = False
    if valid_ending == False:
        return print(error_msg)

    # path to amino acid distributions file
    dist_file = results_dir + 'analysis/freqs_per_cw/' + region + '/aa_freqs_per_cw.tsv'

    # check if nucleotide distributions file is present
    error_msg = 'ERROR: Amino acid distributions file is not present at ' + dist_file + '. Please run the pipeline beforehand and make sure that "pdf" is contained in the outputs argument of backend/config.yaml.'
    try:
        print('Loading Amino Acid Distribution.')
        df = pd.read_csv(dist_file, sep="\t", dtype={"CODON_NUM":str})
    except:
        return print(error_msg)
    valid_codons = df['CODON_NUM'].tolist()
    df.set_index('CODON_NUM', inplace=True)
    calendar_weeks = df.columns[2:]

    # check if codon_number(s) is/are indeed valid
    for pos in pos_array:
        if pos not in valid_codons:
            error_msg = 'ERROR: The codon ' + pos + ' is not a valid codon. Please check the first column of ' + dist_file + ' for all possible codon positions.'
            return print(error_msg)

    # get current date (for file names)
    today = date.today()
    curr_date = today.strftime("%y-%m-%d")

    for i, pos in enumerate(pos_array):
        print('Current Codon:', pos)
        df_pos = df.loc[pos]
        REF_AA = df_pos['REF_AA']
        dict_pos = {
            'count': [],
            'date': [],
            'aa': [],
            'total': [],
            'freq': []
            }
        for cw in calendar_weeks:
            dict_str = df_pos[cw]
            if dict_str != 'no_call':
                cw_dict = ast.literal_eval(dict_str)
                cov = cw_dict['Coverage']
                for key, value in cw_dict.items():
                    if value != 0 and key != 'Coverage':
                        dict_pos['count'].append(value)
                        dict_pos['date'].append(cw)
                        dict_pos['aa'].append(key)
                        dict_pos['total'].append(cov)
                        dict_pos['freq'].append(value/cov)
        df_bar = pd.DataFrame(dict_pos)

        fig = px.bar(df_bar[['date', yaxis, 'aa']], x="date", y=yaxis, color='aa')

        title_text = "Distribution of aa at codon=%(pos)s (first case) with reference-aa=%(REF)s in region=%(region)s." \
            %{'pos':pos,'region':region,'REF':REF_AA}

        fig.update_layout(
        title={
            'text': title_text,
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        font=dict(size=8),
        autosize=True,
        margin=dict(
            l=0,
            r=0,
            pad=0,
            ),
        )

        filename = output_dir + curr_date + '_' + region + '_aa' + pos + '_' + yaxis + '.html'

        offline.plot({
            'data': fig.data,
            'layout': fig.layout},
            auto_open=False,
            output_type='file',
            filename=filename,
        )

        print('Graph for Codon:', pos, 'successfully generated.')

    return 0


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Produces (multilpe) graph(s) of amino acid distribution(s) based on the given input.')
    parser.add_argument('--codon_numbers', type=str, help='First case based codon number(s) (separated by ",") for which graphs are generated.')
    parser.add_argument('--region', type=str, help='Set to "Germany" or "Global" to select the sequences on which the distributions are calculated.')
    parser.add_argument('--yaxis', type=str, help='Set to "count" or "freq" to determine the y-axis scale.')
    parser.add_argument('--results_dir', type=str, help="Provide the path of the backend/results/ directory of CovRadar (Default: '../../backend/results/').", default='../../backend/results/')
    parser.add_argument('--output_dir', type=str, help='Set output directory where the produced graphs are saved.')
    args = parser.parse_args()

    # input
    codon_numbers = args.codon_numbers
    region = args.region 
    yaxis = args.yaxis
    results_dir = args.results_dir
    output_dir = args.output_dir

    produce_graphs(codon_numbers, region, yaxis, results_dir, output_dir)


