import argparse
import os
import pandas as pd
import ast
import plotly.express as px
import plotly.offline as offline
from datetime import date

def produce_graphs(positions, region, yaxis, results_dir, output_dir):

    # check if positions argument is correctly specified
    error_msg = 'ERROR: The positions argument must either be a single position in range 1-3822 (spike length) or multiple positions separated by ",".'
    try:
        # pos_array = list(map(int, positions.split(',')))
        pos_array = positions.split(',')
        min_range, max_range = 1, 3822
        for pos in pos_array:
            if float(pos) < min_range or float(pos) > max_range:
                return print(error_msg)
    except:
        return print(error_msg)

    # check if region argument is correctly specified
    possible_regions = os.listdir(results_dir + 'analysis/freqs_per_cw/')
    error_msg = 'ERROR: The region argument can only be set to ' + str(possible_regions) + ' at the moment. If the desired region is not available, adjust the regions configuration in the backend/config.yaml file and re-run the CovRadar pipeline.'
    if region not in possible_regions:
        return print(error_msg)

    # check if yaxis argument is correctly specified 
    error_msg = 'ERROR: The yaxis argument can only be set to "count" or "freq".'
    if yaxis != "count" and yaxis != "freq":
        return print(error_msg)

    # check if output_dir argument is correctly specified 
    error_msg = 'ERROR: The specified output directory does not exist. Please create the directory beforehand or specify another one.'
    valid_dir = os.path.isdir(output_dir)
    if valid_dir == False:
        return print(error_msg)

    # check if output_dir end with '/'output_dir
    error_msg = 'ERROR: The output directory needs to end with "/".'
    if output_dir[-1] == '/':
        valid_ending = True
    else:
        valid_ending = False
    if valid_ending == False:
        return print(error_msg)

    # path to position table
    pos_table_file = results_dir + 'numbering/position_table.tsv'

    # check if position table is present
    error_msg = 'ERROR: The required position table is not present at ' + pos_table_file + '. Please run the pipeline beforehand and make sure that "pdf" is contained in the outputs configuration of backend/config.yaml.'
    try:
        pos_table = pd.read_csv(pos_table_file, sep="\t", dtype={"msa":int, "reference":str})
    except:
        return print(error_msg)

    # check if position(s) is/are indeed valid
    valid_positions = pos_table['reference'].tolist()
    for pos in pos_array:
        if pos not in valid_positions:
            error_msg = 'ERROR: The position ' + pos + ' is not a valid reference position. Please check ' + pos_table_file + ' for all possible reference positions.'
            return print(error_msg)

    # path to nucleotide distributions file
    dist_file = results_dir + '/analysis/freqs_per_cw/' + region + '/nt_freqs_per_cw.tsv'

    # check if nucleotide distributions file is present
    error_msg = 'ERROR: Nucleotide distributions file is not present at ' + dist_file + '. Please run the pipeline beforehand and make sure that "pdf" is contained in the outputs argument of backend/config.yaml.'
    try:
        print('Loading Nucleotide Distribution.')
        df = pd.read_csv(dist_file, sep="\t")
    except:
        return print(error_msg)
    df.set_index('POS', inplace=True)
    calendar_weeks = df.columns[1:]

    # convert reference positions to msa positions
    positions_msa = []
    for pos in pos_array:
        idx = pos_table.index[pos_table['reference'] == pos].tolist()[0]
        pos_msa = pos_table.iloc[idx]['msa']
        positions_msa.append(pos_msa)

    # get current date (for file names)
    today = date.today()
    curr_date = today.strftime("%y-%m-%d")

    for i, pos in enumerate(positions_msa):
        print('Current Position:', pos_array[i], '(msa=' + str(pos) + ')')
        df_pos = df.loc[pos]
        REF = df_pos['REF']
        dict_pos = {
            'count': [],
            'date': [],
            'nt': [],
            'total': [],
            'freq': []
            }
        for cw in calendar_weeks:
            dict_str = df_pos[cw]
            if dict_str != 'no_call':
                cw_dict = ast.literal_eval(dict_str)
                cov = cw_dict['Coverage']
                for key, value in cw_dict.items():
                    if value != 0 and key != 'Coverage':
                        dict_pos['count'].append(value)
                        dict_pos['date'].append(cw)
                        dict_pos['nt'].append(key)
                        dict_pos['total'].append(cov)
                        dict_pos['freq'].append(value/cov)
        df_bar = pd.DataFrame(dict_pos)

        fig = px.bar(df_bar[['date', yaxis, 'nt']], x="date", y=yaxis, color='nt')

        title_text = "Distribution of nt at position=%(pos)s (first case) with reference-nt=%(REF)s in region=%(region)s." \
            %{'pos':pos_array[i],'region':region,'REF':REF}

        fig.update_layout(
        title={
            'text': title_text,
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        font=dict(size=8),
        autosize=True,
        margin=dict(
            l=0,
            r=0,
            pad=0,
            ),
        )

        filename = output_dir + curr_date + '_' + region + '_nt' + pos_array[i] + '_' + yaxis + '.html'

        offline.plot({
            'data': fig.data,
            'layout': fig.layout},
            auto_open=False,
            output_type='file',
            filename=filename,
        )

        print('Graph for Position:', pos_array[i], 'successfully generated.')

    return 0


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Produces (multilpe) graph(s) of nucleotide distribution(s) based on the given input.')
    parser.add_argument('--positions', type=str, help='First case based position(s) (separated by ",") for which graphs are generated.')
    parser.add_argument('--region', type=str, help='Set region to select the sequences on which the distributions are calculated.')
    parser.add_argument('--yaxis', type=str, help='Set to "count" or "freq" to determine the y-axis scale.')
    parser.add_argument('--results_dir', type=str, help="Path of the `backend/results/` directory of CovRadar (Default: '../../backend/results/').", default='../../backend/results/')
    parser.add_argument('--output_dir', type=str, help='Set output directory where the produced graphs are saved.')
    args = parser.parse_args()

    # input
    positions = args.positions
    region = args.region 
    yaxis = args.yaxis 
    results_dir = args.results_dir
    output_dir = args.output_dir

    produce_graphs(positions, region, yaxis, results_dir, output_dir)


