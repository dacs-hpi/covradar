#!/usr/bin/env python3
import argparse
import glob
import os
from datetime import datetime, timedelta
from multiprocessing import cpu_count
import pandas as pd

parser = argparse.ArgumentParser(
    description="Creates snakemake config file to run the pipeline, based on samples stored in the data folder"
)
parser.add_argument(
    "--decompress",
    type=int,
    default=cpu_count(),
    help=f"Number of threads to decompress files [default: {cpu_count()}]"
)
parser.add_argument(
    "--threads_local_alignment",
    type=int,
    default=cpu_count(),
    help=f"Number of threads to run the local alignment [default: {cpu_count()}]"
)
parser.add_argument(
    "--threads_multiple_sequence_alignment",
    type=int,
    default=cpu_count(),
    help=f"Number of threads to run the multiple sequence alignment [default: {cpu_count()}]",
)
parser.add_argument(
    "--threads_voc_of_seqs",
    type=int,
    default=cpu_count(),
    help=f"Number of threads to run the voc_of_seqs script [default: {cpu_count()}]",
)
parser.add_argument(
    '--threads_variant_calling',
    type=int,
    default=cpu_count(),
    help=f"Number of threads to run the variant_calling script [default: {cpu_count()}]",
)
parser.add_argument(
    "--threads_raxml",
    type=int,
    default=cpu_count(),
    help=f"Number of threads to run the raxml script [default: {cpu_count()}]",
)
parser.add_argument(
    "-s",
    "--samples",
    type=str,
    default="data",
    help="Directory where samples are stored [default: data]",
)
parser.add_argument(
    "-c",
    "--config",
    type=str,
    default="config.yaml",
    help="File to store configuration [default: config.yaml]",
)
parser.add_argument(
    "--subsample_date",
    type=int,
    required=False,
    help="Number of days to subsample data [default: disabled]",
)
parser.add_argument(
    "--subsample_sequences",
    type=int,
    required=False,
    help="Number of sequences to subsample data [default: disabled]",
)
parser.add_argument(
    "--modus",
    type=str,
    default=["website"],
    nargs='+',
    help="Output modi to use. Options: website and pdf [default: website]",
)
args = parser.parse_args()

with open(args.config, "w") as f:

    f.write(f"workdir: '{os.getcwd()}'\n\n")

    f.write("threads:\n")
    f.write(f"    decompress: {args.decompress}\n")
    f.write(f"    local_alignment: {args.threads_local_alignment}\n")
    f.write(
        f"    multiple_sequence_alignment: {args.threads_multiple_sequence_alignment}\n"
    )
    f.write(f"    voc_of_seqs: {args.threads_voc_of_seqs}\n")
    f.write(f"    variant_calling: {args.threads_variant_calling}\n")
    f.write(f"    raxml: {args.threads_raxml}\n")

    samples = [f.name for f in os.scandir(args.samples) if f.is_dir()]

    f.write("\nsamples:\n")

    regions = {"Global"}

    for sample in samples:
        f.write(f"    {sample}:\n")
        genomes = glob.glob(args.samples + "/" + sample + "/*.fasta.*")
        metadata = glob.glob(args.samples + "/" + sample + "/*.tsv.*")
        if len(genomes) > 0 and len(metadata) > 0:
            f.write(f"        genomes: '{genomes[0]}'\n")
            f.write(f"        metadata: '{metadata[0]}'\n")
            df = pd.read_csv(metadata[0], compression="infer", sep="\t")
            for row in range(df.shape[0]):
                region = df.loc[row, "country"]
                if type(region) == str:
                    regions.add(region)

    if args.subsample_date:
        start_date = datetime.today() - timedelta(days=args.subsample_date)
        start_date = start_date.strftime("%d/%m/%Y")
    else:
        start_date = "01/01/2019"

    if args.subsample_sequences:
        sequence_limit = args.subsample_sequences
    else:
        sequence_limit = 0

    f.write("\nsubsample:\n")
    f.write(f"    #Note: If both conditions cannot be fulfilled, sequence_limit is favored.\n")
    f.write(f"    start_date: '{start_date}' # dd/mm/yyyy\n")
    f.write(f"    sequence_limit: '{sequence_limit}' # 0 means no limit\n")

    for el in args.modus:
        assert el in ["pdf", "website"], "Modus needs to be 'pdf' or 'website'."
    f.write("\noutputs: ['"+"', '".join(args.modus) +"']\n")

    f.write("\nfrequency: 'weekly'\n")

    f.write("\nspike: 'data/spike-protein.fasta'\n")

    f.write("\nvoc: 'data/voc.tsv'\n")

    f.write("\nspike_extraction:")
    f.write("\n    max_ns: 5 # percentage")
    f.write("\n    max_length_deviation: 5 # percentage")
    f.write("\n")
    f.write("\nreport:")
    f.write("\n    title: 'SARS-CoV-2 Spike Protein Analysis'")
    f.write("\n    template: 'resources/template.pug'")
    f.write("\n    style: 'resources/style.css'")
    f.write("\n    workflow: 'resources/workflow.svg'")
    f.write("\n    logo: 'resources/rki_logo.svg'")
    f.write("\n    sidenote: 'The content of this report is confidential.")
    f.write(
        " It is strictly forbidden to share any parts of this report with any third party"
    )
    f.write(", without written consent from RKI.'")
    f.write("\n")

    regions = list(regions)
    regions.sort()
    f.write("\nregions:\n")
    for region in regions:
        f.write(f"  - {region}\n")
