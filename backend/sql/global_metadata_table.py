from datetime import datetime
import mysql.connector
from tqdm import tqdm
import pathlib
import pandas as pd


def try_parsing_date(text):
    for fmt in ('%Y-%m-%d', '%d.%m.%Y', '%d/%m/%Y', '%d-%b-%Y'):
        try:
            return datetime.strptime(text, fmt)
        except ValueError:
            pass
    return 'Invalid'


def create_metadata_table(date, config, metadata):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    cursor.execute("DROP TABLE IF EXISTS `temp_global_metadata`")

    indices = []
    metadata = pd.read_csv(metadata, sep='\t',na_values='unknown', low_memory=False).astype(
        str).replace(to_replace='nan', value='', regex=True)
    metadata.rename(
        columns={'primary diagnostic lab pc': 'primary_diagnostic_lab_pc'}, inplace=True)
    metadata.columns = metadata.columns.str.lower()
    columns = list(metadata.columns)
    if 'submitting_lab' in columns:
        metadata.submitting_lab = metadata.submitting_lab.replace(
            to_replace=r'\.0', value='', regex=True)
        metadata.submitting_lab = metadata.submitting_lab.replace(
            to_replace=r'', value='0', regex=True)
    if 'primary_diagnostic_lab_pc' in columns:
        metadata.primary_diagnostic_lab_pc = metadata.primary_diagnostic_lab_pc.replace(
            to_replace=r'\.0', value='', regex=True)
        metadata.primary_diagnostic_lab_pc = metadata.primary_diagnostic_lab_pc.replace(
            to_replace=r'', value='0', regex=True)
    if 'demis_id' in columns:
        metadata.demis_id = metadata.demis_id.replace(to_replace=r'\.0', value='', regex=True)
        metadata.demis_id = metadata.demis_id.replace(to_replace=r'', value='0', regex=True)
    create_statement = """CREATE TABLE IF NOT EXISTS `temp_global_metadata`(
    strain_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,"""
    for column_with_case in columns:
        column = column_with_case.lower()
        if column == 'date':
            create_statement += column + " DATE, "
        elif column == 'reporting_lab':
            create_statement += column + " INT, "
        elif column == 'demis_id':
            create_statement += column + " INT, "
        elif column == 'primary_diagnostic_lab_pc':
            create_statement += column + " INT, "
        elif column == 'strain':
            create_statement += column + " VARCHAR(250), "
            indices.append(column)
        else:
            create_statement += column.replace(" ", "_") + " VARCHAR(500), "
            if column in ('country','host','date'):
                indices.append(column)
    # remove trailing comma and space
    create_statement = create_statement[:-2]
    create_statement += ") ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=utf8mb4_bin"

    cursor.execute(create_statement)
    print("Inserting data into metadata table")
    pbar = tqdm(total=metadata.shape[0] + len(indices))

    data = []

    for row in range(metadata.shape[0]):
        metadata.at[row, 'date'] = try_parsing_date(metadata.at[row, 'date'])
        # only insert row if date is valid
        if metadata.at[row, 'date'] != 'Invalid':
            data.append(tuple(metadata.iloc[row, :]))
        pbar.update(1)

    columns = [i.lower().replace(" ", "_") for i in columns]
    insert_statement = "INSERT INTO temp_global_metadata " + str(tuple(columns)).replace("'", "")
    insert_statement = insert_statement + " VALUES " + \
        str(tuple(["%s"] * len(columns))).replace("'", "")
    cursor.executemany(insert_statement, data)
    for _ in cursor:
        pass
    # Create indices for each column where they might be helpful
    for column in indices:
        # cursor.execute(f"CREATE INDEX global_metadata_{column} ON temp_global_metadata (`{column}`)")
        pbar.update(1)
    conn.commit()
    conn.close()
    pbar.close()

