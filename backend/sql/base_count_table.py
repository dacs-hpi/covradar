import pickle
from os.path import isfile, join
import mysql.connector
from os import listdir
from tqdm import tqdm
from Bio import SeqIO
import os


def create_base_count_table(config, consensus_folder):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_base_count`")

    cursor.execute(
        "CREATE TABLE IF NOT EXISTS `temp_base_count`(region VARCHAR(255) NOT NULL, "
        "date VARCHAR(255) NOT NULL, position INT NOT NULL, count INT NOT NULL, PRIMARY KEY (region, date, position))"
    )

    folders = [f.path for f in os.scandir(consensus_folder) if f.is_dir()]
    rows = 0
    for folder in folders:
        files = [
            f for f in listdir(folder) if isfile(join(folder, f)) and f.endswith(".sav")
        ]
        rows = rows + len(files)
    print("Inserting data into base_count table")
    pbar = tqdm(total=rows)
    for folder in folders:
        files = [
            f for f in listdir(folder) if isfile(join(folder, f)) and f.endswith(".sav")
        ]
        for f in files:
            region = ""
            date = ""
            for record in SeqIO.parse(
                folder + "/" + f.replace(".sav", ".fasta"), "fasta"
            ):
                region = str(record.description).split("_")[0]
                date = str(record.description).split("_")[1]
            with open(folder + "/" + f, "rb") as fin:
                base_count = pickle.load(fin)
                data = [
                    (region, date, i + 1, base_count[i]) for i in range(len(base_count))
                ]
                statement = "INSERT INTO temp_base_count (region, date, position, count) VALUES(%s, %s, %s, %s)"
                cursor.executemany(statement, data)
            pbar.update(1)
    pbar.close()

    conn.commit()
    conn.close()
