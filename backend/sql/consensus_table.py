from os.path import isfile, join
import mysql.connector
from os import listdir
from tqdm import tqdm
from Bio import SeqIO
import os


def create_consensus_table(config, consensus_folder, first_case):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_consensus`")

    cursor.execute(
        "CREATE TABLE IF NOT EXISTS `temp_consensus`(region VARCHAR(255) NOT NULL, "
        "date VARCHAR(255) NOT NULL, consensus TEXT NOT NULL, number_of_sequences INT NOT NULL, PRIMARY KEY (region, date))"
    )

    folders = [f.path for f in os.scandir(consensus_folder) if f.is_dir()]
    rows = 0
    for folder in folders:
        files = [
            f
            for f in listdir(folder)
            if isfile(join(folder, f)) and f.endswith(".fasta")
        ]
        rows = rows + len(files)
    print("Inserting data into consensus table")
    pbar = tqdm(total=rows + 1)
    data = []
    for folder in folders:
        files = [
            f
            for f in listdir(folder)
            if isfile(join(folder, f)) and f.endswith(".fasta")
        ]
        for f in files:
            region = ""
            date = ""
            sequence = ""
            number_of_sequences = 0
            for record in SeqIO.parse(folder + "/" + f, "fasta"):
                region = str(record.description).split("_")[0]
                date = str(record.description).split("_")[1]
                number_of_sequences = str(record.description).split("_")[2]
                sequence = str(record.seq).upper()
            data.append((region, date, sequence, number_of_sequences))
            pbar.update(1)
    for record in SeqIO.parse(first_case, "fasta"):
        region = "Wuhan"
        date = "first-case"
        data.append((region, date, str(record.seq).upper(), 1))
    statement = "INSERT INTO temp_consensus (region, date, consensus, number_of_sequences) VALUES(%s, %s, %s, %s)"
    cursor.executemany(statement, data)
    pbar.update(1)
    pbar.close()

    conn.commit()
    conn.close()
