import mysql.connector
from tqdm import tqdm
import pathlib
import yaml

def create_zip_table(config, zip_coordinates):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_zip_coordinates`")

    cursor.execute("CREATE TABLE IF NOT EXISTS `temp_zip_coordinates`"
                    "(zip INT NOT NULL, "
                    "lon DECIMAL(15,12) NOT NULL, "
                    "lat DECIMAL(15,12) NOT NULL,"
                    "location VARCHAR(255) NOT NULL, "
                    "PRIMARY KEY (zip)) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 COLLATE=utf8mb4_bin")

    rows = 0
    with open(zip_coordinates, "r") as f:
        for i,l in enumerate(f):
            pass
        rows = i
    print("Inserting data into zip - coordinates table")
    pbar = tqdm(total=rows)

    with open(zip_coordinates, "r") as f:
        next(f) # skip header
        i=0
        data = []
        for line in f:
            i = i+1
            pbar.update(1)
            s = line.split('\t')
            postal_code = s[1]
            lon = s[2]
            lat = s[3]
            location = s[4]

            data.append((postal_code, lon, lat, location))

    statement = "INSERT INTO temp_zip_coordinates (" \
                  "zip, lon, lat, location) " \
                  "VALUES (%s, %s, %s, %s)"

    cursor.executemany(statement, data)
    conn.commit()
    conn.close()
    pbar.close()

if __name__ == "__main__":
    import os
    workdir = ".."
    credentials = yaml.load(open("db_name.yml"), Loader=yaml.FullLoader)
    credentials["user"] = os.environ['MYSQL_USER']
    credentials["host"] = os.environ['MYSQL_HOST']
    credentials["password"] = os.environ['MYSQL_PW']
    zip_coordinates = workdir + "/data/zip_coordinates.tsv"
    create_zip_table(credentials,zip_coordinates)
