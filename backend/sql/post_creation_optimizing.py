# This file is meant add keys to the database to speedup access
# after all the data is inserted and the tables have been renamed
import mysql.connector
def create_keys(credentials):
    conn = mysql.connector.connect(**credentials)
    
    cursor =  conn.cursor()
    # cursor.execute("ALTER TABLE global_SNP ADD CONSTRAINT `snp_strain_fk` FOREIGN KEY (`strain_id`) REFERENCES `global_metadata` (`strain_id`)")
    # cursor.execute("ALTER TABLE global_metadata ADD CONSTRAINT `zip_fk` FOREIGN KEY (`primary_diagnostic_lab_pc`) REFERENCES `Zip_coordinates` (`zip`)")
    # cursor.execute("ALTER TABLE global_metadata ADD KEY `country_lookup_key` (`country`)")
    
    conn.close()


