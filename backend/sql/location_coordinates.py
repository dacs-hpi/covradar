import mysql.connector
from tqdm import tqdm


def create_location_coordinates_table(config, location_coordinates):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_location_coordinates`")

    create_statement = """CREATE TABLE temp_location_coordinates(
    location_ID int(6) Primary key,
    country_ID Varchar(2),
    name VARCHAR(50),
    lon decimal(15,10),
    lat decimal(15,10)   
    );"""

    cursor.execute(create_statement)

    with open(location_coordinates, "r") as f:
        for i, l in enumerate(f):
            pass
        rows = i

    print("Inserting data into location_coordinates table")

    pbar = tqdm(total=rows + 5)

    with open(location_coordinates, "r") as f:
        next(f)  # skip header
        data = []
        for line in f:
            s = line.strip().split(",")
            location_id = s[0]
            country_id = s[1]
            name = s[2]
            lon = s[3]
            lat = s[4]
            if lon != "" and lat != "":
                data.append((location_id, country_id, name, lon, lat))
            pbar.update(1)

    statement = (
        "INSERT INTO temp_location_coordinates ("
        "location_ID, country_ID, name, lon, lat) "
        "VALUES (%s, %s, %s, %s, %s)"
    )

    cursor.executemany(statement, data)

    pbar.update(1)

    statement = "ALTER TABLE temp_global_metadata ADD COLUMN location_ID int(6);"

    cursor.execute(statement)

    pbar.update(1)

    statement = "update temp_global_metadata m inner join temp_location_coordinates lc "
    statement += "on m.country = lc.name set m.location_ID = lc.location_ID;"

    cursor.execute(statement)

    pbar.update(1)

    statement = "UPDATE temp_global_metadata SET location_ID=0 WHERE location_ID is null;"

    cursor.execute(statement)

    pbar.update(1)

    try:
        statement = "update temp_global_metadata set location_ID = primary_diagnostic_lab_pc where country = 'Germany';"
        cursor.execute(statement)
    except:
        pass

    pbar.update(1)

    conn.commit()
    conn.close()
    pbar.close()
