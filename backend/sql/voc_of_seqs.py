import mysql.connector
import pandas as pd
from tqdm import tqdm
import tempfile
from sqlalchemy import create_engine

def get_database_connection(config):
    user = config["user"]
    ip = config["host"]
    pw = config["password"]
    db = config["database"]
    return create_engine(f'mysql+pymysql://{user}:{pw}@{ip}/{db}')

def sequence_voc_row_generation(config, voc_of_seqs):
    data = []
    print("Preparing sequence_voc data...")
    tsv_result = pd.read_csv(voc_of_seqs, sep="\t", low_memory=False, index_col='sequence_name')

    pbar = tqdm(total=len(tsv_result))

    sqlalch_con = get_database_connection(config)
    pbar.update(1)
    voc_ids = pd.read_sql_table("temp_variants_of_concern", index_col="amino_acid", columns=["amino_acid","id"], con=sqlalch_con)
    pbar.update(1)
    ids = pd.read_sql_table("temp_global_metadata", index_col="strain", columns=["strain_id","strain"], con=sqlalch_con)
    pbar.update(1)
    for strain_name, line in tsv_result.iterrows():
        try:
            strain_id = int(ids.at[strain_name,'strain_id'])
        except KeyError:
            if strain_name != 'first-case':
                tqdm.write(f'Strain {strain_name} found in voc_of_seqs but not in metadata')
            continue
        pbar.update(1)
        for voc_name, cell in line.items():
            if cell == True:
                voc_id = int(voc_ids.at[voc_name,'id'])
                data.append((strain_id, voc_id))
    return pd.DataFrame(data, columns=['strain_id', 'voc_id'])

def create_voc_of_seqs_table(config, voc_of_seqs):
    """Uploads voc_of_seqs.tsv containing VOC-sequence relationship to database.

    Args:
        config (dict): DB credentials
        voc_of_seqs (str): path to voc_of_seqs.tsv containing VOC-sequence relationship
    """
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_sequence_voc`")

# todo: add foreign keys
    cursor.execute("""CREATE TABLE `temp_sequence_voc` (
  `voc_id` int NOT NULL,
  `strain_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;""")
    df = sequence_voc_row_generation(config, voc_of_seqs)
    print("Inserting data into voc_of_seqs table")
    pbar = tqdm(total=4)
    
    with tempfile.NamedTemporaryFile(mode='w+b') as f:
        statement = f"""
            LOAD DATA LOCAL INFILE '{f.name}'
            INTO TABLE temp_sequence_voc
            FIELDS TERMINATED BY '\t'
            LINES TERMINATED BY '\n'
            (strain_id, voc_id)
    """

        df.to_csv(f, sep="\t", index=False, header=False)
        pbar.update(1)
        cursor.execute(statement)
    pbar.update(1)
    # cursor.execute("CREATE INDEX seuence_voc_voc_IDX ON temp_sequence_voc (`voc_id`)")    
    pbar.update(1)
    # cursor.execute("CREATE INDEX sequence_voc_strain_IDX ON temp_sequence_voc (`strain_id`)")
    pbar.update(1)
    conn.commit()
    conn.close()
