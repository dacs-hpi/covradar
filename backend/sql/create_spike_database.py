import argparse
import datetime
import os

import mysql.connector
from tqdm import tqdm

from consensus_table import create_consensus_table
from base_count_table import create_base_count_table
from data_subset import create_data_subset
from foreign_keys import drop_all_fks, add_all_fks
from global_SNP_table import create_SNP_table
from global_metadata_table import create_metadata_table
from global_position_table import create_position_table
from global_statistics_table import create_statistics_table
from location_coordinates import create_location_coordinates_table
from variants_of_concern import create_variants_of_concern_table
from voc_of_seqs import create_voc_of_seqs_table
from zip_table import create_zip_table

today = datetime.datetime.now().strftime("%d-%m-%Y")

parser = argparse.ArgumentParser(description='Inserts pipeline results into a mysql database')
parser.add_argument('-w', '--workdir', type=str, default='..', help='Pipeline workdir [default: ..]')
parser.add_argument('-d', '--date', type=str, default=today, help='Date [default: %s]' % (today))
parser.add_argument('-u', '--user', type=str, default=os.environ['MYSQL_USER'], help='MySQL user [default: %s]' % (os.environ['MYSQL_USER']))
parser.add_argument('--host', type=str, default=os.environ['MYSQL_HOST'], help='MySQL host [default: %s]' % (os.environ['MYSQL_HOST']))
parser.add_argument('-p', '--password', type=str, default=os.environ['MYSQL_PW'], help='MySQL password [default: %s]' % (os.environ['MYSQL_PW']))
parser.add_argument('-db', '--database', type=str, default='Spike', help='MySQL DB name [default: Spike]')
parser.add_argument('-sdb', '--small-database', type=str, required=False, help='MySQL DB name for table with only recent sequences (not necessary, omitted by default)')
parser.add_argument('-m', '--max-age', type=int, default=90, help='How many days of sequences shall be included in the small database')

args = parser.parse_args()

credentials = {
    "user": args.user,
    "host": args.host,
    "password": args.password,
    "database": args.database,
    "allow_local_infile": True#,
    #"unix_socket": "/var/run/mysqld/mysqld.sock",
}

metadata = args.workdir + "/results/extracted_spikes/extracted_metadata.tsv"
snp = args.workdir + "/results/website/variant_calling/variant_calling.vcf.gz"
bcftools = args.workdir + "/results/website/bcftools/bcftools.txt"
positions = args.workdir + "/results/numbering/position_table.tsv"
consensus_folder = args.workdir + "/results/consensus/consensus"
first_case = args.workdir + "/results/multiple_sequence_alignment/first_case.fasta"
voc = args.workdir + "/results/website/voc/voc.tsv"
voc_of_seqs = args.workdir + "/results/website/voc_of_seqs/voc_of_seqs.tsv"
zip_coordinates = args.workdir + "/data/zip_coordinates.tsv"
location_coordinates = args.workdir + "/data/location_coordinates.csv"

create_statistics_table(args.date, credentials, metadata, bcftools)
create_metadata_table(args.date, credentials, metadata)
create_position_table(credentials, positions)
create_consensus_table(credentials, consensus_folder, first_case)
create_base_count_table(credentials, consensus_folder)
create_SNP_table(credentials, snp)
create_variants_of_concern_table(credentials, voc)
create_voc_of_seqs_table(credentials, voc_of_seqs)
create_zip_table(credentials, zip_coordinates)
create_location_coordinates_table(credentials, location_coordinates)

# The tables are written to temporary ones then renamed
# This drastically reduces downtime to seconds during updates

conn = mysql.connector.connect(**credentials)
cursor = conn.cursor()

drop_all_fks(conn)

tables = [
    "global_statistics",
    "global_metadata",
    "global_SNP",
    "global_position",
    "consensus",
    "base_count",
    "variants_of_concern",
    "sequence_voc",
    "zip_coordinates",
    "location_coordinates",
]

print("Renaming temporary tables")
pbar = tqdm(total=len(tables))
for i in range(len(tables)):
    cursor.execute("DROP TABLE IF EXISTS `{0}`".format(tables[i]))
    cursor.execute("RENAME TABLE `temp_{0}` TO `{0}`".format(tables[i]))
    pbar.update(1)
cursor.close()
pbar.close()

add_all_fks(conn)

conn.commit()
conn.close()

if args.small_database:
    create_data_subset(credentials, args.small_database, args.max_age)
