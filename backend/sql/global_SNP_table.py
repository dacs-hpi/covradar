from tqdm import tqdm
import mysql.connector
from math import ceil
import os
import pandas as pd
from sqlalchemy import create_engine
import tempfile
import gzip
import sys
if sys.version_info[0] < 3:
    from StringIO import StringIO, TextIOWrapper
else:
    from io import StringIO, TextIOWrapper

def get_database_connection(config):
    user = config["user"]
    ip = config["host"]
    pw = config["password"]
    db = config["database"]
    return create_engine(f'mysql+pymysql://{user}:{pw}@{ip}/{db}')

def global_SNP_row_generation(config, snp_path):
    sqlalch_con = get_database_connection(config)
    ids = pd.read_sql_table("temp_global_metadata", index_col="strain", columns=["strain_id","strain"], con=sqlalch_con)
    print('Preparing SNP data')
    with gzip.open(snp_path, 'rb') as f:
        with TextIOWrapper(f, encoding='utf-8') as fin:
            pbar = tqdm(total=sum(1 for _ in fin))
            fin.seek(0)
            fin.readline()
            fin.readline()
            pbar.update(2)
            strains = fin.readline().strip().split('\t')
            pbar.update(3)
            #(strain name, position, ref_allele, alt)
            data = []
            for line in fin:
                pbar.update(1)
                row = line.split("\t")
                alt = row[4].split(",")
                for i in range(8, len(row)):
                    if int(row[i]) > 0:
                        data.append((int(ids.at[strains[i],'strain_id']), row[1], row[3],alt[int(row[i]) - 1]))
            pbar.close()
            return pd.DataFrame(data, columns=["strain_id", "position", "REF_allele", "ALT"])

def create_SNP_table(config, snp_path):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()
    cursor.execute("DROP TABLE IF EXISTS `temp_global_SNP`")
    cursor.execute("""CREATE TABLE `temp_global_SNP` (
  `strain_id` INT NOT NULL,
  `position` INT NOT NULL,
  `REF_allele` CHAR(1),
  `ALT` CHAR(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin""")

    df = global_SNP_row_generation(config, snp_path)
    print("Inserting data into SNP table")
    pbar = tqdm(total=4)
    
    with tempfile.NamedTemporaryFile(mode='w+b') as f:
        statement = f"""
        LOAD DATA LOCAL INFILE '{f.name}'
        INTO TABLE temp_global_SNP
        FIELDS TERMINATED BY '\t'
        LINES TERMINATED BY '\n'
        (strain_id, position, REF_allele, ALT)
    """
        df.to_csv(f, sep='\t', index=False, header=False)
        pbar.update(1)
        cursor.execute(statement)
    pbar.update(1)
    # cursor.execute("ALTER TABLE temp_global_SNP ADD CONSTRAINT global_SNP_PK PRIMARY KEY (`strain_id`,`position`)")
    pbar.update(1)
    # cursor.execute("CREATE INDEX global_SNP_strain_IDX on temp_global_metadata (`strain_id`)")
    pbar.update(1)

    conn.commit()
    pbar.close()
