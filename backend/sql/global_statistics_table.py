import mysql.connector
from tqdm import tqdm
import pathlib
import re
import pandas as pd

def create_statistics_table(date, config, metadata, bcftools_log_path):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_global_statistics`")

    cursor.execute("CREATE TABLE IF NOT EXISTS `temp_global_statistics`(date VARCHAR(10) NOT NULL, "
                       "min_date VARCHAR(10) NOT NULL, max_date VARCHAR(10) NOT NULL, number_seq INT NOT NULL, snps INT NOT NULL, "
                       "multiallelic_sites INT NOT NULL, multiallelic_snp_sites INT NOT NULL, PRIMARY KEY (date))")

    rows = 0
    print("Inserting data into statistics table")
    pbar = tqdm(total=1)

    metadata = pd.read_csv(metadata, sep='\t', usecols=["date"], low_memory=False)
    dates = pd.to_datetime(metadata["date"])
    min_date = dates.min().strftime("%d-%m-%Y")
    max_date = dates.max().strftime("%d-%m-%Y")

    number_seq = len(metadata)

    bcftools_file = open(bcftools_log_path, "r")
    bcftools = bcftools_file.read()
    bcftools_file.close()
    snps = re.findall("(?<=number of SNPs:	).*|$", bcftools)[0]
    multiallelic_sites = re.findall("(?<=number of multiallelic sites:	).*|$", bcftools)[0]
    multiallelic_snp_sites = re.findall("(?<=number of multiallelic SNP sites:	).*|$", bcftools)[0]

    sql_query = []
    sql_query.append("INSERT INTO temp_global_statistics ")
    sql_query.append("(date, min_date, max_date, number_seq, snps, multiallelic_sites, multiallelic_snp_sites) ")
    sql_query.append("VALUES(%s, %s, %s, %s, %s, %s, %s)")
    sql_query = ''.join(sql_query)
    cursor.execute(sql_query, (date, min_date, max_date, number_seq, snps, multiallelic_sites, multiallelic_snp_sites))
    pbar.update(1)

    conn.commit()
    conn.close()
    pbar.close()
