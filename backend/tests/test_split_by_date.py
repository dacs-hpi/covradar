import filecmp
import unittest
from io import StringIO

import pandas as pd
import pytest
from pandas.testing import assert_frame_equal
from pandas.testing import assert_series_equal

from backend.scripts.split_by_date import (
    parse_args,
    load_metadata,
    load_sequences,
    split_daily,
    get_weeks,
    write_sequence,
    split_weekly,
)
from backend.tests.conftest import dpath, delete


class TestSplitByDate(unittest.TestCase):
    def test_parser(self):
        parser = parse_args(
            [
                "--sequences",
                "sequences.fasta",
                "--metadata",
                "metadata.tsv",
                "--output-folder",
                "Germany",
                "--frequency",
                "weekly",
            ]
        )
        self.assertTrue(parser.sequences)
        self.assertEqual(parser.sequences, "sequences.fasta")
        self.assertTrue(parser.metadata)
        self.assertEqual(parser.metadata, "metadata.tsv")
        self.assertTrue(parser.output_folder)
        self.assertEqual(parser.output_folder, "Germany")
        self.assertTrue(parser.frequency)
        self.assertEqual(parser.frequency, "weekly")

    def test_load_metadata(self):
        metadata = StringIO()
        metadata.write("strain\tdate\n")
        metadata.write("IMS-101\t2022-02-16\n")
        metadata.seek(0)
        df = load_metadata(metadata)
        ground_truth = pd.DataFrame({"strain": ["IMS-101"], "date": ["2022-02-16"]})
        assert_frame_equal(ground_truth, df)

    def test_load_sequences(self):
        genomes = dpath("testdata/get_or_delete_region/genomes.fasta")
        sequences = load_sequences(genomes)
        ground_truth = {"1": "ATCG", "2": "CGAT"}
        self.assertDictEqual(ground_truth, sequences)

    def test_split_daily(self):
        with pytest.raises(NotImplementedError):
            split_daily(pd.DataFrame(), {}, "")

    def test_write_sequence_1(self):
        ground_truth = ">Test_1\nATCG\n"
        output = StringIO()
        id = "Test_1"
        sequence = "ATCG"
        write_sequence(output, id, sequence)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_2(self):
        ground_truth = ">Test_1\nAT\nCG\n>Test_2\nAT\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATCG", 2)
        write_sequence(output, "Test_2", "AT", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_3(self):
        ground_truth = ">Test_1\nAT\nC\n>Test_2\nA\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATC", 2)
        write_sequence(output, "Test_2", "A", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_get_weeks(self):
        dates = [
            "2022-01-01",
            "2022-01-02",
            "2022-01-03",
        ]
        metadata = pd.DataFrame({"date": dates})
        weeks = get_weeks(metadata)
        ground_truth = pd.Series(["2021W52", "2021W52", "2022W01"], name="date")
        assert_series_equal(ground_truth, weeks)

    def test_split_weekly(self):

        # Load input files
        in_fasta = dpath("testdata/split_by_date/input.fasta")
        in_metadata = dpath("testdata/split_by_date/input.tsv")

        # Ground truth files
        ground_truth_2021w52_fasta = dpath(
            "testdata/split_by_date/ground_truth_2021W52.fasta"
        )
        ground_truth_2021w52_tsv = dpath(
            "testdata/split_by_date/ground_truth_2021W52.tsv"
        )
        ground_truth_2022w01_fasta = dpath(
            "testdata/split_by_date/ground_truth_2022W01.fasta"
        )
        ground_truth_2022w01_tsv = dpath(
            "testdata/split_by_date/ground_truth_2022W01.tsv"
        )

        # Output files
        w52_fasta = dpath("testdata/split_by_date/2021W52.fasta")
        w52_tsv = dpath("testdata/split_by_date/2021W52.tsv")
        w01_fasta = dpath("testdata/split_by_date/2022W01.fasta")
        w01_tsv = dpath("testdata/split_by_date/2022W01.tsv")

        # Delete output files if they already exist
        delete([w52_fasta, w52_tsv, w01_fasta, w01_tsv])

        # Run algorithm
        metadata = load_metadata(in_metadata)
        sequences = load_sequences(in_fasta)
        output_folder = dpath("testdata/split_by_date")
        split_weekly(metadata, sequences, output_folder)

        # Assert output files are correct
        assert filecmp.cmp(ground_truth_2021w52_fasta, w52_fasta)
        assert filecmp.cmp(ground_truth_2021w52_tsv, w52_tsv)
        assert filecmp.cmp(ground_truth_2022w01_fasta, w01_fasta)
        assert filecmp.cmp(ground_truth_2022w01_tsv, w01_tsv)

        # Delete output files again
        delete([w52_fasta, w52_tsv, w01_fasta, w01_tsv])


if __name__ == "__main__":
    unittest.main()
