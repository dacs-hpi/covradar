"""
Test cases for rule voc_modify
"""
import unittest
import pandas as pd
from backend.scripts.variant_calling import main
from backend.tests.conftest import dpath, delete
import ray

class Test1(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test1_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test1_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test1_result.tsv") #correct
        self.result = dpath("testdata/variant_calling/test1_out.tsv.gz") #result
        ray.init(local_mode=True)
        self.batch_size = 1

    def test_1(self):
        """Tests with dummy data covering possible mutation notations.
        """
        main(self.batch_size, self.msa, self.ref, self.result)
        result = pd.read_csv(self.result, sep="\t", compression="gzip", skiprows=2)
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        delete([self.result])
        
    def tearDown(self):
        ray.shutdown()

class Test2(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test2_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test2_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test2_result.tsv") #correct
        self.result = dpath("testdata/variant_calling/test2_out.tsv.gz") #result
        ray.init(local_mode=True)
        self.batch_size = 1

    def test_2(self):
        """Tests with dummy data covering possible mutation notations.
        """
        main(self.batch_size, self.msa, self.ref, self.result)
        result = pd.read_csv(self.result, sep="\t", compression="gzip", skiprows=2)
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        delete([self.result])

    def tearDown(self):
        ray.shutdown()


class Test3(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test3_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test3_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test3_result.tsv") #correct
        self.result = dpath("testdata/variant_calling/test3_out.tsv.gz") #result
        ray.init(local_mode=True)
        self.batch_size = 1

    def test_3(self):
        """Tests with dummy data covering possible mutation notations.
        """
        main(self.batch_size, self.msa, self.ref, self.result)
        result = pd.read_csv(self.result, sep="\t", compression="gzip", skiprows=2)
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        delete([self.result])

    def tearDown(self):
        ray.shutdown()

class Test4(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test4_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test4_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test4_result.tsv") #correct
        self.result = dpath("testdata/variant_calling/test4_out.tsv.gz") #result
        ray.init(local_mode=True)
        self.batch_size = 1

    def test_4(self):
        """Tests with dummy data covering possible mutation notations.
        """
        main(self.batch_size, self.msa, self.ref, self.result)
        result = pd.read_csv(self.result, sep="\t", compression="gzip", skiprows=2)
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        delete([self.result])

    def tearDown(self):
        ray.shutdown()


class Test5(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test5_global-msa-25-08-2020_and_reference.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test5_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test5_result.tsv") #correct
        self.result = dpath("testdata/variant_calling/test5_out.tsv.gz") #result
        ray.init(local_mode=True)
        self.batch_size = 1

    def test_5(self):
        """Tests with dummy data covering possible mutation notations.
        """
        main(self.batch_size, self.msa, self.ref, self.result)
        result = pd.read_csv(self.result, sep="\t", compression="gzip", skiprows=2)
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False)
        delete([self.result])

    def tearDown(self):
        ray.shutdown()


class Test6(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test6_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test6_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test6_result.tsv") #correct
        self.result = dpath("testdata/variant_calling/test6_out.tsv.gz") #result
        ray.init(local_mode=True)
        self.batch_size = 1

    def test_6(self):
        """Tests with dummy data covering possible mutation notations.
        """
        main(self.batch_size, self.msa, self.ref, self.result)
        result = pd.read_csv(self.result, sep="\t", compression="gzip", skiprows=2)
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        delete([self.result])

    def tearDown(self):
        ray.shutdown()


if __name__ == '__main__':
    unittest.main()
