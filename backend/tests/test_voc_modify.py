"""
Test cases for rule voc_modify
"""
import unittest
import pandas as pd
from backend.scripts.voc_modify import voc_modify
from backend.tests.conftest import dpath


class TestVoc(unittest.TestCase):
    """Test suite voc function"""
    def setUp(self):
        self.voc_path = dpath("testdata/voc/voc.tsv")
        self.position_table = dpath("testdata/voc/position_table.tsv")

    def test_voc_table(self):
        """Tests with dummy data covering possible mutation notations.
        """
        correct_result_path = dpath("testdata/voc/result.tsv")
        correct_table = pd.read_csv(correct_result_path, sep="\t")
        result = voc_modify(self.voc_path, self.position_table)
        pd.testing.assert_frame_equal(result, correct_table)

if __name__ == '__main__':
    unittest.main()
