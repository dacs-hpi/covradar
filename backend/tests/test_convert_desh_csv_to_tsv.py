import unittest
from io import StringIO

import numpy as np
import pandas as pd
from pandas.testing import assert_frame_equal

from backend.scripts.convert_desh_csv_to_tsv import (
    parse_args,
    read_csv,
    rename_columns,
    add_columns,
    write_tsv,
    get_metadata_columns,
    get_lineage_columns,
    add_lineage,
)


class TestConvertDESHCSVToTSV(unittest.TestCase):
    def test_parser(self):
        parser = parse_args(
            [
                "--metadata",
                "metadata.csv",
                "--lineage",
                "lineage.csv",
                "--tsv",
                "metadata.tsv",
            ]
        )
        self.assertTrue(parser.metadata)
        self.assertEqual(parser.metadata, "metadata.csv")
        self.assertTrue(parser.lineage)
        self.assertEqual(parser.lineage, "lineage.csv")
        self.assertTrue(parser.tsv)
        self.assertEqual(parser.tsv, "metadata.tsv")

    def test_get_metadata_columns(self):
        ground_truth = [
            "IMS_ID",
            "DATE_DRAW",
            "SEQUENCING_LAB_PC",
        ]
        self.assertListEqual(ground_truth, get_metadata_columns())

    def test_get_lineage_columns(self):
        ground_truth = [
            "IMS_ID",
            "lineage",
        ]
        self.assertListEqual(ground_truth, get_lineage_columns())

    def test_read_metadata(self):
        data = StringIO()
        data.write("IMS_ID,DATE_DRAW,SEQUENCING_LAB_PC\n")
        data.write("00001,2021-01-14,40225.0\n")
        data.seek(0)
        columns = ["IMS_ID", "DATE_DRAW", "SEQUENCING_LAB_PC"]
        metadata = read_csv(data, columns)
        data.seek(0)
        ground_truth = pd.read_csv(data, dtype={"IMS_ID": str, "SENDING_LAB_PC": str})
        assert_frame_equal(ground_truth, metadata)

    def test_read_lineage(self):
        data = StringIO()
        data.write("IMS_ID,lineage\n")
        data.write("00001,B.1.1.7\n")
        data.write("00002,B.1.160\n")
        data.seek(0)
        columns = ["IMS_ID", "lineage"]
        metadata = read_csv(data, columns)
        data.seek(0)
        ground_truth = pd.read_csv(data, dtype={"IMS_ID": "str"})
        assert_frame_equal(ground_truth, metadata)

    def test_add_lineage(self):
        ground_truth = StringIO()
        ground_truth.write("IMS_ID,DATE_DRAW,SEQUENCING_LAB_PC,lineage\n")
        ground_truth.write("00001,2021-01-14,40225.0,B.1.160\n")
        ground_truth.write("00002,2021-01-15,40226.0,B.1.1.7\n")
        ground_truth.write("00003,2021-01-16,40227.0,\n")
        ground_truth.seek(0)
        ground_truth = pd.read_csv(ground_truth, dtype={"IMS_ID": "str"})
        metadata = StringIO()
        metadata.write("IMS_ID,DATE_DRAW,SEQUENCING_LAB_PC\n")
        metadata.write("00001,2021-01-14,40225.0\n")
        metadata.write("00002,2021-01-15,40226.0\n")
        metadata.write("00003,2021-01-16,40227.0\n")
        metadata.seek(0)
        metadata = pd.read_csv(metadata, dtype={"IMS_ID": "str"})
        lineage = StringIO()
        lineage.write("IMS_ID,lineage\n")
        lineage.write("00002,B.1.1.7\n")
        lineage.write("00001,B.1.160\n")
        lineage.seek(0)
        lineage = pd.read_csv(lineage, dtype={"IMS_ID": "str"})
        merged = add_lineage(metadata, lineage)
        assert_frame_equal(merged, ground_truth)

    def test_rename_columns(self):
        data = StringIO()
        data.write("IMS_ID,DATE_DRAW,SEQUENCING_LAB_PC\n")
        data.write("00001,2021-01-14,40225.0\n")
        data.seek(0)
        output = StringIO()
        output.write("strain,date,primary diagnostic lab pc\n")
        output.write("00001,2021-01-14,40225.0\n")
        output.seek(0)
        ground_truth = pd.read_csv(output)
        metadata = pd.read_csv(data)
        rename_columns(metadata)
        assert_frame_equal(ground_truth, metadata)

    def test_add_columns(self):
        output = StringIO()
        output.write("strain,")
        output.write("virus,")
        output.write("region,")
        output.write("country\n")
        output.write("00001,")
        output.write("SARS-CoV-2,")
        output.write("Europe,")
        output.write("Germany\n")
        output.seek(0)
        ground_truth = pd.read_csv(output)
        ground_truth.replace(np.nan, "", regex=True, inplace=True)
        metadata = pd.read_csv(StringIO("strain\n00001\n"))
        add_columns(metadata)
        assert_frame_equal(ground_truth, metadata)

    def test_write_tsv(self):
        ground_truth = (
            "strain\tvirus\tdate\tregion\tcountry\tlineage\tprimary diagnostic lab pc\n"
        )
        ground_truth += "1\tSARS-CoV-2\t01/01/2021\tEurope\tGermany\tB.1.1.7\t40225.0\n"
        data = StringIO()
        data.write("strain,")
        data.write("virus,")
        data.write("date,")
        data.write("region,")
        data.write("country,")
        data.write("lineage,")
        data.write("primary diagnostic lab pc\n")
        data.write("00001,")
        data.write("SARS-CoV-2,")
        data.write("01/01/2021,")
        data.write("Europe,")
        data.write("Germany,")
        data.write("B.1.1.7,")
        data.write("40225.0")
        data.seek(0)
        metadata = pd.read_csv(data)
        output = StringIO()
        write_tsv(metadata, output)
        output.seek(0)
        self.assertEqual(ground_truth, output.read())


if __name__ == "__main__":
    unittest.main()
