import unittest
from io import StringIO

from backend.scripts.consensus import (
    parse_args,
    write_sequence,
    get_identifier,
    compute_consensus,
    initialize_counters,
    update_counter,
)
from backend.tests.conftest import dpath


class TestConsensus(unittest.TestCase):
    def test_parser(self):
        parser = parse_args(
            [
                "--input",
                "input.fasta",
                "--output",
                "output.fasta",
                "--base-count",
                "base-count.sav",
                "--region",
                "Brazil",
            ]
        )
        self.assertTrue(parser.input)
        self.assertEqual(parser.input, "input.fasta")
        self.assertTrue(parser.output)
        self.assertEqual(parser.output, "output.fasta")
        self.assertTrue(parser.base_count)
        self.assertEqual(parser.base_count, "base-count.sav")
        self.assertTrue(parser.region)
        self.assertEqual(parser.region, "Brazil")

    def test_write_sequence_1(self):
        ground_truth = ">Test_1\nATCG\n"
        output = StringIO()
        identifier = "Test_1"
        sequence = "ATCG"
        write_sequence(output, identifier, sequence)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_2(self):
        ground_truth = ">Test_1\nAT\nCG\n>Test_2\nAT\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATCG", 2)
        write_sequence(output, "Test_2", "AT", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_3(self):
        ground_truth = ">Test_1\nAT\nC\n>Test_2\nA\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATC", 2)
        write_sequence(output, "Test_2", "A", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_get_identifier(self):
        input_file = "results/website/split_by_date/Japan/2020W24.fasta"
        region = "Japan"
        number_of_sequences = 3
        identifier = get_identifier(region, input_file, number_of_sequences)
        ground_truth = "Japan_2020W24_3"
        self.assertEqual(ground_truth, identifier)

    def test_initialize_counters(self):
        sequence = "AATCGAX"
        ground_truth = [
            {"A": 1, "C": 0, "G": 0, "T": 0, "-": 0, "N": 0},
            {"A": 1, "C": 0, "G": 0, "T": 0, "-": 0, "N": 0},
            {"A": 0, "C": 0, "G": 0, "T": 1, "-": 0, "N": 0},
            {"A": 0, "C": 1, "G": 0, "T": 0, "-": 0, "N": 0},
            {"A": 0, "C": 0, "G": 1, "T": 0, "-": 0, "N": 0},
            {"A": 1, "C": 0, "G": 0, "T": 0, "-": 0, "N": 0},
            {"A": 0, "C": 0, "G": 0, "T": 0, "-": 0, "N": 1},
        ]
        counter = initialize_counters(sequence)
        self.assertListEqual(ground_truth, counter)

    def test_update_counter(self):
        counter = {"A": 1, "N": 0}
        update_counter(counter, "A")
        self.assertDictEqual(counter, {"A": 2, "N": 0})
        update_counter(counter, "X")
        self.assertDictEqual(counter, {"A": 2, "N": 1})

    def test_compute_consensus(self):
        genomes = dpath("testdata/consensus/genomes.fasta")
        ground_truth = ("AATCG-N", [1, 2, 2, 2, 2, 2, 3], 3)
        consensus = compute_consensus(genomes)
        self.assertTupleEqual(ground_truth, consensus)
