import unittest
import pandas as pd
import numpy as np
from backend.scripts.base_freq_whole_prot import allele_freq
# %%
# %%
from backend.tests.conftest import dpath


class TestFreq(unittest.TestCase):

    def setUp(self):
        snp_path = dpath("testdata/frequency_plot/test_vcf.tsv")
        self.vcf_data = pd.read_csv(snp_path, sep="\t", skiprows=2)
        self.total_seqs = 16
        self.spike_len = 6

    def test_freqs(self):
        result = allele_freq(self.vcf_data, self.spike_len)
        correct = np.array([float(8/9), float(8/9), float(8/9), float(8/9), 0, float(15/16)])
        np.testing.assert_array_equal(result, correct)


if __name__ == '__main__':
    unittest.main()
