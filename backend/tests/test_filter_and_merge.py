import unittest
import string
from datetime import datetime
from io import StringIO

import pytest

from backend.tests.conftest import dpath, delete
import filecmp
from backend.scripts.filter_and_merge import (
    is_valid_date,
    is_valid_sequence,
    write_sequence,
    parse_args,
    retrieve_columns,
    add_important_columns,
    get_origin,
    filter_metadata,
    filter_genomes,
)


class TestFilterAndMerge(unittest.TestCase):
    def test_parser(self):
        parser = parse_args(
            [
                "--genomes",
                "ebi.fasta",
                "gisaid.fasta",
                "--metadata",
                "ebi.tsv",
                "gisaid.tsv",
                "--valid-genomes",
                "valid.fasta",
                "--valid-metadata",
                "valid.tsv",
                "--discarded-genomes",
                "discarded.fasta",
                "--discarded-metadata",
                "discarded.tsv",
                "--start-date",
                "01/01/2020",
                "--sequence-limit",
                "0"
            ]
        )
        self.assertTrue(parser.genomes)
        self.assertEqual(parser.genomes, ["ebi.fasta", "gisaid.fasta"])
        self.assertTrue(parser.metadata)
        self.assertEqual(parser.metadata, ["ebi.tsv", "gisaid.tsv"])
        self.assertTrue(parser.valid_genomes)
        self.assertEqual(parser.valid_genomes, "valid.fasta")
        self.assertTrue(parser.valid_metadata)
        self.assertEqual(parser.valid_metadata, "valid.tsv")
        self.assertTrue(parser.discarded_genomes)
        self.assertEqual(parser.discarded_genomes, "discarded.fasta")
        self.assertTrue(parser.discarded_metadata)
        self.assertEqual(parser.discarded_metadata, "discarded.tsv")
        self.assertTrue(parser.start_date)
        self.assertEqual(parser.start_date, "01/01/2020")

    def test_is_valid_date(self):
        ground_truth = datetime(2021, 2, 1, 0, 0)
        date = datetime(2019, 1, 1)
        assert is_valid_date("2021-02-01", date) == ground_truth
        assert is_valid_date("01.02.2021", date) == ground_truth
        assert is_valid_date("01/02/2021", date) == ground_truth
        assert is_valid_date("01-Feb-2021", date) == ground_truth
        assert not is_valid_date("01-02", date)
        assert not is_valid_date("01.02", date)
        assert not is_valid_date("01/02", date)
        assert not is_valid_date("01-Feb", date)
        assert not is_valid_date("2021", date)
        assert not is_valid_date("01/02/2019", datetime(2020, 1, 1))

    def test_retrieve_columns(self):
        ground_truth = {"originating_lab", "submitting_lab", "accession"}
        columns = set()
        retrieve_columns("originating lab\tsubmitting lab", columns)
        retrieve_columns("accession", columns)
        self.assertSetEqual(columns, ground_truth)

    def test_add_important_columns(self):
        ground_truth = ["country", "country_exposure", "host", "origin"]
        columns = add_important_columns(set())
        self.assertListEqual(columns, ground_truth)

    def test_get_origin(self):
        assert get_origin("genomes_ebi.fasta") == "ebi"
        assert get_origin("genomes_ebi.tsv") == "ebi"
        assert get_origin("genomes_gisaid.fasta") == "gisaid"
        assert get_origin("genomes_gisaid.tsv") == "gisaid"

    def test_filter_genomes(self):
        fasta_10 = dpath("testdata/filter_and_merge/genomes_ebi10.fasta")
        tsv_10 = dpath("testdata/filter_and_merge/metadata_ebi10.tsv")
        fasta_20 = dpath("testdata/filter_and_merge/genomes_ebi20.fasta")
        tsv_20 = dpath("testdata/filter_and_merge/metadata_ebi20.tsv")
        ground_truth_metadata = dpath("testdata/filter_and_merge/ground_truth.tsv")
        ground_truth_genomes = dpath("testdata/filter_and_merge/ground_truth.fasta")
        ground_truth_discarded_metadata = dpath(
            "testdata/filter_and_merge/ground_truth_discarded.tsv"
        )
        ground_truth_discarded_genomes = dpath(
            "testdata/filter_and_merge/ground_truth_discarded.fasta"
        )
        valid_metadata = dpath("testdata/filter_and_merge/valid_metadata.tsv")
        valid_genomes = dpath("testdata/filter_and_merge/valid_genomes.fasta")
        discarded_metadata = dpath("testdata/filter_and_merge/discarded_metadata.tsv")
        discarded_genomes = dpath("testdata/filter_and_merge/discarded_genomes.fasta")
        start_date = "01/01/2020"
        seq_limit = 0

        # Delete output files if they already exist
        delete([valid_metadata, valid_genomes, discarded_metadata, discarded_genomes])

        # Test filter_metadata
        valid_ids = filter_metadata(
            [tsv_10, tsv_20], valid_metadata, discarded_metadata, start_date, seq_limit
        )
        self.assertDictEqual(
            valid_ids,
            {
                "ebi10": {
                    "BS000686",
                    "BS000688",
                    "BS000687",
                    "BS000685",
                    "BS000689",
                    "BS000690",
                },
                "ebi20": {
                    "BS000693",
                    "BS000697",
                    "BS000688",
                    "BS000692",
                    "BS000701",
                    "BS000702",
                    "BS000700",
                    "BS000687",
                    "BS000689",
                    "BS000698",
                    "BS000695",
                    "BS000691",
                    "BS000690",
                    "BS000686",
                    "BS000703",
                    "BS000685",
                    "BS000694",
                    "BS000699",
                    "BS000704",
                    "BS000696",
                },
            },
        )
        assert filecmp.cmp(valid_metadata, ground_truth_metadata)
        assert filecmp.cmp(discarded_metadata, ground_truth_discarded_metadata)

        # Test filter_genomes
        filter_genomes(
            [fasta_10, fasta_20], valid_genomes, discarded_genomes, valid_ids
        )
        assert filecmp.cmp(valid_genomes, ground_truth_genomes)
        assert filecmp.cmp(discarded_genomes, ground_truth_discarded_genomes)
        # Delete output files again
        delete([valid_metadata, valid_genomes, discarded_metadata, discarded_genomes])

    def test_sequence_number_filter12(self):
        # Limit is reached in the second data set
        fasta_10 = dpath("testdata/filter_and_merge/genomes_ebi10.fasta")
        tsv_10 = dpath("testdata/filter_and_merge/metadata_ebi10.tsv")
        fasta_10b = dpath("testdata/filter_and_merge/genomes_ebi10b.fasta")
        tsv_10b = dpath("testdata/filter_and_merge/metadata_ebi10b.tsv")
        ground_truth_metadata = dpath("testdata/filter_and_merge/ground_truth_filter12.tsv")
        ground_truth_genomes = dpath("testdata/filter_and_merge/ground_truth_filter12.fasta")
        ground_truth_discarded_metadata = dpath(
            "testdata/filter_and_merge/ground_truth_discarded_filter12.tsv"
        )
        ground_truth_discarded_genomes = dpath(
            "testdata/filter_and_merge/ground_truth_discarded_filter12.fasta"
        )
        valid_metadata = dpath("testdata/filter_and_merge/valid_metadata_filter12.tsv")
        valid_genomes = dpath("testdata/filter_and_merge/valid_genomes_filter12.fasta")
        discarded_metadata = dpath("testdata/filter_and_merge/discarded_metadata_filter12.tsv")
        discarded_genomes = dpath("testdata/filter_and_merge/discarded_genomes_filter12.fasta")
        start_date = "01/01/2019"
        seq_limit = 12

        # Delete output files if they already exist
        delete([valid_metadata, valid_genomes, discarded_metadata, discarded_genomes])

        # Test filter_metadata
        valid_ids = filter_metadata(
            [tsv_10, tsv_10b], valid_metadata, discarded_metadata, start_date, seq_limit
        )
        self.assertDictEqual(
            valid_ids,
            {
                "ebi10": {
                    'BS000685',
                    'BS000692',
                    'BS000690',
                    'BS000688',
                    'BS000687',
                    'BS000689',
                    'BS000686',
                },
                "ebi10b": {
                    'BS000695',
                    'BS000697',
                    'BS000696',
                    'BS000699',
                    'BS000698',
                },
            },
        )
        assert filecmp.cmp(valid_metadata, ground_truth_metadata)
        assert filecmp.cmp(discarded_metadata, ground_truth_discarded_metadata)

        # Test filter_genomes
        filter_genomes(
            [fasta_10, fasta_10b], valid_genomes, discarded_genomes, valid_ids
        )
        assert filecmp.cmp(valid_genomes, ground_truth_genomes)
        assert filecmp.cmp(discarded_genomes, ground_truth_discarded_genomes)
        # Delete output files again
        delete([valid_metadata, valid_genomes, discarded_metadata, discarded_genomes])

    def test_sequence_number_filter4(self):
        # Limit is reached in the first data set
        fasta_10 = dpath("testdata/filter_and_merge/genomes_ebi10.fasta")
        tsv_10 = dpath("testdata/filter_and_merge/metadata_ebi10.tsv")
        fasta_10b = dpath("testdata/filter_and_merge/genomes_ebi10b.fasta")
        tsv_10b = dpath("testdata/filter_and_merge/metadata_ebi10b.tsv")
        ground_truth_metadata = dpath("testdata/filter_and_merge/ground_truth_filter4.tsv")
        ground_truth_genomes = dpath("testdata/filter_and_merge/ground_truth_filter4.fasta")
        ground_truth_discarded_metadata = dpath(
            "testdata/filter_and_merge/ground_truth_discarded_filter4.tsv"
        )
        ground_truth_discarded_genomes = dpath(
            "testdata/filter_and_merge/ground_truth_discarded_filter4.fasta"
        )
        valid_metadata = dpath("testdata/filter_and_merge/valid_metadata_filter4.tsv")
        valid_genomes = dpath("testdata/filter_and_merge/valid_genomes_filter4.fasta")
        discarded_metadata = dpath("testdata/filter_and_merge/discarded_metadata_filter4.tsv")
        discarded_genomes = dpath("testdata/filter_and_merge/discarded_genomes_filter4.fasta")
        start_date = "01/01/2020"
        seq_limit = 4

        # Delete output files if they already exist
        delete([valid_metadata, valid_genomes, discarded_metadata, discarded_genomes])

        # Test filter_metadata
        valid_ids = filter_metadata(
            [tsv_10, tsv_10b], valid_metadata, discarded_metadata, start_date, seq_limit
        )
        self.assertDictEqual(
            valid_ids,
            {
                "ebi10": {
                'BS000685',
                'BS000687',
                'BS000688',
                'BS000686',
                },
                "ebi10b": set(),
            },
        )
        assert filecmp.cmp(valid_metadata, ground_truth_metadata)
        assert filecmp.cmp(discarded_metadata, ground_truth_discarded_metadata)

        # Test filter_genomes
        filter_genomes(
            [fasta_10, fasta_10b], valid_genomes, discarded_genomes, valid_ids
        )
        assert filecmp.cmp(valid_genomes, ground_truth_genomes)
        assert filecmp.cmp(discarded_genomes, ground_truth_discarded_genomes)
        # Delete output files again
        delete([valid_metadata, valid_genomes, discarded_metadata, discarded_genomes])

    def test_is_valid_sequence(self):
        iupac_chars = "ATCGNYRWSKMDVHB-atcgnyrwskmdvhb"
        assert is_valid_sequence("ATCG")
        assert is_valid_sequence(iupac_chars)
        not_iupac = set(string.printable).difference(set(iupac_chars))
        for char in not_iupac:
            assert not is_valid_sequence(char)
        assert not is_valid_sequence("")

    def test_filter_metadata(self):
        with pytest.raises(ValueError):
            filter_metadata(None, None, None, '2022', 0)

    def test_write_sequence_1(self):
        ground_truth = ">Test_1\nATCG\n"
        output = StringIO()
        id = "Test_1"
        sequence = "ATCG"
        write_sequence(output, id, sequence)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_2(self):
        ground_truth = ">Test_1\nAT\nCG\n>Test_2\nAT\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATCG", 2)
        write_sequence(output, "Test_2", "AT", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_3(self):
        ground_truth = ">Test_1\nAT\nC\n>Test_2\nA\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATC", 2)
        write_sequence(output, "Test_2", "A", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)


if __name__ == "__main__":
    unittest.main()
