# Data source

This example data is downloaded from the [COVID-19 data portal](https://www.covid19dataportal.org/). It containes 12 SARS-CoV-2 sequences from Danish mink. 

Accession IDs: MT919529, MT919526, MT919533, MT919536, MT919535, MT919525, MT919530, MT919527, MT919528, MT919531, MT919532, MT919534