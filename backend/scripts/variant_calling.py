#!/usr/bin/env python
import numpy as np
from Bio import AlignIO, SeqIO
import argparse
import ray
import gzip
import sparse

#from datetime import datetime
# import logging
# logging.basicConfig(handlers=[logging.FileHandler(filename="vcf_logging.log", 
#                                                  encoding='utf-8', mode='a+')],
#                     format="%(asctime)s %(name)s:%(levelname)s:%(message)s", 
#                     datefmt="%F %A %T", 
#                     level=logging.DEBUG)

@ray.remote
def load_sample_names(align):
    samples = [record.id for record in align]
    return samples

def vcf_table_header(samples, first_case):
    first = SeqIO.read(first_case, "fasta").seq
    header = "##fileformat=VCFv4.2\n"
    vcf_header = header + "##contig=<ID=1,length="+str(len(first))+">\n"
    table_header = np.append(np.array(["#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"]), samples)
    header_rows = vcf_header + "\t".join(table_header)
    return str(first), header_rows

@ray.remote
def vcf_row_generation(first, i, j, align):
    # position_bases: all bases for one position
    bases_batch = np.transpose([list(record.seq) for record in align])
    x = zip(range(i,j), bases_batch)
    result = []
    for position, bases in x:
        ref = first[position]
        set_bases = set(bases)
        if set(ref) != set_bases:
            # this determines the ALT numbers in the VCF file
            base_no = {ref:0}
            alt = sorted(list(l for l in set(bases) if l != ref))
            for i,b in enumerate(alt):
                base_no[b] = i+1
            
            def f(x):
                return base_no[x]
            # assign base to ALT number
            mapped = np.array(list(map(f, bases)), dtype=np.uint8)
            # reducing memory usage
            sparse_mapped = sparse.COO(mapped)
            # prefix "#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"
            prefix = np.array(["1", str(position+1), ".", ref, ",".join(alt), ".", ".", "."])

            result.append([prefix, sparse_mapped])
    return result

def batch_nested(iterable, n=1):
    # splits sample names in MSA in n batches
        l = len(iterable)
        for ndx in range(0, l, n):
            yield iterable[ndx:min(ndx + n, l), :]

def batch_nested_it(iterable, n=1):
    # splits sequence strings in MSA in n batches
    # also returns positions
        l = len(iterable[0])
        for ndx in range(0, l, n):
            yield ndx, min(ndx + n, l), iterable[:, ndx:min(ndx + n, l)]

def main(batch_size, msa, first_case, vcf):
    #start_time = datetime.now()
    #logging.debug('Start: Duration: {}'.format(datetime.now() - start_time))
    # import MSA
    align = AlignIO.read(msa, "fasta")

    # create VCF header
    samples_ids = []
    for x in batch_nested(align, batch_size):
        samples_ids.append(load_sample_names.remote(x))

    samples = []
    for sids in samples_ids:
        samples.extend(ray.get(sids))

    #logging.debug('End: Duration: {}'.format(datetime.now() - start_time))
    first, vcf_header = vcf_table_header(samples, first_case)
    
    # we could also directly write into a file but then it's not with multiprocessing
    # save VCF header
    with gzip.open(vcf, 'wb') as f:
        np.savetxt(f, [], delimiter='\t', fmt='%s', header=vcf_header, comments="", newline='\n')

    # to reduce memory bc this can save >10 Million sequence names
    del vcf_header
    
    # create VCF rows
    row_part_ids = []
    for i, j, x in batch_nested_it(align, batch_size):
        row_part_ids.append(vcf_row_generation.remote(first, i, j, x))

    # save VCF rows
    # batch of rows are saved because all rows will exceed memory
    with gzip.open(vcf, 'ab') as f:
        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if len(rows_part) != 0:
                for row in rows_part:
                    np.savetxt(f, [np.append(row[0], row[1].todense())], delimiter='\t', fmt='%s', comments="", newline='\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='calls variants and stores them in a TSV table')
    parser.add_argument('cpus', type=int, help='Number of CPUs.')
    parser.add_argument('msa', type=str, help='Input FASTA file containing MSA')
    parser.add_argument('first_case', type=str, help='Input FASTA file containing first case sequence mapped to MSA')
    parser.add_argument('vcf', type=str, help='Output TSV table containing variants and positions')

    args = parser.parse_args()

    ray.init(num_cpus=args.cpus)

    # Since the function vcf_row_generation is a very small process, calling it one by one would 
    # lead to an overhead of ray, making this script slower than without multiprocessing, so 
    # the input is divided into as many batches as CPUs
    batch_size = args.cpus

    main(batch_size, args.msa, args.first_case, args.vcf)

    ray.shutdown()