#!/usr/bin/env python3

from Bio import SeqIO
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Extracts the S1 domain from a fastp file, according to coordinates given')
parser.add_argument('spike', type=str, help='Input FASTA file containing the reference spike protein')
parser.add_argument('beginning', type=int, help='Beginning of the S1 domain')
parser.add_argument('end', type=int, help='End of the S1 domain')
parser.add_argument('output', type=str, help='Output FASTA file containing the s1 domain extracted from the spike protein')
args = parser.parse_args()

counter = 0
for record in SeqIO.parse(args.spike, "fasta"):
    counter = counter + 1
    spike = record

if counter == 1:
    s1_domain = []
    spike.seq = spike.seq[args.beginning - 1:args.end]
    s1_domain.append(spike)
    SeqIO.write(s1_domain, args.output, 'fasta')
else:
    print('Error: Spike file should contain only one sequence')
