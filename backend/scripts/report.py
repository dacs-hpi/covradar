#!/usr/bin/env python3

"""Basic PDF report generation from a Pug template with pdf_reports.

A HTML page is generated from a template and rendered as a local PDF file.
"""

from pdf_reports import ReportWriter
from datetime import date
import argparse
import os
import re

parser = argparse.ArgumentParser(description='Creates a pdf report with the results from the pipeline')
parser.add_argument('template', type=str, help='Template file in .pug format')
parser.add_argument('style', type=str, help='CSS style file')
parser.add_argument('workflow', type=str, help='SVG file containing the workflow')
parser.add_argument('logo', type=str, help='SVG file containing the RKI logo')
parser.add_argument('title', type=str, help='Title of the pdf report')
parser.add_argument('sidenote', type=str, help='Sidenote of the pdf report')
# parser.add_argument('raxml', type=str, help='RAxML-NG log')
parser.add_argument('bcftools', type=str, help='BCFtools log')
parser.add_argument('freq_plot', type=str, help='SVG file containing the frequency plot')
parser.add_argument('substitutions', type=str, help='SVG file containing the substitutions plot')
parser.add_argument('output', type=str, help='Output PDF file')
args = parser.parse_args()

# Basic statistics
statistics = []
today = date.today().strftime("[%d/%m/%Y] ")
statistics.append(today)
# raxml_file = open(args.raxml, "r")
# raxml = raxml_file.read()
# raxml_file.close()
# taxa = re.findall("alignment with (.*?) taxa|$", raxml)[0]
# sites = re.findall("taxa and (.*?) sites|$", raxml)[0]
# statistics.append("Multiple sequence alignment with %s taxa and %s sites. " % (taxa, sites))
# undetermined = re.findall("Fully undetermined columns found: (\w+)|$", raxml)[0]
# statistics.append("WARNING: Fully undetermined columns found: %s. " % (undetermined))
# duplicates = re.findall("Duplicate sequences found: (\w+)|$", raxml)[0]
# statistics.append("WARNING: Duplicate sequences found: %s. " % (duplicates))
# partitions = re.findall("comprises (.*?) partitions|$", raxml)[0]
# patterns = re.findall("partitions and (.*?) patterns|$", raxml)[0]
# statistics.append("Alignment comprises %s partition and %s patterns. " % (partitions, patterns))
# gaps = re.findall("Gaps: (.*?) %|$", raxml)[0]
# statistics.append("Gaps: %s%%. " % (gaps))
# invariants = re.findall("Invariant sites: (.*?) %|$", raxml)[0]
# statistics.append("Invariant sites: %s%%. " % (invariants))
bcftools_file = open(args.bcftools, "r")
bcftools = bcftools_file.read()
bcftools_file.close()
snps = re.findall("(?<=number of SNPs:	).*|$", bcftools)[0]
statistics.append("Number of SNPs: %s. " % (snps))
multiallelic_sites = re.findall("(?<=number of multiallelic sites:	).*|$", bcftools)[0]
statistics.append("Number of multiallelic sites: %s. " % (multiallelic_sites))
multiallelic_snp_sites = re.findall("(?<=number of multiallelic SNP sites:	).*|$", bcftools)[0]
statistics.append("Number of multiallelic SNP sites: %s." % (multiallelic_snp_sites))
statistics = ''.join(statistics)

report_writer = ReportWriter(
    default_stylesheets=[args.style],
    default_template=args.template,
    title=args.title,
    sidenote=args.sidenote,
    basic_stats=statistics,
    freq_plot=os.path.abspath(args.freq_plot),
    substitutions=os.path.abspath(args.substitutions),
    workflow=os.path.abspath(args.workflow),
    logo=os.path.abspath(args.logo),
    version="0.2"
)

html = report_writer.pug_to_html(my_name="RKI", my_organization="RKI")
report_writer.write_report(html, args.output)
