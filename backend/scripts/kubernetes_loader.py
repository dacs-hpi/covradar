#!/bin/python3

"""Wrapper for CovRadar database loader script for Kubernetes

This script uses kubectl to create and update the covradar service and mariadb.

Author: Sebastian Beyvers
Contact: sebastian.beyvers@computational.bio.uni-giessen.de

Example:
        ``
        $ python kubernetes_loader.py -s <SPIKE_S3_URL> -r <RKIPUBLIC_S3_URL> [--staging]
        ``

        If --staging is specified the test/staging environment is used.
        otherwise the live environment.
"""

import argparse
import logging
from subprocess import run, PIPE, Popen
from time import sleep


def apply_loader(rki_url, spike_url, is_production, log_file):
  with open(spike_url, "r") as fin:
      spike_url = fin.readline().strip()
  with open(rki_url, "r") as fin:
      rki_url = fin.readline().strip()
  logging.basicConfig(
      format='[%(levelname)s] %(asctime)s %(message)s',
      datefmt='%m/%d/%Y %H:%M:%S',
      level=logging.DEBUG,
      filename=log_file,
      filemode="w",
  )
  logging.info("Loader initialized.")
  logging.info(f"Spike_URL: {spike_url}")
  logging.info(f"RKIPublic_URL: {rki_url}")
  logging.info(f"Use production environment: {is_production}")


  def handle_error(message, func_stdout, func_stderror, func_args = None):
      """handle_error is a function that handles occured errors, based on stdout/stderror and exits the script in case there is one"""

      func_stdout = func_stdout or ""
      func_stderror = func_stderror or ""
      func_args  = func_args or []
      if "error" in func_stdout.lower() or "error" in func_stderror.lower():
          logging.error(f"{message}")
          logging.error(f"Stdout: {func_stdout}") if func_stdout else ""
          logging.error(f"Stderror: {func_stderror}") if func_stderror else ""
          logging.error(f"Args: {func_args}")
          exit(1)

  service = "covradar-db" if is_production else "covradar-staging-db"
  namespace = "covradar" if is_production else "covradar-staging"
  redis_pod = "redis-master-0" if is_production else "redis-staging-master-0"
  db_selector = "mariadb-" if is_production else "mariadb-staging-"
  covradar_baseurl = "https://covradar.net/" if is_production else "https://staging.covradar.biokube.org/"
  deployment_name = "covradarfrontend" if is_production else "covradarfrontend-staging"

  logging.info(f"Getting current active database from service '{service}' in namespace: '{namespace}'")

  # This commands gets the current selected database via the service and its selector
  # Dots in the selector-name must be escaped with \ e.g. "app.kubernetes.io/instance" => "app\.kubernetes\.io/instance"
  # Example: 'kubectl get service <servicename> -n <namespace> -o=jsonpath='{.spec.selector.<selectorname>}'
  getservice = run(['kubectl', 'get', 'service', f"{service}", '-n', f"{namespace}", "-o=jsonpath='{.spec.selector.app\.kubernetes\.io/instance}"], capture_output=True, text=True)
  handle_error("Error while accessing covradar service:", getservice.stdout, getservice.stderr, getservice.args)


  nextdb = ""
  currentdb = getservice.stdout
  logging.info(getservice.stdout)
  if currentdb.endswith("1"):
      nextdb = db_selector + "2"
  else:
      nextdb = db_selector + "1"

  logging.info(f"Current active database is '{currentdb[1:]}' next database will be: '{nextdb}'")

  # The inline yaml file for the loader job.
  yaml = f"""apiVersion: batch/v1
kind: Job
metadata:
  name: loader
spec:
  ttlSecondsAfterFinished: 30
  activeDeadlineSeconds: 3600
  template:
    spec:
      containers:
      - name: loader
        image: quay.io/mariusdieckmann/mariadbk8sloader:latest
        imagePullPolicy: Always
        env:
        - name: SQL_HOST
          value: {nextdb}-primary
        - name: SPIKE_SQLDUMP_DL_LINK
          value: "{spike_url}"
        - name: RKIPublic_SQLDUMP_DL_LINK
          value: "{rki_url}"
        - name: MYSQL_USERNAME
          value: spike
        - name: MYSQL_PWD
          valueFrom:
            secretKeyRef:
              key: mariadb-password
              name: {nextdb}
      restartPolicy: Never
  backoffLimit: 0
  """

  logging.info(yaml)


  logging.info(f"Initializing loader in '{namespace}'")

  delete_old_job =  run(['kubectl', 'delete', 'job', 'loader', '-n', f"{namespace}", '--ignore-not-found=true'])
  handle_error('Error while deleting old job:', delete_old_job.stdout, delete_old_job.stderr, delete_old_job.args)

  # This command applies the loader job to the proper namespace
  # Example: 'kubectl apply -n <namespace> -f -'
  # The '-f -' can be used to pass the yaml via stdinput instead of a regular file.
  applyloader = run(['kubectl', 'apply', "-n", f"{namespace}", "-f", "-"], input=yaml, capture_output=True, encoding='ascii')
  handle_error("Error while applying loader yaml:", applyloader.stdout, applyloader.stderr, applyloader.args)


  # This command waits till the loader job is ready, this is necessary
  # because kubectl logs needs a running container to access the logs.
  # Example: 'kubectl wait -n <namespace> --for=condition=Ready pod -l <label>'
  # The container/pod can be specified by a label selector that refers to the created job e.g. '-l job-name=loader'
  initializing = run(['kubectl', 'wait', "--for=condition=Ready", "-n", f"{namespace}", "pod", "-l", "job-name=loader"], capture_output=True, input=yaml, encoding='ascii')
  handle_error("Error while waiting for loader to be initialized:", initializing.stdout, initializing.stderr, initializing.args)


  logging.info("Loader initialization complete, monitoring...")


  # This command gets the logs of the loader script
  # Example: 'kubectl logs -n <namespace> -l <label> --follow'
  # It uses the same label as the initializing/wait step. 
  # The --follow parameter allows the command to stream the logs to stdout.
  streamloader = Popen(['kubectl', 'logs', "-n", f"{namespace}", "-l", "job-name=loader", "--follow"], shell=False, stdout=PIPE, stderr=PIPE)

  # Listen to the logstream in stdout/stderror
  while True:
      output = streamloader.stdout.readline()
      error = streamloader.stderr.readline()
      handle_error("Error/Timeout in loader execution:", output.decode(), error.decode())
      if streamloader.poll() is not None:
          break
      if output:
          logging.info(f"Loader Log: '{output.decode().strip()[1:-1]}'")
  streamloader.poll()



  # This command updates the service to use the new database
  # Example: 'kubectl patch service <servicename> -n <namespace> -p <jsonpatch>'
  # The jsonpatch specifies the json description of the updated field
  # e.g. '{"spec":{"selector":{"app": "new-app"}}}' updates the selector to the key/value pair "app":"new-app"
  # To properly use f"strings" the curly-braces must be escaped via double curly-braces.
  patchservice = run(f'kubectl patch service {service} -n {namespace} -p \'{{"spec":{{"selector":{{"app.kubernetes.io/instance":"{nextdb}"}}}}}}\'', capture_output=True, text=True, shell=True)

  handle_error("Error while updating service:", patchservice.stdout, patchservice.stderr, patchservice.args)
  logging.info("Loader finished updating service")

  logging.info("Resetting redis db")

  result = run(f"kubectl exec -i -t -n {namespace} {redis_pod} -- /bin/sh -c 'redis-cli -a $REDIS_PASSWORD FLUSHALL'", shell=True,capture_output=True)
  if result.stdout:
    result.stdout = result.stdout.decode('utf-8')
    logging.info(result)
    line = result.stdout.splitlines()[0]
    if 'wrong number of arguments' in line:
      logging.info('Sucess: 0 cache entries were deleted')
    elif '(integer)' in line:
      logging.info(f'Success: {line} cache entries were deleted')
    else:
      handle_error('Error while flushing redis:', '', result.stdout, result.args)
  else:
    handle_error('Error while executing kubectl: ', result.stdout, result.stderr, result.args)

  logging.info("Redeploying the webserver")
  result = run(f"kubectl rollout restart deployment/{deployment_name} -n {namespace}", shell=True, capture_output=True)
  handle_error("Error while starting redeployment: ", initializing.stdout, initializing.stderr, initializing.args)

  logging.info("Waiting for webserver deployment to finish...")
  result = run(f"kubectl rollout status deployment/{deployment_name} -n {namespace}", shell=True, capture_output=True)
  handle_error("Error while redeploying webserver: ", initializing.stdout, initializing.stderr, initializing.args)

  logging.info("Refilling cache...")
  result = run(f"cd init-covradar-cache && npm run start {covradar_baseurl}", shell=True, capture_output=True)
  handle_error("Error while refilling covradar cache ", initializing.stdout, initializing.stderr, initializing.args)
  


  
  logging.info("Database update successful")




if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Wrapper script for the Covradar Loader: Requires kubectl')
  parser.add_argument('-s','--spike', help='S3-URL to the SPIKE SQL-dump', required=True)
  parser.add_argument('-r','--rki', help='S3-URL to the RKIPublic SQL-dump', required=True)
  parser.add_argument('-l','--log', help='File to write log', required=True)
  parser.add_argument('-p',"--production", action="store_true", help="use the production instead of staging/test environment")
  args = parser.parse_args()
  apply_loader(args.rki, args.spike, args.production, args.log)
