#!/usr/bin/env python3

from Bio import SeqIO
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Extracts the first sequence from a msa fasta file')
parser.add_argument('input', type=str, help='Input FASTA file containing the msa')
parser.add_argument('output', type=str, help='Output FASTA file containing the first sequence extracted from the msa')
args = parser.parse_args()

counter = 0
for record in SeqIO.parse(args.input, "fasta"):
    if counter == 0:
        first = record
    counter = counter + 1

sequences = []
sequences.append(first)
SeqIO.write(sequences, args.output, 'fasta')
