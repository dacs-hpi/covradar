#!/usr/bin/env python3
"""Script to filter and merge FASTA files and their metadata."""
import sys
import screed
import pandas as pd
import argparse
from os.path import basename
from datetime import datetime
from argparse import Namespace
from typing import TextIO, Union
from tqdm import tqdm


def is_valid_date(text: str, start_date: datetime) -> Union[datetime, None]:
    """
    Verify if a given date is valid.

    Parameters
    ----------
    text : str
        Text to verify if it is a valid date.
    start_date : datetime
        If the date is before start_date, it will be considered invalid.

    Returns
    -------
    _ : datetime
        Formatted date if successful, otherwise None.
    """
    for fmt in ("%Y-%m-%d", "%d.%m.%Y", "%d/%m/%Y", "%d-%b-%Y"):
        try:
            date = datetime.strptime(text, fmt)
            if date >= start_date:
                return date
        except ValueError:
            pass
    return None


def retrieve_columns(header: str, columns: set) -> None:
    """
    Retrieve columns from a given string.

    Parameters
    ----------
    header : str
        First line of the dataset.
    columns : set
        Where to store new columns found.
    """
    new_columns = header.split("\t")
    for c in new_columns:
        columns.add(c.replace(" ", "_"))


def add_important_columns(columns: set) -> list:
    """
    Add important columns that might be missing in datasets.

    Parameters
    ----------
    columns : set
        Current columns set.

    Returns
    -------
    columns : list
        Sorted list containing previous columns and new columns.
    """
    columns.add("host")
    columns.add("origin")
    columns.add("country")
    columns.add("country_exposure")
    columns = list(columns)
    columns.sort()
    return columns


def get_origin(filename: str) -> str:
    """
    Get name of source.

    Needs to be in the format 'metadata_source.tsv'

    Parameters
    ----------
    filename : str
        Name of the file.

    Returns
    -------
    origin : str
        Name of source.
    """
    return basename(filename).split("_")[-1].split(".")[0]


def read_metadata(filename: str) -> pd.DataFrame:
    """
    Read TSV file containing metadata.

    Parameters
    ----------
    filename : str
        Name of the file.

    Returns
    -------
    metadata : pd.DataFrame
        Metadata.
    """
    metadata = pd.read_csv(filename, sep="\t", low_memory=False)
    metadata.columns = metadata.columns.str.replace(" ", "_")
    return metadata


def append_metadata(
    metadata: pd.DataFrame,
    valid_metadata: list,
    discarded_metadata: list,
    columns: list,
    origin: str,
    start_date: datetime,
    seq_limit: int,
    no_of_seqs: list,
) -> set:
    """
    Append metadata to a list.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame containing metadata.
    valid_metadata : list
        List containing merged metadata.
    discarded_metadata : list
        List containing discarded metadata.
    columns : list
        List containing columns for all datasets.
    origin : str
        Source of data, e.g., ebi, gisaid, etc.
    start_date : datetime
        Minimum date to keep metadata.
    seq_limit : int
        Maximum number of sequences to keep. 0 means keep all.
    no_of_seqs : int
        Number of sequences already in metadata.

    Returns
    -------
    valid_ids : set
        IDs containing valid dates.
    """
    valid_ids = set()
    for (index, row) in metadata.iterrows():
        if seq_limit == 0:  # 0 means keep all
            date = is_valid_date(str(row["date"]), start_date)
            # if date is invalid then do not add metadata to output
            # this is to avoid an error on the website
            r = []
            for column in columns:
                if column in metadata.columns:
                    r.append(row[column])
                elif column == "origin":
                    r.append(origin)
                else:
                    r.append("")
            if date:
                valid_metadata.append(r)
                valid_ids.add(row["strain"])
            else:
                discarded_metadata.append(r)

        else:
            if no_of_seqs[0] < seq_limit:
                date = is_valid_date(str(row["date"]), start_date)
                # if date is invalid then do not add metadata to output
                # this is to avoid an error on the website
                r = []
                for column in columns:
                    if column in metadata.columns:
                        r.append(row[column])
                    elif column == "origin":
                        r.append(origin)
                    else:
                        r.append("")
                if date:
                    valid_metadata.append(r)
                    valid_ids.add(row["strain"])
                    no_of_seqs[0] += 1
                else:
                    discarded_metadata.append(r)
            else:
                break
    return valid_ids


def filter_metadata(
    input_files: list,
    valid_metadata_path: str,
    discarded_metadata_path: str,
    start_date: str,
    seq_limit: int,
) -> dict:
    """
    Filter and merge metadata files.

    Parameters
    ----------
    input_files : list
        Paths of metadata files.
    valid_metadata_path : str
        Path of output file containing merged metadata.
    discarded_metadata_path : str
        Path of output file containing invalid metadata.
    start_date : str
        Minimum date to keep metadata.
    seq_limit : int
        Maximum number of sequences to keep.

    Returns
    -------
    valid_ids : dict
        IDs that contain valid dates.
    """
    # Convert start date to datetime
    minimum_date = is_valid_date(start_date, datetime(1900, 1, 1))
    if minimum_date is None:
        raise ValueError(f"Start date {start_date} is invalid")

    # Retrieve all possible columns from all datasets
    columns = set()
    # for every data input source
    for metadata_input in input_files:
        with open(metadata_input, "r") as fin:
            header = fin.readline().strip()
            retrieve_columns(header, columns)
    columns = add_important_columns(columns)

    # Merge metadata
    no_of_seqs = [0] # if no_of_seqs > seq_limit --> break
    valid_metadata = []
    discarded_metadata = []
    valid_ids = {}
    with tqdm(total=len(input_files)) as pbar:
        pbar.set_description("Merging metadata files")
        for metadata_input in input_files:
            # Origin file needs to be in the format 'metadata_source.tsv'
            origin = get_origin(metadata_input)
            metadata = read_metadata(metadata_input)
            # Compute dataset size and save it to show progress
            # when processing the FASTA files
            valid_ids[origin] = append_metadata(
                metadata,
                valid_metadata,
                discarded_metadata,
                columns,
                origin,
                minimum_date,
                seq_limit,
                no_of_seqs,
            )
            pbar.update()

    valid_metadata = pd.DataFrame(valid_metadata, columns=columns)
    discarded_metadata = pd.DataFrame(discarded_metadata, columns=columns)
    # Append new dataframes to output files
    valid_metadata.to_csv(valid_metadata_path, index=False, sep="\t", mode="w+")
    discarded_metadata.to_csv(discarded_metadata_path, index=False, sep="\t", mode="w+")
    return valid_ids


# Characters allowed in IUPAC code
allowed_iupac = set("ATCGNYRWSKMDVHB-atcgnyrwskmdvhb")


def is_valid_sequence(sequence: str) -> bool:
    """
    Verify if a sequence complies with the IUPAC code.

    https://www.bioinformatics.org/sms/iupac.html

    Parameters
    ----------
    sequence : str
        DNA sequence to verify.

    Returns
    -------
    _ : bool
        True if the sequence is valid, otherwise false.
    """
    return len(sequence) > 0 and set(sequence).issubset(allowed_iupac)


def write_sequence(
    output: TextIO, identifier: str, sequence: str, line_length: int = 80
) -> None:
    """
    Write a sequence to a file.

    Parameters
    ----------
    output : TextIOWrapper
        File where sequence will be written.
    identifier : str
        Identifier of the genome.
    sequence : str
        Sequence of the genome.
    line_length : int, default=80
        Maximum length for each sequence line.
        Does not apply to identifier.
    """
    output.write(f">{identifier}\n")
    formatted_sequence = "".join(
        [
            f"{sequence[i:i + line_length]}\n"
            for i in range(0, len(sequence), line_length)
        ]
    )
    output.write(f"{formatted_sequence}")


def filter_genomes(
    input_files: list, valid_genomes: str, discarded_genomes: str, valid_ids: dict
) -> None:
    """
    Filter and merge multiple FASTA files.

    Parameters
    ----------
    input_files : list
        Path of input FASTA files.
    valid_genomes : str
        Path of output FASTA file to write valid sequences.
    discarded_genomes : str
        Path of output FASTA file to write invalid sequences.
    valid_ids : dict
        IDs that contain valid dates.
    """
    with open(valid_genomes, "w") as valid_sequences:
        with open(discarded_genomes, "w") as discarded_sequences:
            for genomes_input in input_files:
                origin = get_origin(genomes_input)
                with tqdm(total=len(valid_ids[origin])) as pbar:
                    pbar.set_description(f"Merging file {basename(genomes_input)}")
                    with screed.open(genomes_input) as seqfile:
                        for record in seqfile:
                            if record.name in valid_ids[origin] and is_valid_sequence(
                                record.sequence
                            ):
                                write_sequence(
                                    valid_sequences, record.name, record.sequence
                                )
                            else:
                                write_sequence(
                                    discarded_sequences, record.name, record.sequence
                                )
                            pbar.update()


def parse_args(args: list) -> Namespace:
    """
    Parse a list of arguments.

    Parameters
    ----------
    args : list
        Arguments to parse.

    Returns
    -------
    _ : Namespace
        Parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description="Merges metadata from different sources"
    )
    parser.add_argument(
        "--genomes",
        nargs="+",
        type=str,
        required=True,
        help="Input FASTA file(s) containing the sequences",
    )
    parser.add_argument(
        "--metadata",
        nargs="+",
        type=str,
        required=True,
        help="Input TSV file(s) containing the metadata",
    )
    parser.add_argument(
        "--valid-genomes",
        type=str,
        required=True,
        help="Output FASTA file to save the valid sequences",
    )
    parser.add_argument(
        "--valid-metadata",
        type=str,
        required=True,
        help="Output TSV file to save the merged metadata",
    )
    parser.add_argument(
        "--discarded-genomes",
        type=str,
        required=True,
        help="Output FASTA file to save the invalid sequences",
    )
    parser.add_argument(
        "--discarded-metadata",
        type=str,
        required=True,
        help="Output TSV file to save the invalid metadata",
    )
    parser.add_argument(
        "--start-date",
        type=str,
        required=False,
        default="01/01/2019",
        help="Date to start filtering metadata",
    )
    parser.add_argument(
        "--sequence-limit",
        type=int,
        required=False,
        default=0,
        help="Sequence number limit.",
    )
    return parser.parse_args(args)


def main():  # pragma: no cover
    """Filter and merge datasets."""
    args = parse_args(sys.argv[1:])
    valid_ids = filter_metadata(
        args.metadata, args.valid_metadata, args.discarded_metadata, args.start_date, args.sequence_limit
    )
    filter_genomes(args.genomes, args.valid_genomes, args.discarded_genomes, valid_ids)


if __name__ == "__main__":
    main()  # pragma: no cover
