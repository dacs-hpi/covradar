import argparse
import json
import sys
import contextlib


def main():
    parser = argparse.ArgumentParser(
        description="A script that turns strain data from the Gisaid API into a .tsv and .fasta file that can be read by the covradar pipeline"
    )
    parser.add_argument(
        "-t",
        "--tsv",
        type=str,
        default=None,
        help="Where the resulting TSV file should be stored. None equals stdout. Default: None",
    )
    parser.add_argument(
        "-f",
        "--fasta",
        type=str,
        default=None,
        help="Where the resulting fasta file should be stored, omitted if set to None. Only use when the .json contains the actual sequences! Default: None",
    )
    parser.add_argument(
        "-p",
        "--path",
        type=str,
        default=None,
        help="location of Gisaid API json file. Use stdin if set to None (default)",
    )
    parser.add_argument("-s", "--skip-germany", default=False, action="store_true")
    args = parser.parse_args()
    create_files(args.path, args.tsv, args.fasta, args.skip_germany)


@contextlib.contextmanager
def smart_open(filename, mode):
    if filename is None and mode == "w":
        fh = sys.stdout
    elif filename is None and mode == "r":
        fh = sys.stdin
    else:
        fh = open(filename, mode)

    try:
        yield fh
    finally:
        if fh is not sys.stdout and fh is not sys.stdin:
            fh.close()


def create_files(json_path, tsv_path, fasta_path, skip_germany):
    with smart_open(json_path, "r") as json_file:
        with smart_open(tsv_path, "w") as tsv_file:
            print(
                "strain\tvirus\thost\tlineage\taccession\tdate\tregion\tcountry\tdivision\tcity\toriginating lab\tsubmitting lab\tauthors",
                file=tsv_file,
            )

            def add_tsv_cell(string):
                # alternatively could use repr(string) instead of replacing \n and \t manually with spaces, but could cause errors further down the line
                print(
                    string.replace("\t", " ").replace("\n", " "),
                    end="\t",
                    file=tsv_file,
                )

            fasta_file = open(fasta_path, "w") if fasta_path is not None else None
            for line in json_file:
                datapoint = json.loads(line)

                if (
                    "germany" not in datapoint.get("covv_location", "").lower()
                    or not skip_germany
                ):
                    # strain
                    add_tsv_cell(datapoint.get("covv_accession_id", ""))
                    # virus
                    add_tsv_cell("SARS-CoV-2")
                    # host
                    add_tsv_cell(datapoint.get("covv_host", ""))
                    # lineage
                    add_tsv_cell(datapoint.get("covv_lineage", ""))
                    # accession
                    add_tsv_cell("")
                    # date
                    add_tsv_cell(datapoint.get("covv_subm_date", ""))
                    # region, country, division, city
                    location = datapoint.get("covv_location", "").split("/")
                    for part in location[:4]:
                        add_tsv_cell(part.strip())
                    # fill nonexistent parts with \t (usually just city)
                    for i in range(4 - len(location)):
                        add_tsv_cell("")

                    # originating lab
                    add_tsv_cell(datapoint.get("covv_orig_lab", ""))
                    # submitting lab
                    add_tsv_cell(datapoint.get("covv_subm_lab", ""))
                    # 	authors
                    print("", file=tsv_file)
                    if fasta_file is not None:
                        if (
                            not "covv_accession_id" in datapoint
                            or not "sequence" in datapoint
                        ):
                            raise ValueError(
                                "No sequence or accession id in json file, set FASTA output to None"
                            )
                        fasta_file.write(
                            ">" + datapoint.get("covv_accession_id", "") + "\n"
                        )
                        fasta_file.write(
                            datapoint.get("sequence", "").replace("\n", "") + "\n"
                        )


if __name__ == "__main__":
    main()
