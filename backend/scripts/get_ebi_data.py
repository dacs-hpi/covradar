#!/usr/bin/env python

from pandas.core.arrays import boolean
from pandas.core.frame import DataFrame
import requests
import io
import pandas as pd
import numpy as np
import argparse
from Bio import SeqIO
import os
from shutil import copyfile
import sys
import os
from math import ceil
import concurrent.futures
from tqdm import tqdm
from threading import Lock
import traceback
import gzip

def get_metadata(query, limit) -> DataFrame:

    # fields_url = 'https://www.ebi.ac.uk/ena/portal/api/searchFields?result=sequence'
    # res = requests.get(fields_url)
    # content = str(res.text)
    # print(content)
    # valid_fields,desc = zip(*[x.split('\t') for x in content.split('\n')][:-1])
    # print(list(valid_fields))

    url = "https://www.ebi.ac.uk/ena/portal/api/search"  # ?result=sequence&query=tax_tree(2697049)'

    query_fields = [
        "strain",
        "scientific_name",
        "accession",
        "first_public",
        "collection_date",
        "location",
        "country",
        "collected_by",
        "identified_by",
        "host",
    ]

    payload = {
        "result": "sequence",
        "query": query,
        "format": "tsv",
        "limit": limit,
        "gzip": "true",
        "fields": ",".join(query_fields)
    }

    res = requests.post(url, data=payload, headers=None)

    ebi = pd.read_csv(io.StringIO(res.text), sep="\t", low_memory=False)

    # Transform ebi to charite

    splitted = ebi["country"].str.split(":", expand=True)
    ebi["country"], ebi["city"] = splitted[0], splitted[1]
    ebi["division"] = None  # ebi.city
    ebi["region"] = None  # ebi.country
    ebi["authors"] = None  # ebi.identified_by
    # remove gender, age and other info from host
    ebi["host"] = ebi["host"].str.split(";", expand=True)[0]
    ebi = ebi.rename(
        columns={
            "scientific_name": "virus",
            "collection_date": "date",
            "collected_by": "originating lab",
            "identified_by": "submitting lab",
        }
    )
    ebi = ebi.replace({np.nan: None})

    # Transform ebi to charite

    target_columns = set(
        [
            "strain",
            "virus",
            "accession",
            "date",
            "region",
            "country",
            "division",
            "city",
            "originating lab",
            "submitting lab",
            "authors",
        ]
    )

    for row in range(ebi.shape[0]):
        ebi.at[row, "strain"] = ebi.loc[row, "accession"]

    assert target_columns.issubset(set(ebi.columns))
    return ebi


def check_integrity(fasta, tsv) -> boolean:
    """
    Checks whether FASTA and tsv file describe the same sequences in the same order

    Parameters
    ----------
    fasta : pd.DataFrame
        fasta files
    tsv : pd.DataFrame
        metadata

    Returns
    -------
    boolean
        True if files are valid
    """
    tsv_iter = tsv.iterrows()
    for record in fasta:
        tsv_record = next(tsv_iter)[1]
        if record.id != tsv_record["strain"]:
            return False
    try:
        next(tsv_iter)
        return False
    except StopIteration:
        return True


def get_diferences(old, new):
    """
    Downloads all metadata and selects which sequences should be downloaded

    Parameters
    ----------
    old : pd.DataFrame
        old metadata
    new : String or None
        new metadata

    Returns
    -------
    only_old : pd.DataFrame
        Metadata of the sequences that only exist in old
    only_new : pd.DataFrame
        Metadata of the sequences that only exist in new 
    """
    merged = old.merge(new, how="outer", indicator=True)
    only_old = merged.loc[merged["_merge"] == "left_only"]
    only_new = merged.loc[merged["_merge"] == "right_only"]
    only_old = only_old.drop("_merge", axis=1)
    only_new = only_new.drop("_merge", axis=1)
    return only_old, only_new

def setup_without_cache(new_tsv,fasta_path, tsv_path):
    if os.path.isfile(fasta_path):
        os.remove(fasta_path)
    if os.path.isfile(tsv_path):
        os.remove(tsv_path)
    return new_tsv

def setup_and_check_files(
    old_fasta_path, old_tsv_path, fasta_path, tsv_path, query, limit, remove_germany, open_fun
):
    """
    Downloads all metadata and selects which sequences should be downloaded.
    Deletes old files if necessary

    Parameters
    ----------
    old_fasta_path : String or None
        Path to an existing fasta file. 
    old_tsv_path : String or None
        Path to an existing metadata file.  
        Sequences that exist in both the old fasta and tsv file will not be re-downloaded
    fasta_path : String
        Where the sequences should be written to in FASTA format. Can be the same as old_fasta_path
    limit : int
        Only get the first <limit> metadata entries from EBI, for testing
    remove_germany : bool
        If true, german sequences will be removed from outputs. Used to merge with german data from DESH
        This is applied after filter, so there may be less than <filter> sequences.s
    open_fun : Function(path: string, mode: string) -> File
        either open or gzip.open, depending on configuration
    Returns
    -------
    new_entries : pd.DataFrame
        Metadata of the sequences that should be included in the FASTA and tsv files
    """
    new_tsv = get_metadata(query, limit)

    if remove_germany:
        print("Filtering german sequences")
        old_len = len(new_tsv)
        new_tsv = new_tsv[new_tsv['country'] != 'Germany']
        print(f"Filtered {old_len - len(new_tsv)} german sequences")

    if not old_fasta_path or not old_tsv_path:
        return setup_without_cache(new_tsv,fasta_path, tsv_path)
    if not os.path.isfile(old_fasta_path) or not os.path.isfile(old_tsv_path):
        print('Did not find any old files. All sequences will be downloaded.')
        return setup_without_cache(new_tsv,fasta_path, tsv_path)
    
    with open_fun(old_fasta_path, "rt") as old_fasta_handle:
        with open_fun(old_tsv_path, "rt") as old_tsv_handle:
            # don't include lineage here, new_tsv doesn't include it yet 
            old_tsv = pd.read_csv(old_tsv_handle, sep="\t", low_memory=False).drop('lineage',axis=1, errors='ignore')
            old_fasta = SeqIO.parse(old_fasta_handle, "fasta")
            if not check_integrity(old_fasta, old_tsv):
                print("Old files are not usable. Cache will not be used")
                return setup_without_cache(new_tsv,fasta_path, tsv_path)
            old_tsv = old_tsv.replace({np.nan: None})

            removed_entries, new_entries = get_diferences(old_tsv, new_tsv)

            if len(removed_entries):
                print(
                    "Sequences from the old file don't exist in the new one. Cache will not be used"
                )
                return setup_without_cache(new_tsv,fasta_path, tsv_path)
            
            print("Old files found and valid")
            print(f"Found {len(old_tsv)} sequences")
            if old_fasta_path != fasta_path:
                copyfile(old_fasta_path, fasta_path)
            if old_tsv_path != tsv_path:
                copyfile(old_tsv_path, tsv_path)
            return new_entries

def fix_tsv(fasta, tsv):
    """
    At server status 500 the fasta files are skipped. This function deletes the respective entries from the metadata.

    Parameters
    ----------
    fasta : pd.DataFrame
        fasta files
    tsv : pd.DataFrame
        metadata

    Returns
    -------
    pd.DataFrame
        cleaned tsv file
    """
    tsv = tsv.set_index("strain")
    ind_list = [record.id for record in fasta if record.id in tsv.index]
    tsv = tsv.loc[ind_list].reset_index()
    return tsv

def get_ebi_data(
    fasta_path,
    tsv_path,
    old_fasta_path=None,
    old_tsv_path=None,
    seqs_per_worker=100,
    workers=None,
    query="tax_tree(2697049)",
    limit=0,
    separate_german_seqs=False,
    compress=False,
    remove_germany=True
):
    """
        Downloads sequences as FASTA and metadata as TSV from EBI.
        For more info, see argparse arguments at the bottom of the file
    """
    print("Getting metadata from EBI")

    open_fun = gzip.open if compress else open

    new_entries = setup_and_check_files(
        old_fasta_path, old_tsv_path, fasta_path, tsv_path, query, limit, remove_germany, open_fun
    )


    print(f"Getting {len(new_entries)} sequences...")
    partial_lists = [
        new_entries.iloc[i * seqs_per_worker : (i + 1) * seqs_per_worker, :]
        for i in range(ceil(len(new_entries) / seqs_per_worker))
    ]
    result_lock = Lock()
    args = (
        (partial_list, fasta_path, tsv_path, result_lock, open_fun)
        for partial_list in partial_lists
    )
    with tqdm(total=len(partial_lists) * seqs_per_worker) as pbar:
        with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
            futures = {
                executor.submit(lambda p: download_sequences(*p), arg) for arg in args
            }
            for future in concurrent.futures.as_completed(futures):
                pbar.update(seqs_per_worker)
    print("Finished downloading files...")
    
    with open_fun(fasta_path, "rt") as fasta_handle:
        with open_fun(tsv_path, "rt") as tsv_handle:
            tsv_result = pd.read_csv(tsv_handle, sep="\t", low_memory=False)
            fasta_result = SeqIO.parse(fasta_handle, "fasta")
            if separate_german_seqs:
                print("Creating separate Germany files...")
                create_country_files(fasta_result, tsv_result, fasta_path, tsv_path, 'Germany')

            print("Testing if result is fine...")
            
            if not check_integrity(fasta_result, tsv_result):

                print("Cleaning metadata due to Ebi server 500 response.")
                # generator object cannot be copied
                with open_fun(fasta_path, "rt") as fasta_handle:
                    fasta_result = SeqIO.parse(fasta_handle, "fasta")
                    tsv_result = pd.read_csv(tsv_path, sep="\t", low_memory=False)
                    tsv = fix_tsv(fasta_result, tsv_result)
                    
                    tsv.to_csv(tsv_path, sep="\t", index=False)

    print("Done.")


def modify_fasta_lines(line):
    """
    Modifies the FASTA id of a sequence from EBI format to our format.
    The ID should be the same as the 'strain' field in the metadata file.

    Parameters
    ----------
    line String
        one line from a FASTA file as sent by EBI API
    
    Returns
    -------
    String
        modified line. Unchanged if it wasn't a line containing the ID
    """
    if line.startswith(">"):
        return ">" + line.split("|")[1]
    else:
        return line


# returns SeqIO obj
def download_sequences(tsv_data, fasta_path, tsv_path, lock, open_fun):
    """
    Downloads sequence data (FASTA) and appends them to a file.
    Also downloads lineage information if available and appends 
    metadata to the result metadata file. Can be executed in parallel

    Parameters
    ----------
    tsv_data: pd.DataFrame
        metadata of sequences that should be downloaded
    fasta_path: string
        location where sequences should be appended to
    tsv_path: string
        location where metadata should be appended to
    lock: threading.Lock
        Lock object that signals whether files can be written to
    open_fun : Function(path: string, mode: string) -> File
        either open or gzip.open, depending on configuration


    Returns
    -------
        nothing

    Raises
    -------
    Never raises, because exeptions get lost when using multithreading. 
    Prints exceptions and trace instead.
    """
    try:
        # Create temporary file
        accession_string = ",".join(tsv_data["strain"])
        fasta_result = requests.get(
            f"https://www.ebi.ac.uk/ena/browser/api/fasta/{accession_string}"
        )

        fasta_with_modified_titles = [
            modify_fasta_lines(line) for line in fasta_result.text.splitlines()
        ]

        # get lineage info
        params = {
            "query": 'id:' + ' OR id:'.join(tsv_data["strain"]),
            "format": "tsv",
            "fields": "id,lineage",
            "start": 0,
            "size": len(tsv_data)

        }
        lineage_result = requests.get("https://www.ebi.ac.uk/ebisearch/ws/rest/embl-covid19",params=params)
        if lineage_result.text:
            lineage_df = pd.read_csv(io.StringIO(lineage_result.text), sep="\t", low_memory=False)
            tsv_with_lineage = tsv_data.merge(lineage_df, left_on='strain', right_on='id', how='left').drop('id',axis=1)
        else:
            tsv_with_lineage = tsv_data.copy()
            tsv_with_lineage['lineage'] = np.nan

        with lock:
        # find out whether file exists to determine if tsv header should be printed
            try:
                with open_fun(tsv_path, "rt") as tsv:
                    is_start_of_file = False
            except FileNotFoundError:
                is_start_of_file = True

            with open_fun(fasta_path, "at") as fasta:
                with open_fun(tsv_path, "at") as tsv:
                    tsv_with_lineage.to_csv(
                        tsv, sep="\t", header=is_start_of_file, mode="a", index=False
                    )
                    fasta.write("\n".join(fasta_with_modified_titles) + "\n")
    except:
        print(sys.exc_info())
        print(traceback.format_exc())

def create_country_files(fasta, tsv, fasta_path, tsv_path, country):
    tsv_filtered = tsv[tsv['country'] == country]
    tsv_filtered.to_csv(f"{country}_{tsv_path}",sep='\t', index=False)
    strains = tsv_filtered.set_index(['strain'])
    seqs = []
    for seq in fasta:
        if seq.id in strains.index:
            seqs.append(seq)
    SeqIO.write(seqs, f"{country}_{fasta_path}","fasta")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Gets the meta-data + sequences from ebi and stores them as tsv/fasta, as used in the pipeline"
    )
    parser.add_argument(
        "fasta", type=str, help="Output FASTA (.fasta) file with sequences"
    )
    parser.add_argument("tsv", type=str, help="Output TSV file with metadata")
    parser.add_argument(
        "-f",
        "--old-fasta",
        type=str,
        required=False,
        help="Location of existing FASTA file to prevent re-downloading known sequences",
    )
    parser.add_argument(
        "-t",
        "--old-tsv",
        type=str,
        required=False,
        help="Location of existing TSV file",
    )
    parser.add_argument(
        "-s",
        "--seqs-per-worker",
        type=int,
        required=False,
        default=100,
        help=f"Amount of sequences per thread (default 100, >=1000 doesn't work because of URL length limitations",
    )

    parser.add_argument(
        "-w",
        "--workers",
        type=int,
        required=False,
        default=None,
        help="Max number of parallel downloads at any given time. Default: None, which means min(32, os.cpu_count() + 4) (might change in future concurrent.futures versions)",
    )
    parser.add_argument(
        "-q",
        "--query",
        type=str,
        required=False,
        default="tax_tree(2697049)",
        help=f"Query string to be passed to EBI search (default: \"tax_tree(2697049)\"",
    )
    parser.add_argument(
        "-l",
        "--limit",
        type=int,
        required=False,
        default=0,
        help=f"Only get the first LIMIT sequences. Unlimited if 0, default is 0",
    )
    parser.add_argument("-g", "--separate-german-seqs", action='store_true')
    parser.add_argument("-c", "--compress", action='store_true', help="If set, outputs are written in gzipped form")
    parser.add_argument("-r", "--remove-germany", action='store_true', help="If set, german sequences are omitted")

    args = parser.parse_args()
    get_ebi_data(
        args.fasta,
        args.tsv,
        args.old_fasta,
        args.old_tsv,
        args.seqs_per_worker,
        args.workers,
        args.query,
        args.limit,
        args.separate_german_seqs,
        args.compress,
        args.remove_germany
    )
