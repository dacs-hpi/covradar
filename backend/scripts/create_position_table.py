#!/usr/bin/env python
from Bio import SeqIO
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='creates position table for convinient numbering of bases')
parser.add_argument('first_seq', type=str, help='INPUT FASTA file containing first case sequence mapped to MSA')
parser.add_argument('table', type=str, help='Output TSV file containing positions of reference and MSA')
args = parser.parse_args()

mapped_first_case = SeqIO.read(args.first_seq, "fasta").seq

pos = 1
suffix = 1
pos_table = pd.DataFrame(columns=["reference", "msa"])
for i, base in enumerate(mapped_first_case):
    if base != "-": 
        row = pd.DataFrame([[str(pos), i+1]],columns=["reference", "msa"])
        pos_table = pd.concat([pos_table, pd.DataFrame(row, columns=["reference", "msa"])])
        suffix = 1
        pos += 1

    else:
        row = pd.DataFrame([[str(pos-1) + "." + str(suffix), i+1]],columns=["reference", "msa"])
        pos_table = pd.concat([pos_table, pd.DataFrame(row, columns=["reference", "msa"])])
        suffix += 1

pos_table.to_csv(args.table, sep="\t", index=False)
