#!/usr/bin/env python3
"""Script to convert DESH CSV file to TSV format."""
import argparse
import sys
from argparse import Namespace
from typing import TextIO

import pandas as pd


def parse_args(args: list) -> Namespace:
    """
    Parse a list of arguments.

    Parameters
    ----------
    args : list
        Arguments to parse.

    Returns
    -------
    _ : Namespace
        Parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description="Convert DESH CSV to TSV accepted by CovRadar"
    )
    parser.add_argument(
        "--metadata",
        type=str,
        required=True,
        help="Input CSV file containing the metadata.",
    )
    parser.add_argument(
        "--lineage",
        type=str,
        required=True,
        help="Input CSV file containing the lineage.",
    )
    parser.add_argument(
        "--tsv",
        type=str,
        required=True,
        help="Output TSV file to write the metadata with gzip compression.",
    )
    return parser.parse_args(args)


def read_csv(csv: TextIO, columns: list) -> pd.DataFrame:
    """
    Read only important columns from DESH's CSV file.

    Parameters
    ----------
    csv : TextIO
        File path or file handle, e.g., stdin.
    columns : list
        Names of columns to read.

    Returns
    -------
    metadata : pd.DataFrame
        DESH metadata.
    """
    metadata = pd.read_csv(
        csv,
        usecols=columns,
        dtype={"IMS_ID": "str"},
        sep=",",
        compression="infer",
    )
    return metadata


def rename_columns(metadata: pd.DataFrame) -> None:
    """
    Rename columns in dataframe for CovRadar.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame with metadata.
    """
    metadata.rename(
        columns={
            "DATE_DRAW": "date",
            "SEQUENCING_LAB_PC": "primary diagnostic lab pc",
            "IMS_ID": "strain",
        },
        inplace=True,
    )


def add_columns(metadata: pd.DataFrame) -> None:
    """
    Add columns important for CovRadar that are not in DESH.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame with metadata.
    """
    metadata["virus"] = "SARS-CoV-2"
    metadata["region"] = "Europe"
    metadata["country"] = "Germany"


def write_tsv(metadata: pd.DataFrame, tsv: TextIO) -> None:
    """
    Write TSV file with gzip compression.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame with metadata.
    tsv : TextIO
        Path or file handle to write TSV.
    """
    metadata.to_csv(
        tsv,
        sep="\t",
        columns=[
            "strain",
            "virus",
            "date",
            "region",
            "country",
            "lineage",
            "primary diagnostic lab pc",
        ],
        index=False,
        compression="infer",
    )


def get_metadata_columns():
    """
    Return columns important from DESH's metadata file.

    Returns
    -------
    columns : list
        Important columns.
    """
    columns = [
        "IMS_ID",
        "DATE_DRAW",
        "SEQUENCING_LAB_PC",
    ]
    return columns


def get_lineage_columns():
    """
    Return columns important from DESH's lineage file.

    Returns
    -------
    columns : list
        Important columns.
    """
    columns = [
        "IMS_ID",
        "lineage",
    ]
    return columns


def add_lineage(metadata: pd.DataFrame, lineage: pd.DataFrame) -> pd.DataFrame:
    """
    Read only important columns from DESH's CSV file.

    Parameters
    ----------
    metadata : pd.DataFrame
        DESH metadata.
    lineage : pd.DataFrame
        DESH lineage.

    Returns
    -------
    merged : pd.DataFrame
        DESH metadata with lineage.
    """
    merged = pd.merge(metadata, lineage, on="IMS_ID", how="outer")
    return merged


def main():  # pragma: no cover
    """Convert CSV to TSV for CovRadar."""
    args = parse_args(sys.argv[1:])
    metadata = read_csv(args.metadata, get_metadata_columns())
    lineage = read_csv(args.lineage, get_lineage_columns())
    metadata = add_lineage(metadata, lineage)
    rename_columns(metadata)
    add_columns(metadata)
    write_tsv(metadata, args.tsv)


if __name__ == "__main__":
    main()  # pragma: no cover
