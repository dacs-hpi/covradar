#!/usr/bin/env python3

from Bio import SeqIO
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Extracts the first sequence from a msa fasta file')
parser.add_argument('alignment', type=str, help='Input file containing the blastx alignment')
parser.add_argument('output', type=str, help='Output CSV file containing the S1 domain coordinates')
args = parser.parse_args()

alignment = pd.read_csv(args.alignment, header=None, sep='\t')
# Columns headers named according to http://www.metagenomics.wiki/tools/blast/blastn-output-format-6
labels = ['QueryID', 'SubjectID', 'Identity', 'AlignmentLength', 'Mismvatches', 'Gaps',
            'QueryStart', 'QueryEnd', 'SubjectStart', 'SubjectEnd', 'EValue', 'BitScore']
alignment.columns = labels
output = open(args.output, "w")
output.write('start,stop,domain_name\n')
output.write(str(alignment.at[0, 'QueryStart']) + ',' + str(alignment.at[0, 'QueryEnd']) + ',' + 'RBD\n')
output.close()
