import argparse
import pandas as pd
from datetime import datetime
import csv
from collections import defaultdict, Counter
from Bio import SeqIO
import math
import sys
from shared.residue_converter.data_loader import CODONDICT
import os
import numpy as np
import gzip
from io import TextIOWrapper


def calculate_frequencies(snps_path, metadata_path, msa_first_case_path, protein_length, regions, output_path_prefix):

    # setup list of given regions
    regions = regions.split(",")

    # load reference sequence
    first_case_fasta = list(SeqIO.parse(open(msa_first_case_path), 'fasta'))
    reference_seq = 'X' + str(first_case_fasta[0].seq) # insert 'X' at the beginning such that the index of the first position is 1

    # load metadata
    metadata = pd.read_csv(metadata_path, sep='\t', dtype=str)

    # check for invalid dates in metadata
    index_invalid_dates = []
    for i in range(metadata.shape[0]):
        split_date = metadata.at[i, 'date'].split('-')
        if len(split_date) == 3:
            try:
                metadata.at[i, 'date'] = datetime.strptime(metadata.at[i, 'date'], '%Y-%m-%d')

            except ValueError:
                print('Date %s is invalid for sequence %s' % (metadata.at[i, 'date'], metadata.at[i, 'strain']))
                index_invalid_dates.append(i)
        else:
            print('Date %s is in unrecognized format for sequence %s' % (metadata.at[i, 'date'], metadata.at[i, 'strain']))
            index_invalid_dates.append(i)
    metadata.drop(metadata.index[index_invalid_dates], inplace=True)
    metadata.reset_index(drop=True, inplace=True)

    # adjust invalid dates indices for variant calling file 
    if index_invalid_dates:
        index_invalid_dates = [x+9 for x in index_invalid_dates]
        index_invalid_dates = sorted(index_invalid_dates, reverse=True)

    # initialize frequency dict
    init_freq_dict = {
        'A': 0, 'C': 0, 'T': 0, 'G': 0, 'N': 0, '-': 0, 'W': 0, 'S': 0, 'M': 0,
        'K': 0, 'R': 0, 'Y': 0, 'B': 0, 'D': 0, 'H': 0, 'V': 0, 'Coverage': 0,
    }

    # simple iterator to add an entry for each position even if nothing was called at the position in the VCF file
    pos_counter = 1 

    # initializing boolean variable which indicates if additional info to the aa dist is needed
    codon_info_needed = True

    # create output folder for each region and setup dictionary containing the output file paths
    output_file_dict = {}
    for reg in regions:
        output_dir = output_path_prefix + reg + '/'
        os.makedirs(output_dir, exist_ok=True)    
        output_file_dict[reg] = {}
        output_file_dict[reg]['nt'] = output_dir + 'nt_freqs_per_cw.tsv'
        output_file_dict[reg]['aa'] = output_dir + 'aa_freqs_per_cw.tsv'  

    # open vcf file
    with gzip.open(snps_path, "rb") as f:
        with TextIOWrapper(f, encoding='utf-8') as vcf_file:

            # skip first 2 lines
            vcf_file.readline()
            vcf_file.readline()

            # get accession numbers and remove seqs with invalid dates in the metadata
            accession_numbers = vcf_file.readline().strip().split('\t')
            for idx in index_invalid_dates:
                del accession_numbers[idx]
            accession_numbers = accession_numbers[9:]

            # create a membership dictionary storing the indices of the accession numbers 
            # that belong to a respective region and calendar week
            membership_dict = {}
            calendar_weeks = []
            for reg in regions:
                # initialization for each region
                membership_dict[reg] = {}
            for i, acc in enumerate(accession_numbers):
                # convert date to calendar week
                date = str(metadata.at[i, 'date']).split(" ")[0]
                date_time = datetime.strptime(date, '%Y-%m-%d')
                cw = str(date_time.isocalendar()[0]) + 'W' + str("%02d" % date_time.isocalendar()[1])
                if cw not in calendar_weeks:
                    calendar_weeks.append(cw)
                # create region dict with bools indicating if the sequence belongs to the given region or not
                country = str(metadata.at[i, 'country']).lower()
                country_exposure = str(metadata.at[i, 'country_exposure']).lower()
                reg_dict = {}
                for reg in regions:
                    if country == reg.lower() or country_exposure == reg.lower() or reg.lower() == 'global':
                        reg_dict[reg] = True
                    else:
                        reg_dict[reg] = False
                # iterate over reg_dict for this accession number
                for reg_key, reg_value in reg_dict.items():
                    if reg_value == True: # if it belongs to this region
                        if cw in membership_dict[reg_key]: 
                            membership_dict[reg_key][cw].append(i) # append accession number index
                        else:
                            membership_dict[reg_key][cw] = [i] # create a new entry with accession number index

            # add empty lists for region + cw combination not contained in the data
            for reg_key in regions:
                for cw in calendar_weeks:
                    if cw not in membership_dict[reg_key]:
                        membership_dict[reg_key][cw] = []
            
            # setup output files
            calendar_weeks = sorted(calendar_weeks)
            for reg in regions:
                # nt
                with open(output_file_dict[reg]['nt'], 'w') as output_file:
                    tsv_writer = csv.writer(output_file, delimiter='\t')
                    row_to_write = ['POS', 'REF']
                    row_to_write.extend(calendar_weeks)
                    tsv_writer.writerow(row_to_write)
                # aa
                with open(output_file_dict[reg]['aa'], 'w') as output_file:
                    tsv_writer = csv.writer(output_file, delimiter='\t')
                    row_to_write = ['CODON_NUM', 'REF_CODON', 'REF_AA']
                    row_to_write.extend(calendar_weeks)
                    tsv_writer.writerow(row_to_write)

            # initialize codon dict to store the nucleotides of a codon and the indicators for gaps
            codon_dict = {}
            codon_dict['codon_number'] = 0
            codon_dict['ref_codon'] = ""
            codon_dict['ref_aa'] = ""
            codon_dict['gap_of_len_1'] = False
            codon_dict['gap_of_len_10'] = False
            for reg in regions:
                codon_dict[reg] = {}
                for cw in calendar_weeks:
                    codon_dict[reg][cw] = [] # rows_codon
    
            # calculate sequence coverage for each subset
            coverage_dict = {}
            for reg_key in membership_dict:
                coverage_dict[reg_key] = {}
                for cw_key in membership_dict[reg_key]:
                    coverage_dict[reg_key][cw_key] = len(membership_dict[reg_key][cw_key])

            # iterate over lines in vcf file
            for line in vcf_file:
                row = line.split("\t")
                pos = int(row[1])
                ref_nt = row[3]
                alt_alleles = row[4].split(",")
                variant_calls = np.array(row[9:])

                ### actions for positions where no variant call was made ###
                while pos_counter < int(pos):
                    ref_nt = reference_seq[pos_counter]
                    add_base_to_refcodon = True # only needs to be done once per position
                    for reg_key in coverage_dict:
                        nt_row_to_write = []
                        aa_row_to_write = []
                        for cw_key in calendar_weeks:
                            coverage = coverage_dict[reg_key][cw_key] # get coverage from pre-initialized dict
                            freq_dict = init_freq_dict.copy() # initialize new base frequency dictionary
                            freq_dict['Coverage'] = coverage
                            freq_dict[ref_nt] = coverage # set the reference base for each seq because no call is present in the vcf file at this position
                            nt_row_to_write.append(str(freq_dict)) # add base frequencies for this cw to list
                            if check_if_new_codon(pos_counter): # check if current position is the start of a new codon
                                if pos_counter != 1:
                                    if codon_info_needed == True: # if it's the first cw -> get additional info which remains the same per position
                                        aa_row_to_write = []
                                        # calculate aa distribution and additional infos
                                        aa_dist, ref_aa, codon_number, gap_of_len_1, gap_of_len_10 = get_aa_dist_and_info(codon_dict[reg_key][cw_key], codon_dict['codon_number'], codon_dict['ref_codon'], coverage, codon_dict['gap_of_len_1'], codon_dict['gap_of_len_10'])
                                        aa_row_to_write.append(str(aa_dist))
                                        # save these infos in codon_dict
                                        codon_dict['codon_number'] = codon_number
                                        codon_dict['ref_aa'] = ref_aa
                                        codon_dict['gap_of_len_1'] = gap_of_len_1
                                        codon_dict['gap_of_len_10'] = gap_of_len_10
                                        codon_dict[reg_key][cw_key] = []
                                        codon_info_needed = False # re-calculation of these infos not needed for this position
                                    else: # not the first cw
                                        # only calculate aa distribution
                                        aa_dist, ref_aa = calculate_aa_dist(codon_dict[reg_key][cw_key], codon_dict['ref_codon'], coverage)
                                        aa_row_to_write.append(str(aa_dist))
                                        codon_dict[reg_key][cw_key] = [] # reset codon dict
                                else:
                                    codon_dict['ref_codon'] = ref_nt
                            else: 
                                if add_base_to_refcodon:
                                    codon_dict['ref_codon'] = codon_dict['ref_codon'] + ref_nt
                                    add_base_to_refcodon = False
                            # setup nucleotides array with only ref_bases (since ther is no call in the vcf at this position)
                            nucleotides_subset = [ref_nt] * coverage
                            codon_dict[reg_key][cw_key].append(nucleotides_subset)
                        # write the nt distributions for each calendar week to file
                        write_nt_row_to_file(filename=output_file_dict[reg_key]['nt'], position=pos_counter, ref_nt=ref_nt, row_to_write=nt_row_to_write)
                        # if it's the start of a new codon, write the aa distributions for the last codon to file
                        if check_if_new_codon(pos_counter) and pos_counter != 1:
                            write_aa_row_to_file(filename=output_file_dict[reg_key]['aa'], codon_number=codon_dict['codon_number'], ref_codon=codon_dict['ref_codon'], ref_aa=codon_dict['ref_aa'], row_to_write=aa_row_to_write)
                    
                    # initialize new ref_codon at the start of a new codon
                    if check_if_new_codon(pos_counter):
                        codon_dict['ref_codon'] = ref_nt 

                    codon_info_needed = True    
                    pos_counter += 1

                # set to True for each new row in vcf file
                ref_alt_dict_needed = True 
                add_base_to_refcodon = True

                ### actions for positions where a variant call is present in the vcf file ###
                for reg_key in membership_dict:
                    nt_row_to_write = []
                    aa_row_to_write = []
                    for cw_key in calendar_weeks:
                        index_list = membership_dict[reg_key][cw_key] # indices for current region and calendar week combination
                        variant_calls_subset = list(variant_calls[index_list]) # create subset of variant calls for the current region + cw combination
                        coverage = coverage_dict[reg_key][cw_key] # get coverage from pre-initialized dict
                        freq_dict = init_freq_dict.copy() # initialize new base frequency dictionary
                        freq_dict['Coverage'] = coverage
                        ref_nt = row[3] # set reference base of current position
                        if check_if_new_codon(pos): # check if current position is the start of a new codon
                            if pos != 1:
                                if codon_info_needed == True: # if it's the first cw -> get additional info which remains the same per position
                                    aa_row_to_write = []
                                    # calculate aa distribution and additional infos# calculate aa distribution and additional infos
                                    aa_dist, ref_aa, codon_number, gap_of_len_1, gap_of_len_10 = get_aa_dist_and_info(codon_dict[reg_key][cw_key], codon_dict['codon_number'], codon_dict['ref_codon'], coverage, codon_dict['gap_of_len_1'], codon_dict['gap_of_len_10'])
                                    aa_row_to_write.append(str(aa_dist))
                                    # save these infos in codon_dict
                                    codon_dict['codon_number'] = codon_number
                                    codon_dict['ref_aa'] = ref_aa
                                    codon_dict['gap_of_len_1'] = gap_of_len_1
                                    codon_dict['gap_of_len_10'] = gap_of_len_10
                                    codon_dict[reg_key][cw_key] = []
                                    codon_info_needed = False # re-calculation of these infos not needed for this position
                                else: # not the first cw
                                    # only calculate aa distribution
                                    aa_dist, ref_aa = calculate_aa_dist(codon_dict[reg_key][cw_key], codon_dict['ref_codon'], coverage)
                                    aa_row_to_write.append(str(aa_dist))
                                    codon_dict[reg_key][cw_key] = [] # reset codon dict
                            else:
                                codon_dict['ref_codon'] = ref_nt
                        else:
                            if add_base_to_refcodon:
                                codon_dict['ref_codon'] = codon_dict['ref_codon'] + ref_nt
                                add_base_to_refcodon = False

                        # create dictionary for variants
                        if ref_alt_dict_needed == True:
                            alt_list = [a.upper() for a in alt_alleles]
                            ref_alt_dict = { i:j for i,j in zip(range(1,len(alt_list)+1), alt_list)}
                            ref_alt_dict[0] = ref_nt.upper()
                            ref_alt_dict_needed = False
                        # count base distribution at position
                        alts = Counter(variant_calls_subset)
                        # update freq_dict and append nt distribution for this cw to list
                        for key in alts.keys():
                            freq_dict[ref_alt_dict[int(key)]] = alts[key]
                        nt_row_to_write.append(str(freq_dict))

                        # get nucleotides from variant calls and append to list
                        nucleotides_subset = list(map(lambda x: ref_alt_dict[int(x)], variant_calls_subset))
                        codon_dict[reg_key][cw_key].append(nucleotides_subset)
                    # write the nt distributions for each calendar week to file
                    write_nt_row_to_file(filename=output_file_dict[reg_key]['nt'], position=pos, ref_nt=ref_nt, row_to_write=nt_row_to_write)
                    # if it's the start of a new codon, write the aa distributions for the last codon to file
                    if check_if_new_codon(pos) and pos != 1:
                        write_aa_row_to_file(filename=output_file_dict[reg_key]['aa'], codon_number=codon_dict['codon_number'], ref_codon=codon_dict['ref_codon'], ref_aa=codon_dict['ref_aa'], row_to_write=aa_row_to_write)
                
                # initialize new ref_codon at the start of a new codon
                if check_if_new_codon(pos):
                    codon_dict['ref_codon'] = ref_nt

                codon_info_needed = True
                add_base_to_refcodon = True
                pos_counter += 1

            ### proceed until end of spike protein if the last position of the vcf is not the last position of the MSA ###
            pos_last_entry = pos
            if pos_last_entry < protein_length:
                for pos_counter in range(pos_last_entry+1, protein_length+1):
                    ref_nt = reference_seq[pos_counter]
                    for reg_key in coverage_dict:
                        nt_row_to_write = []
                        aa_row_to_write = []
                        for cw_key in calendar_weeks:
                            coverage = coverage_dict[reg_key][cw_key]
                            freq_dict = init_freq_dict.copy()
                            freq_dict['Coverage'] = coverage
                            freq_dict[ref_nt] = coverage
                            nt_row_to_write.append(str(freq_dict))
                            if check_if_new_codon(pos_counter):
                                if codon_info_needed == True:
                                    aa_row_to_write = []
                                    aa_dist, ref_aa, codon_number, gap_of_len_1, gap_of_len_10 = get_aa_dist_and_info(codon_dict[reg_key][cw_key], codon_dict['codon_number'], codon_dict['ref_codon'], coverage, codon_dict['gap_of_len_1'], codon_dict['gap_of_len_10'])
                                    aa_row_to_write.append(str(aa_dist))
                                    codon_dict['codon_number'] = codon_number
                                    codon_dict['ref_aa'] = ref_aa
                                    codon_dict['gap_of_len_1'] = gap_of_len_1
                                    codon_dict['gap_of_len_10'] = gap_of_len_10
                                    codon_dict[reg_key][cw_key] = []
                                    codon_info_needed = False
                                else:
                                    aa_dist, ref_aa = calculate_aa_dist(codon_dict[reg_key][cw_key], codon_dict['ref_codon'], coverage)
                                    aa_row_to_write.append(str(aa_dist))
                                    codon_dict[reg_key][cw_key] = []
                            else:
                                if add_base_to_refcodon:
                                    codon_dict['ref_codon'] = codon_dict['ref_codon'] + ref_nt
                                    add_base_to_refcodon = False
                            nucleotides_subset = [ref_nt] * coverage
                            codon_dict[reg_key][cw_key].append(nucleotides_subset)
                        write_nt_row_to_file(filename=output_file_dict[reg_key]['nt'], position=pos_counter, ref_nt=ref_nt, row_to_write=nt_row_to_write)
                        if check_if_new_codon(pos_counter):
                            write_aa_row_to_file(filename=output_file_dict[reg_key]['aa'], codon_number=codon_dict['codon_number'], ref_codon=codon_dict['ref_codon'], ref_aa=codon_dict['ref_aa'], row_to_write=aa_row_to_write)
                    if check_if_new_codon(pos_counter):
                        codon_dict['ref_codon'] = ref_nt 
                    codon_info_needed = True    
                    pos_counter += 1

            ### write aa distribution of the very last codon to file ###
            for reg_key in coverage_dict:
                aa_row_to_write = []
                for cw_key in calendar_weeks:
                    if codon_info_needed == True:
                        aa_dist, ref_aa, codon_number, gap_of_len_1, gap_of_len_10 = get_aa_dist_and_info(codon_dict[reg_key][cw_key], codon_dict['codon_number'], codon_dict['ref_codon'], coverage, codon_dict['gap_of_len_1'], codon_dict['gap_of_len_10'])
                        aa_row_to_write.append(str(aa_dist))
                        codon_dict['codon_number'] = codon_number
                        codon_dict['ref_aa'] = ref_aa
                        codon_info_needed = False
                    else:
                        aa_dist, ref_aa = calculate_aa_dist(codon_dict[reg_key][cw_key], codon_dict['ref_codon'], coverage)
                        aa_row_to_write.append(str(aa_dist))
                write_aa_row_to_file(filename=output_file_dict[reg_key]['aa'], codon_number=codon_dict['codon_number'], ref_codon=codon_dict['ref_codon'], ref_aa=codon_dict['ref_aa'], row_to_write=aa_row_to_write)


def check_if_new_codon(pos):
    codon_pos_indicator = round((pos/3) % math.floor(pos/3),1) if pos >= 3 else round((pos/3),1) # [0.0|0.3|0.7] 
    if codon_pos_indicator == 0.3:
        return True
    else: 
        return False


def convert_codon_df_to_distribution(codon_list_zipped, ref_codon, coverage):
    ref_aa = get_aa(CODONDICT, ref_codon)
    aa_dist = {
             "A": 0, "C": 0, "D": 0, "E": 0, "F": 0, "G": 0, "H": 0, "I": 0, "K": 0,
             "L": 0, "M": 0, "N": 0, "P": 0, "Q": 0, "R": 0, "S": 0, "T": 0, "V": 0, 
             "W": 0, "Y": 0, "*": 0, "-": 0, "Coverage": coverage
             }
    for codon_list in codon_list_zipped:
        codon = "".join(codon_list)
        if codon != ref_codon:
            aa = get_aa(CODONDICT, codon)
            if aa != False:
                aa_dist[aa] += 1
            else: 
                aa_dist[ref_aa] += 1
        else:
            aa_dist[ref_aa] += 1
    return aa_dist, ref_aa     


def get_aa(codon_dict, val):
    for key, value in codon_dict.items():
        if val in value:
            return key
    return False


def calculate_codon_number_for_gaps(codon_number, gap_of_len_1, gap_of_len_10):
    codon_number = float(codon_number)
    decimal_length = str(codon_number)[::-1].find('.') # get number of decimal digits
    decimal = round(codon_number - int(codon_number), decimal_length) # get decimal rest as float
    if decimal == 0.0: # gap starts 
        codon_number = round(codon_number + 0.1, 1)
        gap_of_len_1 = True
    elif gap_of_len_1:
        if decimal == 0.9: # gap has already length of 9 codons
            codon_number = '{:.2f}'.format(round(int(codon_number) + 0.1, 1)) # set 10th codon to X.10 (as string)
            gap_of_len_1 = False
            gap_of_len_10 = True
        else: # gap has less than 9 codons so far
            codon_number = round(codon_number + 0.1, 1)
    elif gap_of_len_10:
        if decimal == 0.99: # gap has already length of 99 codons
            codon_number = '{:.3f}'.format(round(int(codon_number) + 0.1, 1)) # set 100th codon to X.100
            gap_of_len_10 = False
        else: # gap has length of at least 10 but at most 98 codons
             codon_number = round(codon_number + 0.01, 2)
    else: # gap has length of more than 100 codons
        codon_number = round(codon_number + 0.001, 3)
    return codon_number, gap_of_len_1, gap_of_len_10


def get_aa_dist_and_info(rows_codon, codon_number, ref_codon, coverage, gap_of_len_1, gap_of_len_10):
    # calculate aa distribution
    aa_dist, ref_aa = calculate_aa_dist(rows_codon, ref_codon, coverage)
    # calculate codon number (X.1, X.2 indicate a gap in the msa of the reference)
    if ref_aa != "-":
        codon_number = int(float(codon_number)) # floor after potential previous gap in ref_seq (msa)
        codon_number += 1
    else:
        codon_number, gap_of_len_1, gap_of_len_10 = calculate_codon_number_for_gaps(codon_number, gap_of_len_1, gap_of_len_10) 
    return aa_dist, ref_aa, codon_number, gap_of_len_1, gap_of_len_10


def calculate_aa_dist(rows_codon, ref_codon, coverage):
    codon_list_zipped = list(zip(rows_codon[0], rows_codon[1], rows_codon[2]))
    aa_dist, ref_aa = convert_codon_df_to_distribution(codon_list_zipped, ref_codon, coverage)
    return aa_dist, ref_aa

def write_nt_row_to_file(filename, position, ref_nt, row_to_write):
    with open(filename, 'a') as output_file:
        tsv_writer = csv.writer(output_file, delimiter='\t')
        row_to_write.insert(0, ref_nt)
        row_to_write.insert(0, position)
        tsv_writer.writerow(row_to_write)

def write_aa_row_to_file(filename, codon_number, ref_codon, ref_aa, row_to_write):
    with open(filename, 'a') as output_file:
        tsv_writer = csv.writer(output_file, delimiter='\t')
        row_to_write.insert(0, ref_aa)
        row_to_write.insert(0, ref_codon)
        row_to_write.insert(0, codon_number)
        tsv_writer.writerow(row_to_write)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates TSV files containing the nucleotide and amino acid distributions per position per calendar week for each region.')
    parser.add_argument('snps_path', type=str, help='Input VCF file containing SNPs for all genomes.')
    parser.add_argument('metadata_path', type=str, help='Input TSV files containing the metadata information.')
    parser.add_argument('msa_first_case', type=str, help='FASTA file of first case after MSA.')
    parser.add_argument('numbering', type=str, help='Input TSV file containing the base positions of the MSA and the corresponding positions of the first case sequence.')
    parser.add_argument('regions', type=str, help='Regions for which separated output files are generated.')
    parser.add_argument('output_path_prefix', type=str, help='Prefix of the location where output files are saved.')
    args = parser.parse_args()

    # input
    snps_path = args.snps_path 
    metadata_path = args.metadata_path 
    msa_first_case_path = args.msa_first_case
    position_path = args.numbering 
    regions = args.regions
    output_path_prefix = args.output_path_prefix 

    # protein length in bases
    position_table = pd.read_csv(position_path, sep="\t", dtype={"msa":int, "reference":str})
    protein_length = position_table.iloc[-1,:]["msa"]  # in bases 

    calculate_frequencies(snps_path, metadata_path, msa_first_case_path, protein_length, regions, output_path_prefix)
