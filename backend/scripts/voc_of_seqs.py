"""
This script creates a new table from the VCF file, the position table and the VOC table that says
for each sequence whether it has the VOC or not.
"""
import argparse
import re
import pandas as pd
import numpy as np
import sys
if sys.version_info[0] < 3:
    from StringIO import StringIO, TextIOWrapper
else:
    from io import StringIO, TextIOWrapper
import gzip
import ray

# %%
def _voc_lookup_creator(nt_name, aa_name, msa_poslist):
    """Takes row of VOC table and outputs lookup dictionary for VCF file to find sequences
    carring the VOC.

    The mutation can be a SNP, deletion or insertion:
    - C21614T
    - del:21767:6
    - ins:21767:6:ATCATG
    Multiple mutations are possible:
    - G22132C/G22132T
    - G23012A,A23063T
    - G23012C+A23013C
    The separators "," and "+" can include multiple formats.
    - G23012C+A23013C/A23013T
    - G23012C+del:21767:6
    - ins:21767:6:ATCATG,C21614T
    Args:
        nt_name (string): Nukleotide mutation
        aa_name (string): Amino acid mutation, e.g. L18F or E484K,N501Y
        msa_poslist (list): MSA positions of variant.
    """
    # initialization
    result = {"mut":[], "aa_name":aa_name, "com":{}}

    snp_pattern = re.compile("^[A-Za-z]+[0-9]+[A-Za-z]+$")
    del_pattern = re.compile("^del:[0-9]+:[0-9]$")
    ins_pattern = re.compile("^ins:[0-9]+:[0-9]+:[A-Za-z]+$")

    # split by +,
    i = 0
    for part in re.split(r'\+|,', nt_name):
        pos = msa_poslist[i]
        # first element
        if i == 0:
            if snp_pattern.match(part):
                result["mut"] = [part[-1]]
                i += 1
            elif len(part.split("/")) > 1:
                result["mut"] = [el[-1] for el in part.split("/")]
                i += 1
            elif del_pattern.match(part):
                subparts = part.split(":")
                result["mut"] = ["-"]
                result["com"] = {x+pos:["-"] for x in range(1,int(subparts[-1]))}
                i += int(subparts[-1])
            elif ins_pattern.match(part):
                subparts = part.split(":")
                result["mut"] = [subparts[-1][0]]
                result["com"] = {x+pos:[subparts[-1][x]] for x in range(1,int(subparts[-2]))}
                i += 1
        else:
            if snp_pattern.match(part):
                result["com"][pos] = [part[-1]]
                i += 1
            elif len(part.split("/")) > 1:
                result["com"][pos] = [el[-1] for el in part.split("/")]
                i += 1
            elif del_pattern.match(part):
                subparts = part.split(":")
                for pos_del in range(int(subparts[-1])):
                    result["com"][pos+pos_del] = ["-"]
                i += int(subparts[-1])
            elif ins_pattern.match(part):
                subparts = part.split(":")
                for pos_ins in range(int(subparts[-2])):
                    result["com"][pos+pos_ins] = [subparts[-1][pos_ins]]
                i += 1

    return result

def get_row(snp_path, index):
    # the VCF file is very big, that is why we only grep single rows
    with gzip.open(snp_path, 'rb') as f:
        with TextIOWrapper(f, encoding='utf-8') as fin:
            fin.readline()
            fin.readline()
            columns = fin.readline().strip().split("\t")
            for i in range(index + 1):
                line = fin.readline()
            df = pd.read_csv(StringIO(line), sep="\t", header=None)
            df.columns = columns
            df.index = [index]
            return df

def ini(snp_path, voc_path):
    """Takes VCF file, position table and the VOC table path and returns a new table
       that says for each sequence whether it has the VOC or not.

    Args:
        snp_path (str): Path to VCF file (TSV version)
        voc_path (str): Path to VOC file

    Returns:
        pandas dataframe: stores relationship between each sequence in VCF and VOC in VOC table
    """
    # VCF File
    vcf_data = pd.read_csv(snp_path, compression='gzip', sep="\t", skiprows=2, usecols=["ALT", "POS"])
    # TSV file containing VOC
    voc_data = pd.read_csv(voc_path, sep="\t")

    range_pattern = re.compile("^[0-9]+:[0-9]+$")

    # first 8 columns are '#CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO' and get
    # skipped
    # with open(snp_path) as fin:
    #     fin.readline()
    #     fin.readline()
        # seq_names = fin.readline().strip().split('\t')[8:]
    aa_col = voc_data["amino acid"].values
    nt_col = voc_data["nucleotide"].values
    msa_col = [pos_list.split(",") for pos_list in voc_data["msa"]]
    msa_positions = []
    for i, msa_list in enumerate(msa_col):
        msa_positions.append([])
        for msa_el in msa_list:
            if msa_el == "None":
                msa_positions[i].append(-1)
            elif range_pattern.match(msa_el):
                start, stop = msa_el.split(":")
                msa_positions[i].extend([el for el in range(int(start),int(stop)+1)])
            else:
                msa_positions[i].append(int(msa_el))

    return vcf_data, nt_col, aa_col, msa_positions

@ray.remote
def create_lookup(x):
    # It could be the case that insertions are not in the MSA.
    # In this case, MSA position has a None which is converted into -1.
    result = []
    for nt_col_row, aa_col_row, msa_row in x:
        if msa_row[0] > 0:
            result.append([msa_row[0], _voc_lookup_creator(nt_col_row, aa_col_row, msa_row)])
        else:
            result.append([msa_row[0], {}])
    return result

@ray.remote
def refine_lookup(vcf_data, x):
    # exclude mutations in voc_lookup_list at msa_position if it is not in ALT in VCF file
    result = []
    for msa_pos_val in x:
        msa_pos = msa_pos_val[0]
        msa_mut = msa_pos_val[1]

        msa_mut["use"] = False
        if len(vcf_data.loc[vcf_data['POS'] == msa_pos]) > 0:
            alt_values = vcf_data.loc[vcf_data['POS'] == msa_pos, "ALT"].values[0].split(",")
            muts = msa_mut["mut"]
            # intersection of mutations and alleles is not empty
            if set(muts) & set(alt_values):
                msa_mut["use"] = True
        if len(vcf_data.loc[vcf_data['POS'] == msa_pos]) > 1:
            raise "In VCF file there are multiple rows with the position: " + str(msa_pos)
        result.append([msa_pos, msa_mut])
    return result

@ray.remote
def create_voc_of_seq(vcf_data, snp_path, x):
    # look up in vcf file at msa_pos if non-0 seq have voc
    result = []
    for msa_pos_val in x:
        msa_pos = msa_pos_val[0]
        msa_mut = msa_pos_val[1]

        if msa_mut["use"]:
            
            row_index = vcf_data.index[vcf_data['POS'] == msa_pos].tolist()[0] # assuming only one row is returned from this query
            vcf_row = get_row(snp_path, row_index)
            alt_values = vcf_row["ALT"].values[0].split(",")
            mut_alt = {mut:alt_values.index(mut)+1 for mut in msa_mut["mut"] if mut in alt_values}
            # get all sequences with these ALTs at this position
            seqs_w_mut_bool = vcf_row.iloc[:, 8:].isin(mut_alt.values()).all(0).values
            if seqs_w_mut_bool.any():
                vcf_subset = vcf_row.loc[[True], np.append(8*[True], seqs_w_mut_bool)]
                seq_first_condition = vcf_subset.columns[8:]
                # check for other conditions in voc_lookup["com"]
                if len(msa_mut["com"]) == 0:
                    for seq in seq_first_condition:
                        result.append([seq, msa_mut["aa_name"]])
                else:
                    seq_with_all_conditions = seq_first_condition
                    for com_pos in msa_mut["com"]:
                        try:
                            row_index = vcf_data.index[vcf_data['POS'] == com_pos].tolist()[0] # assuming only one row is returned from this query
                            vcf_row = get_row(snp_path, row_index)
                        except:
                            vcf_row = vcf_data.loc[vcf_data['POS'] == com_pos]
                        if len(vcf_row) == 0:
                            seq_with_all_conditions = []
                            break
                        alt_values = vcf_row["ALT"].values[0].split(",")
                        muts = [m for m in msa_mut["com"][com_pos] if m in alt_values]
                        if not set(muts) & set(alt_values):
                            seq_with_all_conditions = []
                            break
                        mut_alt = {mut:alt_values.index(mut)+1 for mut
                                    in muts}
                        # get all sequences with these ALTs at this position
                        seqs_w_mut_bool = \
                            vcf_row.iloc[:, 8:].isin(mut_alt.values()).all(0).values
                        if not seqs_w_mut_bool.any():
                            seq_with_all_conditions = []
                            break
                        else:
                            vcf_subset = \
                                vcf_row.loc[[True], np.append(8*[True], seqs_w_mut_bool)]
                            seq_with_condition = vcf_subset.columns[8:]
                            # remove all sequences from seq_with_all_conditions not in
                            # seq_with_condition
                            seq_with_all_conditions = \
                                list(set(seq_with_all_conditions) & set(seq_with_condition))
                    for seq in seq_with_all_conditions:
                        result.append([seq, msa_mut["aa_name"]])

    return result

def batch(iterable, n=1):
        l = len(iterable)
        for ndx in range(0, l, n):
            yield iterable[ndx:min(ndx + n, l)]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Takes variants file of sequences and variants \
        of concern. Records variants of concern for each sequence in a table.')
    parser.add_argument('cpus', type=int, help='Number of CPUs')
    parser.add_argument('snp_vcf_path', type=str, help='Input VCF file')
    parser.add_argument('voc_path', type=str, help='Input TSV file containing the variants of \
        concern')
    parser.add_argument('output_path', type=str, help='Output VOC per Sequence TSV table')
    args = parser.parse_args()


    vcf_data, nt_col, aa_col, msa_positions = ini(args.snp_vcf_path, args.voc_path)
    
    ray.init(num_cpus=args.cpus)
    
    batch_size = args.cpus

    li = list(zip(nt_col, aa_col, msa_positions))
    voc_lookup_list_ids = []
    for x in batch(li, batch_size):
        voc_lookup_list_ids.append(create_lookup.remote(x))
    voc_lookup_list = []
    for el in voc_lookup_list_ids:
        voc_lookup_list.extend(ray.get(el))

    voc_lookup_bool_ids = []
    for v in batch(voc_lookup_list, batch_size):
        voc_lookup_bool_ids.append(refine_lookup.remote(vcf_data, v))
    voc_lookup_bool = []
    for el in voc_lookup_bool_ids:
        voc_lookup_bool.extend(ray.get(el))

    voc_seq_table_list_ids = []
    for b in batch(voc_lookup_bool, batch_size):
        voc_seq_table_list_ids.append(create_voc_of_seq.remote(vcf_data, args.snp_vcf_path, b))
    voc_seq_table_list = []
    for el in voc_seq_table_list_ids:
        voc_seq_table_list.extend(ray.get(el))

    ray.shutdown()

    voc_seq_table = pd.DataFrame(voc_seq_table_list, columns=["sequence_name", "variant"])
    voc_seq_table["bool"] = True
    voc_seq_table = voc_seq_table.set_index(["sequence_name", "variant"]).unstack().fillna(False)
    voc_seq_table.columns = voc_seq_table.columns.droplevel()
    voc_seq_table.columns.name = ""

    voc_seq_table.to_csv(args.output_path, sep="\t")

