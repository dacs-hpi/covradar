#!/usr/bin/env python3
"""Computes consensus sequences per calendar week."""
import argparse
import pickle
import sys
from argparse import Namespace
from typing import TextIO, List, Dict, Tuple

import screed


def parse_args(args: list) -> Namespace:
    """
    Parse a list of arguments.

    Parameters
    ----------
    args : list
        Arguments to parse.

    Returns
    -------
    _ : Namespace
        Parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description="Creates consensus sequence from a multiple sequence alignment"
    )
    parser.add_argument(
        "--input",
        type=str,
        required=True,
        help="Input FASTA file containing the sequences",
    )
    parser.add_argument(
        "--output",
        type=str,
        required=True,
        help="Output FASTA file to write consensus",
    )
    parser.add_argument(
        "--base-count",
        type=str,
        required=True,
        help="Output to write pickle object with base count",
    )
    parser.add_argument(
        "--region",
        type=str,
        required=True,
        help="Region where the sequences come from",
    )
    return parser.parse_args(args)


def write_sequence(
    output: TextIO, identifier: str, sequence: str, line_length: int = 80
) -> None:
    """
    Write a sequence to a file.

    Parameters
    ----------
    output : TextIOWrapper
        File where sequence will be written.
    identifier : str
        Identifier of the genome.
    sequence : str
        Sequence of the genome.
    line_length : int, default=80
        Maximum length for each sequence line.
        Does not apply to identifier.
    """
    output.write(f">{identifier}\n")
    formatted_sequence = "".join(
        [
            f"{sequence[i:i + line_length]}\n"
            for i in range(0, len(sequence), line_length)
        ]
    )
    output.write(f"{formatted_sequence}")


def get_identifier(region: str, input_file: str, number_of_sequences: int) -> str:
    """
    Return the identifier of the consensus sequence.

    Parameters
    ----------
    region : str
        Region where the sequences come from.
    input_file : str
        Path of the input file.
    number_of_sequences : int
        Number of sequences used to compute consensus.

    Returns
    -------
    identifier : str
        Identifier of the consensus sequence.
    """
    suffix = input_file.split("/")[-1]
    calendar_week = suffix.replace(".fasta", "")
    identifier = f"{region}_{calendar_week}_{number_of_sequences}"
    return identifier


def initialize_counters(sequence: str) -> List[Dict[str, int]]:
    """
    Initialize a list with dictionary counters.

    Parameters
    ----------
    sequence : str
        Sequence used to initialize the counters.

    Returns
    -------
    counters : List[Dict[str, int]]
        List of dictionaries containing counters.
    """
    counters = [{"A": 0, "C": 0, "G": 0, "T": 0, "-": 0, "N": 0} for _ in sequence]
    for i, base in enumerate(sequence):
        update_counter(counters[i], base)
    return counters


def update_counter(counter: dict, base: str) -> None:
    """
    Update counter.

    Parameters
    ----------
    counter : dict
        Counter to be updated.
    base : str
        Base pair to increment.
    """
    if base in counter:
        counter[base] += 1
    else:
        counter["N"] += 1


def column_consensus(counter: dict) -> str:
    """
    Compute consensus given a dictionary counter.

    Parameters
    ----------
    counter : dict
        Counter with number of times each BP has been seen.

    Returns
    -------
    consensus : str
        Consensus for the given column.
    """
    # A is default in case of ties
    consensus = "A"
    for base in ["C", "G", "T", "-"]:
        if counter[base] > counter[consensus]:
            consensus = base
    # If column only contains something other than
    # A, C, G, T or - then an N is inserted
    if counter[consensus] == 0:
        consensus = "N"
    return consensus


def compute_consensus(input_path: str) -> Tuple[str, List[int], int]:
    """
    Compute consensus sequence.

    Parameters
    ----------
    input_path : str
        Path to FASTA file.

    Returns
    -------
    consensus, base_count, number_of_sequences : Tuple[str, List[int], int]
        Consensus sequence, base count and number of sequences.
    """
    with screed.open(input_path) as seqfile:
        first_sequence = next(seqfile).sequence.upper()
        number_of_sequences = 1
        counters = initialize_counters(first_sequence)
        for record in seqfile:
            sequence = record.sequence.upper()
            number_of_sequences += 1
            for i, base in enumerate(sequence):
                update_counter(counters[i], base)
        consensus = [column_consensus(counters[col]) for col in range(len(counters))]
        consensus = "".join(consensus)
        base_count = get_base_count(consensus, counters)
        return consensus, base_count, number_of_sequences


def get_base_count(consensus: str, counters: List[Dict[str, int]]) -> List[int]:
    """
    Compute base count for consensus.

    Parameters
    ----------
    consensus : str
        Consensus sequence.
    counters : List[Dict[str, int]]
        List of dictionaries containing counters.

    Returns
    -------
    base_count : List[int]
        Base count for consensus sequence.
    """
    base_count = [counters[i][base] for i, base in enumerate(consensus)]
    return base_count


def main():  # pragma: no cover
    """Computes consensus sequences per calendar week."""
    args = parse_args(sys.argv[1:])
    consensus, base_count, number_of_sequences = compute_consensus(args.input)
    with open(args.output, "w") as output:
        identifier = get_identifier(args.region, args.input, number_of_sequences)
        write_sequence(output, identifier, consensus)
    with open(args.base_count, "wb") as output:
        pickle.dump(base_count, output)


if __name__ == "__main__":
    main()  # pragma: no cover
