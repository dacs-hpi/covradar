#!/usr/bin/env python3

import sys
sys.path.append('../')
from shared.version._version import get_versions
import argparse
from art import *

parser = argparse.ArgumentParser(description='Prints covradar version at the end of the pipeline')
parser.add_argument('-v', '--version', action='version', version='CovRadar v{version}'.format(version=get_versions()['version'].split('+')[0]))
args = parser.parse_args()

print(text2art('CovRadar\nv{version}'.format(version=get_versions()['version'].split('+')[0])))
